
package com.google.android.c2dm;

import java.io.IOException;

import com.cipl.liverycab.driverui.AssignedJobDetailActivity;
import com.cipl.liverycab.driverui.AssignedJobListActivity;
import com.cipl.liverycab.driverui.JobListActivity;
import com.cipl.liverycab.passengerui.DriverDetailActivity;
import com.cipl.liverycab.passengerui.LiveryCabActivity;
import com.cipl.liverycab.passengerui.LoginActivity;
import com.cipl.liverycab.passengerui.NotificationActivity;
import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStatics;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * Base class for C2D message receiver. Includes constants for the strings used
 * in the protocol.
 */
public abstract class C2DMBaseReceiver extends IntentService {
	private static final String C2DM_RETRY = "com.google.android.c2dm.intent.RETRY";

	public static final String REGISTRATION_CALLBACK_INTENT = "com.google.android.c2dm.intent.REGISTRATION";
	private static final String C2DM_INTENT = "com.google.android.c2dm.intent.RECEIVE";

	// Logging tag
	private static final String TAG = "C2DM";

	// Extras in the registration callback intents.
	public static final String EXTRA_UNREGISTERED = "unregistered";

	public static final String EXTRA_ERROR = "error";

	public static final String EXTRA_REGISTRATION_ID = "registration_id";

	public static final String ERR_SERVICE_NOT_AVAILABLE = "SERVICE_NOT_AVAILABLE";
	public static final String ERR_ACCOUNT_MISSING = "ACCOUNT_MISSING";
	public static final String ERR_AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED";
	public static final String ERR_TOO_MANY_REGISTRATIONS = "TOO_MANY_REGISTRATIONS";
	public static final String ERR_INVALID_PARAMETERS = "INVALID_PARAMETERS";
	public static final String ERR_INVALID_SENDER = "INVALID_SENDER";
	public static final String ERR_PHONE_REGISTRATION_ERROR = "PHONE_REGISTRATION_ERROR";

	// wakelock
	private static final String WAKELOCK_KEY = "C2DM_LIB";

	private static PowerManager.WakeLock mWakeLock;
	private String senderId="tanisha.agrawal@classicinformatics.com";

	/**
	 * The C2DMReceiver class must create a no-arg constructor and pass the
	 * sender id to be used for registration.
	 */
	public C2DMBaseReceiver(String senderId) {
		// senderId is used as base name for threads, etc.
		super(senderId);
		this.senderId = senderId;
	}

	/**
	 * Called when a cloud message has been received.
	 */
	protected abstract void onMessage(Context context, Intent intent);

	/**
	 * Called on registration error. Override to provide better error messages.
	 * 
	 * This is called in the context of a Service - no dialog or UI.
	 */
	public abstract void onError(Context context, String errorId);

	/**
	 * Called when a registration token has been received.
	 */
	public void onRegistrered(Context context, String registrationId)
			throws IOException {
		// registrationId will also be saved
		/*AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("registrationid" + registrationId)
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// put your code here
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// put your code here
						dialog.cancel();
					}
				});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();*/
	}

	/**
	 * Called when the device has been unregistered.
	 */
	public void onUnregistered(Context context) {
	}

	@Override
	public final void onHandleIntent(Intent intent) {
		try {
			Context context = getApplicationContext();
			if (intent.getAction().equals(REGISTRATION_CALLBACK_INTENT)) {
				handleRegistration(context, intent);
			} else if (intent.getAction().equals(C2DM_INTENT)) {
				//getting data from the Bundle and using it to redirect when notification is received. 
				
					Bundle extras = intent.getExtras();
			      	
					String msg =extras != null ? extras.getString("msg"):"";
			      	String notificationType = extras!=null?extras.getString("NotificationType"):"";
			      	String bidId = extras != null ? extras.getString("bidId"):"";
			      	String driverId = extras != null ? extras.getString("driverId"):"";
			      	String pickupId = extras != null ? extras.getString("pickupid"):"";
			      	String fname = extras != null ? extras.getString("fname"):"";
			    	String lname = extras != null ? extras.getString("lname"):"";
			    	
			      	String ns = Context.NOTIFICATION_SERVICE;
				    NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
				    int icon = R.drawable.icon;    
				    CharSequence tickerText = "Notification Receive";
				    long when = System.currentTimeMillis();
			        Notification notification = new Notification(icon, tickerText, when);
			        
			        Context context1 = context;
				        //*****************
				    final int CUSTOM_VIEW_ID = 1;
				    RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_notification_layout);
				    contentView.setImageViewResource(R.id.image, R.drawable.icon);
				    contentView.setTextViewText(R.id.text, "LiveryCab \n"+msg);
			        notification.contentView = contentView;
				    notification.defaults=Notification.FLAG_ONLY_ALERT_ONCE+Notification.FLAG_AUTO_CANCEL;

				    Intent notificationIntent = null;
				    
				    notification.flags |= Notification.FLAG_AUTO_CANCEL;
				    notification.defaults |= Notification.DEFAULT_SOUND;
				    notification.flags |= Notification.FLAG_SHOW_LIGHTS;
				    
				    LiveryCabStatics.isNotificationReceive = true;
				    if(notificationType != null && notificationType.equals("pass_profile")){
				    	if(LiveryCabStatics.isLoginPassenger)
				    		notificationIntent = new Intent(this,DriverDetailActivity.class);
				    	else
				    		notificationIntent = new Intent(this,LoginActivity.class);
				    }else if(notificationType != null && notificationType.equals("driver_profile")){
				    	if(LiveryCabStatics.isLoginDriver)
				    		notificationIntent = new Intent(this,AssignedJobListActivity.class);
				    	else
				    		notificationIntent = new Intent(this,LoginActivity.class);
				    }else{
				    	  notificationIntent = new Intent(this, NotificationActivity.class);
				    }
				    
				    notificationIntent.putExtra("BidID",bidId);
				    notificationIntent.putExtra("pickupid",pickupId);
				    notificationIntent.putExtra("driverID",driverId);
				    notificationIntent.putExtra("fName",fname);
				    notificationIntent.putExtra("lName",lname);
				    notificationIntent.putExtra("Tag", "C2DMBaseReceiver");
				    PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
				    notification.contentIntent = contentIntent;
				    mNotificationManager.notify(CUSTOM_VIEW_ID, notification);
			    } else if (intent.getAction().equals(C2DM_RETRY)) {
			    	C2DMessaging.register(context, senderId);
			    }
			} finally {
			// Release the power lock, so phone can get back to sleep.
			// The lock is reference counted by default, so multiple
			// messages are ok.

			// If the onMessage() needs to spawn a thread or do something else,
			// it should use it's own lock.
			mWakeLock.release();
		}
	}

	/**
	 * Called from the broadcast receiver. Will process the received intent,
	 * call handleMessage(), registered(), etc. in background threads, with a
	 * wake lock, while keeping the service alive.
	 */
	static void runIntentInService(Context context, Intent intent) {
		if (mWakeLock == null) {
			// This is called from BroadcastReceiver, there is no init.
			PowerManager pm = (PowerManager) context
					.getSystemService(Context.POWER_SERVICE);
			mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
					WAKELOCK_KEY);
		}
		mWakeLock.acquire();

		// Use a naming convention, similar with how permissions and intents are
		// used. Alternatives are introspection or an ugly use of statics.
		String receiver = "com.commonsware.android.c2dm.C2DMReceiver";
		intent.setClassName(context, receiver);

		context.startService(intent);

	}

	private void handleRegistration(final Context context, Intent intent) {
		final String registrationId = intent
				.getStringExtra(EXTRA_REGISTRATION_ID);
		String error = intent.getStringExtra(EXTRA_ERROR);
		String removed = intent.getStringExtra(EXTRA_UNREGISTERED);

		if (Log.isLoggable(TAG, Log.DEBUG)) {
			Log.d(TAG, "dmControl: registrationId = " + registrationId
					+ ", error = " + error + ", removed = " + removed);
		}

		if (removed != null) {
			// Remember we are unregistered
			C2DMessaging.clearRegistrationId(context);
			onUnregistered(context);
			return;
		} else if (error != null) {
			// we are not registered, can try again
			C2DMessaging.clearRegistrationId(context);
			// Registration failed
			Log.e(TAG, "Registration error " + error);
			onError(context, error);
			if ("SERVICE_NOT_AVAILABLE".equals(error)) {
				long backoffTimeMs = C2DMessaging.getBackoff(context);

				Log.d(TAG, "Scheduling registration retry, backoff = "
						+ backoffTimeMs);
				Intent retryIntent = new Intent(C2DM_RETRY);
				PendingIntent retryPIntent = PendingIntent
						.getBroadcast(context, 0 /* requestCode */, retryIntent,
								0 /* flags */);

				AlarmManager am = (AlarmManager) context
						.getSystemService(Context.ALARM_SERVICE);
				am.set(AlarmManager.ELAPSED_REALTIME, backoffTimeMs,
						retryPIntent);

				// Next retry should wait longer.
				backoffTimeMs *= 2;
				C2DMessaging.setBackoff(context, backoffTimeMs);
			}
		} else {
			try {
				onRegistrered(context, registrationId);
				C2DMessaging.setRegistrationId(context, registrationId);
				LiveryCabStatics.registrationID =registrationId;
			} catch (IOException ex) {
				Log.e(TAG, "Registration error " + ex.getMessage());
			}
		}
	}
}