package com.cipl.liverycab.passengerui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class QuickActionWindow extends PopupWindow implements KeyEvent.Callback {
	private final Context mContext;
	private final LayoutInflater mInflater;
	private final WindowManager mWindowManager;
	private Intent intent;
	View contentView;
	
	private int mScreenWidth;
	private int mScreenHeight;
	
	private int mShadowHoriz;
	private int mShadowVert;
	private int mShadowTouch;
	
	private ImageView mArrowUp;
	private ImageView mArrowDown;
	private ViewGroup mTrack;
	private Animation mTrackAnim;
	private LinearLayout actionlayout;
	
	private View mPView;
	private Rect mAnchor;
	private String date_selected,time_selected;
	private OnDateSetListener mDateSetListener = null;
	private OnTimeSetListener mTimeSetListener = null;
	int dilogId = 0;
	
	private ForgotPasswordParserClass objParser;
	private ProgressDialog pd;
	
	private Calendar c = Calendar.getInstance();
	final int cyear = c.get(Calendar.YEAR);
	final int cmonth = c.get(Calendar.MONTH);
	final int cday = c.get(Calendar.DAY_OF_MONTH);
	final int cHour = c.get(Calendar.HOUR_OF_DAY);
	final int cMin = c.get(Calendar.MINUTE);
	final int cSec = c.get(Calendar.SECOND);
	// For lazy loading of icon
	
	public QuickActionWindow(Context context, View pView, Rect rect) {
		super(context);
		
		mPView = pView;
		mAnchor = rect;
		
		mContext = context;
		mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		mInflater = ((Activity)mContext).getLayoutInflater();
		
		setContentView(R.layout.quickaction);
		
		mScreenWidth = mWindowManager.getDefaultDisplay().getWidth();
		mScreenHeight = mWindowManager.getDefaultDisplay().getHeight();
		
		setWindowLayoutMode(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		
		final Resources res = mContext.getResources();
		/*mShadowHoriz = res.getDimensionPixelSize(R.dimen.quickaction_shadow_horiz);
		mShadowVert = res.getDimensionPixelSize(R.dimen.quickaction_shadow_vert);
		mShadowTouch = res.getDimensionPixelSize(R.dimen.quickaction_shadow_touch);*/
		
		setWidth(mScreenWidth + mShadowHoriz + mShadowHoriz);
		setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		
		setBackgroundDrawable(new ColorDrawable(0));
		
		mArrowUp = (ImageView) contentView.findViewById(R.id.arrow_up);
		mArrowDown = (ImageView) contentView.findViewById(R.id.arrow_down);
		actionlayout = (LinearLayout)contentView.findViewById(R.id.quickaction_header);
		//mTrack = (ViewGroup) contentView.findViewById(R.id.quickaction);
		/*mTrackScroll = (HorizontalScrollView) contentView.findViewById(R.id.scroll);

		mFooter = contentView.findViewById(R.id.footer);
		mFooterDisambig = contentView.findViewById(R.id.footer_disambig);
		mResolveList = (ListView) contentView.findViewById(android.R.id.list);
		mSetPrimaryCheckBox = (CheckBox) contentView.findViewById(android.R.id.checkbox);*/
		
		setFocusable(true);
		setTouchable(true);
		setOutsideTouchable(true);
		
		// Prepare track entrance animation
		mTrackAnim = AnimationUtils.loadAnimation(mContext, R.anim.quickaction);
		mTrackAnim.setInterpolator(new Interpolator() {
			public float getInterpolation(float t) {
				// Pushes past the target area, then snaps back into place.
				// Equation for graphing: 1.2-((x*1.6)-1.1)^2
				final float inner = (t * 1.55f) - 1.1f;
				return 1.2f - inner * inner;
			}
		});	
		
		actionlayout.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				if(LiveryCabStatics.isRideLater && LiveryCabStatics.destiMapView)
				{
					DatePickerDialog datepick =  new DatePickerDialog(mContext,  mDateSetListener,  cyear, cmonth, cday);
					datepick.show();
					final TimePickerDialog timepick =  new TimePickerDialog(mContext,  mTimeSetListener,cHour,cMin,false);
					mDateSetListener = new DatePickerDialog.OnDateSetListener() {
						// onDateSet method
						public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
							date_selected = ""+cday+" "+String.valueOf(cmonth+1)+" "+cyear;
							timepick.show();
							mTimeSetListener = new OnTimeSetListener() {
								@Override
								public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
									time_selected = ""+cHour+":"+cMin+":"+cSec;
									if(LiveryCabStatics.isLoginPassenger){
										isbookRideDestiMapView();
									}else
										startActivity(intent);
								}
							};
						}
					};
				}
				else
					startActivity(intent);
			}
		});
	}
	
	public void setIntent(Intent intent)
	{
		this.intent = intent;
	}
	private void startActivity(Intent i)
	{
		if(i!=null)
			mContext.startActivity(this.intent);
	}
	private void setContentView(int resId) {
		contentView = mInflater.inflate(resId, null);
		super.setContentView(contentView);
	}
	
	public View getHeaderView() {
		return contentView.findViewById(R.id.quickaction_header);
	}
	
	public void setTitle(CharSequence title) {
		contentView.findViewById(R.id.quickaction_header_content).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.quickaction_primary_text).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.quickaction_primary_text)).setText(title);
	}
	
	public void setTitle(int resid) {
		setTitle(mContext.getResources().getString(resid));
	}
	
	/*public void setText(CharSequence text) {
		contentView.findViewById(R.id.quickaction_header_content).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.quickaction_secondary_text).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.quickaction_secondary_text)).setText(text);
	}
	*/
	/*public void setText(int resid) {
		setText(mContext.getResources().getString(resid));
	}
	*/
	
	public void setIcon(Bitmap bm) {
		contentView.findViewById(R.id.quickaction_icon).setVisibility(View.VISIBLE);
		final ImageView vImage = (ImageView) contentView.findViewById(R.id.quickaction_icon);
		vImage.setImageBitmap(bm);
	}
	
	public void setIcon(Drawable d) {
		contentView.findViewById(R.id.quickaction_icon).setVisibility(View.VISIBLE);
		final ImageView vImage = (ImageView) contentView.findViewById(R.id.quickaction_icon);
		vImage.setImageDrawable(d);
	}
	
	public void setIcon(int resid) {
		setIcon(mContext.getResources().getDrawable(resid));
	}
	
	/**
	 * Show the correct call-out arrow based on a {@link R.id} reference.
	 */
	private void showArrow(int whichArrow, int requestedX) {
		final View showArrow = (whichArrow == R.id.arrow_up) ? mArrowUp : mArrowDown;
		final View hideArrow = (whichArrow == R.id.arrow_up) ? mArrowDown : mArrowUp;

		// Dirty hack to get width, might cause memory leak
		final int arrowWidth = mContext.getResources().getDrawable(R.drawable.quickaction_arrow_up).getIntrinsicWidth();

		showArrow.setVisibility(View.VISIBLE);
		ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams)showArrow.getLayoutParams();
		param.leftMargin = requestedX - arrowWidth / 2;
		//Log.d("QuickActionWindow", "ArrowWidth: "+arrowWidth+"; LeftMargin for Arrow: "+param.leftMargin);

		hideArrow.setVisibility(View.INVISIBLE);
	}
	
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBackPressed();
			return true;
		}

		return false;
	}
	
	private void onBackPressed() {
			dismiss();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void show() {
		show(mAnchor.centerX());
	}
	
	public void show(int requestedX) {
		super.showAtLocation(mPView, Gravity.NO_GRAVITY, 0, 0);
		
		// Calculate properly to position the popup the correctly based on height of popup
		if (isShowing()) {
			int x, y, windowAnimations;
			this.getContentView().measure(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			final int blockHeight = this.getContentView().getMeasuredHeight();
			
			x = -mShadowHoriz;
			
			//Log.d("QuickActionWindow", "blockHeight: "+blockHeight);
			
			if (mAnchor.top > blockHeight) {
				// Show downwards callout when enough room, aligning bottom block
				// edge with top of anchor area, and adjusting to inset arrow.
				showArrow(R.id.arrow_down, requestedX);
				y = mAnchor.top - blockHeight;
				windowAnimations = R.style.QuickActionAboveAnimation;
	
			} else {
				// Otherwise show upwards callout, aligning block top with bottom of
				// anchor area, and adjusting to inset arrow.
				showArrow(R.id.arrow_up, requestedX);
				y = mAnchor.bottom;
				windowAnimations = R.style.QuickActionBelowAnimation;
			}
			
			setAnimationStyle(windowAnimations);
			//mTrack.startAnimation(mTrackAnim);
			this.update(x, y, -1, -1);
		}
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void isbookRideDestiMapView()
	{
		pd = LiveryCabStaticMethods.returnProgressBar(mContext);
		try {
			objParser = new ForgotPasswordParserClass(UrlStatics
						.getUrlforSavingSourceAndDesti(LiveryCabStatics.userId,
						URLEncoder.encode(LiveryCabStatics.pickupAddress, "utf-8"), 
						URLEncoder.encode(LiveryCabStatics.destinationAddress, "utf-8"),
						URLEncoder.encode(LiveryCabStatics.sourceLatitude, "utf-8"),
						URLEncoder.encode(LiveryCabStatics.sourceLongitude,"utf-8"),
						URLEncoder.encode(LiveryCabStatics.destinationLatitude,"utf-8"),
						URLEncoder.encode(LiveryCabStatics.destinationLongitude,"utf-8"),
						URLEncoder.encode(LiveryCabStaticMethods.getDateTime(), "utf-8"),
						URLEncoder.encode(LiveryCabStatics.ridestatus,"utf-8")),pd);
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		objParser.start();
		if(objParser.loginDataObj.message != null)
		{
			if(objParser.loginDataObj.message.equalsIgnoreCase("ride_booked")){
				LiveryCabStaticMethods.showAlert(mContext, 
						"Success", "Your ride booked successfully",R.drawable.attention,true,false,"Ok", 
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(
									DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								startActivity(new Intent(mContext,BookRideActivity.class));
							}
					
				}, null, null);
			}else if(objParser.loginDataObj.message.equalsIgnoreCase("Some_error_in_ride_booking")){
				LiveryCabStaticMethods.showAlert(mContext, 
						"Failure", "try again",R.drawable.attention,true,false,"Ok", 
						null, null, null);
			}else if(objParser.loginDataObj.message.equalsIgnoreCase("session_expire")){
				LiveryCabStaticMethods.showAlert(mContext, 
						"Failure", "session_expire",R.drawable.attention,true,false,"Ok", 
						null, null, null);
			}
		}
	}
}
