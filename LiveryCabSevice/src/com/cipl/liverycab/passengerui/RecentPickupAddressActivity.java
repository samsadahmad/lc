package com.cipl.liverycab.passengerui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.cipl.liverycab.parserclasses.RecentAddressParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class RecentPickupAddressActivity extends Activity {

	private Context mContext;
	private EditText searchEditText;
	private RecentAddressParserClass objParserClass;
	private ListView recentAddList;
	private int limit = 10;
	private int textlength;
	private String[] from = new String[] { "address", "distance" };
	private int[] to = new int[] { R.id.textviewAddressDetailRecentAddresss,
			R.id.textviewMilesRecentAddresss };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recentpickupaddress);
		// LiveryCabStatics.myList = new ArrayList<ReceentAddressDataClass>();
		mContext = this;
		final List<HashMap<String, String>> fillMaps = new ArrayList<HashMap<String, String>>();
		searchEditText = (EditText) findViewById(R.id.edittextSearchRecentPickUpAddress);
		recentAddList = (ListView) findViewById(R.id.listviewRecentPickUpAddress);

		objParserClass = new RecentAddressParserClass(
				UrlStatics.getRecentAddressUrl(LiveryCabStatics.userId,
						"source", LiveryCabStatics.currentLatitude,
						LiveryCabStatics.currentLongitude, limit));
		objParserClass.start();

		/*
		 * final View footerView = ((LayoutInflater) mContext
		 * .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
		 * R.layout.listviewfooter, null, false);
		 * recentAddList.addFooterView(footerView);
		 */

		LiveryCabStaticMethods.returnProgressBar(
				RecentPickupAddressActivity.this).setOnDismissListener(
				new OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						for (int i = 0; i < LiveryCabStatics.myList.size(); i++) {
							HashMap<String, String> map = new HashMap<String, String>();
							map.put("address",
									LiveryCabStatics.myList.get(i).address);
							map.put("distance",
									LiveryCabStatics.myList.get(i).distanceInMiles);
							fillMaps.add(map);
						}
						SimpleAdapter adapter = new SimpleAdapter(
								RecentPickupAddressActivity.this, fillMaps,
								R.layout.recentaddresscustomcell, from, to);
						recentAddList.setAdapter(adapter);
					}
				});

		/*
		 * footerView.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub if (LiveryCabStatics.myList.size() <
		 * Integer.parseInt(LiveryCabStatics.myList.get(0).count)) {
		 * LiveryCabStaticMethods
		 * .returnProgressBar(RecentPickupAddressActivity.this)
		 * .setOnDismissListener(new OnDismissListener() {
		 * 
		 * @Override public void onDismiss(DialogInterface dialog) { // Setting
		 * adapter to the list SimpleAdapter adapter = new
		 * SimpleAdapter(RecentPickupAddressActivity.this, fillMaps,
		 * R.layout.recentaddresscustomcell, from, to);
		 * recentAddList.setAdapter(adapter); } });
		 * 
		 * objParserClass = new RecentAddressParserClass(UrlStatics
		 * .getRecentAddressUrl
		 * (LiveryCabStatics.userId,"source",LiveryCabStatics
		 * .currentLatitude,LiveryCabStatics.currentLongitude, limit + 10),limit
		 * + 10); limit = limit + 10; objParserClass.start();
		 * 
		 * }else if(Integer.parseInt(LiveryCabStatics.myList.get(0).count) == 0
		 * || Integer.parseInt(LiveryCabStatics.myList.get(0).count)<4){
		 * footerView.setVisibility(View.GONE); System.out.println("Gon first");
		 * }else { footerView.setVisibility(View.GONE);
		 * System.out.println("Gon second"); } } });
		 */

		// action on click of a list item
		recentAddList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				LiveryCabStatics.pickupAddress = LiveryCabStatics.myList
						.get(position).address;
				LiveryCabStatics.sourceLatitude = LiveryCabStatics.myList
						.get(position).addLatitude;
				LiveryCabStatics.sourceLongitude = LiveryCabStatics.myList
						.get(position).addLongitude;
				startActivity(new Intent(RecentPickupAddressActivity.this,
						DestinationAddressActivity.class));

			}
		});

		searchEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				fillMaps.clear();
				textlength = searchEditText.getText().length();
				for (int i = 0; i < LiveryCabStatics.myList.size(); i++) {
					if (textlength <= LiveryCabStatics.myList.get(i).address
							.length()) {
						if (searchEditText
								.getText()
								.toString()
								.equalsIgnoreCase(
										(String) LiveryCabStatics.myList.get(i).address
												.subSequence(0, textlength))) {
							HashMap<String, String> map = new HashMap<String, String>();
							map.put("address",
									LiveryCabStatics.myList.get(i).address);
							map.put("distance",
									LiveryCabStatics.myList.get(i).distanceInMiles);
							fillMaps.add(map);
						}
					}
				}
				if (!fillMaps.isEmpty()) {
					SimpleAdapter adapter = new SimpleAdapter(
							RecentPickupAddressActivity.this, fillMaps,
							R.layout.recentaddresscustomcell, from, to);
					recentAddList.setAdapter(adapter);
				} else {
					LiveryCabStaticMethods.showAlert(
							RecentPickupAddressActivity.this, "Alert!!",
							"No record found", R.drawable.attention, true,
							false, "Ok", null, null, null);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});
	}
}
