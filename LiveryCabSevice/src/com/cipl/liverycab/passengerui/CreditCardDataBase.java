package com.cipl.liverycab.passengerui;
import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class CreditCardDataBase {
	
	private final String DATABASE_NAME = "creditCardDB";
    private final String DATABASE_TABLE = "CCDetail";
   
    private static final int DATABASE_VERSION = 1;
    
    ArrayList<String> results = new ArrayList<String>();
    SQLiteDatabase ccDB = null;
    
    public CreditCardDataBase(Context ctx){
    	ccDB = ctx.openOrCreateDatabase(DATABASE_NAME, DATABASE_VERSION, null);
    	ccDB.execSQL("CREATE TABLE IF NOT EXISTS "+DATABASE_TABLE+"(userID VARCHAR primary key," +
    			"ccType VARCHAR, expMonth VARCHAR, expYear VARCHAR, ccNumber VARCHAR, cvvNumber VARCHAR);");
    }
    public void close() {
    	ccDB.close();
    }
    public boolean insertRow(String UserID,String ccType,String expMnth,
    		String expYr, String ccNum, String cvvNum){    	
    	try {
			ccDB.execSQL("INSERT INTO "+DATABASE_TABLE+"(userID," +
					"ccType, expMonth, expYear, ccNumber, cvvNumber) VALUES('"+UserID+"','"+ccType+"','"+expMnth+"'," +
							"'"+expYr+"','"+ccNum+"','"+cvvNum+"');");
			//ccDB.execSQL("COMMIT ;");
			ccDB.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ccDB.close();
			return false;
		}
    }
    
    public Cursor fetchRow(String userID){
    	return ccDB.rawQuery("SELECT * FROM "+DATABASE_TABLE+" WHERE userID="+userID+";",null);
    }
    public void deleteTable(){
    	try {
			ccDB.execSQL("DELETE FROM CCDetail;");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	//ccDB.close();
    }
}
