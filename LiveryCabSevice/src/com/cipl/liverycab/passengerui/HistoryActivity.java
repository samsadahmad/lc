package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.cipl.liverycab.parserclasses.HIstoryListParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class HistoryActivity extends Activity {

	private HIstoryListParserClass parserObject;
	private ListView hisList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.history);
		hisList = (ListView) findViewById(R.id.listviewHistory);

		if(LiveryCabStaticMethods.isInternetAvailable(this)){
			ProgressDialog pd = LiveryCabStaticMethods
					.returnProgressBar(HistoryActivity.this);
			parserObject = new HIstoryListParserClass(
					UrlStatics.getHistoryListApi(LiveryCabStatics.userId));
			parserObject.start();
			
			pd.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					if (LiveryCabStatics.historyList.isEmpty()) {
						LiveryCabStaticMethods.showAlert(HistoryActivity.this,
								"No Records!!", "No records", R.drawable.attention,
								true, false, "Ok", null, null, null);
					} else if (LiveryCabStatics.historyList.get(0).message
							.equalsIgnoreCase("Record_not_found")) {
						LiveryCabStaticMethods.showAlert(HistoryActivity.this,
								"No Records!!", "No records", R.drawable.attention,
								true, false, "Ok", null, null, null);
					} else {
						hisList.setAdapter(new EfficientAdapter(
								HistoryActivity.this));
					}
				}
			});
		}
		

		hisList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int index,
					long arg3) {
				Intent detailIntent = new Intent(HistoryActivity.this,
						HistoryDetailActivity.class);
				detailIntent.putExtra("driverID",
						LiveryCabStatics.historyList.get(index).driverId);
				detailIntent.putExtra("bidID",
						LiveryCabStatics.historyList.get(index).bidid);
				startActivity(detailIntent);
			}
		});

		((Button) findViewById(R.id.buttonMenuHistory))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						startActivity(new Intent(HistoryActivity.this,
								PassengerMenuActivity.class)
								.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
					}
				});
	}

	class EfficientAdapter extends BaseAdapter {
		private LayoutInflater inflater;

		public EfficientAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		public int getCount() {
			return LiveryCabStatics.historyList.size();
		}

		public Object getItem(int position) {
			return null;
		}

		public long getItemId(int position) {
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			// A ViewHolder is used to keep references to children views
			ViewHolder holder;
			if (convertView == null) {
				convertView = inflater
						.inflate(R.layout.historycustomcell, null);
				holder = new ViewHolder();
				holder.pickupAdd = (TextView) convertView
						.findViewById(R.id.textViewPickupaddressHistory);
				holder.destinationAdd = (TextView) convertView
						.findViewById(R.id.textViewDestinationaddressHistory);
				holder.time = (TextView) convertView
						.findViewById(R.id.textViewTimeHistory);
				holder.bidAmount = (TextView) convertView
						.findViewById(R.id.textviewBidAmountHistoryList);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.pickupAdd
					.setText(LiveryCabStatics.historyList.get(position).source);
			holder.destinationAdd.setText(LiveryCabStatics.historyList
					.get(position).destination);
			holder.time
					.setText(LiveryCabStatics.historyList.get(position).datetime);
			holder.bidAmount.setText("$"
					+ LiveryCabStatics.historyList.get(position).amount);
			return convertView;
		}
	}

	class ViewHolder {
		TextView pickupAdd;
		TextView destinationAdd;
		TextView time;
		TextView bidAmount;
	}
}
