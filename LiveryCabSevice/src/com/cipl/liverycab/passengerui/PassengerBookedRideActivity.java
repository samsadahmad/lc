package com.cipl.liverycab.passengerui;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.cipl.liverycab.dataclasses.BookedRideDataClass;
import com.cipl.liverycab.parserclasses.BookedRideParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class PassengerBookedRideActivity extends Activity {
	/** Variable define here */
 	private Button mbtnPassjobList, mbtnPassJobListRefresh;
	private ListView mlvPassJobList;
	private EfficientAdapter mEfficientAdapter;
  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.passengerbookedride);

		initControls();
	}

	private void initControls() {
		// TODO Auto-generated method stub
		mlvPassJobList = (ListView) findViewById(R.id.lvPassJobList);
		mbtnPassjobList = (Button) findViewById(R.id.btnPassjobList);

		/** Menu button */
		mbtnPassjobList.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(PassengerBookedRideActivity.this,
						PassengerMenuActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
		
		mEfficientAdapter = new EfficientAdapter(this,LiveryCabStatics.passengerBookedRideList);
		mlvPassJobList.setAdapter(mEfficientAdapter);
		if(LiveryCabStaticMethods.isInternetAvailable(this));{
			new PassengerAsynTask(null).execute(true);
		}
		
		mlvPassJobList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int Pos,
					long arg3) {
				// TODO Auto-generated method stub
				if(!LiveryCabStatics.passengerBookedRideList.get(Pos).bidCount.equalsIgnoreCase("0")) {
					Intent mintent = new Intent(PassengerBookedRideActivity.this, PassengerBookedRideDetails.class);
					mintent.putExtra("POSITION", Pos);
					startActivity(mintent);
				}
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		mEfficientAdapter = new EfficientAdapter(this,LiveryCabStatics.passengerBookedRideList);
		mlvPassJobList.setAdapter(mEfficientAdapter);
		super.onResume();
	}

	private class PassengerAsynTask extends AsyncTask<Boolean, Void, String> {
 		private String urlforBooked; 
		private String urlforBookedDel;
		private Integer position;
		private BookedRideParserClass mBookedRideParserClass; 
		private ProgressDialog mProgressDialog;


		public PassengerAsynTask(String pickupId) {
			// TODO Auto-generated constructor stub
 			this.urlforBooked = UrlStatics
					.getUrlToPassengerJobList(LiveryCabStatics.passengerId);
			this.urlforBookedDel = UrlStatics.getUrlToDeletePassJobList(
					LiveryCabStatics.passengerId, pickupId);
			this.position = position;
			
		}

		
		@Override
		protected String doInBackground(Boolean... params) {
			 this.mBookedRideParserClass = new BookedRideParserClass();
			if (params[0]) {
				this.mBookedRideParserClass.fetchBookedRide(urlforBooked);
			} else {
				this.mBookedRideParserClass.fetchBookedRide(urlforBookedDel);
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			
			mProgressDialog.dismiss();
			if(LiveryCabStatics.passengerBookedRideList.size() == 0){
				LiveryCabStaticMethods.showAlert(PassengerBookedRideActivity.this, "Bid Status", "There is no record found for Bid Status", R.drawable.attention, true, false, "Ok", null, null, null);
			}
			EfficientAdapter adapter = (EfficientAdapter)mlvPassJobList.getAdapter();
 			adapter.notifyDataSetChanged();		
		}

		@Override
		protected void onPreExecute() {
			mProgressDialog = ProgressDialog.show(
					PassengerBookedRideActivity.this, "",
					"Loading. Please wait...", true, false);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		}
 
	}

	private class EfficientAdapter extends BaseAdapter {
		private LayoutInflater inflater;
		private ArrayList<BookedRideDataClass> passengerBookedRideList;

		public EfficientAdapter(Context context, ArrayList<BookedRideDataClass> passengerBookedRideList) {
			inflater = LayoutInflater.from(context);
			this.passengerBookedRideList =passengerBookedRideList;			
			
		}

		public int getCount() {
			return passengerBookedRideList.size();
		}

		public String getItem(int position) {
			String pickupId = LiveryCabStatics.passengerBookedRideList
					.get(position).pickupId;

			return pickupId;
		}

		public long getItemId(int position) {
			return 0;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			// A ViewHolder is used to keep references to children views
			ViewHolder holder;
			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.passengerjoblistcustomcell, null);
				holder = new ViewHolder();
				// getting the views from the layout

				holder.pickupAddress = (TextView) convertView
						.findViewById(R.id.textviewPickupAddressJobListCustom);
				holder.destinationAddress = (TextView) convertView
						.findViewById(R.id.textviewDestinationAddressJobListCustom);
				holder.miles = (TextView) convertView
						.findViewById(R.id.textviewMilesJobListCustomCell);

				holder.waitText = (TextView) convertView
						.findViewById(R.id.textviewWaitTextJobListCustomCell);

				holder.tvPickupdate = (TextView) convertView
						.findViewById(R.id.tvPickupdate);

				holder.mbtnDeletePassJob = (Button) convertView
						.findViewById(R.id.btnDeletePassJob);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.mbtnDeletePassJob.setVisibility(View.GONE);
			holder.waitText.setVisibility(View.VISIBLE);

			// Store pickup id Id
			final String pickupId = passengerBookedRideList.get(position).pickupId;

			int beganIndexforMiles = (passengerBookedRideList.get(position).distance).indexOf(".");
			holder.pickupAddress.setText(passengerBookedRideList.get(position).pickupAddress);
			holder.destinationAddress.setText(passengerBookedRideList.get(position).pickupDestination);
			holder.miles.setText((passengerBookedRideList.get(position).distance).substring(0,beganIndexforMiles + 2)+ " miles");
			holder.tvPickupdate.setText(passengerBookedRideList.get(position).dateTime);

			/*if (passengerBookedRideList.get(position).rideCompleted.equalsIgnoreCase("Y")) {
				holder.waitText.setText("Ride completed");
			}else if(passengerBookedRideList.get(position).isRideStatus.equalsIgnoreCase("Y")){
				holder.waitText.setText("Ride Confirmed");
			}else if((passengerBookedRideList.get(position).bidCount.equalsIgnoreCase("0")) && (passengerBookedRideList.get(position).isRideCancel.equalsIgnoreCase("Y"))) {
				holder.waitText.setText("Ride canceled");
			}else if((passengerBookedRideList.get(position).bidCount.equalsIgnoreCase("0")) && (passengerBookedRideList.get(position).isRideCancel.equalsIgnoreCase("N"))) {
				holder.waitText.setVisibility(View.GONE);
				holder.mbtnDeletePassJob.setVisibility(View.VISIBLE);
			}else {
				holder.waitText.setText("Waiting for confirmation");
			}*/
			
			/*if (passengerBookedRideList.get(position).rideCompleted.equalsIgnoreCase("Y")) {
			holder.waitText.setText("Ride completed");
		}else */
		if((passengerBookedRideList.get(position).bidCount.equalsIgnoreCase("0")) && (passengerBookedRideList.get(position).isRideCancel.equalsIgnoreCase("N"))) {
			holder.waitText.setVisibility(View.GONE);
			holder.mbtnDeletePassJob.setVisibility(View.VISIBLE);
		}else {
			holder.waitText.setText("Waiting for confirmation");
		}
			
			
			/** Click on Delete Button */
			holder.mbtnDeletePassJob.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					final String pickupId = passengerBookedRideList
							.get(position).pickupId;
					new PassengerAsynTask(pickupId).execute(false);
				}
			});

			return convertView;
		}

		class ViewHolder {
			TextView pickupAddress;
			TextView destinationAddress;
			TextView waitText;
			TextView miles;
			TextView tvPickupdate;
			Button mbtnDeletePassJob;
		}
	}
}
