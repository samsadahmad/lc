package com.cipl.liverycab.passengerui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.cipl.liverycab.dataclasses.CreditCardDataClass;
import com.cipl.liverycab.driverui.DriverSignUpActivity;
import com.cipl.liverycab.driverui.JobListMapViewActivity;
import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class CreditCardDetailActivity extends Activity {
	
	private String cardTypeArray[] = {"Visa","Master Card","American Express"};
	private String expirationMonthArray[] = {"1","2","3","4","5","6","7","8","9","10","11","12"};
	private String expirationYearArray[] = {"2011","2012","2013","2014","2015","2016"};
	private String selectedItem,cardType,expMonth,expYear;
	private EditText cardNumberEditText,cvvNumEditText;
	private String bidID, passId, driverId, fName,lName,bidAmt;
	private ForgotPasswordParserClass bidConfirmParser;
	private String message ="";
	private ProgressDialog pd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.creditcrddetail);
		
		Bundle extras = getIntent().getExtras();
		bidID = extras != null? extras.getString("BidID"): "";
		driverId = extras != null? extras.getString("driverID"): "";
		fName = extras != null? extras.getString("fName"): "";
		lName = extras != null? extras.getString("lName"): "";
		bidAmt = extras != null? extras.getString("bidAmount"): "";
		passId = LiveryCabStatics.passengerId;
		
		cardNumberEditText =(EditText)findViewById(R.id.editTextCardNumberCreditCardDetail);
		cvvNumEditText =(EditText)findViewById(R.id.editTextCVVNumberCreditCardDetail);
		LiveryCabStatics.ccdata = new CreditCardDataClass();
		
		((Button)findViewById(R.id.buttonSignupcreditCard))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(CreditCardDetailActivity.this,PassengerSignUpActivity.class));
			}
		});
		
		((Button)findViewById(R.id.buttonSaveCreditCardDetail))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LiveryCabStatics.ccdata.cardNumber  = cardNumberEditText.getText().toString();
				LiveryCabStatics.ccdata.cvvNumber  = cvvNumEditText.getText().toString();
				if(LiveryCabStatics.ccdata != null){
					if(LiveryCabStatics.ccdata.cardType !=null){
						if(LiveryCabStatics.ccdata.expirationMonth == null ||
								LiveryCabStatics.ccdata.expirationYear == null || 
								LiveryCabStatics.ccdata.cardNumber == null || 
								LiveryCabStatics.ccdata.cvvNumber == null){
							LiveryCabStaticMethods.showAlert(CreditCardDetailActivity.this,
									"Error Occured!","Credit card information is required in the system before any liverycab ride purchase",R.drawable.attention, true, false, 
									"Ok", null,null,null);
							
						}else if(LiveryCabStatics.ccdata.cardNumber.length()<16 || !(LiveryCabStaticMethods.isCardNumberValid(LiveryCabStatics.ccdata.cardNumber ))){
							LiveryCabStaticMethods.showAlert(CreditCardDetailActivity.this,"Error Occured!","Enter valid card number"
									,R.drawable.attention, true, false, "Ok", null,null,null);
						}else{
							CreditCardDataBase ccDB = new CreditCardDataBase(CreditCardDetailActivity.this);
							ccDB.deleteTable();
							if(ccDB.insertRow(LiveryCabStatics.userId, LiveryCabStatics.ccdata.cardType,
								LiveryCabStatics.ccdata.expirationMonth, 
								LiveryCabStatics.ccdata.expirationYear, 
								LiveryCabStatics.ccdata.cardNumber,
								LiveryCabStatics.ccdata.cvvNumber)){
									LiveryCabStaticMethods.showAlert(CreditCardDetailActivity.this,
											"Success!","Credit card details are saved successfully!",R.drawable.success, true, false, 
											"Ok", new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													finish();
												}
											},null,null);
							}
							
						}
						if(DriverDetailActivity.driverDetailPage){
							pd = LiveryCabStaticMethods.returnProgressBar(CreditCardDetailActivity.this);
							try {
								bidConfirmParser = new ForgotPasswordParserClass(UrlStatics.getBidConfirmUrl(bidID,passId,driverId,
									fName,lName,URLEncoder.encode(LiveryCabStatics.ccdata.cardType,"utf-8"),
									LiveryCabStatics.ccdata.expirationMonth,LiveryCabStatics.ccdata.expirationYear,
									LiveryCabStatics.ccdata.cvvNumber,
									LiveryCabStatics.ccdata.cardNumber, bidAmt),pd);
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							bidConfirmParser.start();
								
							pd.setOnDismissListener(new OnDismissListener() {
								@Override
								public void onDismiss(DialogInterface dialog) {
									if(bidConfirmParser.actualDismiss){
										if(bidConfirmParser.loginDataObj!=null)
											message = bidConfirmParser.loginDataObj.message;
										if(message.equalsIgnoreCase("DoDirect_Payment_Capture_Successfully")){
											LiveryCabStaticMethods.showAlert(CreditCardDetailActivity.this,
												"Success","Your ride has been confirmed successfully.Your payment has been processed. You will get the confirmation mail for successful payment as soon as driver completes the ride"
												,R.drawable.success, true, false, 
												"Ok", new DialogInterface.OnClickListener() {
													@Override
													public void onClick(DialogInterface dialog,int which) {
														CreditCardDataBase ccDB = new CreditCardDataBase(CreditCardDetailActivity.this);
														ccDB.deleteTable();
														ccDB.insertRow(LiveryCabStatics.userId, LiveryCabStatics.ccdata.cardType,
																LiveryCabStatics.ccdata.expirationMonth, 
																LiveryCabStatics.ccdata.expirationYear, 
																LiveryCabStatics.ccdata.cardNumber,
																LiveryCabStatics.ccdata.cvvNumber);
														startActivity(new Intent(CreditCardDetailActivity.this,ListViewActivity.class));
														finish();
													}
												},null,null);
											}else if(message.equalsIgnoreCase("DoDirect_Payment_Capture_Failed")){
												LiveryCabStaticMethods.showAlert(CreditCardDetailActivity.this,
														"Error Occured!","payPal Error",R.drawable.attention, true, false, 
														"Ok", null,null,null);
											}else if(message.equalsIgnoreCase("credit_card_not_valid")){
												LiveryCabStaticMethods.showAlert(CreditCardDetailActivity.this,
														"Error Occured!","Some error in credit card details",R.drawable.attention, true, false, 
														"Ok", null,null,null);
											}else{
												LiveryCabStaticMethods.showAlert(CreditCardDetailActivity.this,
														"Error Occured!","Please try again",R.drawable.attention, true, false, 
														"Ok", null,null,null);
											}
										}
									}
								});
						}
					}
				}
			}
		});
		
		((TextView)findViewById(R.id.textviewCardTypeCreditCardDetail))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder ab = new AlertDialog.Builder(CreditCardDetailActivity.this);
				ab.setTitle("Credit Card Type");
				ab.setSingleChoiceItems(cardTypeArray, 0,new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
							}
						}).setSingleChoiceItems(cardTypeArray, 0,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int item) {
									selectedItem = cardTypeArray[item];
									LiveryCabStatics.ccdata.cardType = selectedItem;
								}
						})
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {	
								cardType = selectedItem;
								LiveryCabStatics.ccdata.cardType = selectedItem;
								((TextView)findViewById(R.id.textviewCardTypeCreditCardDetail))
								.setText(cardType);
						}
					});
				ab.show();
				
			}
		});
		
		((TextView)findViewById(R.id.textviewExpirationMonthCreditCardDetail))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder ab = new AlertDialog.Builder(CreditCardDetailActivity.this);
				ab.setTitle("Expiration Month");
				ab.setSingleChoiceItems(expirationMonthArray, 0,new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
							}
						}).setSingleChoiceItems(expirationMonthArray, 0,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int item) {
									selectedItem = expirationMonthArray[item];
									LiveryCabStatics.ccdata.expirationMonth = selectedItem;
								}
						})
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {	
								expMonth = selectedItem;
								LiveryCabStatics.ccdata.expirationMonth = selectedItem;
								((TextView)findViewById(R.id.textviewExpirationMonthCreditCardDetail))
								.setText(expMonth);
						}
					});
				ab.show();
				
			}
		});
		
		((TextView)findViewById(R.id.textviewExpirationYearCreditCardDetail))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder ab = new AlertDialog.Builder(CreditCardDetailActivity.this);
				ab.setTitle("Expiration Year");
				ab.setSingleChoiceItems(expirationYearArray, 0,new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								
							}
						}).setSingleChoiceItems(expirationYearArray, 0,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int item) {
									selectedItem = expirationYearArray[item];
									LiveryCabStatics.ccdata.expirationYear = selectedItem;
								}
						})
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {	
								expYear = selectedItem;
								LiveryCabStatics.ccdata.expirationYear = selectedItem;
								((TextView)findViewById(R.id.textviewExpirationYearCreditCardDetail))
								.setText(expYear);
						}
					});
				ab.show();
				
			}
		});
	}
	
	@Override
	public void finish() {
		Intent data = new Intent();
		data.putExtra("CardType",cardType);
		data.putExtra("ExpMonth",expMonth);
		data.putExtra("ExpYear",expYear);
		data.putExtra("CardNumber",LiveryCabStatics.ccdata.cardNumber );
		data.putExtra("CVVNumber",LiveryCabStatics.ccdata.cvvNumber );
		setResult(1, data);
		super.finish();
	}
}
