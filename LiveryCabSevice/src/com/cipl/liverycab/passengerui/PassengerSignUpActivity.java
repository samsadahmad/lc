package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.parserclasses.LoginParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class PassengerSignUpActivity extends Activity {

	private boolean checked = true;
	private EditText fNameEditText,lNameEditText,phoneEditText,
					emailEditText,passwordEditText,confirmPasswordEditText; 
	private ProgressDialog pd;
	private LoginParserClass objParserClass;
	private String message ="";
	private ForgotPasswordParserClass objParser;
	private Context context;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.passangersignup);
		context = this;
		fNameEditText = (EditText)findViewById(R.id.edittextFirstNamePassengerSignup);
		lNameEditText = (EditText)findViewById(R.id.edittextLastNamePassengerSignup);
		phoneEditText = (EditText)findViewById(R.id.edittextPhoneNumberPassengerSignup);
		emailEditText = (EditText)findViewById(R.id.edittextEmailPassengerSignup);
		passwordEditText = (EditText)findViewById(R.id.edittextPasswordPassengerSignup);
		confirmPasswordEditText = (EditText)findViewById(R.id.edittextConfirmPasswordPassengerSignup);
		
		final ImageButton checkBtn = (ImageButton)findViewById(R.id.imagebuttonCheckPassengerSignup);
		
		((TextView)findViewById(R.id.textviewCreditCardDetailLableSignup))
			.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startActivityForResult(new Intent(PassengerSignUpActivity.this,CreditCardDetailActivity.class),1);
			}
		});
		
		/**This method for terms and condition*/
		((TextView)findViewById(R.id.tvTerms))
		.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(PassengerSignUpActivity.this,TermCondtionActivity.class),1);
		}
	});
	
		
		
		
		checkBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(checked){
					checked = false;
					checkBtn.setBackgroundResource(R.drawable.check);
				}else{
					checked = true;
					checkBtn.setBackgroundResource(R.drawable.checked);
				}
			}
		});
		
		((Button)findViewById(R.id.buttonSignupPassengerSignUp))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String mEmail = emailEditText.getText().toString();
				String mFName = fNameEditText.getText().toString();
				String mLName = lNameEditText.getText().toString();
				String mPhone = phoneEditText.getText().toString();
				String mPassword = passwordEditText.getText().toString();
				String mConfirmPass = confirmPasswordEditText.getText().toString();
				
				if(mFName.equalsIgnoreCase("")){
					LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","Please enter your first name"
							,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									fNameEditText.requestFocus();
								}
						},null,null);
				}else if(mLName.equalsIgnoreCase("")){
					LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","Please enter your last name"
							,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									lNameEditText.requestFocus();
								}
						},null,null);
				}else if(mPhone.equalsIgnoreCase("")){
					LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","Please enter your cell phone number"
							,R.drawable.attention, true, false, "Ok",new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									phoneEditText.requestFocus();
								}
						},null,null);
				}else if(mEmail.equalsIgnoreCase("")){
					LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","Please enter your email address"
							,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									emailEditText.requestFocus();
								}
						},null,null);
				}else if(!LiveryCabStaticMethods.isEmail(mEmail)){
					LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","Enter a valid email address"
							,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									emailEditText.requestFocus();
								}
						},null,null);
				}else if(mPassword.equalsIgnoreCase("")){
					LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","Please enter your password"
							,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									passwordEditText.requestFocus();
								}
						},null,null);
				}else if(mConfirmPass.equalsIgnoreCase("")){
					LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","Please enter confirm password"
							,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									confirmPasswordEditText.requestFocus();
								}
						},null,null);
				}else if(!(mConfirmPass.equalsIgnoreCase(mPassword))){
					LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","Pasword and confirm password must be same "
							,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									confirmPasswordEditText.requestFocus();
								}
						},null,null);
				}else if(mEmail.equalsIgnoreCase("") && mFName.equalsIgnoreCase("") && 
						mLName.equalsIgnoreCase("") && mPhone.equalsIgnoreCase("")&&
						mPassword.equalsIgnoreCase("") && mConfirmPass.equalsIgnoreCase("")){
					LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","All fields are mendatory"
							,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									fNameEditText.requestFocus();
								}
						},null,null);
				}else if(!checked){
					LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","Terms and condition must be agreed"
							,R.drawable.attention, true, false, "Ok", null,null,null);
				}else{
					pd = LiveryCabStaticMethods.returnProgressBar(context);
					objParserClass = new LoginParserClass(UrlStatics.getPassengerSignupUrl(
										mFName, mLName, mPhone,mEmail,mPassword,"123155","1237455456","android"),pd);
					objParserClass.start();
					
					pd.setOnDismissListener(new OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							if(objParserClass.actualDismiss){
								if(objParserClass.loginDataObj!=null)
									message = objParserClass.loginDataObj.message;
									if(message.equalsIgnoreCase("Registered_sucessfully"))
									{
										LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this, 
											"Success!!", "You have successfully registered as a LiveryCab Passenger!",R.drawable.success,true,false,"Ok", 
												new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													// TODO Auto-generated method stub
													LiveryCabStatics.isLoginPassenger = true;
													LiveryCabStatics.passengerId = objParserClass.loginDataObj.userID;
													LiveryCabStatics.userId = objParserClass.loginDataObj.userID;
													if(LiveryCabStatics.ccdata!= null){
														if(LiveryCabStatics.ccdata.cardType !=null){
															if(LiveryCabStatics.ccdata.expirationMonth == null ||
																	LiveryCabStatics.ccdata.expirationYear == null || 
																	LiveryCabStatics.ccdata.cardNumber == null || 
																	LiveryCabStatics.ccdata.cvvNumber == null){
																LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,
																		"Error Occured!!","Credit card information is required in the system before any liverycab ride purchase",
																		R.drawable.attention, true, false, 
																		"Ok", null,null,null);
																
															}else if(LiveryCabStatics.ccdata.cardNumber.length()<16 || !(LiveryCabStaticMethods.isCardNumberValid(LiveryCabStatics.ccdata.cardNumber ))){
																LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this,"Error Occured!","Enter valid card number"
																		,R.drawable.attention, true, false, "Ok", null,null,null);
															}else{
																CreditCardDataBase ccDB = new CreditCardDataBase(PassengerSignUpActivity.this);
																ccDB.deleteTable();
																ccDB.insertRow(LiveryCabStatics.userId, LiveryCabStatics.ccdata.cardType,
																	LiveryCabStatics.ccdata.expirationMonth, 
																	LiveryCabStatics.ccdata.expirationYear, 
																	LiveryCabStatics.ccdata.cardNumber,
																	LiveryCabStatics.ccdata.cvvNumber);
															}
														}
													}
													if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("History"))
														startActivity(new Intent(PassengerSignUpActivity.this,HistoryActivity.class));
													else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("MapView"))
														startActivity(new Intent(PassengerSignUpActivity.this,MapViewActivity.class));
													else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("Profile"))
														startActivity(new Intent(PassengerSignUpActivity.this,PassengerProfileActivity.class));
													else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("Notification"))
														startActivity(new Intent(PassengerSignUpActivity.this,NotificationActivity.class));
													else if(LiveryCabStatics.isAddPickup) {
														startActivity(new Intent(PassengerSignUpActivity.this,BookRideActivity.class));
														/*try{
														pd = LiveryCabStaticMethods.returnProgressBar(PassengerSignUpActivity.this);
														objParser = new ForgotPasswordParserClass(UrlStatics
																.getUrlforSavingSourceAndDesti(LiveryCabStatics.userId,
																URLEncoder.encode(LiveryCabStatics.pickupAddress, "utf-8"), 
																URLEncoder.encode(LiveryCabStatics.destinationAddress, "utf-8"),
																URLEncoder.encode(LiveryCabStatics.sourceLatitude, "utf-8"),
																URLEncoder.encode(LiveryCabStatics.sourceLongitude,"utf-8"),
																URLEncoder.encode(LiveryCabStatics.destinationLatitude,"utf-8"),
																URLEncoder.encode(LiveryCabStatics.destinationLongitude,"utf-8"),
																URLEncoder.encode(LiveryCabStaticMethods.getDateTime(), "utf-8"),
																URLEncoder.encode(LiveryCabStatics.ridestatus,"utf-8")),pd);
														objParser.start();
														} catch (UnsupportedEncodingException e) {
															// TODO Auto-generated catch block
															e.printStackTrace();
														}
														pd.setOnDismissListener(new OnDismissListener() {
															@Override
															public void onDismiss(DialogInterface arg0) {
																// TODO Auto-generated method stub
																if(objParserClass.actualDismiss){
																if(objParser.loginDataObj!=null)
																	message = objParser.loginDataObj.message;
																if(message.equalsIgnoreCase("ride_booked"))
																{
																	LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this, 
																			"Success", "Your ride booked successfully",R.drawable.attention,true,false,"Ok", 
																			new DialogInterface.OnClickListener() {
																				@Override
																				public void onClick(
																						DialogInterface dialog,
																						int which) {
																					// TODO Auto-generated method stub
																					startActivity(new Intent(PassengerSignUpActivity.this,BookRideActivity.class));
																				}
																		
																	}, null, null);
																	
																}else if(message.equalsIgnoreCase("Some_error_in_ride_booking")){
																	LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this, 
																			"Error Occured!", "Some_error_in_ride_booking",R.drawable.attention,true,false,"Ok", 
																			null, null, null);
																}else if(message.equalsIgnoreCase("session_expire")){
																	LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this, 
																			"Error Occured!", "Please Log in to continue",R.drawable.attention,true,false,"Ok", 
																			null, null, null);
																}else{
																	LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this, 
																			"Error Occured!", "Try again!!",R.drawable.attention,true,false,"Ok", 
																			null, null, null);
																}
															}}
														});*/
														
													}else{
														startActivity(new Intent(PassengerSignUpActivity.this,PassengerMenuActivity.class));
													}
												}
										}, null, null);
									}else if(message.equalsIgnoreCase("email_already_in_use")){
										LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this, 
												"Error Occured!", "This email address is already in use. Please register with another email address.",R.drawable.attention,true,false,"Ok", 
												null, null, null);
									}else if(message.equalsIgnoreCase("invalid_email")){
										LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this, 
												"Error Occured!", "Invalid email address.",R.drawable.attention,true,false,"Ok", 
												null, null, null);
									}else{
										LiveryCabStaticMethods.showAlert(PassengerSignUpActivity.this, 
												"Error Occured!", "Try again",R.drawable.attention,true,false,"Ok", 
												null, null, null);
									}
							}
						}
					});
				
				}
			}
		});
	}
	 
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 1) {
			if (data.hasExtra("CardType")) {
				LiveryCabStatics.ccdata.cardNumber = data.getStringExtra("CardNumber");
				LiveryCabStatics.ccdata.cardType = data.getStringExtra("CardType");
				LiveryCabStatics.ccdata.cvvNumber =data.getStringExtra("CVVNumber");
				LiveryCabStatics.ccdata.expirationMonth = data.getStringExtra("ExpMonth");
				LiveryCabStatics.ccdata.expirationYear =data.getStringExtra("ExpYear");
			}
		}
	}
}