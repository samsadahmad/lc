package com.cipl.liverycab.passengerui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TimePicker;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.parserclasses.RecentAddressParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class RecentDestinationAddressActivity extends Activity {

	private Context mContext;
	static final int TIME_DIALOG_ID = 0;
	static final int DATE_DIALOG_ID = 1;
	private ListView recentAddList;
	private ForgotPasswordParserClass objParser;
	private OnTimeSetListener mTimeSetListener = null;
	private String message = "";
	private ProgressDialog pd;
	private String dateSelected;
	String[] monthArray = { "Jan", "Feb", "Mar", "Apr", "May", "June", "Jul",
			"Aug", "Sep", "Oct", "Nov", "Dec" };
	private RecentAddressParserClass objParserClass;
	private int limit = 10;
	private String[] from = new String[] { "address", "distance" };
	private int[] to = new int[] { R.id.textviewAddressDetailRecentAddresss,
			R.id.textviewMilesRecentAddresss };
	private int textlength;
	private EditText searchEditText;
	public static String time_selected;
	private Calendar c = Calendar.getInstance();
	final int cyear = c.get(Calendar.YEAR);
	final int cmonth = c.get(Calendar.MONTH);
	final int cday = c.get(Calendar.DAY_OF_MONTH);
	final int cHour = c.get(Calendar.HOUR_OF_DAY);
	final int cMin = c.get(Calendar.MINUTE);
	final int cSec = c.get(Calendar.SECOND);
	private String soLat, soLng, disLat, disLng;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recentdestinationaddress);
		mContext = this;
		LiveryCabStatics.recentDesti = true;
		final List<HashMap<String, String>> fillMaps = new ArrayList<HashMap<String, String>>();
		searchEditText = (EditText) findViewById(R.id.edittextSearchRecentDestinationAddress);
		recentAddList = (ListView) findViewById(R.id.listviewRecentDestinationAddress);

		objParserClass = new RecentAddressParserClass(
				UrlStatics.getRecentAddressUrl(LiveryCabStatics.userId,
						"destination", LiveryCabStatics.currentLatitude,
						LiveryCabStatics.currentLongitude, limit));
		objParserClass.start();

		LiveryCabStaticMethods.returnProgressBar(
				RecentDestinationAddressActivity.this).setOnDismissListener(
				new OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						// Setting adapter to the list
						for (int i = 0; i < LiveryCabStatics.myList.size(); i++) {
							HashMap<String, String> map = new HashMap<String, String>();
							map.put("address",
									LiveryCabStatics.myList.get(i).address);
							map.put("distance",
									LiveryCabStatics.myList.get(i).distanceInMiles);
							fillMaps.add(map);
						}
						SimpleAdapter adapter = new SimpleAdapter(
								RecentDestinationAddressActivity.this,
								fillMaps, R.layout.recentaddresscustomcell,
								from, to);
						recentAddList.setAdapter(adapter);
					}
				});

		searchEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				fillMaps.clear();
				textlength = searchEditText.getText().length();
				for (int i = 0; i < LiveryCabStatics.myList.size(); i++) {
					if (textlength <= LiveryCabStatics.myList.get(i).address
							.length()) {
						if (searchEditText
								.getText()
								.toString()
								.equalsIgnoreCase(
										(String) LiveryCabStatics.myList.get(i).address
												.subSequence(0, textlength))) {
							HashMap<String, String> map = new HashMap<String, String>();
							map.put("address",
									LiveryCabStatics.myList.get(i).address);
							map.put("distance",
									LiveryCabStatics.myList.get(i).distanceInMiles);
							fillMaps.add(map);
						}
					}
				}
				if (!fillMaps.isEmpty()) {
					SimpleAdapter adapter = new SimpleAdapter(
							RecentDestinationAddressActivity.this, fillMaps,
							R.layout.recentaddresscustomcell, from, to);
					recentAddList.setAdapter(adapter);
				} else {
					LiveryCabStaticMethods.showAlert(
							RecentDestinationAddressActivity.this, "Alert!!",
							"No record found", R.drawable.attention, true,
							false, "Ok", null, null, null);
				}
			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}
		});

		recentAddList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				LiveryCabStatics.destinationAddress = LiveryCabStatics.myList
						.get(position).address;
				LiveryCabStatics.destinationLatitude = LiveryCabStatics.myList
						.get(position).addLatitude;
				LiveryCabStatics.destinationLongitude = LiveryCabStatics.myList
						.get(position).addLongitude;
				double distance = 0;
				if (LiveryCabStatics.sourceLatitude == null
						|| LiveryCabStatics.sourceLatitude.equalsIgnoreCase("")) {
					soLat = LiveryCabStatics.currentLatitude;
					soLng = LiveryCabStatics.currentLongitude;
				} else {
					soLat = LiveryCabStatics.sourceLatitude;
					soLng = LiveryCabStatics.sourceLongitude;
				}
				if (LiveryCabStatics.destinationLatitude == null
						|| LiveryCabStatics.destinationLatitude
								.equalsIgnoreCase("")) {
					disLat = LiveryCabStatics.currentLatitude;
					disLng = LiveryCabStatics.currentLongitude;
				} else {
					disLat = LiveryCabStatics.destinationLatitude;
					disLng = LiveryCabStatics.destinationLongitude;
				}

				/*
				 * distance = LiveryCabStaticMethods.calculateDistance(
				 * URLEncoder.encode(soLat.trim(), "utf-8"),
				 * URLEncoder.encode(soLng.trim(), "utf-8"),
				 * URLEncoder.encode(disLat.trim(), "utf-8"),
				 * URLEncoder.encode(disLng.trim(), "utf-8"));
				 */
				distance = LiveryCabStaticMethods.calculateDistance(
						soLat.trim(), soLng.trim(), disLat.trim(),
						disLng.trim());
				LiveryCabStatics.isAddPickup = true;
				if (LiveryCabStatics.isRideLater)
					showDialog(DATE_DIALOG_ID);
				else if (LiveryCabStatics.isLoginPassenger) {
					finish();
					startActivity(new Intent(RecentDestinationAddressActivity.this,
							BookRideActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				} else{
					finish();
					startActivity(new Intent(
							RecentDestinationAddressActivity.this,
							LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				}
			}
		});
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Calendar c = Calendar.getInstance();
		int cyear = c.get(Calendar.YEAR);
		int cmonth = c.get(Calendar.MONTH);
		int cday = c.get(Calendar.DAY_OF_MONTH);
		int chour = c.get(Calendar.HOUR_OF_DAY);
		int cmin = c.get(Calendar.MINUTE);
		switch (id) {
		case TIME_DIALOG_ID:
			return new TimePickerDialog(this, mTimeSetListener, chour, cmin,
					false);

		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, cyear, cmonth,
					cday);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int Year, int monthOfYear,
				int dayOfMonth) {
			dateSelected = "" + dayOfMonth + " " + monthArray[monthOfYear]
					+ " " + Year;
			if (dateSelected != null) {
				final Calendar c = Calendar.getInstance();
				int mYear = c.get(Calendar.YEAR);
				int mMonth = c.get(Calendar.MONTH);
				int mDay = c.get(Calendar.DAY_OF_MONTH);
				Calendar currentDate = Calendar.getInstance();
				currentDate.set(Calendar.DAY_OF_MONTH, mDay);
				currentDate.set(Calendar.MONTH, mMonth);
				currentDate.set(Calendar.YEAR, mYear);

				final Calendar selectedDate = Calendar.getInstance();
				selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				selectedDate.set(Calendar.MONTH, monthOfYear);
				selectedDate.set(Calendar.YEAR, Year);
				if (selectedDate.after(currentDate)
						|| selectedDate.equals(currentDate)) {
					dateSelected = ""
							+ String.valueOf(selectedDate
									.get(Calendar.MONTH + 1)) + " "
							+ selectedDate.get(Calendar.DAY_OF_MONTH) + " "
							+ selectedDate.get(Calendar.YEAR);
					mTimeSetListener = new OnTimeSetListener() {
						public void onTimeSet(TimePicker arg0, int arg1,
								int arg2) {
							time_selected = "" + arg1 + ":" + cMin + ":" + 00;
							/* Changes by sam* */
							LiveryCabStatics.date_Selected = getFormatedDate(selectedDate
									.getTimeInMillis());
							if (LiveryCabStatics.isLoginPassenger) {
								finish();
								startActivity(new Intent(
										RecentDestinationAddressActivity.this,
										BookRideActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
							} else{
								finish();
								startActivity(new Intent(RecentDestinationAddressActivity.this,
										LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
							}
						}
					};
					showDialog(TIME_DIALOG_ID);
				} else if (selectedDate.equals(currentDate)) {
					mTimeSetListener = new OnTimeSetListener() {
						@Override
						public void onTimeSet(TimePicker view, int hourOfDay,
								int minute) {
							time_selected = "" + hourOfDay + ":" + minute + ":"
									+ 00;
							final Calendar cc = Calendar.getInstance();
							int mYearc = cc.get(Calendar.YEAR);
							int mMonthc = cc.get(Calendar.MONTH);
							int mDayc = cc.get(Calendar.DAY_OF_MONTH);
							int HOUR = cc.get(Calendar.HOUR_OF_DAY);
							int MINUTE = cc.get(Calendar.MINUTE);

							Calendar currentTime = Calendar.getInstance();
							currentTime.set(Calendar.HOUR_OF_DAY, HOUR);
							currentTime.set(Calendar.MINUTE, MINUTE);
							currentTime.set(Calendar.SECOND, 0);
							currentTime.set(Calendar.MILLISECOND, 0);

							Calendar selectedTime = Calendar.getInstance();
							selectedTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
							selectedTime.set(Calendar.MINUTE, minute);
							selectedTime.set(Calendar.SECOND, 0);
							selectedTime.set(Calendar.MILLISECOND, 0);
							if (selectedTime.after(currentTime)) {
								time_selected = "" + cHour + ":" + cMin + ":"
										+ cSec;
								/* Changes by sam* */
								LiveryCabStatics.date_Selected = getFormatedDate(selectedTime
										.getTimeInMillis());
								if (LiveryCabStatics.isLoginPassenger) {
									finish();
									startActivity(new Intent(
											RecentDestinationAddressActivity.this,
											BookRideActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
								} else{
									finish();
									startActivity(new Intent(
											RecentDestinationAddressActivity.this,
											LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
								}
							} else {
								LiveryCabStaticMethods.showAlert(mContext,
										"Alert!!", "Enter a valid time",
										R.drawable.attention, true, false,
										"Ok", null, null, null);
							}
						}
					};
					showDialog(TIME_DIALOG_ID);
				} else {
					LiveryCabStaticMethods
							.showAlert(
									RecentDestinationAddressActivity.this,
									"Alert!!",
									"Previous Date cannot be selected. Please choose date in the future",
									R.drawable.attention, true, false, "Ok",
									null, null, null);
				}
			}
		}
	};

	private String getFormatedDate(long timeInMillis) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat();
			Date d = new Date(timeInMillis);
			sdf.applyPattern("EEE MMM d,yyyy h:mm a");
			String formatedDate = sdf.format(d);
			return formatedDate;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
}