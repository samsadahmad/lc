package com.cipl.liverycab.passengerui;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cipl.liverycab.driverui.DriverReviewActivity;
import com.cipl.liverycab.parserclasses.DriverDetailParser;
import com.cipl.liverycab.parserclasses.RideConfirmParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class DriverDetailActivity extends Activity implements OnClickListener{

	private String bidID, driverId, fName,lName,phoneNum,
	bidAmt,ccType = "",expMnth = "",expYr = "",cvvNum = "",cardNum = "";
	private String message ="";
	private ProgressDialog pd;
	private DriverDetailParser parserObject;
	private RideConfirmParserClass bidConfirmParser;
	private CreditCardDataBase ccDB;
	public static boolean driverDetailPage = false;
	private String DRIVER_NAME;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.driverdetail);
		
		Bundle extras = getIntent().getExtras();
		if(extras != null){
			bidID = extras.getString("BidID") != null? extras.getString("BidID"): "";
			driverId = extras.getString("driverID") != null? extras.getString("driverID"): "";
			fName = extras.getString("fName") != null? extras.getString("fName"): "";
			lName = extras.getString("lName") != null? extras.getString("lName"): "";
			bidAmt = extras.getString("bidAmount") != null? extras.getString("bidAmount"): "";
			phoneNum = extras.getString("phoneNum") != null? extras.getString("phoneNum"): "";
		}
		
		setItUpview();		
	}
	private void setItUpview() {
		// TODO Auto-generated method stub
		final TextView driverNameText =(TextView)findViewById(R.id.textviewDriverNameDriverDetail);
		final TextView dlNumberText =(TextView)findViewById(R.id.textviewLicencePlateNumberDriverDetail);
		dlNumberText.setSelected(true);
		final TextView etaTimeText =(TextView)findViewById(R.id.textviewETATimeDriverDetail);
		final TextView numberOfReviewText =(TextView)findViewById(R.id.textviewDriverReviewDriverDetail);
		final TextView pickUpAddText =(TextView)findViewById(R.id.textViewPickupaddressDriverDetail);
		final TextView destinationAddText =(TextView)findViewById(R.id.textViewDestinationaddressDriverDetail);
		final TextView datTimeText =(TextView)findViewById(R.id.textViewTimeDateDriverDetail);
		final TextView bidAmtTextWithBg =(TextView)findViewById(R.id.textViewBidAmtWithBGDriverDetail);
		final TextView bidAmtText =(TextView)findViewById(R.id.textviewBidAmountDriverDetail);
		final ImageView driverImage = (ImageView)findViewById(R.id.imageviewDriverImageDriverDetail);
		final ImageView carImage = (ImageView)findViewById(R.id.imageviewCarImageDriverDetail);
		final TextView mtvHandicapedvalue =(TextView)findViewById(R.id.tvHandicapedvalue);
		
		DriverDetailActivity.driverDetailPage = true;
		if(LiveryCabStaticMethods.isInternetAvailable(this)){
			pd = LiveryCabStaticMethods.returnProgressBar(DriverDetailActivity.this);
			pd.setCancelable(false);
			parserObject = new DriverDetailParser(UrlStatics.getDriverDetailUrl(bidID),pd,DriverDetailActivity.this);
			parserObject.start();
			
			pd.setOnDismissListener(new OnDismissListener() {
				

				@Override
				public void onDismiss(DialogInterface dialog) {
					if(parserObject.actualDismiss){
						if(parserObject.dataObject!=null)
							message = parserObject.dataObject.message;
						if(message.equalsIgnoreCase("Sucess"))
						{
							DRIVER_NAME = parserObject.dataObject.firsName+
									" "+parserObject.dataObject.lastName;
							driverNameText.setText(DRIVER_NAME);
							dlNumberText.setText(parserObject.dataObject.licenceNumber);
							etaTimeText.setText(parserObject.dataObject.etaTime+" minute");
							numberOfReviewText.setText("("+parserObject.dataObject.numberOfReview+")");
							pickUpAddText.setText(parserObject.dataObject.pickUpAddress);
							destinationAddText.setText(parserObject.dataObject.destinationAdd);
							datTimeText.setText(parserObject.dataObject.dateTime);
							mtvHandicapedvalue.setText(parserObject.dataObject.handicapped.equalsIgnoreCase("Y") ? "Yes" : "No");
							bidAmtTextWithBg.setText("$"+parserObject.dataObject.bidAmt);
							bidAmtText.setText("$"+parserObject.dataObject.bidAmt);
							if(parserObject.dataObject.driverImage!=null );
								driverImage.setImageDrawable(parserObject.dataObject.driverImage);
							if(parserObject.dataObject.carImage!=null );
								carImage.setImageDrawable(parserObject.dataObject.carImage);
						}
					}
				}
			});
		}		
		/**Click on review text*/
		numberOfReviewText.setOnClickListener(this);
		
		/**Click on Call Button*/
		Button mbtnCallToDriver = (Button)findViewById(R.id.btnCallToDriver);
		mbtnCallToDriver.setOnClickListener(this);
		
		/**Click on confirm ride button*/
		Button mbuttonConfirmDriverDetail = (Button)findViewById(R.id.buttonConfirmDriverDetail);
		mbuttonConfirmDriverDetail.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.buttonConfirmDriverDetail:
			btnConfirm();
			break;
		
		case R.id.btnCallToDriver:
			callToDriver();
			break; 
			
		case R.id.textviewDriverReviewDriverDetail:
			callReview();
			break;

		default:
			break;
		}
	}
	private void callReview() {
		// TODO Auto-generated method stub
		String did = parserObject.dataObject.driverId;
		if(parserObject.dataObject.numberOfReview.equalsIgnoreCase("0")){
			LiveryCabStaticMethods.showAlert(DriverDetailActivity.this,"Review","There is no current review for "+DRIVER_NAME
					,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
						}
				},null,null);
		}else{
			Intent i = new Intent(DriverDetailActivity.this,DriverReviewActivity.class);
			i.putExtra("DriverId", did);
			i.putExtra("DriverName", DRIVER_NAME);
			startActivity(i);
		}		
	}
	private void callToDriver() {
		// TODO Auto-generated method stub
		if(phoneNum.equalsIgnoreCase("")){
			Toast.makeText(DriverDetailActivity.this, "Invalid phone number", 0).show();
		}else{
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:"+phoneNum));
			startActivity(callIntent);
		}
	}
	private void btnConfirm() {
		// TODO Auto-generated method stub
		if(LiveryCabStaticMethods.isInternetAvailable(DriverDetailActivity.this)){
			pd = LiveryCabStaticMethods.returnProgressBar(DriverDetailActivity.this);
			try {
				bidConfirmParser = new RideConfirmParserClass(
						UrlStatics.getBidConfirmUrl(bidID,
						LiveryCabStatics.passengerId,driverId,
						fName,lName,URLEncoder.encode("","utf-8"),
						"11","12","539","1234567890123456", "10"),pd);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bidConfirmParser.start();
			
			pd.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					if(bidConfirmParser.actualDismiss){
						if(bidConfirmParser.loginDataObj!=null)
							message = bidConfirmParser.loginDataObj.message;
						if(message.equalsIgnoreCase("DoDirect_Payment_Capture_Successfully")){
							LiveryCabStaticMethods.showAlert(DriverDetailActivity.this,
								"Success","Your ride is confirmed and payment has been processed.  After ride is complete you will be mailed a reciept"
								,R.drawable.success, true, false, 
								"Ok", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,int which) {
										startActivity(new Intent(DriverDetailActivity.this,ListViewActivity.class));
										finish();
									}
								},null,null);
							}else if(message.equalsIgnoreCase("DoDirect_Payment_Capture_Failed")){
								LiveryCabStaticMethods.showAlert(DriverDetailActivity.this,
										"Error Occured","payPal Error",R.drawable.attention, true, false, 
										"Ok", null,null,null);
							}else if(message.equalsIgnoreCase("credit_card_not_valid")){
								LiveryCabStaticMethods.showAlert(DriverDetailActivity.this,
										"Error Occured","Some error in credit card details",R.drawable.attention, true, false, 
										"Ok", null,null,null);
							}else{
								LiveryCabStaticMethods.showAlert(DriverDetailActivity.this,
										"Error Occured","Please try again",R.drawable.attention, true, false, 
										"Ok", null,null,null);
							}
						}
					}
				});
		}
	}
}