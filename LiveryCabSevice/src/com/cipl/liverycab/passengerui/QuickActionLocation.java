package com.cipl.liverycab.passengerui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class QuickActionLocation extends PopupWindow implements KeyEvent.Callback {
	private final Context mContext;
	private final LayoutInflater mInflater;
	private final WindowManager mWindowManager;
	private Intent intent;
	View contentView;
	
	private int mScreenWidth;
	private int mScreenHeight;
	
	private int mShadowHoriz;
	private int mShadowVert;
	private int mShadowTouch;
	
	private ImageView mArrowUp;
	private ImageView mArrowDown;
	private ImageView fwdArrow;
	private ViewGroup mTrack;
	private Animation mTrackAnim;
	private LinearLayout actionlayout;
	
	private View mPView;
	private Rect mAnchor;
	public static String date_selected;
	public static String time_selected;
	private OnDateSetListener mDateSetListener = null;
	private OnTimeSetListener mTimeSetListener = null;
	int dilogId = 0;
	
	
	TimePickerDialog timepick;
	DatePickerDialog datepick;
	private Calendar c = Calendar.getInstance();
	final int cyear = c.get(Calendar.YEAR);
	final int cmonth = c.get(Calendar.MONTH);
	final int cday = c.get(Calendar.DAY_OF_MONTH);
	final int cHour = c.get(Calendar.HOUR_OF_DAY);
	final int cMin = c.get(Calendar.MINUTE);
	final int cSec = c.get(Calendar.SECOND);
	// For lazy loading of icon
	
	public QuickActionLocation(Context context, View pView, Rect rect) {
		super(context);
		
		mPView = pView;
		mAnchor = rect;
		
		mContext = context;
		mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		mInflater = ((Activity)mContext).getLayoutInflater();
		
		setContentView(R.layout.quickactionlocation);
		
		mScreenWidth = mWindowManager.getDefaultDisplay().getWidth();
		mScreenHeight = mWindowManager.getDefaultDisplay().getHeight();
		
		setWindowLayoutMode(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		
		final Resources res = mContext.getResources();
		
		setWidth(mScreenWidth + mShadowHoriz + mShadowHoriz);
		setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		
		setBackgroundDrawable(new ColorDrawable(0));
		
		mArrowUp = (ImageView) contentView.findViewById(R.id.arrow_up_bookride);
		mArrowDown = (ImageView) contentView.findViewById(R.id.arrow_down_bookride);
		//fwdArrow = (ImageView)contentView.findViewById(R.id.quickaction_arrow_bookride);
		
		
		setFocusable(true);
		setTouchable(true);
		setOutsideTouchable(true);
		
		// Prepare track entrance animation
		mTrackAnim = AnimationUtils.loadAnimation(mContext, R.anim.quickaction);
		mTrackAnim.setInterpolator(new Interpolator() {
			public float getInterpolation(float t) {
				// Pushes past the target area, then snaps back into place.
				// Equation for graphing: 1.2-((x*1.6)-1.1)^2
				final float inner = (t * 1.55f) - 1.1f;
				return 1.2f - inner * inner;
			}
		});	
		
		/*fwdArrow.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {
				double distance = 0;
				if(LiveryCabStatics.destiMapView){
					try {
						distance = LiveryCabStaticMethods.calculateDistance(
								URLEncoder.encode(LiveryCabStatics.sourceLatitude, "utf-8"),
								URLEncoder.encode(LiveryCabStatics.sourceLongitude, "utf-8"),
								URLEncoder.encode(LiveryCabStatics.destinationLatitude, "utf-8"),
								URLEncoder.encode(LiveryCabStatics.destinationLongitude, "utf-8"));
						} catch (UnsupportedEncodingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if(distance <= 1000){
							LiveryCabStaticMethods.showAlert(mContext, 
							"Alert!!", "Your pickup address and destination are very near to each other. Please re-select the addresses for booking the ride.",
							R.drawable.attention,true,false,"Ok", 
							null, null, null);
						}else if(LiveryCabStatics.isRideLater){
							mDateSetListener = new DatePickerDialog.OnDateSetListener() {
								// onDateSet method
								@Override
								public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
									date_selected = ""+cday+" "+String.valueOf(cmonth+1)+" "+cyear;
									if(date_selected!=null){
										final Calendar c = Calendar.getInstance();
										int mYear = c.get(Calendar.YEAR);
										int mMonth = c.get(Calendar.MONTH);
										int mDay = c.get(Calendar.DAY_OF_MONTH);
										Calendar currentDate = Calendar.getInstance();
										currentDate.set(Calendar.DAY_OF_MONTH, mDay);
										currentDate.set(Calendar.MONTH, mMonth);
										currentDate.set(Calendar.YEAR, mYear);

										Calendar selectedDate = Calendar.getInstance();
										selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
										selectedDate.set(Calendar.MONTH, monthOfYear);
										selectedDate.set(Calendar.YEAR, year);								
										if (selectedDate.after(currentDate)
												|| selectedDate.equals(currentDate)) {
											date_selected = ""+String.valueOf(selectedDate.get(Calendar.MONTH+1))+" "
															+selectedDate.get(Calendar.DAY_OF_MONTH)+" "
															+selectedDate.get(Calendar.YEAR);
											mTimeSetListener = new OnTimeSetListener() {
											@Override
											public void onTimeSet(TimePicker view, int hourOfDay, int minute) {											
												time_selected = ""+cHour+":"+cMin+":"+cSec;
												if(LiveryCabStatics.isLoginPassenger){
													DestinationMapViewActivity.isbookRideDestiMapView();
												}else
													startActivity(intent);										}
											};
											timepick =  new TimePickerDialog(mContext,  mTimeSetListener,cHour,cMin,false);
											timepick.show();
										}else if(selectedDate.equals(currentDate)){
											mTimeSetListener = new OnTimeSetListener() {
											@Override
											public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
													time_selected = ""+cHour+":"+cMin+":"+cSec;
													final Calendar cc = Calendar.getInstance();
													int mYearc = cc.get(Calendar.YEAR);
													int mMonthc = cc.get(Calendar.MONTH);
													int mDayc = cc.get(Calendar.DAY_OF_MONTH);
													int HOUR = cc.get(Calendar.HOUR_OF_DAY);
													int MINUTE = cc.get(Calendar.MINUTE);
													
													Calendar currentTime = Calendar.getInstance();
													currentTime.set(Calendar.HOUR_OF_DAY, HOUR);
													currentTime.set(Calendar.MINUTE, MINUTE);
													currentTime.set(Calendar.SECOND, 0);
													currentTime.set(Calendar.MILLISECOND, 0);
													
													Calendar selectedTime = Calendar.getInstance();
													selectedTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
													selectedTime.set(Calendar.MINUTE, minute);
													selectedTime.set(Calendar.SECOND, 0);
													selectedTime.set(Calendar.MILLISECOND, 0);
													if (selectedTime.after(currentTime)) {
														time_selected = ""+cHour+":"+cMin+":"+cSec;
														startActivity(intent);
													}else{
														LiveryCabStaticMethods.showAlert(mContext,
																"Alert!!","Enter a valid time",R.drawable.attention,
																true,false,"Ok",null,null,null);
													}
											}
										};
										timepick =  new TimePickerDialog(mContext,  mTimeSetListener,cHour,cMin,false);
										timepick.show();
									}else {
										LiveryCabStaticMethods.showAlert(mContext,
												"Alert!!","Back date is not allowed",R.drawable.attention,
												true,false,"Ok",null,null,null);
									}
								}
										
							}
						};
						datepick =  new DatePickerDialog(mContext,  mDateSetListener,  cyear, cmonth, cday);
						datepick.show();
					}else if(LiveryCabStatics.isLoginPassenger){
						DestinationMapViewActivity.isbookRideDestiMapView();
					}else 
						startActivity(intent);
				}else 
					startActivity(intent);
			}
		});*/
	}
	
	public void setIntent(Intent intent)
	{
		this.intent = intent;
	}
	private void setContentView(int resId) {
		contentView = mInflater.inflate(resId, null);
		super.setContentView(contentView);
	}
	
	public View getHeaderView() {
		return contentView.findViewById(R.id.quickaction_header);
	}
	
	public void setTitle(CharSequence title) {
		contentView.findViewById(R.id.quickaction_header_content_bookride).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.quickaction_primary_text_bookride).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.quickaction_primary_text_bookride)).setText(title);
	}
	
	public void setTitle(int resid) {
		setTitle(mContext.getResources().getString(resid));
	}
	
	public void setText(CharSequence text) {
		contentView.findViewById(R.id.quickaction_header_content_bookride).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.quickaction_secondary_text_bookride).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.quickaction_secondary_text_bookride)).setText(text);
	}
	
	public void setText(int resid) {
		setText(mContext.getResources().getString(resid));
	}
	
	/**
	 * Show the correct call-out arrow based on a {@link R.id} reference.
	 */
	private void showArrow(int whichArrow, int requestedX) {
		final View showArrow = (whichArrow == R.id.arrow_up_bookride) ? mArrowUp : mArrowDown;
		final View hideArrow = (whichArrow == R.id.arrow_up_bookride) ? mArrowDown : mArrowUp;

		// Dirty hack to get width, might cause memory leak
		final int arrowWidth = mContext.getResources().getDrawable(R.drawable.quickaction_arrow_up).getIntrinsicWidth();

		showArrow.setVisibility(View.VISIBLE);
		ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams)showArrow.getLayoutParams();
		param.leftMargin = requestedX - arrowWidth / 2;
		//Log.d("QuickActionWindow", "ArrowWidth: "+arrowWidth+"; LeftMargin for Arrow: "+param.leftMargin);

		hideArrow.setVisibility(View.INVISIBLE);
	}
	
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBackPressed();
			return true;
		}
		return false;
	}
	
	private void onBackPressed() {
			dismiss();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return false;
	}

	public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
		return false;
	}
	
	public void show() {
		show(mAnchor.centerX());
	}
	
	public void show(int requestedX) {
		super.showAtLocation(mPView, Gravity.NO_GRAVITY, 0, 0);
		
		// Calculate properly to position the popup the correctly based on height of popup
		if (isShowing()) {
			int x, y, windowAnimations;
			this.getContentView().measure(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			final int blockHeight = this.getContentView().getMeasuredHeight();
			
			x = -mShadowHoriz;
			
			if (mAnchor.top > blockHeight) {
				// Show downwards callout when enough room, aligning bottom block
				// edge with top of anchor area, and adjusting to inset arrow.
				showArrow(R.id.arrow_down_bookride, requestedX);
				y = mAnchor.top - blockHeight;
				windowAnimations = R.style.QuickActionAboveAnimation;
	
			}else {
				// Otherwise show upwards callout, aligning block top with bottom of
				// anchor area, and adjusting to inset arrow.
				showArrow(R.id.arrow_up_bookride, requestedX);
				y = mAnchor.bottom;
				windowAnimations = R.style.QuickActionBelowAnimation;
			}
			setAnimationStyle(windowAnimations);
			this.update(x, y, -1, -1);
		}
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
}
