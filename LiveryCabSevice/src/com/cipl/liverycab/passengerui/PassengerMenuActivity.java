package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.CellLocation;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class PassengerMenuActivity extends Activity implements LocationListener {
	private double latitude, longitude;
	private LocationManager mLocManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.passengermenu);
		
		/**SetUpView*/
		mLocManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
				this);
		mLocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
				0, this);
		CellLocation.requestLocationUpdate();	
		
		
		String userTypeLogin = getPreferences("LOGIN");
		String userType = getPreferences("USER_TYPE");
		String passId = getPreferences("PASS_ID");
		
		if(userTypeLogin.equalsIgnoreCase("TRUE_PASSENGER") && userType.equalsIgnoreCase("passenger") && !passId.equalsIgnoreCase("")){
			LiveryCabStatics.isLoginPassenger = true;
			LiveryCabStatics.passengerId = passId;
			LiveryCabStatics.userId = passId;
			//Delete data from shared prefrence
			deletePreferences("DRIVER_ID");
			LiveryCabStatics.isLoginDriver = false;
		}
		
		if (LiveryCabStatics.isLoginPassenger){
			((Button) findViewById(R.id.buttonLoginLogoutPassengerMenu))
			.setBackgroundResource(R.drawable.logout_btn_selector);
		}else{
			((Button) findViewById(R.id.buttonLoginLogoutPassengerMenu))
			.setBackgroundResource(R.drawable.login_btn_selector);			
		}

		((Button) findViewById(R.id.buttonBookRidePassangerMenu))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						LiveryCabStatics.passengerMenuOptionId = "BookRide";
						startActivity(new Intent(PassengerMenuActivity.this,
								BookRideActivity.class));
					}
				});

		((Button) findViewById(R.id.buttonMapViewPassangerMenu))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(LiveryCabStaticMethods.isInternetAvailable(PassengerMenuActivity.this)){
							LiveryCabStatics.passengerMenuOptionId = "MapView";
							if (LiveryCabStatics.isLoginPassenger) {
								startActivity(new Intent(
										PassengerMenuActivity.this,
										MapViewActivity.class));
							} else {
								LiveryCabStaticMethods.showAlert(
										PassengerMenuActivity.this, "Login!",
										"Please log in to continue",
										R.drawable.attention, true, false, "Ok",
										new DialogInterface.OnClickListener() {
											@Override
											public void onClick(
													DialogInterface arg0, int arg1) {
												// TODO Auto-generated method stub
												startActivity(new Intent(
														PassengerMenuActivity.this,
														LoginActivity.class));
											}
										}, null, null);
							}
						}
					}
				});

		((Button) findViewById(R.id.buttonProfilePassangerMenu))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(LiveryCabStaticMethods.isInternetAvailable(PassengerMenuActivity.this)){
							LiveryCabStatics.passengerMenuOptionId = "Profile";
							if (LiveryCabStatics.isLoginPassenger)
								startActivity(new Intent(
										PassengerMenuActivity.this,
										PassengerProfileActivity.class));
							else {
								LiveryCabStaticMethods.showAlert(
										PassengerMenuActivity.this, "Login ",
										"Please log in to continue",
										R.drawable.attention, true, false, "Ok",
										new DialogInterface.OnClickListener() {
											@Override
											public void onClick(
													DialogInterface arg0, int arg1) {
												// TODO Auto-generated method stub
												startActivity(new Intent(
														PassengerMenuActivity.this,
														LoginActivity.class));
											}
										}, null, null);
							}
						}						
					}
				});
		
		/**Booked status*/
		((Button) findViewById(R.id.buttonBookedPassangerMenu))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(LiveryCabStaticMethods.isInternetAvailable(PassengerMenuActivity.this)){
					LiveryCabStatics.passengerMenuOptionId = "BOOKED_STATUS";
					if (LiveryCabStatics.isLoginPassenger)
						startActivity(new Intent(
								PassengerMenuActivity.this,
								PassengerBookedRideActivity.class));
					else {
						LiveryCabStaticMethods.showAlert(
								PassengerMenuActivity.this, "Login ",
								"Please log in to continue",
								R.drawable.attention, true, false, "Ok",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(
											DialogInterface arg0, int arg1) {
										// TODO Auto-generated method stub
										startActivity(new Intent(
												PassengerMenuActivity.this,
												LoginActivity.class));
									}
								}, null, null);
					}
				}				
			}
		});

		((Button) findViewById(R.id.buttonNotificationPassangerMenu))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(LiveryCabStaticMethods.isInternetAvailable(PassengerMenuActivity.this)){
							LiveryCabStatics.passengerMenuOptionId = "Notification";
							if (LiveryCabStatics.isLoginPassenger)
								startActivity(new Intent(
										PassengerMenuActivity.this,
										NotificationActivity.class));
							else {
								LiveryCabStaticMethods.showAlert(
										PassengerMenuActivity.this, "Login ",
										"Please log in to continue",
										R.drawable.attention, true, false, "Ok",
										new DialogInterface.OnClickListener() {
											@Override
											public void onClick(
													DialogInterface arg0, int arg1) {
												// TODO Auto-generated method stub
												startActivity(new Intent(
														PassengerMenuActivity.this,
														LoginActivity.class));
											}
										}, null, null);
							}
						}						
					}
				});

		((Button) findViewById(R.id.buttonHistoryPassengerMenu))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if(LiveryCabStaticMethods.isInternetAvailable(PassengerMenuActivity.this)){
							LiveryCabStatics.passengerMenuOptionId = "History";
							if (LiveryCabStatics.isLoginPassenger)
								startActivity(new Intent(
										PassengerMenuActivity.this,
										HistoryActivity.class));
							else {
								LiveryCabStaticMethods.showAlert(
										PassengerMenuActivity.this, "Login!!",
										"Please log in to continue",
										R.drawable.attention, true, false, "Ok",
										new DialogInterface.OnClickListener() {
											@Override
											public void onClick(
													DialogInterface arg0, int arg1) {
												// TODO Auto-generated method stub
												startActivity(new Intent(
														PassengerMenuActivity.this,
														LoginActivity.class));
											}
										}, null, null);
							}
						}						
					}
				});

		((Button) findViewById(R.id.buttonLoginLogoutPassengerMenu))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (LiveryCabStatics.isLoginPassenger){		
							LiveryCabStatics.isLoginPassenger = false;
							LiveryCabStatics.passengerId = null;
							LiveryCabStatics.userId = null;
							
							startActivity(new Intent(
									PassengerMenuActivity.this,
									LiveryCabActivity.class)
									.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
							deletePreferences("USER_TYPE");
							deletePreferences("PASS_ID");
							deletePreferences("DRIVER_ID");
							deletePreferences("TRUE_PASSENGER");
						}else{
							LiveryCabStatics.passengerMenuOptionId = "PassengerMenu";
							Intent mIntent = new Intent(PassengerMenuActivity.this,LoginActivity.class);
							startActivity(mIntent);
						}												
					}
				});
		// This is method for Contact Developer
		((Button) findViewById(R.id.buttonDeveloperPassengerMenu))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						
						 Intent emailIntent = new Intent(
									android.content.Intent.ACTION_SEND);
							emailIntent.setType("plain/text");
							String msg = "";
							emailIntent.putExtra(
									android.content.Intent.EXTRA_SUBJECT,
									"Contact Liverycab Support");
							emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
									new String[]{"help@liverycab.com"});
							emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
									msg);
							startActivity(Intent.createChooser(emailIntent,
									"Send mail..."));
					}
				});
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		String userTypeLogin = getPreferences("LOGIN");
		String userType = getPreferences("USER_TYPE");
		String passId = getPreferences("PASS_ID");
		
		if(userTypeLogin.equalsIgnoreCase("TRUE_PASSENGER") && userType.equalsIgnoreCase("passenger") && !passId.equalsIgnoreCase("")){
			LiveryCabStatics.isLoginPassenger = true;
			LiveryCabStatics.passengerId = passId;
			LiveryCabStatics.userId = passId;
			//Delete data from shared prefrence
			deletePreferences("DRIVER_ID");
			LiveryCabStatics.isLoginDriver = false;
		}
		
		if (LiveryCabStatics.isLoginPassenger){
			((Button) findViewById(R.id.buttonLoginLogoutPassengerMenu))
			.setBackgroundResource(R.drawable.logout_btn_selector);
		}else{
			((Button) findViewById(R.id.buttonLoginLogoutPassengerMenu))
			.setBackgroundResource(R.drawable.login_btn_selector);			
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			LiveryCabStatics.currentLatitude = "" + latitude;
			LiveryCabStatics.currentLongitude = "" + longitude;
			mLocManager.removeUpdates(this);
			Log.v("Location Tag", "Current Latitude:" + latitude
					+ "Current Longitude:" + longitude);
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		Toast.makeText(PassengerMenuActivity.this, "Gps Disabled",
				Toast.LENGTH_SHORT).show();
		Intent intent = new Intent(
				android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		PassengerMenuActivity.this.startActivity(intent);
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}
	
	/** Method used to get Shared Preferences */
	public SharedPreferences getPreferences() 
	{
	    return getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);
	}
	/** Method used to delete Preferences */
	public boolean deletePreferences(String key)
	{
	    SharedPreferences.Editor editor=getPreferences().edit();
	    editor.remove(key).commit();
	    return false;
	}
	
	/**Method used to load Preferences */
	public String getPreferences(String key) 
	{
	    try {
	        SharedPreferences sharedPreferences = getPreferences();
	        String strSavedMemo = sharedPreferences.getString(key, "");
	        return strSavedMemo;
	    } catch (NullPointerException nullPointerException) 
	    {
	        Log.e("Error caused at  LiveryCab loadPreferences method",
	                ">======>" + nullPointerException);
	        return null;
	    }
	}
}