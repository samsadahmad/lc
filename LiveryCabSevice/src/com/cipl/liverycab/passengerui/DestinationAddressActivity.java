package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class DestinationAddressActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.destinationaddress);

		((Button) findViewById(R.id.buttonMenuDestinationAddress))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(DestinationAddressActivity.this,PassengerMenuActivity.class)
				.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			}
		});

		((Button) findViewById(R.id.buttonWhereIamNowDestination))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(DestinationAddressActivity.this,
						DestinationMapViewActivity.class));
			}
		});

		((Button) findViewById(R.id.buttonRecentDestinationAddressDestination))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(DestinationAddressActivity.this,
						RecentDestinationAddressActivity.class));
			}
		});

		((Button) findViewById(R.id.buttonPopulerAddressDestination))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(DestinationAddressActivity.this,
						PopularDestinationAddressActivity.class));
			}
		});
	}

}
