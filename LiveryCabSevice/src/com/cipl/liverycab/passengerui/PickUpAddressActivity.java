package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class PickUpAddressActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pickupaddress);
		
		((Button)this.findViewById(R.id.buttonMenuPickUpAddress))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(PickUpAddressActivity.this,PassengerMenuActivity.class)
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			}
		});
		
		((Button)this.findViewById(R.id.buttonWhereIamNowPickup))
    		.setOnClickListener(new OnClickListener() {
    		@Override
    		public void onClick(View v) {
    			// TODO Auto-generated method stub
    			startActivity(new Intent(PickUpAddressActivity.this, WhereIamNowActivity.class));
    		}
    	});
		
		((Button)this.findViewById(R.id.buttonRecentPickupAddressPickup))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(PickUpAddressActivity.this, RecentPickupAddressActivity.class));
			}
		});
		
		((Button)this.findViewById(R.id.buttonPopularAddPickup))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(PickUpAddressActivity.this,PopularPickupAddressActivity.class));
			}
		});
	}
}
