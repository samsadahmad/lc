package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.cipl.liverycab.parserclasses.PassengerProfilParser;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class PassengerProfileActivity extends Activity {

	private ProgressDialog pd;
	private PassengerProfilParser objParserClass;
	private String message ="";
	private TextView firstNameTextView,lastNameTextView,emailTextView,cellNumberTextView,
			cardTypeTextView,cvvNumberTextView,expirationTextView;
	private CreditCardDataBase ccDB;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.passengerprofile);
		firstNameTextView = (TextView)findViewById(R.id.textViewFirstNameProfile);
		lastNameTextView = (TextView)findViewById(R.id.textViewLastNameProfile);
		emailTextView = (TextView)findViewById(R.id.textViewEmailAddressProfile);
		cellNumberTextView = (TextView)findViewById(R.id.textViewCellPhoneNumberProfile);
		cardTypeTextView = (TextView)findViewById(R.id.textViewTypeProfile);
		cvvNumberTextView = (TextView)findViewById(R.id.textViewCVVProfile);
		expirationTextView = (TextView)findViewById(R.id.textViewExpirationProfile);
		
		((Button)findViewById(R.id.buttonChangePasswordProfile))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(PassengerProfileActivity.this,ChangePasswordActivity.class));
			}
		});
		
		((Button)findViewById(R.id.buttonEditProfileProfile))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(PassengerProfileActivity.this,PassengerEditProfileActivity.class));
			}
		});
		
		((Button)findViewById(R.id.buttonMenuProfile))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(PassengerProfileActivity.this,PassengerMenuActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			}
		});
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(LiveryCabStaticMethods.isInternetAvailable(this)){
			pd = LiveryCabStaticMethods.returnProgressBar(PassengerProfileActivity.this);
			objParserClass = new PassengerProfilParser(UrlStatics.getProfile(LiveryCabStatics.passengerId,LiveryCabStatics.userType), pd);
			objParserClass.start();
			pd .setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface arg0) {
					// TODO Auto-generated method stub
					firstNameTextView.setText(objParserClass.profileDataObj.firstName);
					lastNameTextView.setText(objParserClass.profileDataObj.lastName);
					emailTextView.setText(objParserClass.profileDataObj.email);
					cellNumberTextView.setText(objParserClass.profileDataObj.phone);
					ccDB = new CreditCardDataBase(PassengerProfileActivity.this);
					Cursor c = ccDB.fetchRow(LiveryCabStatics.userId);
					if(c != null){
						c.moveToFirst();
						if(c.getCount() > 0){
							String ccType = c.getString(c.getColumnIndex("ccType"));
							String expMnth = c.getString(c.getColumnIndex("expMonth"));	
							String expYr = c.getString(c.getColumnIndex("expYear"));
							String cvvNum = c.getString(c.getColumnIndex("cvvNumber"));
							cardTypeTextView.setText(ccType);
							cvvNumberTextView.setText(cvvNum);
							expirationTextView.setText(expMnth+"/"+expYr);
						}					
					}
					ccDB.close();
				}
			});	
		}
	}	
}
