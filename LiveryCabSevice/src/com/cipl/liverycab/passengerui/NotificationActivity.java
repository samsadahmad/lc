package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.cipl.liverycab.parserclasses.NotificationListParser;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class NotificationActivity extends Activity {
	private Context mContext; 
	private ListView notificationList;
	private NotificationListParser parserObj;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notification);
		
		mContext = this;
		notificationList = (ListView)findViewById(R.id.listviewNotification);
		parserObj = new NotificationListParser(UrlStatics
				.getNotificationList(LiveryCabStatics.userId));
		parserObj.start();
		
		LiveryCabStaticMethods.returnProgressBar(NotificationActivity.this)
			.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
			// Setting adapter to the list
				if(LiveryCabStatics.notificationList.isEmpty()){
					LiveryCabStaticMethods.showAlert(NotificationActivity.this,"Error Occured!",
						"No record found",R.drawable.attention,true,false,"Ok",null,null,null);
				}else if(LiveryCabStatics.notificationList.get(0).message.equalsIgnoreCase("record_not_found")){
					LiveryCabStaticMethods.showAlert(NotificationActivity.this,"Error Occured!",
						"No record found",R.drawable.attention,true,false,"Ok",null,null,null);
				}else if(LiveryCabStatics.notificationList.get(0).message.equalsIgnoreCase("session_expire")){
					LiveryCabStaticMethods.showAlert(NotificationActivity.this,"Error Occured!",
						"You are not Logged in. Please log in",R.drawable.attention,true,false,"Ok",null,null,null);
				}else{
					notificationList.setAdapter(new EfficientAdapter(mContext));
				}
			}
		});
		notificationList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, final int position,
					long arg3) {
					LiveryCabStaticMethods.showAlert(NotificationActivity.this,"Alert!",
					"Would you like to call? "+LiveryCabStatics.notificationList.get(position).fname+" "+
					LiveryCabStatics.notificationList.get(position).lname,R.drawable.attention,true,true,"Yes",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							try {
								 String number = "tel:" + LiveryCabStatics.notificationList.get(position).phone.trim();
							     Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number)); 
							     startActivity(callIntent);						       
						    } catch (ActivityNotFoundException e) {
						        Log.e("LiveryCab Tag", "Calling Failed in Notification", e);
						    }
						}
				},"No",null);
			}
		});
		
		((Button)findViewById(R.id.buttonMenuNotification))
    		.setOnClickListener(new OnClickListener() {
    		@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(NotificationActivity.this,PassengerMenuActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			}
    	});
	}
	
	class EfficientAdapter extends BaseAdapter 
	{
		private LayoutInflater inflater;

		public EfficientAdapter(Context context) 
		{
			inflater = LayoutInflater.from(context);
		}

		public int getCount() 
		{
			return LiveryCabStatics.notificationList.size();
		}

		public Object getItem(int position) 
		{
			return null;
		}

		public long getItemId(int position) 
		{
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			// A ViewHolder is used to keep references to children views
			ViewHolder holder;
			if (convertView == null) 
			{
				convertView = inflater.inflate(R.layout.notificationcustomcell,null);
				holder = new ViewHolder();
				holder.msgNotification = (TextView) convertView
						.findViewById(R.id.textViewMessageNotification);
				convertView.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) convertView.getTag();
			}
			holder.msgNotification.setText(LiveryCabStatics.notificationList.get(position).fname+" "+
					LiveryCabStatics.notificationList.get(position).lname+" has been confirmed the ride on "+
					LiveryCabStatics.notificationList.get(position).timestamp+"... ETA "+
					LiveryCabStatics.notificationList.get(position).eta);
			return convertView;
		}
	}
	class ViewHolder 
	{
		TextView msgNotification;
	}
}
