package com.cipl.liverycab.passengerui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cipl.liverycab.dataclasses.DriverProfileParser;
import com.cipl.liverycab.driverui.DriverReviewActivity;
import com.cipl.liverycab.parserclasses.GetJsonObjectClass;
import com.cipl.liverycab.parserclasses.RideConfirmParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class PassengerBookedRideDetails extends Activity implements
		android.view.View.OnClickListener {

	private DriverProfileParser objectParser;
	private int position;
	private TextView numberOfReviewText;
	private ProgressDialog pd;
	private RideConfirmParserClass bidConfirmParser;
	private String DRIVER_NAME;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pass_booked_ride_details);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			position = extras.getInt("POSITION");
		}

		setUpView();
	}

	private void setUpView() {
		// TODO Auto-generated method stub
		final TextView driverNameText = (TextView) findViewById(R.id.textviewDriverNameDriverDetail);
		final TextView dlNumberText = (TextView) findViewById(R.id.textviewLicencePlateNumberDriverDetail);
		dlNumberText.setSelected(true);
		final TextView etaTimeText = (TextView) findViewById(R.id.textviewETATimeDriverDetail);
		numberOfReviewText = (TextView) findViewById(R.id.textviewDriverReviewDriverDetail);
		final TextView pickUpAddText = (TextView) findViewById(R.id.textViewPickupaddressDriverDetail);
		final TextView destinationAddText = (TextView) findViewById(R.id.textViewDestinationaddressDriverDetail);
		final TextView datTimeText = (TextView) findViewById(R.id.textViewTimeDateDriverDetail);
		final TextView bidAmtTextWithBg = (TextView) findViewById(R.id.textViewBidAmtWithBGDriverDetail);
		final TextView bidAmtText = (TextView) findViewById(R.id.textviewBidAmountDriverDetail);
		final ImageView driverImage = (ImageView) findViewById(R.id.imageviewDriverImageDriverDetail);
		final ImageView carImage = (ImageView) findViewById(R.id.imageviewCarImageDriverDetail);
		final TextView mtvHandicapedvalue = (TextView) findViewById(R.id.tvHandicapedvalue);

		if (LiveryCabStaticMethods.isInternetAvailable(this)) {
			objectParser = new DriverProfileParser(
					UrlStatics.getProfile(
							LiveryCabStatics.passengerBookedRideList
									.get(position).bidDriverId, "driver"),
					PassengerBookedRideDetails.this);
			objectParser.start();

			LiveryCabStaticMethods.returnProgressBar(
					PassengerBookedRideDetails.this).setOnDismissListener(
					new OnDismissListener() {

						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							DRIVER_NAME = objectParser.dataObject.firstName
									+ " " + objectParser.dataObject.lastName;
							driverNameText.setText(DRIVER_NAME);
							dlNumberText
									.setText(objectParser.dataObject.dlNumber);
							etaTimeText
									.setText(LiveryCabStatics.passengerBookedRideList
											.get(position).bidEta + " minute");
							numberOfReviewText.setText("("
									+ objectParser.dataObject.numberOfReviews
									+ ")");
							pickUpAddText
									.setText(LiveryCabStatics.passengerBookedRideList
											.get(position).pickupAddress);
							destinationAddText
									.setText(LiveryCabStatics.passengerBookedRideList
											.get(position).pickupDestination);
							datTimeText
									.setText(LiveryCabStatics.passengerBookedRideList
											.get(position).dateTime);
							mtvHandicapedvalue
									.setText(objectParser.dataObject.handicapped
											.equalsIgnoreCase("Y") ? "Yes"
											: "No");
							bidAmtTextWithBg.setText("$"
									+ LiveryCabStatics.passengerBookedRideList
											.get(position).bidAmount);
							bidAmtText.setText("$"
									+ LiveryCabStatics.passengerBookedRideList
											.get(position).bidAmount);
							if (objectParser.dataObject.driverImage != null)
								;
							driverImage
									.setImageDrawable(objectParser.dataObject.driverImage);
							if (objectParser.dataObject.carImage != null)
								;
							carImage.setImageDrawable(objectParser.dataObject.carImage);
						}
					});
		}

		/** Click on review text */
		numberOfReviewText.setOnClickListener(this);

		/** Click on Call Button */
		Button mbtnCallToDriver = (Button) findViewById(R.id.btnCallToDriver);
		mbtnCallToDriver.setOnClickListener(this);

		/** Click on confirm ride button */
		Button mbuttonConfirmDriverDetail = (Button) findViewById(R.id.buttonConfirmDriverDetail);
		mbuttonConfirmDriverDetail.setOnClickListener(this);

		Button mbuttonDeleteBid = (Button) findViewById(R.id.buttonDeleteBid);
		mbuttonDeleteBid.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.buttonConfirmDriverDetail:
			btnConfirm();
			break;

		case R.id.btnCallToDriver:
			callToDriver();
			break;

		case R.id.textviewDriverReviewDriverDetail:
			callReview();
			break;

		case R.id.buttonDeleteBid:
			deletPick();
			break;
		}
	}

	private void deletPick() {
		// TODO Auto-generated method stub
		new DeletePickUp().execute();
	}

	private void callReview() {
		// TODO Auto-generated method stub
		String did = LiveryCabStatics.passengerBookedRideList.get(position).bidDriverId;
		if (objectParser.dataObject.numberOfReviews.equalsIgnoreCase("0")) {
			LiveryCabStaticMethods.showAlert(PassengerBookedRideDetails.this,
					"Review", "There is no current review for " + DRIVER_NAME,
					R.drawable.attention, true, false, "Ok",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
						}
					}, null, null);
		} else {
			Intent i = new Intent(PassengerBookedRideDetails.this,
					DriverReviewActivity.class);
			i.putExtra("DriverId", did);
			i.putExtra("DriverName", DRIVER_NAME);
			startActivity(i);
		}
	}

	private void callToDriver() {
		// TODO Auto-generated method stub
		if (objectParser.dataObject.phone.equalsIgnoreCase("")) {
			Toast.makeText(PassengerBookedRideDetails.this,
					"Invalid phone number", 0).show();
		} else {
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri
					.parse("tel:" + objectParser.dataObject.phone));
			startActivity(callIntent);
		}
	}

	private void btnConfirm() {
		// TODO Auto-generated method stub
		if (LiveryCabStaticMethods
				.isInternetAvailable(PassengerBookedRideDetails.this)) {
			pd = LiveryCabStaticMethods
					.returnProgressBar(PassengerBookedRideDetails.this);
			try {
				bidConfirmParser = new RideConfirmParserClass(
						UrlStatics.getBidConfirmUrl(
								LiveryCabStatics.passengerBookedRideList
										.get(position).bidId,
								LiveryCabStatics.passengerId,
								LiveryCabStatics.passengerBookedRideList
										.get(position).bidDriverId,
								objectParser.dataObject.firstName,
								objectParser.dataObject.lastName, URLEncoder
										.encode("", "utf-8"), "11", "12",
								"539", "1234567890123456", "10"), pd);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bidConfirmParser.start();

			pd.setOnDismissListener(new OnDismissListener() {
				private String message;

				@Override
				public void onDismiss(DialogInterface dialog) {
					if (bidConfirmParser.actualDismiss) {
						if (bidConfirmParser.loginDataObj != null)
							message = bidConfirmParser.loginDataObj.message;
						if (message
								.equalsIgnoreCase("DoDirect_Payment_Capture_Successfully")) {
							LiveryCabStaticMethods
									.showAlert(
											PassengerBookedRideDetails.this,
											"Success",
											"Your ride is confirmed and payment has been processed.  After ride is complete you will be mailed a reciept",
											R.drawable.success,
											true,
											false,
											"Ok",
											new DialogInterface.OnClickListener() {
												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {
													startActivity(new Intent(
															PassengerBookedRideDetails.this,
															PassengerBookedRideActivity.class));
													finish();
												}
											}, null, null);
						} else if (message
								.equalsIgnoreCase("DoDirect_Payment_Capture_Failed")) {
							LiveryCabStaticMethods.showAlert(
									PassengerBookedRideDetails.this,
									"Error Occured", "payPal Error",
									R.drawable.attention, true, false, "Ok",
									null, null, null);
						} else if (message
								.equalsIgnoreCase("credit_card_not_valid")) {
							LiveryCabStaticMethods.showAlert(
									PassengerBookedRideDetails.this,
									"Error Occured",
									"Some error in credit card details",
									R.drawable.attention, true, false, "Ok",
									null, null, null);
						} else {
							LiveryCabStaticMethods.showAlert(
									PassengerBookedRideDetails.this,
									"Error Occured", "Please try again",
									R.drawable.attention, true, false, "Ok",
									null, null, null);
						}
					}
				}
			});
		}
	}

	class DeletePickUp extends AsyncTask<Void, Void, String> {
		private String deletePickUpMsg;

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String uslDeletePickUp = UrlStatics
					.deletePickupAndBid(LiveryCabStatics.passengerBookedRideList
							.get(position).pickupId);
			try {
				JSONObject jObject = GetJsonObjectClass
						.getJSONObjectfromURL(uslDeletePickUp);
				if (jObject != null) {
					deletePickUpMsg = jObject.get("Message").toString();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return deletePickUpMsg;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = LiveryCabStaticMethods
					.returnProgressBar(PassengerBookedRideDetails.this);
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			if (pd.isShowing()) {
				pd.dismiss();
			}
			if (result.equalsIgnoreCase("ride_delete")) {
				Toast.makeText(PassengerBookedRideDetails.this,
						"Your ride has been deleted sucessfully", 0).show();
				startActivity(new Intent(PassengerBookedRideDetails.this,
						PassengerBookedRideActivity.class));
				finish();
			}
			super.onPostExecute(result);
		}
	}
}
