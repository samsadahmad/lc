package com.cipl.liverycab.passengerui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.parserclasses.PassengerProfilParser;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class PassengerEditProfileActivity extends Activity implements OnClickListener{

	private ProgressDialog pd;
	private PassengerProfilParser objProfileParserClass;
	private ForgotPasswordParserClass objParserClass;
	private String message = "";

	private String cardTypeArray[] = { "Visa", "Master Card",
			"American Express" };
	private String expirationMonthArray[] = { "1", "2", "3", "4", "5", "6","7", "8", "9", "10", "11", "12" };
	private int year = Calendar.getInstance().get(Calendar.YEAR);
	private String expirationYearArray[] = { ""+year, ""+(year+1), ""+(year+2), ""+(year+3),
			""+(year+4), ""+(year+5),""+(year+6),""+(year+7),""+(year+8) };

	private EditText fNameEditText, lNameEditText, cellNumberEditText,cardNumberEditText, cvvNumberEditText;
	private String selectedItem, expirationYear, expirationMonth,creditCardType, creditCardNumber, cvvNumber;
	private TextView expYr, expMnth, cardType;
	private CreditCardDataBase ccDB;
	private String selectedItemCT,selectedItemCM,selectedItemCY;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.passengereditprofile);

		fNameEditText = (EditText) findViewById(R.id.edittextFirstNamePassengerEditProfile);
		lNameEditText = (EditText) findViewById(R.id.edittextLastNamePassengerEditProfile);
		cellNumberEditText = (EditText) findViewById(R.id.edittextPhoneNumberPassengerEditProfile);
		cardNumberEditText = (EditText) findViewById(R.id.editTextCardNumberPassengerEditProfile);
		cvvNumberEditText = (EditText) findViewById(R.id.editTextCVVNumberPassengerEditProfile);
		
		expYr = (TextView) findViewById(R.id.textviewExpirationYearPassengerEditProfile);
		expYr.setOnClickListener(this);
		
		expMnth = (TextView) findViewById(R.id.textviewExpirationMonthPassengerEditProfile);
		expMnth.setOnClickListener(this);
		
		cardType = (TextView) findViewById(R.id.textviewCardTypePassengerEditProfile);
		cardType.setOnClickListener(this);

		pd = LiveryCabStaticMethods
				.returnProgressBar(PassengerEditProfileActivity.this);
		objProfileParserClass = new PassengerProfilParser(
				UrlStatics.getProfile(LiveryCabStatics.passengerId,
						LiveryCabStatics.userType), pd);
		objProfileParserClass.start();
		pd.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface arg0) {
				// TODO Auto-generated method stub
				fNameEditText.setText(objProfileParserClass.profileDataObj.firstName);
				lNameEditText.setText(objProfileParserClass.profileDataObj.lastName);
				cellNumberEditText.setText(objProfileParserClass.profileDataObj.phone);
				ccDB = new CreditCardDataBase(PassengerEditProfileActivity.this);
				Cursor c = ccDB.fetchRow(LiveryCabStatics.userId);
				
				if (c != null) {
					c.moveToFirst();
					if (c.getCount() > 0) {
						creditCardType = c.getString(c.getColumnIndex("ccType"));
						expirationMonth = c.getString(c.getColumnIndex("expMonth"));
						expirationYear = c.getString(c.getColumnIndex("expYear"));
						cvvNumber = c.getString(c.getColumnIndex("cvvNumber"));
						creditCardNumber = c.getString(c.getColumnIndex("ccNumber"));
						cardType.setText(creditCardType);
						expYr.setText(expirationYear);
						expMnth.setText(expirationMonth);
						cardNumberEditText.setText(creditCardNumber);
						cvvNumberEditText.setText(cvvNumber);
					}
					ccDB.close();
				}
			}
		});
		((Button) findViewById(R.id.buttonSavePassengereditProfile))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String fname = fNameEditText.getText().toString();
						String lname = lNameEditText.getText().toString();
						String phone = cellNumberEditText.getText().toString();
						creditCardNumber = cardNumberEditText.getText().toString();
						cvvNumber = cvvNumberEditText.getText().toString();
						if (fname.equalsIgnoreCase("")){
							LiveryCabStaticMethods.showAlert(
									PassengerEditProfileActivity.this,
									"Error Occured!", "Please enter first name",
									R.drawable.attention, true, false, "Ok",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface arg0, int arg1) {
											// TODO Auto-generated method stub
											fNameEditText.requestFocus();
										}
									}, null, null);
						}else if (lname.equalsIgnoreCase("")){
							LiveryCabStaticMethods.showAlert(
									PassengerEditProfileActivity.this,
									"Error Occured!", "Please enter last name",
									R.drawable.attention, true, false, "Ok",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface arg0, int arg1) {
											// TODO Auto-generated method stub
											lNameEditText.requestFocus();
										}
									}, null, null);
						}else if (phone.equalsIgnoreCase("")){
							LiveryCabStaticMethods.showAlert(
									PassengerEditProfileActivity.this,
									"Error Occured!", "Please enter phone number",
									R.drawable.attention, true, false, "Ok",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface arg0, int arg1) {
											// TODO Auto-generated method stub
											cellNumberEditText
											.requestFocus();
										}
									}, null, null);
						}else if(creditCardType != null){
							if(expirationMonth == null || expirationYear == null || creditCardNumber == null || cvvNumber == null) {
								LiveryCabStaticMethods.showAlert(PassengerEditProfileActivity.this,
									"Error Occured!","Credit card information is required in the system before any liverycab ride purchase",
									R.drawable.attention, true,false, "Ok", null, null,null);
							} else if(creditCardNumber.length() < 16	|| !(LiveryCabStaticMethods.isCardNumberValid(creditCardNumber))) {
								LiveryCabStaticMethods.showAlert(PassengerEditProfileActivity.this,
										"Error Occured!","Please enter valid card number",R.drawable.attention, true, false,
										"Ok", null, null, null);
							} else {
								CreditCardDataBase ccDB = new CreditCardDataBase(PassengerEditProfileActivity.this);
								ccDB.deleteTable();
								ccDB.insertRow(LiveryCabStatics.userId,creditCardType,expirationMonth,expirationYear,creditCardNumber,cvvNumber);
								ccDB.close();
								//Update user information remotely
								pd = LiveryCabStaticMethods.returnProgressBar(PassengerEditProfileActivity.this);
								try {
									objParserClass = new ForgotPasswordParserClass(
										UrlStatics.getPassengerEditUrl(
										LiveryCabStatics.userId,URLEncoder.encode(fname,"utf-8"),
										URLEncoder.encode(lname,"utf-8"),
										URLEncoder.encode(phone,"utf-8")),pd);
								} catch (UnsupportedEncodingException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								objParserClass.start();
								pd.setOnDismissListener(new OnDismissListener() {
									@Override
									public void onDismiss(
											DialogInterface dialog) {
										// TODO Auto-generated method stub
										if (objParserClass.actualDismiss) {
											if (objParserClass.loginDataObj != null)
												message = objParserClass.loginDataObj.message;
											if (message.equalsIgnoreCase("profile_sucessfully_updated")) {
												LiveryCabStaticMethods.showAlert(
																PassengerEditProfileActivity.this,
																"Success",
																"Profile successfully updated",
																R.drawable.success,true,false,
																"Ok",new DialogInterface.OnClickListener() {
																	@Override
																	public void onClick(DialogInterface arg0,int arg1) {
																		finish();
																	}
																}, null,null);
											} else if (message.equalsIgnoreCase("session_expire")) {
												LiveryCabStaticMethods.showAlert(PassengerEditProfileActivity.this,
																"Error Occured!","Try again",R.drawable.attention,
																true,false,"Ok", null,null, null);
											}
										}
									}
								});								
							}
						}else {
								pd = LiveryCabStaticMethods.returnProgressBar(PassengerEditProfileActivity.this);
								try {
									objParserClass = new ForgotPasswordParserClass(
										UrlStatics.getPassengerEditUrl(
										LiveryCabStatics.userId,URLEncoder.encode(fname,"utf-8"),
										URLEncoder.encode(lname,"utf-8"),
										URLEncoder.encode(phone,"utf-8")),pd);
								} catch (UnsupportedEncodingException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								objParserClass.start();
								pd.setOnDismissListener(new OnDismissListener() {
									@Override
									public void onDismiss(
											DialogInterface dialog) {
										// TODO Auto-generated method stub
										if (objParserClass.actualDismiss) {
											if (objParserClass.loginDataObj != null)
												message = objParserClass.loginDataObj.message;
											if (message.equalsIgnoreCase("profile_sucessfully_updated")) {
												LiveryCabStaticMethods.showAlert(
																PassengerEditProfileActivity.this,
																"Success",
																"Profile successfully updated",
																R.drawable.success,true,false,
																"Ok",new DialogInterface.OnClickListener() {
																	@Override
																	public void onClick(DialogInterface arg0,int arg1) {
																		finish();
																	}
																}, null,null);
											} else if (message.equalsIgnoreCase("session_expire")) {
												LiveryCabStaticMethods.showAlert(PassengerEditProfileActivity.this,
																"Error Occured!","Try again",R.drawable.attention,
																true,false,"Ok", null,null, null);
											}
										}
									}
								});								
							}
						
					}
					
				});			
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.textviewCardTypePassengerEditProfile:
			showCreditCardType();
			break;		
			
		case R.id.textviewExpirationMonthPassengerEditProfile:
			showExpiryMonth();
			break; 
			
		case R.id.textviewExpirationYearPassengerEditProfile:
			showExpiryYear();
			break;
		}
	}

	private void showExpiryYear() {
		// TODO Auto-generated method stub
		AlertDialog.Builder expiryYearAlert = new AlertDialog.Builder(PassengerEditProfileActivity.this);
		expiryYearAlert.setTitle("Expiration Year");
		int arrayPostion = getPaymentDetails(expirationYear,expirationYearArray);
		expiryYearAlert.setSingleChoiceItems(expirationYearArray, arrayPostion,new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int item) {
								selectedItemCY = expirationYearArray[item];
							}
						})
			.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int whichButton) {
							
							if(selectedItemCY==null && expirationYear==null){
								expYr.setText("Expiration Year");
							}else if(selectedItemCY==null && expirationYear!=null){								
								expYr.setText(expirationYear);								
							}else{
								expirationYear = selectedItemCY;
								expYr.setText(expirationYear);
							}
							//LiveryCabStatics.ccdata.expirationYear = expirationYear;
						}
					});
		expiryYearAlert.show();
	}

	private void showExpiryMonth() {
		// TODO Auto-generated method stub
		AlertDialog.Builder expiryMonthAlert = new AlertDialog.Builder(PassengerEditProfileActivity.this);
		expiryMonthAlert.setTitle("Expiration Month");
		int arrayPostion = getPaymentDetails(expirationMonth,expirationMonthArray);
		
		expiryMonthAlert.setSingleChoiceItems(expirationMonthArray, arrayPostion,
				new DialogInterface.OnClickListener() {
			public void onClick(
					DialogInterface dialog,
					int item) {
				selectedItemCM = expirationMonthArray[item];
			}
		})
		.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int whichButton) {
							
							if(selectedItemCM==null && expirationMonth==null){
								expMnth.setText("Expiration Month");
							}else if(selectedItemCM==null && expirationMonth!=null){								
								expMnth.setText(expirationMonth);								
							}else{
								expirationMonth = selectedItemCM;
								expMnth.setText(expirationMonth);
							}
							//LiveryCabStatics.ccdata.expirationMonth = expirationMonth;
						}
					});
		expiryMonthAlert.show();
	}

	private void showCreditCardType() {
		// TODO Auto-generated method stub

		AlertDialog.Builder creditAlert = new AlertDialog.Builder(PassengerEditProfileActivity.this);
		creditAlert.setTitle("Credit Card Type");
		int arrayPostion = getPaymentDetails(creditCardType,cardTypeArray);	
		
		creditAlert.setSingleChoiceItems(cardTypeArray, arrayPostion,new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int item) {
								selectedItemCT = cardTypeArray[item];
							}
						})
			.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int whichButton) {
							
							if(selectedItemCT==null && creditCardType == null){
								cardType.setText("Credit Card Type");
							}else if(selectedItemCT==null && creditCardType!=null){								
								cardType.setText(creditCardType);								
							}else{
								creditCardType = selectedItemCT;
								cardType.setText(creditCardType);
							}
							//LiveryCabStatics.ccdata.cardType = creditCardType;
						}
					});
		creditAlert.show();
	}
	
	public int getPaymentDetails(String cardType,String[] cardInfoArray){
		int itempostition = 0;
		
		for(int i=0;i<cardInfoArray.length;i++){
			if(cardInfoArray[i].equalsIgnoreCase(cardType)){
				itempostition = i;
				break;
			}
		}		
		return itempostition;
	}
}