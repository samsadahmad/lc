package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class QuickActionMapView extends PopupWindow implements KeyEvent.Callback {

	private final Context mContext;
	private final LayoutInflater mInflater;
	private final WindowManager mWindowManager;
	private Intent intent;
	View contentView;
	private int mScreenWidth;
	private int mScreenHeight;
	private int mShadowHoriz;
	private int mShadowVert;
	private int mShadowTouch;
	private ImageView fwdArrow;
	private ImageView mArrowUp;
	private ImageView mArrowDown;
	private ViewGroup mTrack;
	private Animation mTrackAnim;
	private RelativeLayout actionlayout;
	
	private View mPView;
	private Rect mAnchor;
	
	
	public QuickActionMapView(Context context, View pView, Rect rect) {
		super(context);
		mPView = pView;
		mAnchor = rect;
		mContext = context;
		mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		mInflater = ((Activity)mContext).getLayoutInflater();
		
		setContentView(R.layout.quickactionmapview);
		
		mScreenWidth = mWindowManager.getDefaultDisplay().getWidth();
		mScreenHeight = mWindowManager.getDefaultDisplay().getHeight();
		
		setWindowLayoutMode(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		setWidth(mScreenWidth + mShadowHoriz + mShadowHoriz);
		setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		setBackgroundDrawable(new ColorDrawable(0));
		
		mArrowUp = (ImageView) contentView.findViewById(R.id.arrow_up_mapview);
		mArrowDown = (ImageView) contentView.findViewById(R.id.arrow_down_mapview);
		fwdArrow = (ImageView)contentView.findViewById(R.id.quickaction_fwdArrow_mapview);
		
		setFocusable(true);
		setTouchable(true);
		setOutsideTouchable(true);
		mTrackAnim = AnimationUtils.loadAnimation(mContext, R.anim.quickaction);
		mTrackAnim.setInterpolator(new Interpolator() {
			public float getInterpolation(float t) {
				// Pushes past the target area, then snaps back into place.
				// Equation for graphing: 1.2-((x*1.6)-1.1)^2
				final float inner = (t * 1.55f) - 1.1f;
				return 1.2f - inner * inner;
			}
		});	
		
		fwdArrow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(intent);
			}
		});
	}
	
	private void setContentView(int resId) {
		contentView = mInflater.inflate(resId, null);
		super.setContentView(contentView);
	}
	public void setIntent(Intent intent)
	{
		this.intent = intent;
	}
	private void startActivity(Intent i)
	{
		if(i!=null)
			mContext.startActivity(this.intent);
	}
	public View getHeaderView() {
		return contentView.findViewById(R.id.quickaction_header_mapview);
	}
	
	public void setTitle(CharSequence title) {
		contentView.findViewById(R.id.quickaction_content_mapview).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.quickaction_primarytext_mapview).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.quickaction_primarytext_mapview)).setText(title);
	}
	
	public void setTitle(int resid) {
		setTitle(mContext.getResources().getString(resid));
	}
	
	public void setText(CharSequence text) {
		contentView.findViewById(R.id.quickaction_content_mapview).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.quickaction_secondarytext_value_mapview).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.quickaction_secondarytext_value_mapview)).setText(text);
	}
	public void setMiles(CharSequence text) {
		contentView.findViewById(R.id.relativelayoutMilesBgQAMapview).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.textviewMilesQAMapview).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.textviewMilesQAMapview)).setText(text);
	}
	
	public void setBidAmount(CharSequence text) {
		contentView.findViewById(R.id.relativelayoutBidAmtBgQAMapview).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.textviewBidQAMapview).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.textviewBidQAMapview)).setText(text);
	}
	
	public void setText(int resid) {
		setText(mContext.getResources().getString(resid));
	}
	
	public void setIcon(Bitmap bm) {
		contentView.findViewById(R.id.quickaction_fwdArrow_mapview).setVisibility(View.VISIBLE);
		final ImageView vImage = (ImageView) contentView.findViewById(R.id.quickaction_fwdArrow_mapview);
		vImage.setImageBitmap(bm);
	}
	
	public void setIcon(Drawable d) {
		contentView.findViewById(R.id.quickaction_fwdArrow_mapview).setVisibility(View.VISIBLE);
		final ImageView vImage = (ImageView) contentView.findViewById(R.id.quickaction_fwdArrow_mapview);
		vImage.setImageDrawable(d);
	}
	
	public void setIcon(int resid) {
		setIcon(mContext.getResources().getDrawable(resid));
	}
	
	
	
	private void showArrow(int whichArrow, int requestedX) {
		final View showArrow = (whichArrow == R.id.arrow_up_mapview) ? mArrowUp : mArrowDown;
		final View hideArrow = (whichArrow == R.id.arrow_up_mapview) ? mArrowDown : mArrowUp;

		// Dirty hack to get width, might cause memory leak
		final int arrowWidth = mContext.getResources().getDrawable(R.drawable.quickaction_arrow_up).getIntrinsicWidth();

		showArrow.setVisibility(View.VISIBLE);
		ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams)showArrow.getLayoutParams();
		param.leftMargin = requestedX - arrowWidth / 2;

		hideArrow.setVisibility(View.INVISIBLE);
	}
	
	private void onBackPressed() {
			dismiss();
	}
	
	public void show() {
		show(mAnchor.centerX());
	}
	
	public void show(int requestedX) {
		super.showAtLocation(mPView, Gravity.NO_GRAVITY, 0, 0);
		
		// Calculate properly to position the popup the correctly based on height of popup
		if (isShowing()) {
			int x, y, windowAnimations;
			this.getContentView().measure(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			final int blockHeight = this.getContentView().getMeasuredHeight();
			
			x = -mShadowHoriz;
			if (mAnchor.top > blockHeight) {
				showArrow(R.id.arrow_down_mapview, requestedX);
				y = mAnchor.top - blockHeight;
				windowAnimations = R.style.QuickActionAboveAnimation;
			} else {
				showArrow(R.id.arrow_up_mapview, requestedX);
				y = mAnchor.bottom;
				windowAnimations = R.style.QuickActionBelowAnimation;
			}
			setAnimationStyle(windowAnimations);
			this.update(x, y, -1, -1);
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBackPressed();
			return true;
		}
		return false;
	}
	
}
