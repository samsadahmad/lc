/**
 * 
 */
package com.cipl.liverycab.passengerui;

import org.json.JSONException;
import org.json.JSONObject;

import android.R.string;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.cipl.liverycab.dataclasses.LoginDataClass;
import com.cipl.liverycab.driverui.AssignedJobListActivity;
import com.cipl.liverycab.parserclasses.GetJsonObjectClass;
import com.cipl.liverycab.parserclasses.LoginParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;
import com.google.android.gcm.GCMBaseIntentService;

/**
 * Class is used for registering GCM
 * @author {Samsad Ahmad}
 *
 */
public class GCMIntentService  extends GCMBaseIntentService
{

    /**
     * @param senderId
     */
	
    public GCMIntentService() 
    {
    	super("626040078258");
    }

    /* (non-Javadoc)
     * @see com.google.android.gcm.GCMBaseIntentService#onError(android.content.Context, java.lang.String)
     */
    @Override
    protected void onError(Context arg0, String arg1) 
    {
	// TODO Auto-generated method stub
	Log.d(TAG, "Error: " + "sError");
    }

    /*
     *@see Used when message comes form GCM server. Call your notification class here 
     */
     
    @Override
    protected void onMessage(Context context, Intent arg1) 
    {
	// TODO Auto-generated method stub
    	long when = System.currentTimeMillis();
    	String refine_msg = null,lname = null,fname = null,notificationType = null,driverId = null,bidId = null,pickupId = null;
    	Bundle extras = arg1.getExtras();
    	
     	String msg =extras != null ? extras.getString("msg"):"";
    	try {
			JSONObject jsonObject = new JSONObject(msg);
			JSONObject apsJson = jsonObject.getJSONObject("aps");
			refine_msg = apsJson.getString("alert");			
			JSONObject appDetailsJson = jsonObject.getJSONObject("app_details");
			if (appDetailsJson != null) {							
				notificationType = appDetailsJson.getString("NotificationType");
				if(notificationType.equalsIgnoreCase("pass_profile")){
					fname = appDetailsJson.getString("fname");				
					lname = appDetailsJson.getString("lname");	
					driverId = appDetailsJson.getString("driverId");					
					bidId = appDetailsJson.getString("bidId");
				}else if(notificationType.equalsIgnoreCase("notifyonly")){
					
				}else{
					pickupId = appDetailsJson.getString("pickupId");
				}
			}		
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		int icon = R.drawable.icon;    
	    CharSequence tickerText = "Notifications Received";
	    Notification notification = new Notification(icon, tickerText, when);
	    
	    Context context1 = context;
        //*****************
	    final int CUSTOM_VIEW_ID = 1;
	    
	    RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_notification_layout);
	    contentView.setImageViewResource(R.id.image, R.drawable.icon);
	    contentView.setTextViewText(R.id.text, ""+refine_msg);
        notification.contentView = contentView;
	    notification.defaults=Notification.FLAG_ONLY_ALERT_ONCE+Notification.FLAG_AUTO_CANCEL;
	    
	    Intent notificationIntent = null;
	    
	    notification.flags |= Notification.FLAG_AUTO_CANCEL;
	    notification.defaults = Notification.DEFAULT_ALL;
	    
	    LiveryCabStatics.isNotificationReceive = true;
		
	    if(notificationType != null && notificationType.equals("pass_profile")){
	    	if(LiveryCabStatics.isLoginPassenger)
	    		notificationIntent = new Intent(this,DriverDetailActivity.class);
	    	else{
	    		savePreferences("USER_TYPE","passenger");
	    		LiveryCabStatics.userType  = "passenger";	    		
	    		notificationIntent = new Intent(this,LoginActivity.class);
	    	}
	    		
	    }else if(notificationType != null && notificationType.equals("driver_profile")){
	    	if(LiveryCabStatics.isLoginDriver)
	    		notificationIntent = new Intent(this,AssignedJobListActivity.class);
	    	else{
	    		savePreferences("USER_TYPE","driver");
	    		LiveryCabStatics.userType  = "driver";	    	
	    		notificationIntent = new Intent(this,LoginActivity.class);
	    	}
	    		
	    }else if(notificationType != null && notificationType.equals("notifyonly")){
	    	notificationIntent = new Intent();
	    }
	    else{
	    	if(LiveryCabStatics.isLoginPassenger){
	    		notificationIntent = new Intent(this, NotificationActivity.class);
	    	}else{
	    		notificationIntent = new Intent(this,LoginActivity.class);
	    	}	    	  
	    }
	    notificationIntent.putExtra("BidID",bidId);
	    notificationIntent.putExtra("pickupid",pickupId);
	    notificationIntent.putExtra("driverID",driverId);
	    notificationIntent.putExtra("fName",fname);
	    notificationIntent.putExtra("lName",lname);
	    notificationIntent.putExtra("Tag", "C2DMBaseReceiver");
	    PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
	    notification.contentIntent = contentIntent;
	    mNotificationManager.notify(CUSTOM_VIEW_ID, notification);	    
    }

    /* (non-Javadoc)
     * @see com.google.android.gcm.GCMBaseIntentService#onRegistered(android.content.Context, java.lang.String)
     */
    @Override
    protected void onRegistered(Context context, String arg1) {
	 // TODO Auto-generated method stub
    	sendBroadcast(new Intent(LoginActivity.ACTION_UPDATE));
    }

    /* (non-Javadoc)
     * @see com.google.android.gcm.GCMBaseIntentService#onUnregistered(android.content.Context, java.lang.String)
     */
    @Override
    protected void onUnregistered(Context arg0, String arg1) {
	// TODO Auto-generated method stub
	
    }
    
    class Dologin extends AsyncTask<Void, Void, Void>{
    	
    	private String deviceToken;
		public Dologin(String deviceToken) {
			// TODO Auto-generated constructor stub
			this.deviceToken = deviceToken;
		}
    	@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			JSONObject jObject;
			LoginDataClass loginDataObj = new LoginDataClass();
			//JSON object get collected here
			//UrlStatics
			String url = UrlStatics.getLoginUrl("sam@gmail.com","qwe",LiveryCabStatics.DeviceID,deviceToken,
			LiveryCabStatics.deviceType, LiveryCabStatics.userType);
			
			System.out.println("url=="+url);
			jObject = GetJsonObjectClass.getJSONObjectfromURL(url);
			
			try {
				//getting values from JSON object
				loginDataObj.message =jObject.getString("Message");
				loginDataObj.firstName =jObject.getString("fname");
				loginDataObj.lastName =jObject.getString("lname");
				if(LiveryCabStatics.isDriver)
				{
					if(jObject.getString("driverId") != null && !(jObject.getString("driverId").equalsIgnoreCase("")))
						loginDataObj.userID = jObject.getString("driverId");
				}
				if(LiveryCabStatics.isPassenger){
					if(jObject.getString("passengerId") != null && !(jObject.getString("passengerId").equalsIgnoreCase("")))
						loginDataObj.userID = jObject.getString("passengerId");
				}
				if(jObject.getString("Status")!= null)
					loginDataObj.status = jObject.getString("Status");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				
			}
			return null;
		}
    	
    	@Override
    	protected void onPostExecute(Void result) {
    		// TODO Auto-generated method stub
    		super.onPostExecute(result);
    	}    	
    }
    
    /** Method used to get Shared Preferences */
	public SharedPreferences getPreferences() 
	{
	    return getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);
	}
	
	/** Method used to save Preferences */
	public void savePreferences(String key, String value) 
	{
	    SharedPreferences sharedPreferences = getPreferences();
	    SharedPreferences.Editor editor = sharedPreferences.edit();
	    editor.putString(key, value);
	    editor.commit();
	}
}
