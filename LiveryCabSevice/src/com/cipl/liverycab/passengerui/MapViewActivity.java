package com.cipl.liverycab.passengerui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import com.cipl.liverycab.dataclasses.BidListDataClass;
import com.cipl.liverycab.parserclasses.BidListParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class MapViewActivity extends MapActivity {

	private MapController mapController;
	private BidListParserClass parserObject;
	private MapView mapView;
	private SitesOverlay mapOverlay;
	private String addName;
	private GeoPoint p;
	private MyLocationOverlay myOverlay;
	private Drawable marker;
	private ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.mapview);
		
		//marker to show pin on map
		marker = getResources().getDrawable(R.drawable.bluebgforpin);
		mapView = (MapView) findViewById(R.id.mapviewMapView);
		LinearLayout zoomLayout = (LinearLayout) findViewById(R.id.linearLayoutzoomMapView);
		View zoomView = mapView.getZoomControls();
		
		//this will add a zoom control to mapview
		zoomLayout.addView(zoomView, new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		
		mapView.displayZoomControls(true);
		mapController = mapView.getController();
		mapController.setZoom(14);
		
		//mapView.invalidate();
		((Button) findViewById(R.id.buttonMenuMapView))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MapViewActivity.this,PassengerMenuActivity.class)
								.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				}
		});

		((Button) findViewById(R.id.buttonListViewMapView))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MapViewActivity.this,ListViewActivity.class));
			}
		});
		
		((Button)findViewById(R.id.buttonRefreshMapView))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(LiveryCabStaticMethods.isInternetAvailable(MapViewActivity.this)){
					getBidListFromServer();
				}
			}
		});
	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(LiveryCabStaticMethods.isInternetAvailable(MapViewActivity.this)){
			getBidListFromServer();
		}		
	}
	
	private void getBidListFromServer() {
		// TODO Auto-generated method stub
		mProgressDialog = ProgressDialog.show(MapViewActivity.this, "","Loading. Please wait...", true, false);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
		parserObject =  new BidListParserClass(UrlStatics.getPassengerBidListUrl(LiveryCabStatics.userId),MapViewActivity.this,mProgressDialog);
		parserObject.start();
		
		//bid list fetching is done		
		mProgressDialog.setOnDismissListener(new OnDismissListener(){
			@Override
			public void onDismiss(DialogInterface dialog){
				if(LiveryCabStatics.bidList.isEmpty()){
					LiveryCabStaticMethods.showAlert(MapViewActivity.this, 
								"No Records!!","Please wait for drivers to bid ride",R.drawable.attention,true,false,"Ok",null,null,null);
				}else if(LiveryCabStatics.bidList.get(0).message.equalsIgnoreCase("Record_not_found")){
					LiveryCabStaticMethods.showAlert(MapViewActivity.this, 
								"No Records!!","Please wait for drivers to bid ride",R.drawable.attention,true,false,"Ok",null,null,null);
				}else{
					// adding overlay items on the mapview
					mapOverlay = new SitesOverlay(marker);
					List<Overlay> listOfOverlays = mapView.getOverlays();
					listOfOverlays.clear();
					listOfOverlays.add(mapOverlay);
				}
			}
		});		
	}


	private GeoPoint getPoint(double lat, double lon) {
		return (new GeoPoint((int) (lat * 1000000.0), (int) (lon * 1000000.0)));
	}

	private class SitesOverlay extends ItemizedOverlay<OverlayItem> {
		private List<OverlayItem> items = new ArrayList<OverlayItem>();
		private Drawable marker = null;
		private Bitmap mBubbleIcon;
		private Paint	mInnerPaint, mBorderPaint, mTextPaint;
		
		public SitesOverlay(Drawable marker) {
			super(marker);
			this.marker = marker;
			mBubbleIcon = BitmapFactory.decodeResource(getResources(), R.drawable.carformap);
			
			for(int index=0; index<LiveryCabStatics.bidList.size();index++)
			{
				items.add(new OverlayItem(getPoint(
						Double.parseDouble(LiveryCabStatics.bidList.get(index).latitude),
						Double.parseDouble(LiveryCabStatics.bidList.get(index).longitude)),
						LiveryCabStatics.bidList.get(index).firstName+" "
						+LiveryCabStatics.bidList.get(index).lastName,
						LiveryCabStatics.bidList.get(index).etaTime));
				mapController.animateTo(getPoint(
						Double.parseDouble(LiveryCabStatics.bidList.get(index).latitude),
						Double.parseDouble(LiveryCabStatics.bidList.get(index).longitude)));
				
			}
			
			populate();
		}
		
		@Override
		protected OverlayItem createItem(int i) {
			return (items.get(i));
		}
			
		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			super.draw(canvas, mapView, shadow);
			//boundCenterBottom(marker);	
		    drawMapLocations(canvas, mapView, shadow);
		}
		
	    private void drawMapLocations(Canvas canvas, MapView	mapView, boolean shadow) {	    	
			
				Point screenCoords = new Point();
				for(int i=0; i < items.size(); i++){
					mapView.getProjection().toPixels(items.get(i).getPoint(), screenCoords);
					//canvas.drawBitmap(mBubbleIcon, screenCoords.x, screenCoords.y - mBubbleIcon.getHeight(),null);
					canvas.drawBitmap(mBubbleIcon, screenCoords.x - mBubbleIcon.getWidth()/2, screenCoords.y - mBubbleIcon.getHeight(),null);
					
						//Setup the info window
			    		int INFO_WINDOW_WIDTH = 130;
						int INFO_WINDOW_HEIGHT = 50;
						RectF infoWindowRect = new RectF(0,0,INFO_WINDOW_WIDTH,INFO_WINDOW_HEIGHT);				
						int infoWindowOffsetX = screenCoords.x-INFO_WINDOW_WIDTH/2;
						int infoWindowOffsetY = screenCoords.y;
						infoWindowRect.offset(infoWindowOffsetX,infoWindowOffsetY);
						
						//Drawing the inner info window
						canvas.drawRoundRect(infoWindowRect, 5, 5, getmInnerPaint());
						
						//Drawing the border for info window
						canvas.drawRoundRect(infoWindowRect, 5, 5, getmBorderPaint());
							
						//  Draw the MapLocation's name
						int TEXT_OFFSET_X = 10;
						int TEXT_OFFSET_Y = 15;
						
						Paint strokePaint = new Paint();
			            strokePaint.setARGB(255, 255, 255, 255);	              
			            strokePaint.setStyle(Style.STROKE);  
			            
			            int beganIndexforMiles = (LiveryCabStatics.bidList.get(i).distance).indexOf(".");
			            String bidamount = "Bid Amount: $"+LiveryCabStatics.bidList.get(i).bidAmt;
			            String ETA = ("ETA: "+LiveryCabStatics.bidList.get(i).etaTime);
			           
			            
						canvas.drawText(bidamount,infoWindowOffsetX+TEXT_OFFSET_X,infoWindowOffsetY+TEXT_OFFSET_Y,getmTextPaint());
						canvas.drawText(ETA,infoWindowOffsetX+TEXT_OFFSET_X,infoWindowOffsetY+TEXT_OFFSET_Y+20,getmTextPaint());
				}	    
		    }
		
		 @Override
		 protected boolean onTap(int index) {
		        OverlayItem itemClicked = items.get(index);
		        GeoPoint p = itemClicked.getPoint();
		        Point screenPts = new Point();
		        mapView.getProjection().toPixels(p, screenPts);
		        Rect rect = new Rect(screenPts.x, screenPts.y + 60, screenPts.x
						+ mapView.getWidth(), screenPts.y + 60);
		        QuickActionMapView qa = new QuickActionMapView(MapViewActivity.this,mapView,rect);
		        qa.setTitle(LiveryCabStatics.bidList.get(index).firstName+" "+
		        		LiveryCabStatics.bidList.get(index).lastName);
		        qa.setText(LiveryCabStatics.bidList.get(index).etaTime+" minutes");
		        int beganIndexforMiles = (LiveryCabStatics.bidList.get(index).distance).indexOf(".");
		        qa.setMiles((LiveryCabStatics.bidList.get(index).distance).substring(0,beganIndexforMiles+2)+" miles");
		        qa.setBidAmount("$"+LiveryCabStatics.bidList.get(index).bidAmt);
		        qa.setIcon(R.drawable.blue_arrow);
		        Intent i =new Intent(MapViewActivity.this,DriverDetailActivity.class);
		        
		        i.putExtra("BidID",LiveryCabStatics.bidList.get(index).bidId);
		        i.putExtra("driverID",LiveryCabStatics.bidList.get(index).driverId);
		        i.putExtra("fName",LiveryCabStatics.bidList.get(index).firstName);
		        i.putExtra("lName",LiveryCabStatics.bidList.get(index).lastName);
		        i.putExtra("bidAmount",LiveryCabStatics.bidList.get(index).bidAmt);
		        i.putExtra("phoneNum",LiveryCabStatics.bidList.get(index).phoneNum);
		        
		        qa.setIntent(i);
		        qa.show();
		        return true;
		 }
		@Override
		public int size() {
			return (items.size());
		}
		
		public Paint getmInnerPaint() {
			if ( mInnerPaint == null) {
				mInnerPaint = new Paint();
				mInnerPaint.setARGB(225, 50, 50, 50); //inner color
				mInnerPaint.setAntiAlias(true);
			}
			return mInnerPaint;
		}

		public Paint getmBorderPaint() {
			if ( mBorderPaint == null) {
				mBorderPaint = new Paint();
				mBorderPaint.setARGB(255, 255, 255, 255);
				mBorderPaint.setAntiAlias(true);
				mBorderPaint.setStyle(Style.STROKE);
				mBorderPaint.setStrokeWidth(2);
			}
			return mBorderPaint;
		}

		public Paint getmTextPaint() {
			if ( mTextPaint == null) {
				mTextPaint = new Paint();
				mTextPaint.setARGB(255, 255, 255, 255);
				mTextPaint.setAntiAlias(true);
			}
			return mTextPaint;
		}
		
		@SuppressWarnings("unused")
		public void addOverlay(OverlayItem overlay) {
			// add item to our overlay
			items.add(overlay);
			populate();
		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void getAddress(double lat, double lng) {
		Geocoder geocoder = new Geocoder(MapViewActivity.this,
				Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
			if (!addresses.isEmpty()) {
				Address obj = addresses.get(0);
				String add = obj.getAddressLine(0);

				add = add + "," + obj.getCountryName();
				add = add + "," + obj.getCountryCode();
				add = add + "," + obj.getAdminArea();
				add = add + "," + obj.getPostalCode();
				add = add + "," + obj.getSubAdminArea();
				add = add + "," + obj.getLocality();
				add = add + "," + obj.getSubThoroughfare();
				addName = add;
			}
		} catch (IOException e) {
			e.printStackTrace();
			//Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
	
	
}
