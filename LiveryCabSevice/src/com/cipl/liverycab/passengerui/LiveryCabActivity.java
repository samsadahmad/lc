package com.cipl.liverycab.passengerui;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.cipl.liverycab.statics.LiveryCabStatics;
import com.google.android.gcm.GCMRegistrar;

public class LiveryCabActivity extends Activity implements
		android.view.View.OnClickListener, LocationListener {

	private static final String TAG = "LiveryCabActivity";
	private LocationManager locationManager;
	private String addName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);

		setUpView();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		locationManager.removeUpdates(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 1000, 10, this);
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onLocationChanged with location " + location.toString());

		try {
			// Convert latitude and longitude into int that the GeoPoint
			// constructor can understand
			double latitude = location.getLatitude();
			double longitude = location.getLongitude();

			LiveryCabStatics.currentLatitude = "" + latitude;
			LiveryCabStatics.currentLongitude = "" + longitude;
			getAddress(latitude, longitude);
		} catch (Exception e) {
			Log.e("LocateMe", "Could not get Geocoder data", e);
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	private void setUpView() {
		// TODO Auto-generated method stub
		getLatLang(this);

		Button btnDriver = (Button) findViewById(R.id.button_home_driver);
		Button btnPassenger = (Button) findViewById(R.id.buttonPassengerHomeScreen);
		btnDriver.setOnClickListener(this);
		btnPassenger.setOnClickListener(this);
	}

	private void getLatLang(Context context) {
		// TODO Auto-generated method stub

		locationManager = (LocationManager) this
				.getSystemService(LOCATION_SERVICE);

		Location location = locationManager
				.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if (location != null) {
			Log.d(TAG, location.toString());
			this.onLocationChanged(location);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.button_home_driver:

			LiveryCabStatics.isDriver = true;
			LiveryCabStatics.isPassenger = false;
			LiveryCabStatics.userType = "driver";	
			
			//deletePreferences("PASS_ID");			
			LiveryCabStatics.isLoginPassenger = false;
			LiveryCabStatics.passengerId = "";
			
			
			startActivity(new Intent(LiveryCabActivity.this,
					LoginActivity.class));

			break;

		case R.id.buttonPassengerHomeScreen:

			LiveryCabStatics.isDriver = false;
			LiveryCabStatics.isPassenger = true;
			LiveryCabStatics.userType = "passenger";
			
			//deletePreferences("DRIVER_ID");
			LiveryCabStatics.isLoginDriver = false;
			LiveryCabStatics.driverId = "";
			
			startActivity(new Intent(LiveryCabActivity.this,
					PassengerMenuActivity.class));

			break;

		default:
			break;
		}

	}

	/** Get Address from lat and lng */
	/**
	 * This is the method used to get the address of the location
	 * */
	public void getAddress(double lat, double lng) {
		Geocoder geocoder = new Geocoder(LiveryCabActivity.this,
				Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
			if (!addresses.isEmpty()) {
				Address obj = addresses.get(0);
				String add = obj.getAddressLine(0);

				add = add + "," + obj.getCountryName();
				add = add + "," + obj.getCountryCode();
				add = add + "," + obj.getAdminArea();
				add = add + "," + obj.getPostalCode();
				add = add + "," + obj.getSubAdminArea();
				add = add + "," + obj.getLocality();
				add = add + "," + obj.getSubThoroughfare();
				addName = add;
				LiveryCabStatics.currentAddress = addName;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
	/** Method used to get Shared Preferences */
	public SharedPreferences getPreferences() 
	{
	    return getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);
	}
	
	/** Method used to save Preferences */
	public void savePreferences(String key, String value) 
	{
	    SharedPreferences sharedPreferences = getPreferences();
	    SharedPreferences.Editor editor = sharedPreferences.edit();
	    editor.putString(key, value);
	    editor.commit();
	}
	
	/**Method used to load Preferences */
	public String getPreferences(String key) 
	{
	    try {
	        SharedPreferences sharedPreferences = getPreferences();
	        String strSavedMemo = sharedPreferences.getString(key, "");
	        return strSavedMemo;
	    } catch (NullPointerException nullPointerException) 
	    {
	        Log.e("Error caused at  LiveryCab loadPreferences method",
	                ">======>" + nullPointerException);
	        return null;
	    }
	}
	
	/** Method used to delete Preferences */
	public boolean deletePreferences(String key)
	{
	    SharedPreferences.Editor editor=getPreferences().edit();
	    editor.remove(key).commit();
	    return false;
	}
}