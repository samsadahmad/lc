package com.cipl.liverycab.passengerui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TimePicker;

import com.cipl.liverycab.dataclasses.PopularAddressCategoryDataClass;
import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.parserclasses.PopularAddressCategoryParserClass;
import com.cipl.liverycab.parserclasses.PopularAddressParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class PopularDestinationAddressActivity extends Activity {
	
	private Context mContext;
	private PopularAddressParserClass parserObject;
	private PopularAddressCategoryParserClass parserObjectCat;
	private ListView popularAddList,list;
	static final int TIME_DIALOG_ID = 0;
    static final int DATE_DIALOG_ID = 1;
    private ForgotPasswordParserClass objParser;
    private String message ="";
	private ProgressDialog pd;
	private int textlength;
	private int textlength2 = 0;
	private String [] from = new String[]{"address type","address","distance"};
	private int [] to = new int[]{R.id.imageviewAddressTypeRecentAddress,R.id.textviewAddressDetailRecentAddresss,R.id.textviewMilesRecentAddresss};
	private String dateSelected;
	public static String time_selected;
	String[] monthArray = {"Jan","Feb","Mar","Apr","May","June","Jul","Aug","Sep","Oct","Nov","Dec"};
	private OnTimeSetListener mTimeSetListener = null;
	private Button searchButton;
	private EditText searchEditText;
	private Builder dialog;
	private CategoryGridAdapter mCategoryGridAdapter;
	private AlertDialog alert;
	private SimpleAdapter adapter;	
	private String CAT_ID = "";
	private ProgressDialog mProgressDialog;
	private final ArrayList<PopularAddressCategoryDataClass> popularAddCatListSearch = new ArrayList<PopularAddressCategoryDataClass>();
	final List<HashMap<String, String>> fillMaps = new ArrayList<HashMap<String, String>>();
	private String soLat,soLng,disLat,disLng;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.populardestination);
		LiveryCabStatics.popularDesti = true;
		mContext = this;
		popularAddList = (ListView)findViewById(R.id.listviewPopularDestinationAddress);
		
		searchButton = (Button) findViewById(R.id.btnSearchPopularPickupAddress);
		searchEditText = (EditText) findViewById(R.id.edittextSearchPopularDestinationAddress);
		
		searchButton = (Button) findViewById(R.id.btnSearchPopularPickupAddress);
		searchEditText = (EditText) findViewById(R.id.edittextSearchPopularDestinationAddress);	
		
		
		/**parser object for popular address category*/
		parserObjectCat = new PopularAddressCategoryParserClass(UrlStatics.getFourSquareApiCat());
		parserObject = new  PopularAddressParserClass();
		/**Thread start for popular pickup*/
		mProgressDialog = ProgressDialog.show(PopularDestinationAddressActivity.this, "",
				"Loading. Please wait...", true, false);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		if(LiveryCabStatics.currentLatitude.equalsIgnoreCase("0.0") &&
				LiveryCabStatics.currentLongitude.equalsIgnoreCase("0.0")){
			LiveryCabStaticMethods.showAlert(PopularDestinationAddressActivity.this, 
					"Error Occured!!","Current location not availabale",R.drawable.attention,true,false,"Ok",null,null,null);
		}else{			
			 new Thread(new Runnable() {
					@Override
					public void run() {						
						parserObjectCat.fetchPopularAddressCategory();	
						mpopularPickupHandler.post(new Runnable() {
							@Override
							public void run() {
								parserObject.fetchPopularAddress(UrlStatics
										.getFourSquareApi(LiveryCabStatics.currentLatitude+","
												+LiveryCabStatics.currentLongitude,CAT_ID));
								mpopularPickupHandler.sendEmptyMessage(0);
							}
						});
					}
				}).start();	 
		}
	
		popularAddList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				LiveryCabStatics.destinationAddress = LiveryCabStatics.popularAddList.get(position).address; 
				LiveryCabStatics.destinationLatitude = LiveryCabStatics.popularAddList.get(position).latitude;
				LiveryCabStatics.destinationLongitude = LiveryCabStatics.popularAddList.get(position).longitude;
				double distance = 0;
				if(LiveryCabStatics.sourceLatitude == null || LiveryCabStatics.sourceLatitude.equalsIgnoreCase("")){
					soLat = LiveryCabStatics.currentLatitude;
					soLng = LiveryCabStatics.currentLongitude;
				}else{
					soLat = LiveryCabStatics.sourceLatitude;
					soLng = LiveryCabStatics.sourceLongitude;
				}
				if(LiveryCabStatics.destinationLatitude== null || LiveryCabStatics.destinationLatitude.equalsIgnoreCase("")){
					soLat = LiveryCabStatics.currentLatitude;
					soLng = LiveryCabStatics.currentLongitude;
				}else{
					disLat = LiveryCabStatics.destinationLatitude;
					disLng = LiveryCabStatics.destinationLongitude;
				}
				
				distance = LiveryCabStaticMethods.calculateDistance(soLat.trim(),soLng.trim(),disLat.trim(),disLng.trim());
				LiveryCabStatics.isAddPickup = true;
				if(LiveryCabStatics.isRideLater){
					showDialog(DATE_DIALOG_ID);
				}else if(LiveryCabStatics.isLoginPassenger){
					finish();
					Intent mintentBook = new Intent(PopularDestinationAddressActivity.this,BookRideActivity.class);
					mintentBook.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(mintentBook);
				}
				else{
					finish();
					Intent mintentLogin = new Intent(PopularDestinationAddressActivity.this,LoginActivity.class);
					mintentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(mintentLogin);
				}
			}
		});
		
		/**Search address a/c to category*/	
		searchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// custom dialog
				final Dialog dialog = new Dialog(PopularDestinationAddressActivity.this);
				dialog.setContentView(R.layout.spinner_dialog);
				dialog.setTitle("Choose Category");
				final EditText msearchBox = (EditText) dialog.findViewById(R.id.et_searchBox);
				msearchBox.setHint("Search");
				msearchBox.setSingleLine(true);
				list = (ListView) dialog.findViewById(R.id.lv_spinnerItems);
				
				list.setBackgroundColor(0xFFFFFF);
				list.setCacheColorHint(00000000);		
				list.setFastScrollEnabled(true);
				mCategoryGridAdapter = new CategoryGridAdapter(PopularDestinationAddressActivity.this,LiveryCabStatics.popularAddCatList);			
				list.setAdapter(mCategoryGridAdapter);				
				
				//Category text box search here..
				msearchBox.addTextChangedListener(new TextWatcher() {
					@Override
					public void onTextChanged(CharSequence s, int start, int before,
							int count) {
						popularAddCatListSearch.clear();
						textlength2 = msearchBox.getText().length();
		                 for (int i = 0; i < LiveryCabStatics.popularAddCatList.size(); i++) {
		                	 if (textlength2 <= LiveryCabStatics.popularAddCatList.get(i).catName.length()) {
		                		 if (msearchBox.getText().toString() .equalsIgnoreCase(
		                              (String) LiveryCabStatics.popularAddCatList.get(i).catName.subSequence(0,textlength2))) {
		                			 PopularAddressCategoryDataClass mccc = new PopularAddressCategoryDataClass();
		                			 mccc.catId = LiveryCabStatics.popularAddCatList.get(i).catId;
		                			 mccc.catName = LiveryCabStatics.popularAddCatList.get(i).catName;
		                			 mccc.message = LiveryCabStatics.popularAddCatList.get(i).message;
		                			 mccc.type = LiveryCabStatics.popularAddCatList.get(i).type;
		                			 popularAddCatListSearch.add(mccc);		                			 
		                         }
		                     }
		                 }
		                 if(!popularAddCatListSearch.isEmpty()){		                	 
		                	 CategoryGridAdapter mmCategoryGridAdapter = new CategoryGridAdapter(PopularDestinationAddressActivity.this,popularAddCatListSearch);		                	 
		                	 list.setAdapter(mmCategoryGridAdapter);
		                 }               
					}
					@Override
					public void afterTextChanged(Editable arg0) {
						// TODO Auto-generated method stub
						
					}
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub
						
					}
				});
				
				//Click on list category item..
				list.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int pos, long arg3) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						if(textlength2 > 0){
							searchButton.setText(popularAddCatListSearch.get(pos).catName.toString());
							CAT_ID = popularAddCatListSearch.get(pos).catId;
						}else{
							searchButton.setText(LiveryCabStatics.popularAddCatList.get(pos).catName.toString());
							CAT_ID = LiveryCabStatics.popularAddCatList.get(pos).catId;
						}
						
						searchEditText.setText("");
						/**Thread start for popular pickup*/
						mProgressDialog = ProgressDialog.show(PopularDestinationAddressActivity.this, "",
								"Loading. Please wait...", true, false);
						mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
						
						new Thread(){
							public void run() {
								parserObject.fetchPopularAddress(UrlStatics
										.getFourSquareApi(LiveryCabStatics.currentLatitude+","
												+LiveryCabStatics.currentLongitude,CAT_ID));
								mpopularPickupAfterHandler.sendEmptyMessage(0);
							};
						}.start();					
					}
				});
				dialog.show();
			}
		});
		/**EditTextBox Searching Start here..*/
		searchEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				 fillMaps.clear();
                 textlength = searchEditText.getText().length();
                 for (int i = 0; i < LiveryCabStatics.popularAddList.size(); i++) {
                	 if (textlength <= LiveryCabStatics.popularAddList.get(i).address.length()) {
                		 if (searchEditText.getText().toString() .equalsIgnoreCase(
                              (String) LiveryCabStatics.popularAddList.get(i).address.subSequence(0,textlength))) {
                              HashMap<String, String> map = new HashMap<String, String>();
                              map.put("address type", LiveryCabStatics.popularAddList.get(i).addressType);
                              map.put("address", LiveryCabStatics.popularAddList.get(i).address);
                              map.put("distance",LiveryCabStatics.popularAddList.get(i).distance);
                              fillMaps.add(map);
                         }
                     }
                 }
                 if(!fillMaps.isEmpty()){
                	 SimpleAdapter adapter = new SimpleAdapter(PopularDestinationAddressActivity.this, fillMaps, R.layout.recentaddresscustomcell,
                                 from, to);
                	 popularAddList.setAdapter(adapter);
                 }else{
                	 LiveryCabStaticMethods.showAlert(PopularDestinationAddressActivity.this,
                			 "Alert!!","No record found", R.drawable.attention,
                			 true,false,"Ok",null,null,null);
                 }
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
		});
		/**All Searching End here*/		
	}
	/**List Adapter for alert box list*/
	public class CategoryGridAdapter extends BaseAdapter {
		private LayoutInflater inflater = null;
		private ArrayList<PopularAddressCategoryDataClass> popularAddCatListDM;

		public CategoryGridAdapter(Context context,ArrayList<PopularAddressCategoryDataClass> popularAddCatListD) {
			inflater = LayoutInflater.from(context);
			this.popularAddCatListDM = popularAddCatListD;
		}

		public int getCount() {
			return popularAddCatListDM.size();
		}

		public Object getItem(int paramInt) {
			return paramInt;
		}

		public long getItemId(int paramInt) {
			return paramInt;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			EventViewHolder holder;

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.customcategorylayout,
						null);
				holder = new EventViewHolder();
				holder.mtvPopulatCategory = (TextView) convertView
						.findViewById(R.id.tvPopulatCategory);
				
				convertView.setTag(holder);
			} else {
				holder = (EventViewHolder) convertView.getTag();
			}
			if(popularAddCatListDM.get(position).type.toString().equalsIgnoreCase("1")){
				holder.mtvPopulatCategory
				.setText("----"+""+popularAddCatListDM.get(position).catName);
			}else if(popularAddCatListDM.get(position).type.toString().equalsIgnoreCase("2")){
				holder.mtvPopulatCategory
				.setText("--------"+""+popularAddCatListDM.get(position).catName);
			}else{
				holder.mtvPopulatCategory
				.setText(""+popularAddCatListDM.get(position).catName);
			}			
			return convertView;
		}

		public class EventViewHolder {
			private TextView mtvPopulatCategory;			
		}
	}
	
	@Override    
    protected Dialog onCreateDialog(int id) 
    {
    	Calendar c = Calendar.getInstance();
    	int cyear = c.get(Calendar.YEAR);
    	int cmonth = c.get(Calendar.MONTH);
    	int cday = c.get(Calendar.DAY_OF_MONTH);
    	int chour = c.get(Calendar.HOUR_OF_DAY);
    	int cmin = c.get(Calendar.MINUTE);
        switch (id) {
           case TIME_DIALOG_ID: 
                return new TimePickerDialog(
                    this, mTimeSetListener, chour, cmin, false);
 
            case DATE_DIALOG_ID: 
                return new DatePickerDialog(
                    this, mDateSetListener, cyear, cmonth, cday);
        }
        return null;    
    }
 
 
    private DatePickerDialog.OnDateSetListener mDateSetListener =
    new DatePickerDialog.OnDateSetListener() 
    {
        public void onDateSet(DatePicker view, int Year, int monthOfYear,
                int dayOfMonth) 
        {
        	dateSelected = ""+dayOfMonth+" "+monthArray[monthOfYear]+" "+Year;
        	if(dateSelected!=null){
        		final Calendar c = Calendar.getInstance();
				int mYear = c.get(Calendar.YEAR);
				int mMonth = c.get(Calendar.MONTH);
				int mDay = c.get(Calendar.DAY_OF_MONTH);
				Calendar currentDate = Calendar.getInstance();
				currentDate.set(Calendar.DAY_OF_MONTH, mDay);
				currentDate.set(Calendar.MONTH, mMonth);
				currentDate.set(Calendar.YEAR, mYear);

				final Calendar selectedDate = Calendar.getInstance();
				selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				selectedDate.set(Calendar.MONTH, monthOfYear);
				selectedDate.set(Calendar.YEAR, Year);	
				if (selectedDate.after(currentDate)
						|| selectedDate.equals(currentDate)) {
					dateSelected = ""+String.valueOf(selectedDate.get(Calendar.MONTH+1))+" "
							+selectedDate.get(Calendar.DAY_OF_MONTH)+" "
							+selectedDate.get(Calendar.YEAR);
					mTimeSetListener = new OnTimeSetListener(){
						public void onTimeSet(TimePicker arg0, int arg1, int arg2) {
							time_selected = ""+arg1+":"+arg2+":"+00;
							/*Changes by sam**/
							LiveryCabStatics.date_Selected = getFormatedDate(selectedDate.getTimeInMillis());
							if(LiveryCabStatics.isLoginPassenger){
								finish();
								startActivity(new Intent(PopularDestinationAddressActivity.this,BookRideActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
							}else{
								finish();
								startActivity(new Intent(PopularDestinationAddressActivity.this,LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
							}
						}
					};
					showDialog(TIME_DIALOG_ID);
				}else if(selectedDate.equals(currentDate)){
					mTimeSetListener = new OnTimeSetListener() {
						@Override
						public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
							time_selected = ""+hourOfDay+":"+minute+":"+00;
							final Calendar cc = Calendar.getInstance();
							int mYearc = cc.get(Calendar.YEAR);
							int mMonthc = cc.get(Calendar.MONTH);
							int mDayc = cc.get(Calendar.DAY_OF_MONTH);
							int HOUR = cc.get(Calendar.HOUR_OF_DAY);
							int MINUTE = cc.get(Calendar.MINUTE);
							
							Calendar currentTime = Calendar.getInstance();
							currentTime.set(Calendar.HOUR_OF_DAY, HOUR);
							currentTime.set(Calendar.MINUTE, MINUTE);
							currentTime.set(Calendar.SECOND, 0);
							currentTime.set(Calendar.MILLISECOND, 0);
							
							Calendar selectedTime = Calendar.getInstance();
							selectedTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
							selectedTime.set(Calendar.MINUTE, minute);
							selectedTime.set(Calendar.SECOND, 0);
							selectedTime.set(Calendar.MILLISECOND, 0);
							if (selectedTime.after(currentTime)) {
								time_selected = ""+hourOfDay+":"+minute+":"+00;
								/*Changes by sam**/
								LiveryCabStatics.date_Selected = getFormatedDate(selectedTime.getTimeInMillis());
								if(LiveryCabStatics.isLoginPassenger){
									finish();
									startActivity(new Intent(PopularDestinationAddressActivity.this,BookRideActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
								}else{
									finish();
									startActivity(new Intent(PopularDestinationAddressActivity.this,LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
								}
							}else{
								LiveryCabStaticMethods.showAlert(mContext,
										"Alert!!","Enter a valid time",R.drawable.attention,
										true,false,"Ok",null,null,null);
							}
						}
					};
					showDialog(TIME_DIALOG_ID);
				}else{
					LiveryCabStaticMethods.showAlert(PopularDestinationAddressActivity.this,
							"Alert!!","Back date is not allowed",R.drawable.attention,
							true,false,"Ok",null,null,null);
				}
        	}
        }
    };

	
	class EfficientAdapter extends BaseAdapter 
	{
		private LayoutInflater inflater;

		public EfficientAdapter(Context context) 
		{
			inflater = LayoutInflater.from(context);
		}

		public int getCount() 
		{
			return LiveryCabStatics.popularAddList.size();
		}

		public Object getItem(int position) 
		{
			return null;
		}

		public long getItemId(int position) 
		{
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			// A ViewHolder is used to keep references to children views
			ViewHolder holder;
			if (convertView == null) 
			{
				convertView = inflater.inflate(R.layout.recentaddresscustomcell,null);
				holder = new ViewHolder();
				// getting the views from the layout
				holder.recentAddress = (TextView) convertView
						.findViewById(R.id.textviewAddressDetailRecentAddresss);
				holder.miles = (TextView) convertView
						.findViewById(R.id.textviewMilesRecentAddresss);
				holder.addressType = (ImageView) convertView
						.findViewById(R.id.imageviewAddressTypeRecentAddress);
				convertView.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) convertView.getTag();
			}
			
			holder.recentAddress.setText(LiveryCabStatics.popularAddList.get(position).address);
			holder.miles.setText(LiveryCabStatics.popularAddList.get(position).distance+" miles");
			if(LiveryCabStatics.popularAddList.get(position).addressType.equalsIgnoreCase("1"))
				holder.addressType.setBackgroundResource(R.drawable.airway);
			else if(LiveryCabStatics.popularAddList.get(position).addressType.equalsIgnoreCase("2"))
				holder.addressType.setBackgroundResource(R.drawable.homeaddicon);
			else if(LiveryCabStatics.popularAddList.get(position).addressType.equalsIgnoreCase("3"))
				holder.addressType.setBackgroundResource(R.drawable.resto);
			else
				holder.addressType.setBackgroundResource(R.drawable.homeaddicon);
			return convertView;
		}
	}
	class ViewHolder 
	{
		TextView recentAddress;
		TextView miles;
		ImageView addressType;
	}
	/**Hadler for popular pickup address*/
	 Handler mpopularPickupHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();
			}
			
			if(LiveryCabStatics.popularAddList.isEmpty() || LiveryCabStatics.popularAddList.get(0).message.equalsIgnoreCase("Record not found")){
				LiveryCabStaticMethods.showAlert(PopularDestinationAddressActivity.this, 
					"No Records!!","No address availabale",R.drawable.attention,true,false,"Ok",null,null,null);
			}else{
				fillMaps.clear();				
				 for (int i = 0; i < LiveryCabStatics.popularAddList.size(); i++) {
		                HashMap<String, String> map = new HashMap<String, String>();
		                map.put("address", LiveryCabStatics.popularAddList.get(i).address);
		                map.put("distance",LiveryCabStatics.popularAddList.get(i).distance);
		                fillMaps.add(map);
		        }
				 adapter = new SimpleAdapter(PopularDestinationAddressActivity.this, fillMaps,
	                        R.layout.recentaddresscustomcell, from, to);
	    		 popularAddList.setAdapter(adapter);
			}			
		};
	};
	
	/**Hadler for popular pickup address after category selection*/
	Handler mpopularPickupAfterHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();
			}
			if(LiveryCabStatics.popularAddList.size() > 0){
				fillMaps.clear();
				fillMaps.clear();				
				 for (int i = 0; i < LiveryCabStatics.popularAddList.size(); i++) {
		                HashMap<String, String> map = new HashMap<String, String>();
		                map.put("address", LiveryCabStatics.popularAddList.get(i).address);
		                map.put("distance",LiveryCabStatics.popularAddList.get(i).distance);
		                fillMaps.add(map);
		        }
				 adapter = new SimpleAdapter(PopularDestinationAddressActivity.this, fillMaps,
	                        R.layout.recentaddresscustomcell, from, to);
	             popularAddList.setAdapter(adapter);
			}else{
				DialogInterface.OnClickListener recordNotFoundOkAlertListener = new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						fillMaps.clear();
						adapter = new SimpleAdapter(PopularDestinationAddressActivity.this, fillMaps,
		                        R.layout.recentaddresscustomcell, from, to);
		                popularAddList.setAdapter(adapter);
					}
				};										 
				LiveryCabStaticMethods.showAlert(PopularDestinationAddressActivity.this, 
						"No Records!!","No address availabale",R.drawable.attention,true,false,"Ok",recordNotFoundOkAlertListener,null,null);
			}		
		};
	};
	private String getFormatedDate(long timeInMillis)  {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat();
			Date d = new Date(timeInMillis);
			sdf.applyPattern("EEE MMM d,yyyy h:mm a");
			String formatedDate = sdf.format(d);
			return formatedDate;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
}
