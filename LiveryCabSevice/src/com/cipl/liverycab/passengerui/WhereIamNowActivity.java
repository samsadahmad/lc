package com.cipl.liverycab.passengerui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cipl.liverycab.statics.LiveryCabStatics;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class WhereIamNowActivity extends MapActivity {

	private MapView mapView;
	private MapController mapController;
	private LocationManager mLocManager;
	private GeoPoint myCurrentLocation;
	private Context mContext;
	private LinearLayout zoomLayout;
	private EditText editTextSearch;
	private double latitude, longitude;
	private boolean isPopup = true;
	private String name = "";
	private MyLocationOverlay myOverlay;
	private CustomizedOverlay myCustomizedOverlay;
	private Display display;
	private int screenWidth;
	private int screenHeight;
	private ProgressBar progressBar;
	private String add,sourceLat,sourceLong;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.whereiamnow);
		mContext = WhereIamNowActivity.this;

		mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		progressBar = (ProgressBar)findViewById(R.id.progressBarWhereIamNow);
		
		EditText medittextSearchWhereIamNow = (EditText)findViewById(R.id.edittextSearchWhereIamNow);
		medittextSearchWhereIamNow.setHint(" Enter PickUp ");
		
		// getting map and map controller
		mapView = (MapView) findViewById(R.id.mapviewWhereIamNow);
		mapController = mapView.getController();
		zoomLayout = (LinearLayout) findViewById(R.id.zoomWhereIamNow);
		View zoomView1 = mapView.getZoomControls();
		zoomLayout.addView(zoomView1, new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		mapView.displayZoomControls(true);
		mapController = mapView.getController();
		mapController.setZoom(17);

		editTextSearch = (EditText) findViewById(R.id.edittextSearchWhereIamNow);

		myOverlay = new MyLocationOverlay(this, mapView);
		myOverlay.enableMyLocation();
		mapView.getOverlays().add(myOverlay);

		ImageButton btnsearch = (ImageButton) findViewById(R.id.imageButtonSearchIconWhereIamNow);
		btnsearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(
						editTextSearch.getWindowToken(), 0);
				Geocoder geocoder = new Geocoder(WhereIamNowActivity.this,
						Locale.getDefault());
				List<Address> addresses;
				try {
					addresses = geocoder.getFromLocationName(editTextSearch
							.getText().toString(), 5);
					if (addresses.size() > 0) {
						myCurrentLocation = new GeoPoint((int) (addresses
								.get(0).getLatitude() * 1E6), (int) (addresses
								.get(0).getLongitude() * 1E6));

						mapController.animateTo(myCurrentLocation);
						mapController.setZoom(17);
						myCustomizedOverlay.addOverlay(new OverlayItem(
								myCurrentLocation, "", ""));
						startAddrssPopup(myCurrentLocation, "Pickup Addresss",
								R.drawable.bubbleicon);
						isPopup = true;
						mapView.invalidate();
						editTextSearch.setText("");
					} else {
						AlertDialog.Builder adb = new AlertDialog.Builder(WhereIamNowActivity.this);
						adb.setTitle("Google Map");
						adb.setMessage("Please Provide the Proper Place");
						adb.setPositiveButton("Close", null);
						adb.show();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		
		((Button)findViewById(R.id.buttonSelectPickUpWhereIamNow))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(LiveryCabStatics.destiMapView){
					if(LiveryCabStatics.isLoginPassenger)
						startActivity(new Intent(WhereIamNowActivity.this,BookRideActivity.class));
					else
						startActivity(new Intent(WhereIamNowActivity.this,LoginActivity.class));
				}else{
					LiveryCabStatics.sourceLatitude = sourceLat;
					LiveryCabStatics.sourceLongitude = sourceLong;
					LiveryCabStatics.pickupAddress = name;
					startActivity(new Intent(WhereIamNowActivity.this,DestinationAddressActivity.class));
				}
			}
		});
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void getAddress(double lat, double lng) {
		Geocoder geocoder = new Geocoder(WhereIamNowActivity.this,
				Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
			if (!addresses.isEmpty()) {
				Address obj = addresses.get(0);
				add = obj.getAddressLine(0);

				add = add + "," + obj.getCountryName();
				add = add + "," + obj.getCountryCode();
				add = add + "," + obj.getAdminArea();
				add = add + "," + obj.getPostalCode();
				add = add + "," + obj.getSubAdminArea();
				add = add + "," + obj.getLocality();
				add = add + "," + obj.getSubThoroughfare();
				name = add;
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	private class MyLocationOverlay extends com.google.android.maps.MyLocationOverlay {
		// For storing our location manager
		LocationManager myLocationManager;
		Context mContext;

		public MyLocationOverlay(Context context, MapView mapView) {
			super(context, mapView);
			mContext = context;
			myLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		}

		@Override
		public synchronized void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			super.onLocationChanged(location);
			latitude = location.getLatitude();
			longitude = location.getLongitude();

			if (location != null) {
				if (isPopup) {
					progressBar.setVisibility(ProgressBar.GONE);
					myCurrentLocation = new GeoPoint((int) (latitude * 1E6),
							(int) (longitude * 1E6));
					myCustomizedOverlay = new CustomizedOverlay(
							WhereIamNowActivity.this, WhereIamNowActivity.this
									.getResources().getDrawable(
											R.drawable.bluepin));
					myCustomizedOverlay.addOverlay(new OverlayItem(
							myCurrentLocation, "", ""));
					mapView.getOverlays().add(myCustomizedOverlay);
					mapController.animateTo(myCurrentLocation);
					getAddress(latitude, longitude);
					this.disableMyLocation();
					isPopup = false;
				}
			}
		}
	}

	private class CustomizedOverlay extends ItemizedOverlay<OverlayItem> {

		private ArrayList<OverlayItem> mOverlaysItems = new ArrayList<OverlayItem>();
		private Context mContext;
		private Drawable marker;
		private OverlayItem inDrag = null;
		private int xDragImageOffset = 0;
		private int yDragImageOffset = 0;
		private int xDragTouchOffset = 0;
		private int yDragTouchOffset = 0;
		private ImageView dragImage = null;

		public CustomizedOverlay(Context context, Drawable defaultMarker) {
			super(boundCenterBottom(defaultMarker));
			mContext = context;
			marker = defaultMarker;
			dragImage = (ImageView) findViewById(R.id.dragimage);
			xDragImageOffset = dragImage.getDrawable().getIntrinsicWidth() / 2;
			yDragImageOffset = dragImage.getDrawable().getIntrinsicHeight();
		}

		// needed methods
		@Override
		protected OverlayItem createItem(int i) {
			return mOverlaysItems.get(i);
		}

		@Override
		public int size() {
			return mOverlaysItems.size();
		}

		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			super.draw(canvas, mapView, shadow);
			boundCenterBottom(marker);
		}

		public void addOverlay(OverlayItem overlay) {
			// add item to our overlay
			mOverlaysItems.add(overlay);
			populate();
		}

		@Override
		public boolean onTouchEvent(MotionEvent event, MapView mapView) {
			final int action = event.getAction();
			final int x = (int) event.getX();
			final int y = (int) event.getY();
			boolean result = false;

			if (action == MotionEvent.ACTION_DOWN) {
				for (OverlayItem item : mOverlaysItems) {
					Point p = new Point(0, 0);
					WhereIamNowActivity.this.mapView.getProjection().toPixels(
							item.getPoint(), p);
					if (hitTest(item, marker, x - p.x, y - p.y)) {
						result = true;
						inDrag = item;
						mOverlaysItems.remove(inDrag);
						populate();

						xDragTouchOffset = 0;
						yDragTouchOffset = 0;

						setDragImagePosition(p.x, p.y);
						dragImage.setVisibility(View.VISIBLE);

						xDragTouchOffset = x - p.x;
						yDragTouchOffset = y - p.y;

						break;
					}
				}
			} else if (action == MotionEvent.ACTION_MOVE && inDrag != null) {
				setDragImagePosition(x, y);
				result = true;
			} else if (action == MotionEvent.ACTION_UP && inDrag != null) {
				dragImage.setVisibility(View.GONE);
				GeoPoint pt = WhereIamNowActivity.this.mapView.getProjection()
						.fromPixels(x - xDragTouchOffset, y - yDragTouchOffset);
				mOverlaysItems.remove(myOverlay);
				myCustomizedOverlay.addOverlay(new OverlayItem(pt, "", ""));
				LiveryCabStatics.destiMapView = false;
				startAddrssPopup(pt, "Pickup Address", R.drawable.blue_arrow);
				sourceLat = ""+(pt.getLatitudeE6()) / 1E6;
				sourceLong = ""+(pt.getLongitudeE6()) / 1E6;
				populate();

				inDrag = null;
				result = true;
			}

			return (result || super.onTouchEvent(event, mapView));
		}

		private void setDragImagePosition(int x, int y) {
			RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) dragImage
					.getLayoutParams();

			lp.setMargins(x - xDragImageOffset - xDragTouchOffset, y
					- yDragImageOffset - yDragTouchOffset, 0, 0);
			dragImage.setLayoutParams(lp);
		}
	}

	private boolean startAddrssPopup(GeoPoint p, String title, int icon) {
		// GET add fromGEO Point
		Geocoder geoCoder = new Geocoder(getApplicationContext(),
				Locale.getDefault());
		List<Address> addresses = null;
		try {
			addresses = geoCoder.getFromLocation(p.getLatitudeE6() / 1E6,
					p.getLongitudeE6() / 1E6, 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String add = "";
		if (addresses.size() > 0) {
			// for (int i=0; i<addresses.get(0).getMaxAddressLineIndex();i++)
			add += addresses.get(0).getAddressLine(0) + "\n";
			name = add;
		}
		// End Address getting
		Point screenPts = new Point();
		mapView.getProjection().toPixels(p, screenPts);
		Rect rect = new Rect(screenPts.x, screenPts.y + 80, screenPts.x
				+ mapView.getWidth(), screenPts.y + 70);
		QuickActionLocation qa = new QuickActionLocation(mContext, mapView, rect);
		qa.setTitle(title); // set QA title
		if (myCurrentLocation != null)
			qa.setText(name);
		else
			qa.setText("");
		qa.show(screenPts.x);
		return true;
	}
}
