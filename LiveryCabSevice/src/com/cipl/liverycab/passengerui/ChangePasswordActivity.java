package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class ChangePasswordActivity extends Activity {
	
	private EditText oldPassEditText,newPassEditText,confPassEditText;
	private ProgressDialog pd;
	private ForgotPasswordParserClass objParserClass;
	private String message ="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.changepassword);
		oldPassEditText = (EditText)this.findViewById(R.id.editTextOldPasswordChangePassword);
		newPassEditText = (EditText)this.findViewById(R.id.editTextNewPasswordChangePassword);
		confPassEditText = (EditText)this.findViewById(R.id.editTextConfirmPasswordChangePassword);
		
		//submitButton =(Button)this.findViewById(R.id.buttonSubmitChangePassword1);
		findViewById(R.id.buttonSubmitChangePassword1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String oldPass = oldPassEditText.getText().toString();
				String newPass = newPassEditText.getText().toString();
				String confPass = confPassEditText.getText().toString();
				
				if(oldPass.equalsIgnoreCase(""))
					oldPassEditText.setError("Please enter old password");
				else if(newPass.equalsIgnoreCase(""))
					newPassEditText.setError("Please enter new password");
				else if(confPass.equalsIgnoreCase(""))
					confPassEditText.setError("Please enter confirm password");
				else if(oldPass.equalsIgnoreCase("") && newPass.equalsIgnoreCase("") && confPass.equalsIgnoreCase(""))
					LiveryCabStaticMethods.showMessageDialog(ChangePasswordActivity.this,"All the fields are required");
				else if(!(newPass.equalsIgnoreCase(confPass)))
					LiveryCabStaticMethods.showMessageDialog(ChangePasswordActivity.this,"New password and confirm password didn't match");
				else{
					
					pd = LiveryCabStaticMethods.returnProgressBar(ChangePasswordActivity.this);
					objParserClass = new ForgotPasswordParserClass(UrlStatics
								.getChangePasswordUrl(LiveryCabStatics.userId,
								oldPass,newPass,LiveryCabStatics.userType),pd);
					objParserClass.start();
					pd.setOnDismissListener(new OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							if(objParserClass.actualDismiss){
								if(objParserClass.loginDataObj!=null)
									message = objParserClass.loginDataObj.message;
									if(message.equalsIgnoreCase("Password_change_sucessfully")){
										oldPassEditText.setText("");
										newPassEditText.setText("");
										newPassEditText.setText("");
										LiveryCabStaticMethods.showAlert(ChangePasswordActivity.this,
												"Success","Password changed successfully",R.drawable.success, true, false, 
												"Ok", new DialogInterface.OnClickListener() 
										{
											@Override
											public void onClick(DialogInterface arg0, int arg1) {
												// TODO Auto-generated method stub
												finish();
											}
										},null,null);
									}
									else if(message.equalsIgnoreCase("enter_correct_new_and_old password!")){
										LiveryCabStaticMethods.showAlert(ChangePasswordActivity.this,
												"Error Occured!","New password didn't match confirm password!",R.drawable.attention, true, false, 
												"Ok", null,null,null);
									}else if(message.equalsIgnoreCase("enter_correct_password!")){
										LiveryCabStaticMethods.showAlert(ChangePasswordActivity.this,
												"Error Occured!","Please enter correct old password",R.drawable.attention, true, false, 
												"Ok", null,null,null);
									}else
										LiveryCabStaticMethods.showAlert(ChangePasswordActivity.this,
												"Error Occured!","Old password is incorrect or new password didn't match confirm password!",R.drawable.attention, true, false, 
												"Ok", null,null,null);
							}
						}
					});
				}	
			}
		});
	}
}
