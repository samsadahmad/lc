package com.cipl.liverycab.passengerui;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

/**
 * This is the class which user can see by default and after booking the ride
 */
public class BookRideActivity extends MapActivity implements OnClickListener 
{
	private MapView mapView;
	private MapController mapController;
	private Context mContext;
	private LinearLayout zoomLayout;
	private String addName;
	private double latitude,longitude;
	private boolean isPopup = true;
	private GeoPoint myCurrentLocation;
	private MyLocationOverlay myOverlay;
	private CustomizedOverlay myCustomizedOverlay;
	private ProgressBar progressBarBookride;
	private ForgotPasswordParserClass objParser;
	private ProgressDialog pd;
	private String message ="";
	private String soLat,soLng,disLat,disLng,soAdd,disAdd;
	private QuickActionWindow qa;

	@Override
	protected void onCreate(Bundle bundle){
		// TODO Auto-generated method stub
		super.onCreate(bundle);
		setContentView(R.layout.bookride);
		
		setUpViewControls();		
	}
	
	private void setUpViewControls() {
		// TODO Auto-generated method stub
		progressBarBookride = (ProgressBar)findViewById(R.id.progressBarBookRide);

		//getting map view and some basic controls on it 
		mapView = (MapView)findViewById(R.id.mapviewBookRide);
		mapController = mapView.getController();

		zoomLayout = (LinearLayout) findViewById(R.id.zoomBookride);
		View zoomView = mapView.getZoomControls();

		zoomLayout.addView(zoomView, new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		mapView.displayZoomControls(true);
		mapController = mapView.getController();
		mapController.setZoom(17);

		//adding overlay class to the map view
		myOverlay = new MyLocationOverlay(this, mapView);
		myOverlay.enableMyLocation();
		mapView.getOverlays().add(myOverlay);
		
		
		((Button)findViewById(R.id.buttonMenuBookRide)).setOnClickListener(this);
		((Button)findViewById(R.id.buttonRidenowBookride)).setOnClickListener(this);
		((Button)findViewById(R.id.buttonRidelaterBookride)).setOnClickListener(this);
		
		bookRide();

	}

	private void bookRide() {
		// TODO Auto-generated method stub
		if(LiveryCabStatics.destiMapView ||LiveryCabStatics.recentDesti || LiveryCabStatics.popularDesti){
			double distance = 0;
			try{
				if(LiveryCabStatics.sourceLatitude == null || LiveryCabStatics.sourceLatitude.equalsIgnoreCase("")){
					soLat = LiveryCabStatics.currentLatitude;
					soLng = LiveryCabStatics.currentLongitude;
					soAdd = LiveryCabStatics.currentAddress;
				}else{
					soLat = LiveryCabStatics.sourceLatitude;
					soLng = LiveryCabStatics.sourceLongitude;
					soAdd = LiveryCabStatics.pickupAddress;
				}
				if(LiveryCabStatics.destinationLatitude== null || LiveryCabStatics.destinationLatitude.equalsIgnoreCase("")){
					disLat = LiveryCabStatics.currentLatitude;
					disLng = LiveryCabStatics.currentLongitude;
					disAdd = LiveryCabStatics.currentAddress;
				}else{
					disLat = LiveryCabStatics.destinationLatitude;
					disLng = LiveryCabStatics.destinationLongitude;
					disAdd = LiveryCabStatics.destinationAddress;
				}
				
				distance = LiveryCabStaticMethods.calculateDistance(soLat.trim(),soLng.trim(),disLat.trim(),disLng.trim());
				if(LiveryCabStatics.isAddPickup){
					if(LiveryCabStaticMethods.isInternetAvailibility(BookRideActivity.this)){
						pd = LiveryCabStaticMethods.returnProgressBar(BookRideActivity.this);
						objParser = new ForgotPasswordParserClass(UrlStatics
								.getUrlforSavingSourceAndDesti(LiveryCabStatics.userId,
										URLEncoder.encode(soAdd, "utf-8"), 
										URLEncoder.encode(disAdd, "utf-8"),
										URLEncoder.encode(soLat, "utf-8"),
										URLEncoder.encode(soLng,"utf-8"),
										URLEncoder.encode(disLat,"utf-8"),
										URLEncoder.encode(disLng,"utf-8"),
										URLEncoder.encode(LiveryCabStaticMethods.getDateTime(), "utf-8"),
										URLEncoder.encode(LiveryCabStatics.ridestatus,"utf-8")),pd);
						objParser.start();
						pd.setOnDismissListener(new OnDismissListener() {
							@Override
							public void onDismiss(DialogInterface arg0) {
								// TODO Auto-generated method stub
								if(objParser.loginDataObj!=null)
									message = objParser.loginDataObj.message;
								if(message.equalsIgnoreCase("ride_booked"))
								{
									//Your ride booked successfully
									LiveryCabStaticMethods.showAlert(BookRideActivity.this, 
											"Success", "Ride request sent, Please wait till driver bids.",R.drawable.success,true,false,"Ok", 
											null, null, null);

								}else if(message.equalsIgnoreCase("Some_error_in_ride_booking")){
									LiveryCabStaticMethods.showAlert(BookRideActivity.this, 
											"Error Occured!", "Some_error_in_ride_booking",R.drawable.attention,true,false,"Ok", 
											null, null, null);
								}else if(message.equalsIgnoreCase("session_expire")){
									LiveryCabStaticMethods.showAlert(BookRideActivity.this, 
											"Error Occured!", "Please Log in to continue",R.drawable.attention,true,false,"Ok", 
											null, null, null);
								}else{
									LiveryCabStaticMethods.showAlert(BookRideActivity.this, 
											"Error Occured!", "Try again!!",R.drawable.attention,true,false,"Ok", 
											null, null, null);
								}
							}
						});
						LiveryCabStatics.isAddPickup = false;
					}else{
						DialogInterface.OnClickListener NoNetworkOkAlertListener = new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								Intent intent = new Intent(BookRideActivity.this, PassengerMenuActivity.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(intent);
								finish();							
							}
						};					
						LiveryCabStaticMethods.showAlert(BookRideActivity.this, 
								"Network", "There is no network connection right now. Please try again later !",R.drawable.attention,true,false,"Ok", 
								NoNetworkOkAlertListener, null, null);
					}
				}
			}catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			LiveryCabStatics.destiMapView = false;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.buttonRidelaterBookride:
			LiveryCabStatics.isRideNow =  false;
			LiveryCabStatics.isRideLater =  true;
			LiveryCabStatics.ridestatus = "rideLater";
			startActivity(new Intent(BookRideActivity.this,PickUpAddressActivity.class));
			break;
			
		case R.id.buttonRidenowBookride:
			LiveryCabStatics.isRideNow =  true;
			LiveryCabStatics.isRideLater =  false;
			LiveryCabStatics.ridestatus = "rideNow";
			startActivity(new Intent(BookRideActivity.this,PickUpAddressActivity.class));
			break;
		
		case R.id.buttonMenuBookRide:
			Intent mIntent = new Intent(BookRideActivity.this,PassengerMenuActivity.class);
			mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(mIntent);
			break;				
		}
	}

	/**
	 * This is the method used to get the address of the location
	 * */
	public void getAddress(double lat, double lng) {
		Geocoder geocoder = new Geocoder(BookRideActivity.this,
				Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
			if (!addresses.isEmpty()) {
				Address obj = addresses.get(0);
				String add = obj.getAddressLine(0);

				add = add + "," + obj.getCountryName();
				add = add + "," + obj.getCountryCode();
				add = add + "," + obj.getAdminArea();
				add = add + "," + obj.getPostalCode();
				add = add + "," + obj.getSubAdminArea();
				add = add + "," + obj.getLocality();
				add = add + "," + obj.getSubThoroughfare();
				addName = add;
				LiveryCabStatics.currentAddress = addName;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * This is the class to fetch the current location of the device.
	 * */
	private class MyLocationOverlay extends com.google.android.maps.MyLocationOverlay {
		// For storing our location manager
		LocationManager myLocationManager;
		Context mContext;

		public MyLocationOverlay(Context context, MapView mapView) {
			super(context, mapView);
			mContext = context;
			myLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		}



		@Override
		public synchronized void onLocationChanged(Location location) {
			super.onLocationChanged(location);
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			if (location != null) {
				if (isPopup) {
					progressBarBookride.setVisibility(ProgressBar.GONE);
					myCurrentLocation = new GeoPoint((int) (latitude * 1E6),(int) (longitude * 1E6));
					myCustomizedOverlay = new CustomizedOverlay(BookRideActivity.this, BookRideActivity.this
							.getResources().getDrawable(R.drawable.bluepin));

					myCustomizedOverlay.addOverlay(new OverlayItem(myCurrentLocation, "", ""));
					mapView.getOverlays().add(myCustomizedOverlay);
					mapController.animateTo(myCurrentLocation);
					LiveryCabStatics.currentLatitude = ""+latitude;
					LiveryCabStatics.currentLongitude = ""+longitude;
					getAddress(latitude, longitude);

					//startAddrssPopup(myCurrentLocation,"Current Location",R.drawable.bubbleicon);

					this.disableMyLocation();
					isPopup = false;
				}
			}
		}
	}

	/**
	 * This is the class used to add item on the map view as overlay items 
	 * */
	private class CustomizedOverlay extends ItemizedOverlay<OverlayItem> {

		private ArrayList<OverlayItem> mOverlaysItems = new ArrayList<OverlayItem>();

		private Drawable marker;

		public CustomizedOverlay(Context context, Drawable defaultMarker) {
			super(boundCenterBottom(defaultMarker));
			mContext = context;
			marker = defaultMarker;
		}

		// needed methods
		@Override
		protected OverlayItem createItem(int i) {
			return mOverlaysItems.get(i);
		}

		@Override
		public int size() {
			return mOverlaysItems.size();
		}

		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			super.draw(canvas, mapView, shadow);
			boundCenterBottom(marker);
		}

		@Override
		protected boolean onTap(int index) {
			// TODO Auto-generated method stub
			OverlayItem itemClicked = mOverlaysItems.get(index);
			GeoPoint p = itemClicked.getPoint();
			Point screenPts = new Point();
			mapView.getProjection().toPixels(p, screenPts);
			startAddrssPopup(p, "Current Location", R.drawable.bubbleicon);
			return true;
		}

		public void addOverlay(OverlayItem overlay) {
			// add item to our overlay
			mOverlaysItems.add(overlay);
			populate();
		}
	}

	/**
	 * This method is used to get the address and show the address popup on screen.
	 * */
	private boolean startAddrssPopup(GeoPoint p, String title, int icon) {
		// GET add fromGEO Point
		Geocoder geoCoder = new Geocoder(mContext,Locale.getDefault());
		List<Address> addresses = null;
		try {
			addresses = geoCoder.getFromLocation(p.getLatitudeE6() / 1E6,
					p.getLongitudeE6() / 1E6, 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String add = "";
		if (addresses!=null && addresses.size() > 0) {
			// for (int i=0; i<addresses.get(0).getMaxAddressLineIndex();i++)
			add += addresses.get(0).getAddressLine(0) + "\n";
			addName = add ;
			Point screenPts = new Point();
			mapView.getProjection().toPixels(p, screenPts);
			Rect rect = new Rect(screenPts.x, screenPts.y + 60, screenPts.x
					+ mapView.getWidth(), screenPts.y + 60);
			if (qa == null || mContext!=null ) {
				qa = new QuickActionWindow(mContext, mapView, rect);
				qa.setTitle(title); // set QA title
				qa.setIcon(icon); // set QA icon
				qa.show(screenPts.x);
				qa.setBackgroundDrawable(null);
			}
		}

		return true;
	}
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(qa != null){
			qa.dismiss();
			qa = null;
		}
		myOverlay.disableMyLocation();
		super.onBackPressed();
	}	
}