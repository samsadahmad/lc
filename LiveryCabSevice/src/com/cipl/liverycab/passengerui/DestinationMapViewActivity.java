package com.cipl.liverycab.passengerui;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TimePicker;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class DestinationMapViewActivity extends MapActivity {

	public static ForgotPasswordParserClass objParser;
	public static ProgressDialog pd;

	private MapView mapView;
	private MapController mapController;
	private LocationManager mLocManager;
	private GeoPoint myCurrentLocation;
	public static Context mContext;
	private LinearLayout zoomLayout;
	private EditText editTextSearch;
	private double latitude, longitude;
	String destinationLat, destinationLong;
	private boolean isPopup = true;
	private String name = "";
	private MyLocationOverlay myOverlay;
	private CustomizedOverlay myCustomizedOverlay;
	private Display display;
	private int screenWidth;
	private int screenHeight;
	private ProgressBar progressBar;
	private String add;
	private OnDateSetListener mDateSetListener = null;
	private OnTimeSetListener mTimeSetListener = null;
	TimePickerDialog timepick;
	DatePickerDialog datepick;
	public static String date_selected;
	public static String time_selected;
	private Calendar c = Calendar.getInstance();
	final int cyear = c.get(Calendar.YEAR);
	final int cmonth = c.get(Calendar.MONTH);
	final int cday = c.get(Calendar.DAY_OF_MONTH);
	final int cHour = c.get(Calendar.HOUR_OF_DAY);
	final int cMin = c.get(Calendar.MINUTE);
	final int cSec = c.get(Calendar.SECOND);
	private String soLat,soLng,disLat,disLng;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.whereiamnow);
		mContext = DestinationMapViewActivity.this;
		mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		progressBar = (ProgressBar) findViewById(R.id.progressBarWhereIamNow);
		
		EditText medittextSearchWhereIamNow = (EditText)findViewById(R.id.edittextSearchWhereIamNow);
		medittextSearchWhereIamNow.setHint(" Type in destination address ");
		
		Button selectDestination = (Button) findViewById(R.id.buttonSelectPickUpWhereIamNow);
		RelativeLayout mrrwhereIamAddress = (RelativeLayout)findViewById(R.id.rrwhereIamAddress);
		LiveryCabStatics.destiMapView = true;
		if (LiveryCabStatics.destiMapView) {
			/*Select destination address*/
			selectDestination.setText("Finish");
			selectDestination.setBackgroundResource(R.drawable.redbutton);
			mrrwhereIamAddress.setBackgroundResource(R.drawable.destinationaddressheading);
			
		}
		// getting map and map controller
		mapView = (MapView) findViewById(R.id.mapviewWhereIamNow);
		mapController = mapView.getController();
		zoomLayout = (LinearLayout) findViewById(R.id.zoomWhereIamNow);
		View zoomView1 = mapView.getZoomControls();
		zoomLayout.addView(zoomView1, new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		mapView.displayZoomControls(true);
		mapController = mapView.getController();
		mapController.setZoom(17);

		editTextSearch = (EditText) findViewById(R.id.edittextSearchWhereIamNow);

		myOverlay = new MyLocationOverlay(this, mapView);
		myOverlay.enableMyLocation();
		mapView.getOverlays().add(myOverlay);

		ImageButton btnsearch = (ImageButton) findViewById(R.id.imageButtonSearchIconWhereIamNow);
		btnsearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(
						editTextSearch.getWindowToken(), 0);
				Geocoder geocoder = new Geocoder(
						DestinationMapViewActivity.this, Locale.getDefault());
				List<Address> addresses;
				try {
					addresses = geocoder.getFromLocationName(editTextSearch
							.getText().toString(), 5);
					if (addresses.size() > 0) {
						myCurrentLocation = new GeoPoint((int) (addresses
								.get(0).getLatitude() * 1E6), (int) (addresses
								.get(0).getLongitude() * 1E6));

						mapController.animateTo(myCurrentLocation);
						mapController.setZoom(17);
						myCustomizedOverlay.addOverlay(new OverlayItem(
								myCurrentLocation, "", ""));
						startAddrssPopup(myCurrentLocation,
								"Click blue arrow to proceed",
								R.drawable.bubbleicon);
						isPopup = true;
						mapView.invalidate();
						editTextSearch.setText("");
					} else {
						AlertDialog.Builder adb = new AlertDialog.Builder(
								DestinationMapViewActivity.this);
						adb.setTitle("Google Map");
						adb.setMessage("Please Provide the Proper Place");
						adb.setPositiveButton("Close", null);
						adb.show();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		((Button) findViewById(R.id.buttonSelectPickUpWhereIamNow))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (LiveryCabStatics.destiMapView) {
							double distance = 0;
							LiveryCabStatics.destinationLatitude = destinationLat;
							LiveryCabStatics.destinationLongitude = destinationLong;
							LiveryCabStatics.destinationAddress = name;
							if(LiveryCabStatics.sourceLatitude == null || LiveryCabStatics.sourceLatitude.equalsIgnoreCase("")){
								soLat = LiveryCabStatics.currentLatitude;
								soLng = LiveryCabStatics.currentLongitude;
							}else{
								soLat = LiveryCabStatics.sourceLatitude;
								soLng = LiveryCabStatics.sourceLongitude;
							}
							if(LiveryCabStatics.destinationLatitude == null || LiveryCabStatics.destinationLatitude.equalsIgnoreCase("")){
								disLat = LiveryCabStatics.currentLatitude;
								disLng = LiveryCabStatics.currentLongitude;
							}else{
								disLat = LiveryCabStatics.destinationLatitude;
								disLng = LiveryCabStatics.destinationLongitude;
							}
							distance = LiveryCabStaticMethods.calculateDistance(soLat.trim(),soLng.trim(),disLat.trim(),disLng.trim());
							LiveryCabStatics.isAddPickup = true;
							if (LiveryCabStatics.isRideLater) {
								mDateSetListener = new DatePickerDialog.OnDateSetListener() {
									// onDateSet method
									@Override
									public void onDateSet(DatePicker view,int year, int monthOfYear,
											int dayOfMonth) {
										date_selected = "" + cday + " "+ String.valueOf(cmonth + 1)
												+ " " + cyear;
										if (date_selected != null) {
											final Calendar c = Calendar
													.getInstance();
											int mYear = c.get(Calendar.YEAR);
											int mMonth = c.get(Calendar.MONTH);
											int mDay = c.get(Calendar.DAY_OF_MONTH);
											Calendar currentDate = Calendar.getInstance();
											currentDate.set(Calendar.DAY_OF_MONTH,mDay);
											currentDate.set(Calendar.MONTH,mMonth);
											currentDate.set(Calendar.YEAR,mYear);
											

											final Calendar selectedDate = Calendar.getInstance();
											selectedDate.set(Calendar.DAY_OF_MONTH,dayOfMonth);
											selectedDate.set(Calendar.MONTH,monthOfYear);
											selectedDate.set(Calendar.YEAR,year);
											
											if (selectedDate.after(currentDate)|| selectedDate.equals(currentDate)) {
												date_selected = ""+ String.valueOf(selectedDate
													.get(Calendar.MONTH))+ " "
														+ selectedDate.get(Calendar.DAY_OF_MONTH)
														+ " "+ selectedDate.get(Calendar.YEAR);
												mTimeSetListener = new OnTimeSetListener() {
													@Override
													public void onTimeSet(TimePicker view,int hourOfDay,
															int minute) {
														time_selected = ""+ cHour + ":"+ cMin + ":"+ cSec;
														
														//Calendar selectedTime = Calendar.getInstance();
														selectedDate.set(Calendar.HOUR_OF_DAY,hourOfDay);
														selectedDate.set(Calendar.MINUTE,minute);
														selectedDate.set(Calendar.SECOND,0);
														selectedDate.set(Calendar.MILLISECOND,0);
														/*Changes by sam**/
														LiveryCabStatics.date_Selected = getFormatedDate(selectedDate.getTimeInMillis());
														if (LiveryCabStatics.isLoginPassenger) {
															finish();
															startActivity(new Intent(DestinationMapViewActivity.this,BookRideActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
														} else{
															finish();
															startActivity(new Intent(DestinationMapViewActivity.this,LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
														}
													}
												};
												timepick = new TimePickerDialog(mContext,mTimeSetListener,cHour, cMin, false);
												timepick.show();
											} else if (selectedDate
													.equals(currentDate)) {
												mTimeSetListener = new OnTimeSetListener() {
													@Override
													public void onTimeSet(TimePicker view,int hourOfDay,
															int minute) {
														time_selected = ""+ cHour + ":"+ cMin + ":"
																+ cSec;
														final Calendar cc = Calendar.getInstance();
														int mYearc = cc.get(Calendar.YEAR);
														int mMonthc = cc.get(Calendar.MONTH);
														int mDayc = cc.get(Calendar.DAY_OF_MONTH);
														int HOUR = cc.get(Calendar.HOUR_OF_DAY);
														int MINUTE = cc.get(Calendar.MINUTE);

														Calendar currentTime = Calendar.getInstance();
														currentTime.set(Calendar.HOUR_OF_DAY,HOUR);
														currentTime.set(Calendar.MINUTE,MINUTE);
														currentTime.set(Calendar.SECOND,0);
														currentTime.set(Calendar.MILLISECOND,0);

														Calendar selectedTime = Calendar.getInstance();
														selectedTime.set(Calendar.HOUR_OF_DAY,hourOfDay);
														selectedTime.set(Calendar.MINUTE,minute);
														selectedTime.set(Calendar.SECOND,0);
														selectedTime.set(Calendar.MILLISECOND,0);
														/*Changes by sam**/
														LiveryCabStatics.date_Selected = getFormatedDate(selectedTime.getTimeInMillis());
														if (selectedTime.after(currentTime)) {
															finish();
															startActivity(new Intent(DestinationMapViewActivity.this,BookRideActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
														} else {
															LiveryCabStaticMethods.showAlert(mContext,"Alert!!","Enter a valid time",
																R.drawable.attention,true,false,"Ok",null,
																			null,null);
														}
													}
												};
												timepick = new TimePickerDialog(mContext,mTimeSetListener,cHour, cMin, false);
												timepick.show();
											} else {
												LiveryCabStaticMethods.showAlert(mContext,"Alert!!","Back date is not allowed",
													R.drawable.attention,true, false,"Ok", null,null, null);
											}
										}

									}
								};
								datepick = new DatePickerDialog(mContext,
										mDateSetListener, cyear, cmonth, cday);
								datepick.show();
							} else if (LiveryCabStatics.isLoginPassenger) {
								finish();
								startActivity(new Intent(DestinationMapViewActivity.this,BookRideActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
							} else{
								finish();
								startActivity(new Intent(DestinationMapViewActivity.this,LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
							}
						} else {
							startActivity(new Intent(DestinationMapViewActivity.this,DestinationAddressActivity.class));
						}
					}
				});
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void getAddress(double lat, double lng) {
		Geocoder geocoder = new Geocoder(DestinationMapViewActivity.this,
				Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
			if (!addresses.isEmpty()) {
				Address obj = addresses.get(0);
				add = obj.getAddressLine(0);

				add = add + "," + obj.getCountryName();
				add = add + "," + obj.getCountryCode();
				add = add + "," + obj.getAdminArea();
				add = add + "," + obj.getPostalCode();
				add = add + "," + obj.getSubAdminArea();
				add = add + "," + obj.getLocality();
				add = add + "," + obj.getSubThoroughfare();
				name = add;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	private class MyLocationOverlay extends
			com.google.android.maps.MyLocationOverlay {
		// For storing our location manager
		LocationManager myLocationManager;
		Context mContext;

		public MyLocationOverlay(Context context, MapView mapView) {
			super(context, mapView);
			mContext = context;
			myLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		}

		@Override
		public synchronized void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			super.onLocationChanged(location);
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			if (location != null) {
				if (isPopup) {
					progressBar.setVisibility(ProgressBar.GONE);
					myCurrentLocation = new GeoPoint((int) (latitude * 1E6),
							(int) (longitude * 1E6));
					myCustomizedOverlay = new CustomizedOverlay(
							DestinationMapViewActivity.this,
							DestinationMapViewActivity.this.getResources()
									.getDrawable(R.drawable.bluepin));
					myCustomizedOverlay.addOverlay(new OverlayItem(
							myCurrentLocation, "", ""));
					mapView.getOverlays().add(myCustomizedOverlay);
					mapController.animateTo(myCurrentLocation);
					getAddress(latitude, longitude);
					this.disableMyLocation();
					isPopup = false;
				}
			}
		}
	}

	private class CustomizedOverlay extends ItemizedOverlay<OverlayItem> {

		private ArrayList<OverlayItem> mOverlaysItems = new ArrayList<OverlayItem>();
		private Context mContext;
		private Drawable marker;
		private OverlayItem inDrag = null;
		private int xDragImageOffset = 0;
		private int yDragImageOffset = 0;
		private int xDragTouchOffset = 0;
		private int yDragTouchOffset = 0;
		private ImageView dragImage = null;

		public CustomizedOverlay(Context context, Drawable defaultMarker) {
			super(boundCenterBottom(defaultMarker));
			mContext = context;
			marker = defaultMarker;
			dragImage = (ImageView) findViewById(R.id.dragimage);
			xDragImageOffset = dragImage.getDrawable().getIntrinsicWidth() / 2;
			yDragImageOffset = dragImage.getDrawable().getIntrinsicHeight();
		}

		// needed methods
		@Override
		protected OverlayItem createItem(int i) {
			return mOverlaysItems.get(i);
		}

		@Override
		public int size() {
			return mOverlaysItems.size();
		}

		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			super.draw(canvas, mapView, shadow);
			boundCenterBottom(marker);
		}

		public void addOverlay(OverlayItem overlay) {
			// add item to our overlay
			mOverlaysItems.add(overlay);
			populate();
		}

		@Override
		public boolean onTouchEvent(MotionEvent event, MapView mapView) {
			final int action = event.getAction();
			final int x = (int) event.getX();
			final int y = (int) event.getY();
			boolean result = false;

			if (action == MotionEvent.ACTION_DOWN) {
				for (OverlayItem item : mOverlaysItems) {
					Point p = new Point(0, 0);
					DestinationMapViewActivity.this.mapView.getProjection()
							.toPixels(item.getPoint(), p);
					if (hitTest(item, marker, x - p.x, y - p.y)) {
						result = true;
						inDrag = item;
						mOverlaysItems.remove(inDrag);
						populate();

						xDragTouchOffset = 0;
						yDragTouchOffset = 0;

						setDragImagePosition(p.x, p.y);
						dragImage.setVisibility(View.VISIBLE);

						xDragTouchOffset = x - p.x;
						yDragTouchOffset = y - p.y;

						break;
					}
				}
			} else if (action == MotionEvent.ACTION_MOVE && inDrag != null) {
				setDragImagePosition(x, y);
				result = true;
			} else if (action == MotionEvent.ACTION_UP && inDrag != null) {
				dragImage.setVisibility(View.GONE);
				GeoPoint pt = DestinationMapViewActivity.this.mapView
						.getProjection().fromPixels(x - xDragTouchOffset,
								y - yDragTouchOffset);
				mOverlaysItems.remove(myOverlay);
				myCustomizedOverlay.addOverlay(new OverlayItem(pt, "", ""));
				startAddrssPopup(pt, "Destination address",
						R.drawable.blue_arrow);
				destinationLat = "" + (pt.getLatitudeE6()) / 1E6;
				destinationLong = "" + (pt.getLongitudeE6()) / 1E6;
				populate();
				inDrag = null;
				result = true;
			}

			return (result || super.onTouchEvent(event, mapView));
		}

		private void setDragImagePosition(int x, int y) {
			RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) dragImage
					.getLayoutParams();

			lp.setMargins(x - xDragImageOffset - xDragTouchOffset, y
					- yDragImageOffset - yDragTouchOffset, 0, 0);
			dragImage.setLayoutParams(lp);
		}
	}

	private boolean startAddrssPopup(GeoPoint p, String title, int icon) {
		// GET add fromGEO Point
		Geocoder geoCoder = new Geocoder(getApplicationContext(),
				Locale.getDefault());
		List<Address> addresses = null;
		try {
			addresses = geoCoder.getFromLocation(p.getLatitudeE6() / 1E6,
					p.getLongitudeE6() / 1E6, 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String add = "";
		if (addresses != null && addresses.size() > 0) {
			// for (int i=0; i<addresses.get(0).getMaxAddressLineIndex();i++)
			add += addresses.get(0).getAddressLine(0) + "\n";
			name = add;
			Point screenPts = new Point();
			mapView.getProjection().toPixels(p, screenPts);
			Rect rect = new Rect(screenPts.x, screenPts.y + 80, screenPts.x
					+ mapView.getWidth(), screenPts.y + 70);
			QuickActionLocation qa = new QuickActionLocation(mContext, mapView,
					rect);
			qa.setTitle(title); // set QA title
			if (myCurrentLocation != null)
				qa.setText(name);
			else
				qa.setText("");
			qa.show(screenPts.x);
		}
		return true;
	}
	private String getFormatedDate(long timeInMillis)  {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat();
			Date d = new Date(timeInMillis);
			sdf.applyPattern("EEE MMM d,yyyy h:mm a");
			String formatedDate = sdf.format(d);
			return formatedDate;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
}