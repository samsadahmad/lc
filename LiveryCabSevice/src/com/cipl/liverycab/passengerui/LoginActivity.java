package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.cipl.liverycab.driverui.AssignedJobListActivity;
import com.cipl.liverycab.driverui.DriverProfileActivity;
import com.cipl.liverycab.driverui.DriverSignUpActivity;
import com.cipl.liverycab.driverui.JobListMapViewActivity;
import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.parserclasses.LoginParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;
import com.google.android.gcm.GCMBroadcastReceiver;
import com.google.android.gcm.GCMRegistrar;

public class LoginActivity extends Activity implements OnClickListener{

	private EditText userNameEditText, passwordEditText;
	private LoginParserClass objLoginParser;
	private ForgotPasswordParserClass objParser;
	private String message = "";
	private ProgressDialog pd;
	private String userType="";
	private String mUserName,mPassword,deviceToken,userId,passId,driverId;
	private String TAG = "GCM Already register";
	private String SENDER_ID ="626040078258";
	
	public static final String ACTION_UPDATE = "ACTION_UPDATE";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		LiveryCabStatics.context = this;
		
		/**registering the receiver*/
		registerReceiver(UpdateList, new IntentFilter(ACTION_UPDATE));

		initControls();
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(UpdateList);
		GCMRegistrar.onDestroy(LoginActivity.this);

	}
	
	
	private void initControls() {
		// TODO Auto-generated method stub
		TextView mtvLoginType = (TextView)findViewById(R.id.tvLoginType);
		
		if(LiveryCabStatics.userType.equalsIgnoreCase("passenger")){
			mtvLoginType.setText("(Passenger Login)");
		}else{
			mtvLoginType.setText("(Driver Login)");
		}
		
		Button menuBtn = (Button)findViewById(R.id.buttonMenuLogin);
		menuBtn.setOnClickListener(this);
		
		TextView mtvForgot = (TextView)findViewById(R.id.textviewForgotPasswordLogin);
		mtvForgot.setOnClickListener(this);
		
		TextView mtvSignUpLogin = (TextView)findViewById(R.id.textviewSignUpLogin);
		mtvSignUpLogin.setOnClickListener(this);
		
		Button mbtnLogin = (Button)findViewById(R.id.buttonLogin);
		mbtnLogin.setOnClickListener(this);
		
		passwordEditText = (EditText)findViewById(R.id.edittextPasswordLogin);
		userNameEditText = (EditText)findViewById(R.id.edittextUsernameLogin);
		
		/**For Device ID and Device Token*/
		TelephonyManager tManager = (TelephonyManager)getSystemService
				(Context.TELEPHONY_SERVICE);
		LiveryCabStatics.DeviceID = tManager.getDeviceId();
		if(LiveryCabStatics.isDriver)	menuBtn.setVisibility(Button.GONE);
		
		
		passId = getPreferences("PASS_ID");
		driverId = getPreferences("DRIVER_ID");
		if(userType.equalsIgnoreCase(LiveryCabStatics.userType) && userType.equalsIgnoreCase("passenger")){
			if(passId.length() > 0){
				
				LiveryCabStatics.isLoginPassenger = true;				
				LiveryCabStatics.passengerId = passId;
				LiveryCabStatics.userId = passId;
				//Delete data from shared prefrence
				deletePreferences("DRIVER_ID");
				LiveryCabStatics.isLoginDriver = false;
				
				if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("History"))
					startActivity(new Intent(LoginActivity.this,HistoryActivity.class));
				else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("MapView"))
					startActivity(new Intent(LoginActivity.this,MapViewActivity.class));
				else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("Profile"))
					startActivity(new Intent(LoginActivity.this,PassengerProfileActivity.class));
				else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("Notification"))
					startActivity(new Intent(LoginActivity.this,NotificationActivity.class));
				else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("PassengerMenu"))
					startActivity(new Intent(LoginActivity.this,PassengerMenuActivity.class));
				else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("BOOKED_STATUS"))
					startActivity(new Intent(LoginActivity.this,PassengerBookedRideActivity.class));
				else {
					startActivity(new Intent(LoginActivity.this,BookRideActivity.class).setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
				}
				finish();
			}
		}else if(userType.equalsIgnoreCase(LiveryCabStatics.userType) && 
				userType.equalsIgnoreCase("driver")){
			if(driverId.length() > 0){
				LiveryCabStatics.isLoginDriver = true;
				LiveryCabStatics.driverId = driverId;
				LiveryCabStatics.userId = driverId;
				//Delete data from shared prefrence
				deletePreferences("PASS_ID");
				deletePreferences("TRUE_PASSENGER");				
				LiveryCabStatics.isLoginPassenger = false;
				
				if(LiveryCabStatics.driverMenuOptionId.equalsIgnoreCase("AssignedJobList"))
					startActivity(new Intent(LoginActivity.this,AssignedJobListActivity.class));
				else if(LiveryCabStatics.driverMenuOptionId.equalsIgnoreCase("Profile"))
					startActivity(new Intent(LoginActivity.this,DriverProfileActivity.class));
				else 
					startActivity(new Intent(LoginActivity.this,JobListMapViewActivity.class));
				finish();
			}			
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.buttonMenuLogin:
			btnMenu();
			break; 
		case R.id.textviewForgotPasswordLogin:
			txtForgotLogin();
			break;
		case R.id.textviewSignUpLogin:
			txtSignUpLogin();
			break; 
		case R.id.buttonLogin:
			validation();
			break; 

		default:
			break;
		}
	}
	
	private void txtSignUpLogin() {
		// TODO Auto-generated method stub
		if(LiveryCabStatics.isPassenger)
			startActivity(new Intent(LoginActivity.this,PassengerSignUpActivity.class));
		else
			startActivity(new Intent(LoginActivity.this,DriverSignUpActivity.class));
	}


	private void txtForgotLogin() {
		// TODO Auto-generated method stub
		startActivity(new Intent(LoginActivity.this,ForgotPasswordActivity.class));
	}


	private void btnMenu() {
		// TODO Auto-generated method stub
		startActivity(new Intent(LoginActivity.this,PassengerMenuActivity.class)
		.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
	}


	protected void validation() {
		// TODO Auto-generated method stub
		//For GCM
		deviceToken = getPreferences("DEVICE_TOKEN");		
		
		InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(
				userNameEditText.getWindowToken(), 0);
			mUserName = userNameEditText.getText().toString();
			mPassword = passwordEditText.getText().toString();
			if(mUserName.equalsIgnoreCase("")){
				LiveryCabStaticMethods.showAlert(LoginActivity.this,"Login failed","Please enter your email address"
					,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							userNameEditText.requestFocus();
						}
				},null,null);
			}else if (!(LiveryCabStaticMethods.isEmail(mUserName))){
				LiveryCabStaticMethods.showAlert(LoginActivity.this,"Login failed","Enter a valid email address"
					,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							passwordEditText.requestFocus();
						}
				},null,null);
		}else if(mPassword.equalsIgnoreCase("")){
				LiveryCabStaticMethods.showAlert(LoginActivity.this,"Login failed","Please enter your password"
					,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							passwordEditText.requestFocus();
						}
				},null,null);
			}else if ((mUserName.equalsIgnoreCase("")) && (mPassword.equalsIgnoreCase(""))){
				LiveryCabStaticMethods.showAlert(LoginActivity.this,"Login failed","Missing username or password"
					,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							userNameEditText.requestFocus();
						}
				},null,null);
			}else { 
				if(deviceToken.equalsIgnoreCase("")){
					GCMRegistrar.checkDevice(LoginActivity.this);
			        GCMRegistrar.checkManifest(LoginActivity.this);
			        
			        GCMRegistrar.register(LoginActivity.this, SENDER_ID);
			        
			        pd = LiveryCabStaticMethods.returnProgressBar(LoginActivity.this);
					
				}else{
					System.out.println("Data from shared");
					pd = LiveryCabStaticMethods.returnProgressBar(LoginActivity.this);
					doLoginFromServer();
				}
		}
	}

	private void doLoginFromServer() {
		// TODO Auto-generated method stub
		if(LiveryCabStaticMethods.isInternetAvailable(LoginActivity.this)){
			//pd = LiveryCabStaticMethods.returnProgressBar(LoginActivity.this);
			objLoginParser = new LoginParserClass(UrlStatics
						.getLoginUrl(mUserName,mPassword,LiveryCabStatics.DeviceID,deviceToken,
						LiveryCabStatics.deviceType, LiveryCabStatics.userType), pd);
			objLoginParser.start();
			pd.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					if(objLoginParser.actualDismiss){
						if(objLoginParser.loginDataObj!=null)
							message = objLoginParser.loginDataObj.message;
							if(message.equalsIgnoreCase("sucess"))
							{
								userNameEditText.setText("");
								passwordEditText.setText("");
								String userStatus = LiveryCabStatics.userType;
								if(LiveryCabStatics.userType.equalsIgnoreCase("passenger")){
									userType = "Passenger";
								}else{
									userType = "Driver";
								}
								
								/**Data commit in SharedPrefrence*/
								savePreferences("USER_TYPE",""+LiveryCabStatics.userType);
								
								if(LiveryCabStatics.userType.equalsIgnoreCase("passenger")){
									
									LiveryCabStatics.isLoginPassenger = true;
									LiveryCabStatics.passengerId = objLoginParser.loginDataObj.userID;
									LiveryCabStatics.userId = objLoginParser.loginDataObj.userID;
									//Save data into shared prefrence
									savePreferences("PASS_ID",""+objLoginParser.loginDataObj.userID);
									savePreferences("LOGIN","TRUE_PASSENGER");
									
									if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("History"))
										startActivity(new Intent(LoginActivity.this,HistoryActivity.class));
									else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("MapView"))
										startActivity(new Intent(LoginActivity.this,MapViewActivity.class));
									else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("Profile"))
										startActivity(new Intent(LoginActivity.this,PassengerProfileActivity.class));
									else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("Notification"))
										startActivity(new Intent(LoginActivity.this,NotificationActivity.class));
									else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("PassengerMenu"))
										startActivity(new Intent(LoginActivity.this,PassengerMenuActivity.class));
									else if(LiveryCabStatics.passengerMenuOptionId.equalsIgnoreCase("BOOKED_STATUS"))
										startActivity(new Intent(LoginActivity.this,PassengerBookedRideActivity.class));
									else {
										startActivity(new Intent(LoginActivity.this,BookRideActivity.class));
									}
									finish();
								}else{	
									LiveryCabStatics.isLoginDriver = true;
									LiveryCabStatics.userId = objLoginParser.loginDataObj.userID;
									LiveryCabStatics.driverId = objLoginParser.loginDataObj.userID;
									//Save data into shared prefrence
									savePreferences("DRIVER_ID",""+objLoginParser.loginDataObj.userID);
									
									if(LiveryCabStatics.driverMenuOptionId.equalsIgnoreCase("AssignedJobList"))
										startActivity(new Intent(LoginActivity.this,AssignedJobListActivity.class));
									else if(LiveryCabStatics.driverMenuOptionId.equalsIgnoreCase("Profile"))
										startActivity(new Intent(LoginActivity.this,DriverProfileActivity.class));
									else 
										startActivity(new Intent(LoginActivity.this,JobListMapViewActivity.class));
									finish();
								}
							}else if(message.equalsIgnoreCase("missing_device_id_token_type")){
								
								DialogInterface.OnClickListener deviceTokenMissingLitener = new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										
										deviceToken = checkTokanId();
								        doLoginFromServer();
									}
								};
								LiveryCabStaticMethods.showAlert(LoginActivity.this, 
										"Login failed,", "Thanks For Joining Liverycab ! Please click OK to proceed",R.drawable.attention,true,true,"OK", 
										deviceTokenMissingLitener, null, null);
								
								
							}else if(message.equalsIgnoreCase("Deactive")){
								LiveryCabStaticMethods.showAlert(LoginActivity.this, 
										"Login failed", "Admin has deactivated your account!",R.drawable.attention,true,false,"Ok", 
										null, null, null);
							}else if(message.equalsIgnoreCase("Invalid")){
								LiveryCabStaticMethods.showAlert(LoginActivity.this, 
										"Login failed", "Invalid user",R.drawable.attention,true,false,"Ok", 
										null, null, null);
							}else if(message.equalsIgnoreCase("Invalid_user_type")){
								LiveryCabStaticMethods.showAlert(LoginActivity.this, 
										"Login failed", "Invalid user",R.drawable.attention,true,false,"Ok", 
										null, null, null);
							}else{
								LiveryCabStaticMethods.showAlert(LoginActivity.this, 
										"Login failed", "Try again",R.drawable.attention,true,false,"Ok", 
										null, null, null);
							}
					}
				}
			});
		}
	}


	/** Method used to get Shared Preferences */
	public SharedPreferences getPreferences() 
	{
	    return getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);
	}
	
	/** Method used to save Preferences */
	public void savePreferences(String key, String value) 
	{
	    SharedPreferences sharedPreferences = getPreferences();
	    SharedPreferences.Editor editor = sharedPreferences.edit();
	    editor.putString(key, value);
	    editor.commit();
	}
	
	/**Method used to load Preferences */
	public String getPreferences(String key) 
	{
	    try {
	        SharedPreferences sharedPreferences = getPreferences();
	        String strSavedMemo = sharedPreferences.getString(key, "");
	        return strSavedMemo;
	    } catch (NullPointerException nullPointerException) 
	    {
	        Log.e("Error caused at  LiveryCab loadPreferences method",
	                ">======>" + nullPointerException);
	        return null;
	    }
	}
	
	/** Method used to delete Preferences */
	public boolean deletePreferences(String key)
	{
	    SharedPreferences.Editor editor=getPreferences().edit();
	    editor.remove(key).commit();
	    return false;
	}
	
	/**GCM Controller here*/
	/**GCM Controller here*/
	 public String checkTokanId()
	 {
         String gcmregID = GCMRegistrar.getRegistrationId(LoginActivity.this);
         savePreferences("DEVICE_TOKEN",""+gcmregID);
		return gcmregID;
	 }
	 
	 BroadcastReceiver UpdateList = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				deviceToken = GCMRegistrar.getRegistrationId(LoginActivity.this);;
				savePreferences("DEVICE_TOKEN",""+deviceToken);
				doLoginFromServer();
			}
		};
}
