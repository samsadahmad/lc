package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.cipl.liverycab.parserclasses.LoginParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class ForgotPasswordActivity extends Activity {
	
	private EditText emailEditText;
	private Button sendButton;
	private ProgressDialog pd;
	private LoginParserClass objParserClass;
	private String message ="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgotpassword);
		emailEditText = (EditText)findViewById(R.id.edittextEmailForgotPassword);
		sendButton = (Button)findViewById(R.id.buttonSendForgotPassword);

		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager inputManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(emailEditText.getWindowToken(), 0);
				String emailAddress = emailEditText.getText().toString();
				if (emailAddress.equalsIgnoreCase("")) {
					LiveryCabStaticMethods.showAlert(ForgotPasswordActivity.this,"Error Occured!","Enter an email address",R.drawable.attention, true, false, "Ok",null,null,null);
				}else if (!(LiveryCabStaticMethods.isEmail(emailAddress))) {
					LiveryCabStaticMethods.showAlert(ForgotPasswordActivity.this,"Error Occured!","Enter a valid email address",R.drawable.attention, true, false, "Ok",null,null,null);
				}else{
					pd = LiveryCabStaticMethods.returnProgressBar(ForgotPasswordActivity.this);
					objParserClass = new LoginParserClass(UrlStatics.getForgotPasswordUrl
							(emailAddress,LiveryCabStatics.userType), pd);
					objParserClass.start();
					pd.setOnDismissListener(new OnDismissListener() {
						
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							if(objParserClass.actualDismiss){
								if(objParserClass.loginDataObj!=null)
									message = objParserClass.loginDataObj.message;
								if(message.equalsIgnoreCase("check_mail")){
									LiveryCabStaticMethods.showAlert(ForgotPasswordActivity.this,
											"Success","Please check your email",R.drawable.success, true, false, 
											"Ok", new DialogInterface.OnClickListener() 
									{
										@Override
										public void onClick(DialogInterface arg0, int arg1) {
											// TODO Auto-generated method stub
											startActivity(new Intent(ForgotPasswordActivity.this,LoginActivity.class));
											finish();
										}
									},null,null);
								}else if(message.equalsIgnoreCase("invalid_email!")){
									LiveryCabStaticMethods.showAlert(ForgotPasswordActivity.this,"Error Occured!","Enter valid email address"
												,R.drawable.attention, true, false, "Ok", null,null,null);
								}else if(message.equalsIgnoreCase("Invalid_user_type")){
									LiveryCabStaticMethods.showAlert(ForgotPasswordActivity.this,"Error Occured!","Invalid user"
											,R.drawable.attention, true, false, "Ok", null,null,null);
								}else
									LiveryCabStaticMethods.showAlert(ForgotPasswordActivity.this,"Error Occured!","Try again"
											,R.drawable.attention, true, false, "Ok", null,null,null);
							}
						}
					});
				}
			}
		});
	}
}
