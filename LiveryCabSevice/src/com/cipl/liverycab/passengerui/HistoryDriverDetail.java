package com.cipl.liverycab.passengerui;

import com.cipl.liverycab.dataclasses.DriverProfileParser;
import com.cipl.liverycab.driverui.DriverProfileActivity;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class HistoryDriverDetail extends Activity {

	private ImageView driverimgImageView,carImageimgView;
	private TextView driverNameTxtView,emailTxtView,dlPlateNumberTxtView,
			numberOfReviewsTxtView,phoneNumTxtView;
	public static DriverProfileParser objectParser;
	private String driverId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.historydriverdetail);
		
		Bundle extras = getIntent().getExtras();
		driverId = (String) (extras!=null ? extras.getString("driverID"):"");
		
		driverimgImageView = (ImageView)findViewById(R.id.imageviewDriverImageHistoryDriverDetail);
		carImageimgView = (ImageView)findViewById(R.id.imageviewCarImageHistoryHistoryDriverDetail);
		driverNameTxtView = (TextView)findViewById(R.id.textviewDriverNameHistoryDriverDetail);
		emailTxtView = (TextView)findViewById(R.id.textviewLicencePlateNumberHistoryDriverDetail);
		dlPlateNumberTxtView = (TextView)findViewById(R.id.textviewBidAmountHistoryDriverDetail);
		numberOfReviewsTxtView = (TextView)findViewById(R.id.textviewETATimeHistoryDriverDetail);
		phoneNumTxtView = (TextView)findViewById(R.id.textviewDriverReviewHistoryDriverDetail);
		
		objectParser = new DriverProfileParser(UrlStatics.getProfile(
				LiveryCabStatics.driverId,"driver"),
				HistoryDriverDetail.this);
		objectParser.start();
		
		LiveryCabStaticMethods.returnProgressBar(HistoryDriverDetail.this)
		.setOnDismissListener(new OnDismissListener() {
		@Override
		public void onDismiss(DialogInterface dialog) {
			// TODO Auto-generated method stub
			driverNameTxtView.setText(objectParser.dataObject.firstName+" "+objectParser.dataObject.lastName);
			emailTxtView.setText(objectParser.dataObject.email);
			dlPlateNumberTxtView.setText(objectParser.dataObject.dlNumber);
			numberOfReviewsTxtView.setText("("+objectParser.dataObject.numberOfReviews+")");
			phoneNumTxtView.setText(objectParser.dataObject.phone);
			if(objectParser.dataObject.driverImage!=null)
				driverimgImageView.setImageDrawable(objectParser.dataObject.driverImage);
			if(objectParser.dataObject.carImage!=null)
				carImageimgView.setImageDrawable(objectParser.dataObject.carImage);
			
		}
	});
	}
}
