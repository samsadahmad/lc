 package com.cipl.liverycab.passengerui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.cipl.liverycab.dataclasses.PopularAddressCategoryDataClass;
import com.cipl.liverycab.parserclasses.PopularAddressCategoryParserClass;
import com.cipl.liverycab.parserclasses.PopularAddressParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class PopularPickupAddressActivity extends Activity {

	private Context mContext; 
	private PopularAddressParserClass parserObject;
	private PopularAddressCategoryParserClass parserObjectCat;
	private ListView popularAddList,list;
	private int textlength = 0;
	private int textlength2 = 0;
	private String [] from = new String[]{"address","distance"};
	private int [] to = new int[]{R.id.textviewAddressDetailRecentAddresss,R.id.textviewMilesRecentAddresss};
	private Button searchButton;
	private EditText searchEditText,mEditBox;
	private Builder dialog;
	private CategoryGridAdapter mCategoryGridAdapter;
	private AlertDialog alert;
	private ProgressDialog mProgressDialog;
	private String CAT_ID = "";
	private SimpleAdapter adapter;
	final List<HashMap<String, String>> fillMaps = new ArrayList<HashMap<String, String>>();
	private final ArrayList<PopularAddressCategoryDataClass> popularAddCatListSearch = new ArrayList<PopularAddressCategoryDataClass>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popularpickup);
		
		mContext = this;
		popularAddList = (ListView)findViewById(R.id.listviewPopularPickupAddress);		
		searchButton = (Button) findViewById(R.id.btnSearchPopularPickupAddress);
		searchEditText = (EditText) findViewById(R.id.edittextSearchPopularPickupAddress);
		
		/**parser object for popular address category*/
		parserObjectCat = new PopularAddressCategoryParserClass(UrlStatics.getFourSquareApiCat());
		
		parserObject = new  PopularAddressParserClass();
		/**Thread start for popular pickup*/
		mProgressDialog = ProgressDialog.show(PopularPickupAddressActivity.this, "",
				"Loading. Please wait...", true, false);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		if(LiveryCabStatics.currentLatitude.equalsIgnoreCase("0.0") &&
				LiveryCabStatics.currentLongitude.equalsIgnoreCase("0.0")){
			LiveryCabStaticMethods.showAlert(PopularPickupAddressActivity.this, 
					"Error Occured!!","Current location not availabale",R.drawable.attention,true,false,"Ok",null,null,null);
		}else{			
			 new Thread(new Runnable() {
					@Override
					public void run() {						
						parserObjectCat.fetchPopularAddressCategory();	
						mpopularPickupHandler.post(new Runnable() {
							@Override
							public void run() {
								parserObject.fetchPopularAddress(UrlStatics
										.getFourSquareApi(LiveryCabStatics.currentLatitude+","
												+LiveryCabStatics.currentLongitude,CAT_ID));
								mpopularPickupHandler.sendEmptyMessage(0);
							}
						});
					}
				}).start();	 
		}
		
		
		/**set click on popular pickup address*/
		popularAddList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				LiveryCabStatics.pickupAddress = LiveryCabStatics.popularAddList.get(position).address; 
				LiveryCabStatics.sourceLatitude = LiveryCabStatics.popularAddList.get(position).latitude;
				LiveryCabStatics.sourceLongitude = LiveryCabStatics.popularAddList.get(position).longitude;
				startActivity(new Intent(PopularPickupAddressActivity.this,DestinationAddressActivity.class));
			}
		});
		
		/**Search address a/c to category*/	
		searchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// custom dialog
				final Dialog dialog = new Dialog(PopularPickupAddressActivity.this);
				dialog.setContentView(R.layout.spinner_dialog);
				dialog.setTitle("Choose Category");
				final EditText msearchBox = (EditText) dialog.findViewById(R.id.et_searchBox);
				msearchBox.setHint("Search");
				msearchBox.setSingleLine(true);
				list = (ListView) dialog.findViewById(R.id.lv_spinnerItems);
				
				list.setBackgroundColor(0xFFFFFF);
				list.setCacheColorHint(00000000);		
				list.setFastScrollEnabled(true);
				mCategoryGridAdapter = new CategoryGridAdapter(PopularPickupAddressActivity.this,LiveryCabStatics.popularAddCatList);			
				list.setAdapter(mCategoryGridAdapter);
				
				//Category text box search here..
				msearchBox.addTextChangedListener(new TextWatcher() {
					@Override
					public void onTextChanged(CharSequence s, int start, int before,
							int count) {
						popularAddCatListSearch.clear();
						textlength2 = msearchBox.getText().toString().trim().length();
		                 for (int i = 0; i < LiveryCabStatics.popularAddCatList.size(); i++) {
		                	 if (textlength2 <= LiveryCabStatics.popularAddCatList.get(i).catName.length()) {
		                		 if (msearchBox.getText().toString() .equalsIgnoreCase(
		                              (String) LiveryCabStatics.popularAddCatList.get(i).catName.subSequence(0,textlength2))) {
		                			 PopularAddressCategoryDataClass mccc = new PopularAddressCategoryDataClass();
		                			 mccc.catId = LiveryCabStatics.popularAddCatList.get(i).catId;
		                			 mccc.catName = LiveryCabStatics.popularAddCatList.get(i).catName;
		                			 mccc.message = LiveryCabStatics.popularAddCatList.get(i).message;
		                			 mccc.type = LiveryCabStatics.popularAddCatList.get(i).type;
		                			 popularAddCatListSearch.add(mccc);		                			 
		                         }
		                     }
		                 }
		                 if(!popularAddCatListSearch.isEmpty()){		                	 
		                	 CategoryGridAdapter mmCategoryGridAdapter = new CategoryGridAdapter(PopularPickupAddressActivity.this,popularAddCatListSearch);		                	 
		                	 list.setAdapter(mmCategoryGridAdapter);
		                 }               
					}
					@Override
					public void afterTextChanged(Editable arg0) {
						// TODO Auto-generated method stub
						
					}
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub
						
					}
				});
				
				//Click on list category item..
				list.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int pos, long arg3) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						if(textlength2 > 0){
							searchButton.setText(popularAddCatListSearch.get(pos).catName.toString());
							CAT_ID = popularAddCatListSearch.get(pos).catId;
						}else{
							searchButton.setText(LiveryCabStatics.popularAddCatList.get(pos).catName.toString());
							CAT_ID = LiveryCabStatics.popularAddCatList.get(pos).catId;
						}
						
						
						searchEditText.setText("");
						
						/**Thread start for popular pickup*/
						mProgressDialog = ProgressDialog.show(PopularPickupAddressActivity.this, "",
								"Loading. Please wait...", true, false);
						mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
						
						new Thread(){
							public void run() {
								parserObject.fetchPopularAddress(UrlStatics
										.getFourSquareApi(LiveryCabStatics.currentLatitude+","
												+LiveryCabStatics.currentLongitude,CAT_ID));
								mpopularPickupAfterHandler.sendEmptyMessage(0);
							};
						}.start();	
					}
				});
				dialog.show();
			}
		});
		
		
		/**Searching for edittext box here*/
		searchEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				 fillMaps.clear();
                 textlength = searchEditText.getText().length();
                 for (int i = 0; i < LiveryCabStatics.popularAddList.size(); i++) {
                	 if (textlength <= LiveryCabStatics.popularAddList.get(i).address.length()) {
                		 if (searchEditText.getText().toString() .equalsIgnoreCase(
                              (String) LiveryCabStatics.popularAddList.get(i).address.subSequence(0,textlength))) {
                              HashMap<String, String> map = new HashMap<String, String>();
                              map.put("address type", LiveryCabStatics.popularAddList.get(i).addressType);
                              map.put("address", LiveryCabStatics.popularAddList.get(i).address);
                              map.put("distance",LiveryCabStatics.popularAddList.get(i).distance);
                              fillMaps.add(map);
                         }
                     }
                 }
                 if(!fillMaps.isEmpty()){
                	 SimpleAdapter adapter = new SimpleAdapter(PopularPickupAddressActivity.this, fillMaps, R.layout.recentaddresscustomcell,
                                 from, to);
                	 popularAddList.setAdapter(adapter);
                 }               
			}
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	/**All Searching End here*/
	/**List Adapter for alert box list*/
	public class CategoryGridAdapter extends BaseAdapter {
		private LayoutInflater inflater = null;
		private ArrayList<PopularAddressCategoryDataClass> popularAddCatListDM;

		public CategoryGridAdapter(Context context,ArrayList<PopularAddressCategoryDataClass> popularAddCatListD) {
			inflater = LayoutInflater.from(context);
			this.popularAddCatListDM = popularAddCatListD;
		}

		public int getCount() {
			return popularAddCatListDM.size();
		}

		public Object getItem(int paramInt) {
			return paramInt;
		}

		public long getItemId(int paramInt) {
			return paramInt;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			EventViewHolder holder;

			if (convertView == null) {
				convertView = inflater.inflate(R.layout.customcategorylayout,
						null);
				holder = new EventViewHolder();
				holder.mtvPopulatCategory = (TextView) convertView
						.findViewById(R.id.tvPopulatCategory);
				
				convertView.setTag(holder);
			} else {
				holder = (EventViewHolder) convertView.getTag();
			}
			if(popularAddCatListDM.get(position).type.toString().equalsIgnoreCase("1")){
				holder.mtvPopulatCategory
				.setText("----"+""+popularAddCatListDM.get(position).catName);
			}else if(popularAddCatListDM.get(position).type.toString().equalsIgnoreCase("2")){
				holder.mtvPopulatCategory
				.setText("--------"+""+popularAddCatListDM.get(position).catName);
			}else{
				holder.mtvPopulatCategory
				.setText(""+popularAddCatListDM.get(position).catName);
			}			
			return convertView;
		}

		public class EventViewHolder {
			private TextView mtvPopulatCategory;			
		}
	}
	
	/**Hadler for popular pickup address*/
	Handler mpopularPickupHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();
			}
			
			if(LiveryCabStatics.popularAddList.isEmpty() || LiveryCabStatics.popularAddList.get(0).message.equalsIgnoreCase("Record not found")){
				LiveryCabStaticMethods.showAlert(PopularPickupAddressActivity.this, 
					"No Records!!","No address availabale",R.drawable.attention,true,false,"Ok",null,null,null);
			}else{
				fillMaps.clear();				
				 for (int i = 0; i < LiveryCabStatics.popularAddList.size(); i++) {
		                HashMap<String, String> map = new HashMap<String, String>();
		                map.put("address", LiveryCabStatics.popularAddList.get(i).address);
		                map.put("distance",LiveryCabStatics.popularAddList.get(i).distance);
		                fillMaps.add(map);
		        }
				 adapter = new SimpleAdapter(PopularPickupAddressActivity.this, fillMaps,
	                        R.layout.recentaddresscustomcell, from, to);
	    		 popularAddList.setAdapter(adapter);
			}			
		};
	};
	/**Hadler for popular pickup address after category selection*/
	Handler mpopularPickupAfterHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			if(mProgressDialog.isShowing()){
				mProgressDialog.dismiss();
			}
			if(LiveryCabStatics.popularAddList.size() > 0){
				fillMaps.clear();
				fillMaps.clear();				
				 for (int i = 0; i < LiveryCabStatics.popularAddList.size(); i++) {
		                HashMap<String, String> map = new HashMap<String, String>();
		                map.put("address", LiveryCabStatics.popularAddList.get(i).address);
		                map.put("distance",LiveryCabStatics.popularAddList.get(i).distance);
		                fillMaps.add(map);
		        }
				 adapter = new SimpleAdapter(PopularPickupAddressActivity.this, fillMaps,
	                        R.layout.recentaddresscustomcell, from, to);
	             popularAddList.setAdapter(adapter);
			}else{
				DialogInterface.OnClickListener recordNotFoundOkAlertListener = new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						fillMaps.clear();
						adapter = new SimpleAdapter(PopularPickupAddressActivity.this, fillMaps,
		                        R.layout.recentaddresscustomcell, from, to);
		                popularAddList.setAdapter(adapter);
					}
				};										 
				LiveryCabStaticMethods.showAlert(PopularPickupAddressActivity.this, 
						"No Records!!","No address availabale",R.drawable.attention,true,false,"Ok",recordNotFoundOkAlertListener,null,null);
			}		
		};
	};
}