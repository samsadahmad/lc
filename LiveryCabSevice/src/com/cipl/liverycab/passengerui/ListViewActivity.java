package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.cipl.liverycab.parserclasses.BidListParserClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;
import com.cipl.liverycab.tools.ImageLoader;

public class ListViewActivity extends Activity implements OnClickListener {

	private Context mContext; 
	private ListView bidlistListView;
	private BidListParserClass parserObject;
	private EfficientAdapter bidListAdapter;
	private ImageLoader imageLoader;
	private ProgressDialog mProgressDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview);
		mContext = this;
		
		setUpView();	
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	private void setUpView() {
		// TODO Auto-generated method stub
		imageLoader = new ImageLoader(ListViewActivity.this);
		
		Button mbuttonMenuListView = (Button)findViewById(R.id.buttonMenuListView);
		mbuttonMenuListView.setOnClickListener(this);
		
		Button mbuttonMapViewListView = (Button)findViewById(R.id.buttonMapViewListView);
		mbuttonMapViewListView.setOnClickListener(this);
		
		Button mbuttonRefreshListView = (Button)findViewById(R.id.buttonRefreshListView);
		mbuttonRefreshListView.setOnClickListener(this);
		
		
		
		bidlistListView = (ListView)findViewById(R.id.listviewListView);
		bidListAdapter = new EfficientAdapter(ListViewActivity.this);
		
		if(LiveryCabStaticMethods.isInternetAvailable(this)){
			getBidListFromServer();
			
		}
		
		bidlistListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				Intent driverDetailIntent = new Intent(ListViewActivity.this,DriverDetailActivity.class);
				driverDetailIntent.putExtra("BidID",LiveryCabStatics.bidList.get(position).bidId);
				driverDetailIntent.putExtra("driverID",LiveryCabStatics.bidList.get(position).driverId);
				driverDetailIntent.putExtra("fName",LiveryCabStatics.bidList.get(position).firstName);
				driverDetailIntent.putExtra("lName",LiveryCabStatics.bidList.get(position).lastName);
				driverDetailIntent.putExtra("bidAmount",LiveryCabStatics.bidList.get(position).bidAmt);
				driverDetailIntent.putExtra("phoneNum",LiveryCabStatics.bidList.get(position).phoneNum);
				startActivity(driverDetailIntent);
			}
		});
	}
	
	private void getBidListFromServer() {
		// TODO Auto-generated method stub
		mProgressDialog = ProgressDialog.show(ListViewActivity.this, "","Loading. Please wait...", true, false);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		
		parserObject = new BidListParserClass(UrlStatics.getPassengerBidListUrl(LiveryCabStatics.userId), mContext,mProgressDialog);
		parserObject.start();
		mProgressDialog.setOnDismissListener(new OnDismissListener() {
		@Override
		public void onDismiss(DialogInterface arg0) {
			if(LiveryCabStatics.bidList.isEmpty()){
				LiveryCabStaticMethods.showAlert(ListViewActivity.this, 
						"No Records!!","Please wait for drivers to bid ride",R.drawable.attention,true,false,"Ok",null,null,null);
			}else if(LiveryCabStatics.bidList.get(0).message.equalsIgnoreCase("Record_not_found")){
				LiveryCabStaticMethods.showAlert(ListViewActivity.this, 
						"No Records!!","Please wait for drivers to bid ride",R.drawable.attention,true,false,"Ok",null,null,null);
			}else{
				bidlistListView.setAdapter(bidListAdapter);
			}
		}
	  });
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.buttonMenuListView:
			clickOnMenu();
			break;
			
		case R.id.buttonMapViewListView:
			clickOnMapView();
			break;

		case R.id.buttonRefreshListView:
			clickOnRefresh();
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
		startActivity(new Intent(ListViewActivity.this, MapViewActivity.class));
	}

	private void clickOnRefresh() {
		// TODO Auto-generated method stub
		if(LiveryCabStaticMethods.isInternetAvailable(ListViewActivity.this)){
			getBidListFromServer();
		}		
	}

	private void clickOnMapView() {
		// TODO Auto-generated method stub
		startActivity(new Intent(ListViewActivity.this,MapViewActivity.class));
	}

	private void clickOnMenu() {
		// TODO Auto-generated method stub
		startActivity(new Intent(ListViewActivity.this,PassengerMenuActivity.class)
		.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		
	}

	class EfficientAdapter extends BaseAdapter 
	{
		private LayoutInflater inflater;

		public EfficientAdapter(Context context) 
		{
			inflater = LayoutInflater.from(context);
		}

		public int getCount() 
		{
			return LiveryCabStatics.bidList.size();
		}

		public Object getItem(int position) 
		{
			return null;
		}

		public long getItemId(int position) 
		{
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			// A ViewHolder is used to keep references to children views
			ViewHolder holder;
			if (convertView == null) 
			{
				convertView = inflater.inflate(R.layout.listviewcustomcell,null);
				holder = new ViewHolder();
				// getting the views from the layout
				holder.driverName = (TextView) convertView
						.findViewById(R.id.textviewDriverNameListView);
				holder.timeETA = (TextView) convertView
						.findViewById(R.id.textviewETAListView);
				holder.miles = (TextView) convertView
						.findViewById(R.id.textviewMilesListView);
				holder.bidAmount = (TextView) convertView
						.findViewById(R.id.textviewBidAmountListView);
				holder.driverImage = (ImageView) convertView
						.findViewById(R.id.imageviewDriverImageListView);
				convertView.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) convertView.getTag();
			}
			int beganIndexforMiles = (LiveryCabStatics.bidList.get(position).distance).indexOf(".");
			
			String fullName = LiveryCabStatics.bidList.get(position).firstName+" "+
					LiveryCabStatics.bidList.get(position).lastName;
			if(fullName.length() > 20){
				holder.driverName.setText(fullName.substring(0, 20)+"..");
			}else{
				holder.driverName.setText(fullName);
			}
			
			holder.timeETA.setText(LiveryCabStatics.bidList.get(position).etaTime);
			holder.miles.setText((LiveryCabStatics.bidList.get(position).distance).substring(0,beganIndexforMiles+2)+" miles");
			holder.bidAmount.setText("$"+LiveryCabStatics.bidList.get(position).bidAmt);
			
			holder.driverImage.setTag(""+LiveryCabStatics.bidList.get(position).driverImage);
			imageLoader.DisplayImage(""+LiveryCabStatics.bidList.get(position).driverImage,ListViewActivity.this, holder.driverImage);
			return convertView;
		}
		class ViewHolder 
		{
			TextView driverName;
			TextView timeETA;
			TextView miles;
			TextView bidAmount;
			ImageView driverImage;
		}
	}

	
}
