package com.cipl.liverycab.passengerui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.parserclasses.HistoryDetailParser;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;


public class HistoryDetailActivity extends Activity implements OnClickListener{
	
	private String driverId,bidID;
	private HistoryDetailParser parserObject;
	private ForgotPasswordParserClass parsObject;
	private ProgressDialog pd;
	private String message ="";
	private TextView mtvRateValue;
	private EditText reviewText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.historydetail);
		Bundle extras = getIntent().getExtras();
		driverId = (String) (extras!=null ? extras.getString("driverID"):"");
		bidID = (String) (extras!=null ? extras.getString("bidID"):"");
		
		setUpView();
	}

	private void setUpView() {
		// TODO Auto-generated method stub
		reviewText = (EditText)findViewById(R.id.editTextReviewHistoryDetail);
		final ImageView driverImage = (ImageView)findViewById(R.id.imageviewDriverImageHistoryDetail);
		final TextView sourceAddText = (TextView)findViewById(R.id.textViewPickupaddressHistoryDetail);
		final TextView destiAddText = (TextView)findViewById(R.id.textViewDestinationaddressHistoryDetail);
		final TextView dateTimeText = (TextView)findViewById(R.id.textViewTimeDateHistoryDetail);
		final TextView bidAmtText = (TextView)findViewById(R.id.textviewBidAmountHistoryDetail);
		final TextView driverNameText = (TextView)findViewById(R.id.textViewDriverNameHistoryDetail);
		final TextView driverIDText = (TextView)findViewById(R.id.textViewIdHistoryDetail);
		//driverRating = (RatingBar)findViewById(R.id.RatingBarHistoryDetail);
		mtvRateValue = (TextView)findViewById(R.id.tvRateValue);
		
		Button buttonSubmitHistoryDetail = (Button)findViewById(R.id.buttonSubmitHistoryDetail);
		buttonSubmitHistoryDetail.setOnClickListener(this);
		
		TableRow mtableRowRatingBar = (TableRow)findViewById(R.id.tableRowRatingBar);
		mtableRowRatingBar.setOnClickListener(this);
		
		/**Check internet connectivity and fetching data from server*/
		if(LiveryCabStaticMethods.isInternetAvailable(this)){
			pd = LiveryCabStaticMethods.returnProgressBar(HistoryDetailActivity.this);
			parserObject = new HistoryDetailParser(UrlStatics.getHistoryDetailApi(driverId,bidID,LiveryCabStatics.passengerId),pd, HistoryDetailActivity.this);
			parserObject.start();
			
			pd.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					if(parserObject.actualDismiss){
						if(parserObject.dataObject!=null)
							message = parserObject.dataObject.message;
						if(message.equalsIgnoreCase("Sucess")){
							sourceAddText.setText(parserObject.dataObject.pickupAddres);
							destiAddText.setText(parserObject.dataObject.destination);
							dateTimeText.setText(parserObject.dataObject.datetime);
							bidAmtText.setText("$"+parserObject.dataObject.amount);
							driverNameText.setText(parserObject.dataObject.firstName+" "+parserObject.dataObject.lastName);
							driverIDText.setText(parserObject.dataObject.licence);
							driverImage.setImageDrawable(parserObject.dataObject.driverImage);							
							mtvRateValue.setText("("+parserObject.dataObject.rating+")");
						}
					}
				}
			});
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.buttonSubmitHistoryDetail:
			doRating();
			break;		
			
		case R.id.tableRowRatingBar:
			checkRating();
			break;
		}
	}

	private void checkRating() {
		// TODO Auto-generated method stub
		if(parserObject.dataObject.ratid.equalsIgnoreCase("") || parserObject.dataObject.ratid.equalsIgnoreCase("0") )
		{
			int noOfLike = Integer.parseInt(parserObject.dataObject.rating);
			mtvRateValue.setText("("+""+(noOfLike+1+")"));
		}
		else{
			Toast.makeText(HistoryDetailActivity.this, "You have allready rated, so you now can do comment only!", 0).show();
		}
	}

	private void doRating() {
		// TODO Auto-generated method stub
		String reviewmsg = reviewText.getText().toString();
		String reviewmsgab =  reviewmsg.replace(" ", "%20");
		String rateval = "1";
		
		if(reviewmsg.equalsIgnoreCase("")){
			Toast.makeText(HistoryDetailActivity.this, "Please enter review message", 0).show();
		}else if(LiveryCabStaticMethods.isInternetAvailable(HistoryDetailActivity.this)){
			
			pd = LiveryCabStaticMethods.returnProgressBar(HistoryDetailActivity.this);
			
			//pd = ProgressDialog.show(HistoryDetailActivity.this, "","Loading. Please wait...", true, false);
			//pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			parsObject = new ForgotPasswordParserClass(UrlStatics.getApiforReviewSubmission(LiveryCabStatics.userId,
					driverId,""+rateval,reviewmsgab,parserObject.dataObject.ratid), pd);
			parsObject.start();
			
			pd.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					if(parsObject.actualDismiss){
						if(parsObject.loginDataObj!=null)
							message = parsObject.loginDataObj.message;
						if(message.equalsIgnoreCase("all_ready_rated")){
							reviewText.setText("");
							DialogInterface.OnClickListener HistoryAllreadyRateOkAlertListener = new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									(HistoryDetailActivity.this).finish();
								}
							};
							LiveryCabStaticMethods.showAlert(HistoryDetailActivity.this, 
									"Success!!","Your review has been sent.",R.drawable.success,true,false,"Ok",HistoryAllreadyRateOkAlertListener,null,null);
						}else if(message.equalsIgnoreCase("rate_sucess")){
							reviewText.setText("");
							DialogInterface.OnClickListener HistoryRateOkAlertListener = new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									(HistoryDetailActivity.this).finish();
								}
							};							
							LiveryCabStaticMethods.showAlert(HistoryDetailActivity.this,"Success!!","You have rated this driver successfully.",R.drawable.success,true,false,"Ok",HistoryRateOkAlertListener,null,null);
						}
					}
				}
			});
		}
	}
}
