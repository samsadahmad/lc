package com.cipl.liverycab.dataclasses;


public class HistoryDataClass {
	public String message;
	public String status;
	public String driverId;
	public String bidid;
	public String source;
	public String destination;
	public String datetime;
	public String amount;
}
