package com.cipl.liverycab.dataclasses;

public class AssignedJobListDataClass {
	public String status;
	public String message;
	public String passengerId;
	public String firstName;
	public String lastName;
	public String pickupAdd;
	public String destinationAdd;
	public String distanceInMiles;
	public String bidAmount;
	public String etaTime;
	public String dateTime;
	public String pickupId;
	public String notification;
}
