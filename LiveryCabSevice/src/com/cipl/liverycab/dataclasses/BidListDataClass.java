package com.cipl.liverycab.dataclasses;


public class BidListDataClass {

	public String message;
	public String status;
	public String driverId;
	public String firstName;
	public String lastName;
	public String driverImage;
	public String distance;
	public String etaTime;
	public String bidAmt;
	public String latitude;
	public String longitude;
	public String bidId;
	public String phoneNum;
}
