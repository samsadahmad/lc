package com.cipl.liverycab.dataclasses;

public class DriverSignUpDataClass {
	public String message;
	public String firstName;
	public String lastName;
	public String dlNumber;
	public String phone;
	public String email;
	public String password;
	public String deviceId;
	public String deviceToken;
	public String deviceType;
	public String driverImage;
	public String carImage;
	public String status;
}
