package com.cipl.liverycab.dataclasses;

public class NotificationData {
	
	public String message;
	public String fname;
	public String lname;
	public String eta;
	public String phone;
	public String timestamp;
	public String status;
}
