package com.cipl.liverycab.dataclasses;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.CellLocation;
import android.util.Log;
import android.widget.Toast;

import com.cipl.liverycab.statics.LiveryCabStatics;

/**
 * This is the class going to use for fetching current location
 * */
public class FetchCurrentLocation implements LocationListener{
	public double latitude, longitude;
	private LocationManager mLocManager;
	private Context context;
	
	public FetchCurrentLocation(Context mContext){
		this.context = mContext; 
		mLocManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
		
		mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
				this);
		mLocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
				0, this);
		CellLocation.requestLocationUpdate();	
	}
	
	
	@Override
	public void onLocationChanged(Location location) {
		if(location != null){
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			LiveryCabStatics.currentLatitude = ""+latitude;
			LiveryCabStatics.currentLongitude = ""+longitude;
			Log.v("Location Tag", "Current Latitude:"+latitude+"Current Longitude:"+longitude);
		}
	}
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		Toast.makeText(context, "Gps Disabled", Toast.LENGTH_SHORT).show();
		Intent intent = new Intent(
				android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		context.startActivity(intent);
	}
	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
}
