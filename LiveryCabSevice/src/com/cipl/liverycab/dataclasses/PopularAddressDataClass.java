package com.cipl.liverycab.dataclasses;

public class PopularAddressDataClass {
	
	public String message;
	public String status;
	public String address;
	public String latitude;
	public String longitude;
	public String distance;
	public String addressType;
}
