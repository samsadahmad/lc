package com.cipl.liverycab.dataclasses;

import android.graphics.drawable.Drawable;

public class HistoryDetailDataClass {
	public String message;
	public String status;
	public String firstName;
	public String lastName;
	public String licence;
	public String destination;
	public String pickupAddres;
	public Drawable driverImage;
	public String datetime;
	public String amount;
	public String rating;
	public String ratid;
	
}
