package com.cipl.liverycab.dataclasses;

import android.graphics.drawable.Drawable;

public class DriverProfileData {
	public String message;
	public String firstName;
	public String lastName;
	public String dlNumber;
	public String phone;
	public String email;
	public String ratingValue;
	public String numberOfReviews;
	public Drawable driverImage;
	public Drawable carImage;
	public String status;
	public String settingInfo;
	public String handicapped;
}
