package com.cipl.liverycab.dataclasses;

public class CreditCardDataClass {
	
	public String cardType;
	public String expirationYear;
	public String expirationMonth;
	public String cardNumber;
	public String cvvNumber;
}
