package com.cipl.liverycab.dataclasses;

public class PassengerDataClass {
	public String message;
	public String firstName;
	public String lastName;
	public String phone;
	public String email;
	public String status;
}
