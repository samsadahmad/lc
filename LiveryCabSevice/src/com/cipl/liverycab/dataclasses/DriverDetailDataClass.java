package com.cipl.liverycab.dataclasses;

import android.graphics.drawable.Drawable;

public class DriverDetailDataClass {
	public String message;
	public String driverId;
	public String bidAmt;
	public String status;
	public String pickUpAddress;
	public String destinationAdd;
	public String firsName;
	public String lastName;
	public Drawable driverImage = null;
	public Drawable carImage;
	public String numberOfReview;
	public String licenceNumber;
	public String etaTime;
	public String dateTime;
	public String handicapped;
}
