package com.cipl.liverycab.dataclasses;

public class JobListDataClass {
	public String count;
	public String status;
	public String message;
	public String pickUpId;
	public String firstName;
	public String lastName;
	public String pickupAdd;
	public String destinationAdd;
	public String pickUpLatitude;
	public String pickupLongitude;
	public String distanceInMiles;
	public String bidAmount;
	public String etaTime;
	public String dateTime;
	public String bidId;
	public String isRideCancel;
	public String phoneP;
}
