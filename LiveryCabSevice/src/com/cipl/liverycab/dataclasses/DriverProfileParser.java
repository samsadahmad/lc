package com.cipl.liverycab.dataclasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.cipl.liverycab.statics.LiveryCabStaticMethods;

public class DriverProfileParser extends Thread {

	private String urlDriverProfile;
	public boolean actualDismiss;
	public DriverProfileData dataObject; 
	public Context context;

	public DriverProfileParser(String url, Context mContext){
		super();
		this.urlDriverProfile = url;
		this.context = mContext;
		Log.d("url",url);
	}	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		JSONObject jObject;
		dataObject =  new DriverProfileData();
		
		jObject = getJSONfromURL(urlDriverProfile);
		if(jObject==null){
			return;
		}			
			
		try {
			dataObject.message =jObject.getString("Message");
			dataObject.firstName =jObject.getString("fname");
			dataObject.lastName =jObject.getString("lname");
			dataObject.phone =jObject.getString("phone");
			dataObject.email =jObject.getString("email");
			dataObject.numberOfReviews =jObject.getString("totalReview");
			dataObject.ratingValue =jObject.getString("rating");
			dataObject.dlNumber =jObject.getString("licenceID");
			dataObject.settingInfo =jObject.getString("miles");
			dataObject.handicapped =jObject.getString("handicapped");
			if(jObject.getString("driverImage")!=null){
				String imgDUrl = jObject.getString("driverImage").replace(" ", "%20").trim();
				dataObject.driverImage =LiveryCabStaticMethods.LoadImage(imgDUrl, new BitmapFactory.Options(),context);
			}
			if(jObject.getString("carImage")!=null){
				String imgUrl = jObject.getString("carImage").replace(" ", "%20").trim();
				
				dataObject.carImage =LiveryCabStaticMethods.LoadImage(imgUrl, new BitmapFactory.Options(),context);
			}
			dataObject.status = jObject.getString("Status");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(LiveryCabStaticMethods.mProgressDialog.isShowing()){
        	actualDismiss =true;
        	LiveryCabStaticMethods.mProgressDialog.dismiss();
		}
	}
	public  JSONObject  getJSONfromURL(String url) 
	{
		InputStream mIs = null;
	    String result = "";
	    JSONObject jObjectLogin = null;
	    try {
	    	HttpClient httpclient = new DefaultHttpClient();
	    	HttpPost httppost = new HttpPost(url);
	    	HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			mIs = entity.getContent();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try{
	        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(mIs,"iso-8859-1"),8);
	        StringBuilder stringBuilder = new StringBuilder();
	        String line = null;
	        while ((line = bufferReader.readLine()) != null) {
	        	if(line.trim().equals("\n"))
	        		continue;
	        	stringBuilder.append(line + "\n");
	        }
	        mIs.close();
	        result=stringBuilder.toString();
	    }catch(Exception e){
	    	 Log.e("log_tag", "Error converting result "+e.toString());
	    }
	    try {
			jObjectLogin = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jObjectLogin;
	}
}
