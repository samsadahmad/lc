package com.cipl.liverycab.driverui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class JobDetailActivity extends Activity implements OnClickListener{

	private int index = 0;
	private String message = "";
	private Button btnSubmit,btnCalltoDriver;
	private EditText bidAmtText;
	private TextView etaText;
	private ProgressDialog pd;
	private String etaString = "";
	private ForgotPasswordParserClass parserObject;
	static final int TIME_DIALOG_ID = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jobdetail);
		Bundle extras = getIntent().getExtras();
		index = (Integer) (extras!=null ? extras.getInt("Index"):"");

		final TextView name = (TextView)findViewById(R.id.textviewDriverNameJobDetail);
		final TextView pickUp = (TextView)findViewById(R.id.textviewPickupAddressJobDetail);
		final TextView desti = (TextView)findViewById(R.id.textviewDestinationAddressJobDetail);
		final TextView miles = (TextView)findViewById(R.id.textviewMilesJobDetail);
		final TextView dateTime = (TextView)findViewById(R.id.textviewPickupDateTimeJobDetail);
		TextView etaTime = (TextView)findViewById(R.id.textviewEtaTimeJobDetail);
		final TextView bidAmt = (TextView)findViewById(R.id.textviewBidAmtJobDetail);

		btnCalltoDriver = (Button)findViewById(R.id.btnCallToDriver);
		btnCalltoDriver.setOnClickListener(this);

		btnSubmit  = (Button)findViewById(R.id.buttonSubmitJobDetail);
		btnSubmit.setOnClickListener(this);

		bidAmtText  = (EditText)findViewById(R.id.editTextBidAmountJobDetail);

		etaText  = (TextView)findViewById(R.id.textViewETATimeJobDetail);
		etaText.setOnClickListener(this);

		int beganIndexforMiles = (LiveryCabStatics.jobList.get(index).distanceInMiles).indexOf(".");
		name.setText(LiveryCabStatics.jobList.get(index).firstName+" "+LiveryCabStatics.jobList.get(index).lastName);
		pickUp.setText(LiveryCabStatics.jobList.get(index).pickupAdd);
		desti.setText(LiveryCabStatics.jobList.get(index).destinationAdd);
		miles.setText((LiveryCabStatics.jobList.get(index).distanceInMiles).substring(0,beganIndexforMiles+2)+" miles");
		dateTime.setText(LiveryCabStatics.jobList.get(index).dateTime);

		if(LiveryCabStatics.jobList.get(index).etaTime.equalsIgnoreCase("0")){
			RelativeLayout mWaitingText;
			mWaitingText = (RelativeLayout)findViewById(R.id.relativelayoutWaitingTextBgJobListCustomCell);
			mWaitingText.setVisibility(View.INVISIBLE);
		}else{
			etaTime.setText(LiveryCabStatics.jobList.get(index).etaTime);
		}
		bidAmt.setText("$"+LiveryCabStatics.jobList.get(index).bidAmount);
		if(!LiveryCabStatics.jobList.get(index).bidAmount.equalsIgnoreCase("0.00") || LiveryCabStatics.jobList.get(index).isRideCancel.equalsIgnoreCase("Y")){
			((RelativeLayout)findViewById(R.id.relativelayoutCallOutBgJobDetail))
			.setVisibility(RelativeLayout.GONE);
			btnSubmit.setVisibility(EditText.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.textViewETATimeJobDetail:
			showDialog(TIME_DIALOG_ID);
			break; 

		//Fetch Call button and open caller dialer
		case R.id.btnCallToDriver:
			if(LiveryCabStatics.jobList.get(index).phoneP.equalsIgnoreCase("")){
				Toast.makeText(JobDetailActivity.this, "Phone number not available", 0).show();
			}else{
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel:"+LiveryCabStatics.jobList.get(index).phoneP));
				startActivity(callIntent);
			}
			break;
		//Fetch Call button and this used to add bid on this Ride.
		case R.id.buttonSubmitJobDetail:
			doBid();
			break; 
		}
	}

	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			/*Toast.makeText(JobDetailActivity.this,
					"Time is=" + hourOfDay + ":" + minute, Toast.LENGTH_SHORT)
					.show();*/
			final Calendar cc = Calendar.getInstance();
			int HOUR = cc.get(Calendar.HOUR_OF_DAY);
			int MINUTE = cc.get(Calendar.MINUTE);

			Calendar currentTime = Calendar.getInstance();
			currentTime.set(Calendar.HOUR_OF_DAY, HOUR);
			currentTime.set(Calendar.MINUTE, MINUTE);
			currentTime.set(Calendar.SECOND,0);
			currentTime.set(Calendar.MILLISECOND,0);

			Calendar selectedTime = Calendar.getInstance();
			selectedTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			selectedTime.set(Calendar.MINUTE, minute);
			selectedTime.set(Calendar.SECOND, 0);
			selectedTime.set(Calendar.MILLISECOND, 0);

			String amorPm = "";
			String min = "", hour = "";
			
			if (selectedTime.get(Calendar.AM_PM) == Calendar.AM)
				amorPm = "AM";
		    else if (selectedTime.get(Calendar.AM_PM) == Calendar.PM)
		    	amorPm = "PM";
			hour = (selectedTime.get(Calendar.HOUR) == 0) ?"12":selectedTime.get(Calendar.HOUR)+"";

			/*if(hour <= 9)
				hour = "0"+hour;
			else
				hour = ""+hour;*/
			if(minute <= 9)
				min = "0"+minute;
			else
				min = ""+minute;		
			 

			if (selectedTime.after(currentTime)) {
				etaString = ""+hour+":"+min+" "+amorPm;
				etaText.setText(etaString);
			}else{
				LiveryCabStaticMethods.showAlert(JobDetailActivity.this,
						"Alert!!","Enter a valid time",R.drawable.attention,
						true,false,"Ok",null,null,null);
			}
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case TIME_DIALOG_ID:
		{
			final Calendar cc = Calendar.getInstance();
			int HOUR = cc.get(Calendar.HOUR_OF_DAY);
			int MINUTE = cc.get(Calendar.MINUTE);
			return new TimePickerDialog(this, mTimeSetListener, HOUR, MINUTE, false);

		}
		}
		return null;
	}
	
	protected void doBid() {
		// TODO Auto-generated method stub
		if(bidAmtText.length() > 5){
			bidAmtText.setError(getString(R.string.bid_amount_error));
		}else{
			String bidAmt = bidAmtText.getText().toString();
			if(bidAmt.equalsIgnoreCase("")){
				LiveryCabStaticMethods.showAlert(JobDetailActivity.this,"Error Occured!","Enter bid amount"
						,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						bidAmtText.requestFocus();
					}
				},null,null);
			}else if(etaString.equalsIgnoreCase("")){
				LiveryCabStaticMethods.showAlert(JobDetailActivity.this,"Error Occured!","Please choose ETA time"
						,R.drawable.attention, true, false, "Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						etaText.requestFocus();
					}
				},null,null);
			}else if((LiveryCabStatics.jobList.get(index).etaTime.equalsIgnoreCase("0") ||
					LiveryCabStatics.jobList.get(index).etaTime.equalsIgnoreCase(""))&& 
					(LiveryCabStatics.jobList.get(index).bidAmount.equalsIgnoreCase("0.00") ||
							LiveryCabStatics.jobList.get(index).bidAmount.equalsIgnoreCase(""))){

				pd = LiveryCabStaticMethods.returnProgressBar(JobDetailActivity.this);
				try {
					parserObject = new ForgotPasswordParserClass(UrlStatics
							.getUrlForEnteringBidAmount(LiveryCabStatics.userId,
									""+LiveryCabStatics.jobList.get(index).pickUpId, bidAmt, URLEncoder.encode(etaString, "utf-8")),pd);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				parserObject.start();
				pd.setOnDismissListener(new OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						if(parserObject.actualDismiss){
							if(parserObject.loginDataObj!=null)
								message = parserObject.loginDataObj.message;
							if(message.equalsIgnoreCase("already_bidded")){
								LiveryCabStaticMethods.showAlert(JobDetailActivity.this,
										"Error Occured!","You have already bid for this ride",R.drawable.attention, true, false, 
										"Ok", null,null,null);
							}else if(message.equalsIgnoreCase("session_expire")){
								LiveryCabStaticMethods.showAlert(JobDetailActivity.this,
										"Error Occured!","Your are not login. Please login again",R.drawable.attention, true, false, 
										"Ok", null,null,null);
							}else if(message.equalsIgnoreCase("bidding_added_successfully")){
								bidAmtText.setText("");
								etaText.setText("");
								LiveryCabStaticMethods.showAlert(JobDetailActivity.this,
										"Success!!","You have successfully bid for this ride",R.drawable.success, true, false, 
										"Ok", new DialogInterface.OnClickListener() 
								{
									@Override
									public void onClick(DialogInterface arg0, int arg1) {
										//TODO Auto-generated method stub
										//startActivity(new Intent(JobDetailActivity.this,JobListActivity.class));
										finish();
									}
								},null,null);
							}else
								LiveryCabStaticMethods.showAlert(JobDetailActivity.this,
										"Error Occured!","Try again!",R.drawable.attention, true, false, 
										"Ok", null,null,null);
						}
					}
				});
			}else{
				LiveryCabStaticMethods.showAlert(JobDetailActivity.this,
						"Error Occured!","Already bidded please wait till passenger confirms it", R.drawable.attention,
						true,false,"Ok",null,null,null);
			}
		}
	}
}