package com.cipl.liverycab.driverui;

import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.passengerui.TermCondtionActivity;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class DriverSignUpActivity extends Activity {

	String selectedPathDriverImage = "NONE";
	String selectedPathCarImage = "NONE";
	JSONObject jObject = null;
	JSONObject jobj  = null;
	private String SignUPResult = "";
	private boolean checked = true;
	private EditText fNameEditText, lNameEditText, dlNumberEditTExt,
			cellNumberEditText, emailEditText, passwordEditText,
			confirmPasswordEditText;
	private ImageButton checkBtn;
	private TextView driverImageTextView, carImageTextView;
	private ProgressDialog pd;
	private ImageView driverPic,carPic;
	
	private String[] pictureItemsMenu = { "Choose Existing Photo",
			"Take New Photo" };
	private int SELECT_PICTURE_DRIVER = 0;
	private int START_CAMERA_FORDRIVER = 1;

	private int SELECT_PICTURE_CAR = 2;
	private int START_CAMERA_FORCAR = 3;
	private Uri mImageCaptureUri_driver,mImageCaptureUri_Pass;
	private File fileDriver,filePass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup);

		fNameEditText = (EditText) findViewById(R.id.edittextFirstNameSignUp);
		lNameEditText = (EditText) findViewById(R.id.edittextLastNameSignUp);
		dlNumberEditTExt = (EditText) findViewById(R.id.edittextLicencePlateNumberSignUp);
		cellNumberEditText = (EditText) findViewById(R.id.edittextPhoneNumberSignUp);
		emailEditText = (EditText) findViewById(R.id.edittextEmailAddressSignUp);
		passwordEditText = (EditText) findViewById(R.id.edittextPasswordSignUp);
		confirmPasswordEditText = (EditText) findViewById(R.id.edittextConfirmPasswordSignUp);
		checkBtn = (ImageButton) findViewById(R.id.imagebuttonCheckDriverSignup);		
		driverImageTextView = (TextView) findViewById(R.id.textviewAddDriverPicSignupDriver);
		carImageTextView = (TextView) findViewById(R.id.textviewEditCarPicSignupDriver);
		driverPic = (ImageView)findViewById(R.id.imageviewDriverPicSignupDriver);
		carPic = (ImageView)findViewById(R.id.imageviewCarPicSignupDriver);
		
		/**This method for terms and condition*/
		((TextView)findViewById(R.id.tvTerms))
		.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(DriverSignUpActivity.this,TermCondtionActivity.class),1);
		}
	});
		
		driverImageTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						DriverSignUpActivity.this);
				builder.setTitle("Select Picture");
				builder.setItems(pictureItemsMenu,
						new DialogInterface.OnClickListener() {

							

							public void onClick(DialogInterface dialog, int item) {
								if (item == 0) {
									Intent intent = new Intent();
									intent.setType("image/*");
									intent.setAction(Intent.ACTION_GET_CONTENT);
									startActivityForResult(Intent
											.createChooser(intent,
													"Select Picture"),
											SELECT_PICTURE_DRIVER);
								} else if (item == 1) {
									Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					                fileDriver = new File(Environment.getExternalStorageDirectory(),
					                                    "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
					                mImageCaptureUri_driver = Uri.fromFile(fileDriver);

					                try {
					                    intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri_driver);
					                    intent.putExtra("return-data", true);

					                    startActivityForResult(intent, START_CAMERA_FORDRIVER);
					                } catch (Exception e) {
					                    e.printStackTrace();
					                }
								}
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});

		carImageTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						DriverSignUpActivity.this);
				builder.setTitle("Select Picture");
				builder.setItems(pictureItemsMenu,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int item) {
								if (item == 0) {
									Intent intent = new Intent();
									intent.setType("image/*");
									intent.setAction(Intent.ACTION_GET_CONTENT);
									startActivityForResult(Intent
											.createChooser(intent,
													"Select Picture"),
											SELECT_PICTURE_CAR);
								} else if (item == 1) {
									Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					                filePass = new File(Environment.getExternalStorageDirectory(),
					                                    "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
					                mImageCaptureUri_Pass = Uri.fromFile(filePass);

					                try {
					                    intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri_Pass);
					                    intent.putExtra("return-data", true);

					                    startActivityForResult(intent, START_CAMERA_FORCAR);
					                } catch (Exception e) {
					                    e.printStackTrace();
					                }
								}
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});

		checkBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (checked) {
					checked = false;
					checkBtn.setBackgroundResource(R.drawable.check);
				} else {
					checked = true;
					checkBtn.setBackgroundResource(R.drawable.checked);
				}
			}
		});

		((Button) findViewById(R.id.buttonSignUpSignUp))
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						final String mEmail = emailEditText.getText().toString();
						final String mFName = fNameEditText.getText().toString();
						final String mLName = lNameEditText.getText().toString();
						final String mPhone = cellNumberEditText.getText().toString();
						final String mPassword = passwordEditText.getText()
								.toString();
						final String mLicence = dlNumberEditTExt.getText().toString();
						final String mConfirmPass = confirmPasswordEditText.getText()
								.toString();
						if (mFName.equalsIgnoreCase("") || mFName.length() > 25) {
							LiveryCabStaticMethods
							.showAlert(
									DriverSignUpActivity.this,
									"Error Occured!",
									"Please enter first name",
									R.drawable.attention, true, false,
									"Ok", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											fNameEditText.requestFocus();
										}
								}, null, null);
						} else if (mLName.equalsIgnoreCase("") || mLName.length() > 25) {
							LiveryCabStaticMethods
							.showAlert(
									DriverSignUpActivity.this,
									"Error Occured!",
									"Please enter last name",
									R.drawable.attention, true, false,
									"Ok", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											lNameEditText.requestFocus();
										}
								}, null, null);
						}else if (mLicence.equalsIgnoreCase("")|| mLicence.length() > 25) {
							LiveryCabStaticMethods
							.showAlert(
									DriverSignUpActivity.this,
									"Error Occured!",
									"Please enter licence plate number",
									R.drawable.attention, true, false,
									"Ok", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											dlNumberEditTExt.requestFocus();
										}
								}, null, null);
						}else if (mPhone.equalsIgnoreCase("") || mPhone.length() > 15) {
							LiveryCabStaticMethods
							.showAlert(
									DriverSignUpActivity.this,
									"Error Occured!",
									"Please enter correct cell phone number",
									R.drawable.attention, true, false,
									"Ok", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											cellNumberEditText.requestFocus();
										}
								}, null, null);
						}else if (mEmail.equalsIgnoreCase("") || mEmail.length() > 50) {
							LiveryCabStaticMethods
							.showAlert(DriverSignUpActivity.this,"Error Occured!",
									"Please enter your email address",
									R.drawable.attention, true, false,
									"Ok",new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											emailEditText.requestFocus();
										}
								}, null, null);
						}else if (!LiveryCabStaticMethods.isEmail(mEmail)) {
							LiveryCabStaticMethods.showAlert(
									DriverSignUpActivity.this, "Error Occured!",
									"Please enter a valid email address",
									R.drawable.attention, true, false, "Ok",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											emailEditText.requestFocus();
										}
								}, null, null);
						}else if (mPassword.equalsIgnoreCase("")) {
							LiveryCabStaticMethods
							.showAlert(
									DriverSignUpActivity.this,
									"Error Occured!",
									"Please enter password",
									R.drawable.attention, true, false,
									"Ok", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											passwordEditText.requestFocus();
										}
								}, null, null);
						} else if (mConfirmPass.equalsIgnoreCase("")) {
							LiveryCabStaticMethods
							.showAlert(
									DriverSignUpActivity.this,
									"Error Occured!",
									"Please enter confirm password",
									R.drawable.attention, true, false,
									"Ok",new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											confirmPasswordEditText.requestFocus();
										}
								}, null, null);
						} else if (!(mConfirmPass.equalsIgnoreCase(mPassword))) {
							LiveryCabStaticMethods
									.showAlert(
											DriverSignUpActivity.this,
											"Error Occured!",
											"Pasword and confirm password must be same ",
											R.drawable.attention, true, false,
											"Ok", new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													// TODO Auto-generated method stub
													confirmPasswordEditText.requestFocus();
												}
										}, null, null);
						} else if (mEmail.equalsIgnoreCase("")
								&& mFName.equalsIgnoreCase("")
								&& mLName.equalsIgnoreCase("")
								&& mPhone.equalsIgnoreCase("")
								&& mPassword.equalsIgnoreCase("")
								&& mConfirmPass.equalsIgnoreCase("")) {
							LiveryCabStaticMethods.showAlert(
									DriverSignUpActivity.this, "Error Occured!",
									"All fields are mendatory",
									R.drawable.attention, true, false, "Ok",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											fNameEditText.requestFocus();
										}
								}, null, null);
						}  else if (!checked) {
							LiveryCabStaticMethods.showAlert(
									DriverSignUpActivity.this, "Error Occured!",
									"Terms and condition must be agreed",
									R.drawable.attention, true, false, "Ok",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											checkBtn.requestFocus();
										}
								}, null, null);
						} else {
							pd = LiveryCabStaticMethods.returnProgressBar(DriverSignUpActivity.this);
							Thread thread = new Thread(new Runnable() {
								@Override
								public void run() {
									// TODO Auto-generated method stub
									SignUPResult = uploadDriverData(mFName, mLName,
											mEmail, mPassword, mLicence, mPhone);
								}
							});
							thread.start();
							pd.setOnDismissListener(new OnDismissListener() {
								@Override
								public void onDismiss(DialogInterface dialog) {
									// TODO Auto-generated method stub
									try {
										if(SignUPResult.length()== 0){
											return;
										}
										jobj = new JSONObject(SignUPResult);
										if (jobj.getString("Status").equalsIgnoreCase("1")) {
											if (jobj.getString("Message").equalsIgnoreCase("Registered_sucessfully")) {
												jobj.getString("fname");
												jobj.getString("lname");
												LiveryCabStatics.driverId = jobj.getString("driverId");
									 			LiveryCabStatics.userId = jobj.getString("driverId");
												LiveryCabStatics.isLoginDriver = true;
												LiveryCabStaticMethods.showAlert(DriverSignUpActivity.this,"Success!!",
														"You have successfully registered as a LiveryCab Driver!",R.drawable.success, true,false,"Ok", 
														new DialogInterface.OnClickListener() {
															@Override
															public void onClick(DialogInterface dialog, int which) {
																startActivity(new Intent(DriverSignUpActivity.this,JobListMapViewActivity.class));
																finish();
															}
														}, null, null);
											}
										} else if (jobj.getString("Status").equalsIgnoreCase("0")) {
											if (jobj.getString("Message").equalsIgnoreCase("email_already_in_use")) {
												LiveryCabStaticMethods.showAlert(DriverSignUpActivity.this,"Error Occured!",
														"email_already_in_use",R.drawable.attention, true,false, "Ok", null, null, null);
											} else if (jobj.getString("Message").equalsIgnoreCase("invalid_email")) {
												LiveryCabStaticMethods.showAlert(DriverSignUpActivity.this,"Error Occured!", "Invalid User",
														R.drawable.attention, true,false, "Ok", null, null, null);
											} else {
												LiveryCabStaticMethods.showAlert(DriverSignUpActivity.this,"Error Occured!", "Invalid User",
														R.drawable.attention, true,false, "Ok", null, null, null);
											}
										}
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							});
						}
					}
				});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == RESULT_OK) {
			Uri selectedImageUri = null;
			if (data != null) {
				if (requestCode == SELECT_PICTURE_DRIVER) {
					selectedImageUri = data.getData();
					selectedPathDriverImage = getPath(selectedImageUri);
					driverPic.setImageURI(Uri.parse(selectedPathDriverImage));
				}				
				if (requestCode == SELECT_PICTURE_CAR) {
					selectedImageUri = data.getData();
					selectedPathCarImage = getPath(selectedImageUri);
					carPic.setImageURI(Uri.parse(selectedPathCarImage));
				}
				
			} else {
				if (requestCode == START_CAMERA_FORDRIVER) {
					selectedPathDriverImage = fileDriver.getAbsolutePath();
					driverPic.setImageURI(Uri.parse(selectedPathDriverImage));
				}
				if (requestCode == START_CAMERA_FORCAR) {
					selectedPathCarImage = filePass.getAbsolutePath();
					carPic.setImageURI(Uri.parse(selectedPathCarImage));
				}
			}
			Toast.makeText(DriverSignUpActivity.this, "Images Selected",
					Toast.LENGTH_LONG).show();
		}
	}

	private String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	private String uploadDriverData(String fname, String lname,
			String email, String pass, String license, String phone) {
		File driverImage=null;
		File carImage =null;
		String response_str = "";
		if(!selectedPathDriverImage.equals("NONE"))
			driverImage = new File(selectedPathDriverImage);
		if(!selectedPathDriverImage.equals("NONE"))
			carImage = new File(selectedPathCarImage);
		String urlString = UrlStatics.demoServerLink
				+ "LIVERYCAB_DRIVER_REGISTRATION&fname=&lname=&licence=&phone=&email="
				+ "&password=&driverimage=&carimage=&deviceId=&deviceToken=&deviceType=";
		try{
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(urlString);
			FileBody bin1=null,bin2=null;
			MultipartEntity reqEntity = new MultipartEntity();
			if(driverImage!=null){
				bin1 = new FileBody(driverImage);
				reqEntity.addPart("driverimage", bin1);
			}
			if(carImage!=null){
					 bin2 = new FileBody(carImage);
					 reqEntity.addPart("carimage", bin2);
			}
			reqEntity.addPart("fname", new StringBody(fname));
			reqEntity.addPart("lname", new StringBody(lname));
			reqEntity.addPart("licence", new StringBody(license));
			reqEntity.addPart("phone", new StringBody(phone));
			reqEntity.addPart("email", new StringBody(email));
			reqEntity.addPart("password", new StringBody(pass));
			reqEntity.addPart("deviceId", new StringBody("1234"));
			reqEntity.addPart("deviceToken", new StringBody("1234555"));
			reqEntity.addPart("deviceType", new StringBody(
					LiveryCabStatics.deviceType));
			post.setEntity(reqEntity);
			HttpResponse response = client.execute(post);
			HttpEntity resEntity = response.getEntity();
			response_str = EntityUtils.toString(resEntity);
			if (resEntity != null) {
				Log.i("RESPONSE", response_str);
				runOnUiThread(new Runnable() {
					public void run() {
						try {
							//jObject = new JSONObject(response_str);
							if(pd.isShowing())
								pd.dismiss();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		}catch (Exception ex) {
			Log.e("Debug", "error: " + ex.getMessage(), ex);
		}
		return response_str;
	}
}