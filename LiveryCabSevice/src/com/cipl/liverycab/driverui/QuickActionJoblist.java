package com.cipl.liverycab.driverui;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.passengerui.R;

public class QuickActionJoblist extends PopupWindow implements KeyEvent.Callback {

	private final Context mContext;
	private final LayoutInflater mInflater;
	private final WindowManager mWindowManager;
	private Intent intent;
	View contentView;
	
	private int mScreenWidth;
	private int mScreenHeight;
	
	private int mShadowHoriz;
	private int mShadowVert;
	private int mShadowTouch;
	
	private ImageView mArrowUp;
	private ImageView mArrowDown;
	private ImageView fwdArrow;
	private ViewGroup mTrack;
	private Animation mTrackAnim;

	private View mPView;
	private Rect mAnchor;
	
	int dilogId = 0;
	
	// For lazy loading of icon
	
	public QuickActionJoblist(Context context, View pView, Rect rect) {
		super(context);
		
		mPView = pView;
		mAnchor = rect;
		
		mContext = context;
		mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		mInflater = ((Activity)mContext).getLayoutInflater();
		
		setContentView(R.layout.quickactionjoblist);
		
		mScreenWidth = mWindowManager.getDefaultDisplay().getWidth();
		mScreenHeight = mWindowManager.getDefaultDisplay().getHeight();
		setWindowLayoutMode(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		final Resources res = mContext.getResources();
		
		setWidth(mScreenWidth + mShadowHoriz + mShadowHoriz);
		setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		
		setBackgroundDrawable(new ColorDrawable(0));
		
		mArrowUp = (ImageView) contentView.findViewById(R.id.arrow_up_joblist);
		mArrowDown = (ImageView) contentView.findViewById(R.id.arrow_down_joblist);
		fwdArrow = (ImageView)contentView.findViewById(R.id.quickaction_arrow_joblist);

		setFocusable(true);
		setTouchable(true);
		setOutsideTouchable(true);
		
		// Prepare track entrance animation
		mTrackAnim = AnimationUtils.loadAnimation(mContext, R.anim.quickaction);
		mTrackAnim.setInterpolator(new Interpolator() {
			public float getInterpolation(float t) {
				// Pushes past the target area, then snaps back into place.
				// Equation for graphing: 1.2-((x*1.6)-1.1)^2
				final float inner = (t * 1.55f) - 1.1f;
				return 1.2f - inner * inner;
			}
		});	
		
		fwdArrow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(intent);
			}
		});
	}
	
	public void setIntent(Intent intent)
	{
		this.intent = intent;
	}
	private void startActivity(Intent i)
	{
		if(i!=null)
			mContext.startActivity(this.intent);
	}
	private void setContentView(int resId) {
		contentView = mInflater.inflate(resId, null);
		super.setContentView(contentView);
	}
	
	public View getHeaderView() {
		return contentView.findViewById(R.id.quickaction_header_joblist);
	}
	
	public void setTitle(CharSequence title) {
		contentView.findViewById(R.id.quickaction_header_content_joblist).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.quickaction_primary_text_joblist).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.quickaction_primary_text_joblist)).setText(title);
	}
	
	public void setTitle(int resid) {
		setTitle(mContext.getResources().getString(resid));
	}
	
	public void setText(CharSequence text) {
		contentView.findViewById(R.id.quickaction_header_content_joblist).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.quickaction_secondary_text_joblist).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.quickaction_secondary_text_joblist)).setText(text);
	}
	
	public void setText(int resid) {
		setText(mContext.getResources().getString(resid));
	}
	
	public void setTextMiles(CharSequence text) {
		contentView.findViewById(R.id.quickaction_miles_text_joblist).setVisibility(View.VISIBLE);
		contentView.findViewById(R.id.quickaction_miles_text_joblist).setVisibility(View.VISIBLE);
		((TextView) contentView.findViewById(R.id.quickaction_miles_text_joblist)).setText(text);
	}
	
	public void setTextMiles(int resid) {
		setText(mContext.getResources().getString(resid));
	}
	
	public void setIcon(Bitmap bm) {
		contentView.findViewById(R.id.quickaction_arrow_joblist).setVisibility(View.VISIBLE);
		final ImageView vImage = (ImageView) contentView.findViewById(R.id.quickaction_arrow_joblist);
		vImage.setImageBitmap(bm);
	}
	
	public void setIcon(Drawable d) {
		contentView.findViewById(R.id.quickaction_arrow_joblist).setVisibility(View.VISIBLE);
		final ImageView vImage = (ImageView) contentView.findViewById(R.id.quickaction_arrow_joblist);
		vImage.setImageDrawable(d);
	}
	
	public void setIcon(int resid) {
		setIcon(mContext.getResources().getDrawable(resid));
	}
	
	/**
	 * Show the correct call-out arrow based on a {@link R.id} reference.
	 */
	private void showArrow(int whichArrow, int requestedX) {
		final View showArrow = (whichArrow == R.id.arrow_up_joblist) ? mArrowUp : mArrowDown;
		final View hideArrow = (whichArrow == R.id.arrow_up_joblist) ? mArrowDown : mArrowUp;

		// Dirty hack to get width, might cause memory leak
		final int arrowWidth = mContext.getResources().getDrawable(R.drawable.quickaction_arrow_up).getIntrinsicWidth();

		showArrow.setVisibility(View.VISIBLE);
		ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams)showArrow.getLayoutParams();
		param.leftMargin = requestedX - arrowWidth / 2;
		//Log.d("QuickActionWindow", "ArrowWidth: "+arrowWidth+"; LeftMargin for Arrow: "+param.leftMargin);

		hideArrow.setVisibility(View.INVISIBLE);
	}
	
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBackPressed();
			return true;
		}

		return false;
	}
	
	private void onBackPressed() {
			dismiss();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void show() {
		show(mAnchor.centerX());
	}
	
	public void show(int requestedX) {
		super.showAtLocation(mPView, Gravity.NO_GRAVITY, 0, 0);
		
		// Calculate properly to position the popup the correctly based on height of popup
		if (isShowing()) {
			int x, y, windowAnimations;
			this.getContentView().measure(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			final int blockHeight = this.getContentView().getMeasuredHeight();
			
			x = -mShadowHoriz;
			
			//Log.d("QuickActionWindow", "blockHeight: "+blockHeight);
			
			if (mAnchor.top > blockHeight) {
				// Show downwards callout when enough room, aligning bottom block
				// edge with top of anchor area, and adjusting to inset arrow.
				showArrow(R.id.arrow_down_bookride, requestedX);
				y = mAnchor.top - blockHeight;
				windowAnimations = R.style.QuickActionAboveAnimation;
	
			} else {
				// Otherwise show upwards callout, aligning block top with bottom of
				// anchor area, and adjusting to inset arrow.
				showArrow(R.id.arrow_up_bookride, requestedX);
				y = mAnchor.bottom;
				windowAnimations = R.style.QuickActionBelowAnimation;
			}
			
			setAnimationStyle(windowAnimations);
			//mTrack.startAnimation(mTrackAnim);
			this.update(x, y, -1, -1);
		}
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
}
