package com.cipl.liverycab.driverui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cipl.liverycab.dataclasses.JobListDataClass;
import com.cipl.liverycab.parserclasses.GetJsonObjectClass;
import com.cipl.liverycab.parserclasses.JobLIstParserClass;
import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;
//import com.sun.istack.internal.FinalArrayList;

public class JobListActivity extends Activity implements OnClickListener{

	private ListView jobList;
	private JobLIstParserClass parserObject;
	private int limit = 0;
	private EfficientAdapter jobListAdapter;
	private int index =0;
	private Button btnMenuJobList,btnRefreshJobList,buttonMapViewJobList;
	private ProgressDialog pd;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.joblist);
		initControls();	
	}
	
	private void initControls() {
		// TODO Auto-generated method stub
		jobList = (ListView)findViewById(R.id.listviewJobList);
		
		btnMenuJobList = (Button)findViewById(R.id.buttonMenuJobList);
		btnMenuJobList.setOnClickListener(this);
		
		btnRefreshJobList =(Button)findViewById(R.id.buttonRefreshJobList);
		btnRefreshJobList.setOnClickListener(this);
		
		buttonMapViewJobList = (Button)findViewById(R.id.buttonMapViewJobList);
		buttonMapViewJobList.setOnClickListener(this);
		
		jobListAdapter = new EfficientAdapter(JobListActivity.this,LiveryCabStatics.jobList);
		jobList.setAdapter(jobListAdapter);
		//Click on list item
		jobList.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				index = position;
				Intent detailIntent = new Intent(JobListActivity.this,JobDetailActivity.class);
				detailIntent.putExtra("Index", position);
				startActivity(detailIntent);
				
			}
		});
		/**Fetch data from server*/
		//fetchJobList("");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.buttonMenuJobList:
			btnMenu();
			break;
			
		case R.id.buttonRefreshJobList:
			btnrefresh();
			break;
		
		case R.id.buttonMapViewJobList:
			btnMapViewJobList();
			break;

		default:
			break;
		}
	}
	
	private void btnMapViewJobList() {
		// TODO Auto-generated method stub
		startActivity(new Intent(JobListActivity.this,JobListMapViewActivity.class));
	}

	private void btnrefresh() {
		// TODO Auto-generated method stub
		if(LiveryCabStaticMethods.isInternetAvailable(this)){
			fetchJobList("");
		}		
	}

	private void btnMenu() {
		// TODO Auto-generated method stub
		finish();
		Intent intent = new Intent(JobListActivity.this,DriverMenuActivity.class);
		startActivity(intent);
	}
	

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		LiveryCabStatics.jobList.clear();
		if(LiveryCabStaticMethods.isInternetAvailable(this)){
			fetchJobList("");
		}
	}

	class EfficientAdapter extends BaseAdapter 
	{
		private LayoutInflater inflater;
		private ArrayList<JobListDataClass> jobList;

		public EfficientAdapter(Context context, ArrayList<JobListDataClass> jobList) 
		{
			inflater = LayoutInflater.from(context);
			this.jobList = jobList;
		}

		public int getCount() 
		{
			return jobList.size();
		}

		public Object getItem(int position) 
		{
			return null;
		}

		public long getItemId(int position) 
		{
			return 0;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {
			// A ViewHolder is used to keep references to children views
			ViewHolder holder;
			if (convertView == null) 
			{
				convertView = inflater.inflate(R.layout.joblistcustomcell,null);
				holder = new ViewHolder();
				// getting the views from the layout
				holder.driverName = (TextView) convertView
						.findViewById(R.id.textviewDriverNameJobListCustom);
				holder.pickupAddress = (TextView) convertView
						.findViewById(R.id.textviewPickupAddressJobListCustom);
				holder.destinationAddress = (TextView) convertView
						.findViewById(R.id.textviewDestinationAddressJobListCustom);
				holder.miles = (TextView) convertView
						.findViewById(R.id.textviewMilesJobListCustomCell);
				holder.bidAmount = (TextView) convertView
						.findViewById(R.id.textviewBidAmtJobListCustomCell);
				holder.waitText = (TextView) convertView
						.findViewById(R.id.textviewWaitTextJobListCustomCell);
				holder.tvPickupdate = (TextView) convertView
						.findViewById(R.id.tvPickupdate);
				convertView.setTag(holder);
			}
			else{
				holder = (ViewHolder) convertView.getTag();
			}
			holder.tvPickupdate.setVisibility(View.GONE);
			
			int beganIndexforMiles = (jobList.get(position).distanceInMiles).indexOf(".");
			holder.driverName.setText(jobList.get(position).firstName+" "+jobList.get(position).lastName);
			holder.pickupAddress.setText(jobList.get(position).pickupAdd);
			holder.destinationAddress.setText(jobList.get(position).destinationAdd);
			holder.miles.setText((jobList.get(position).distanceInMiles).substring(0,beganIndexforMiles+2)+"miles");
			holder.bidAmount.setText("$"+jobList.get(position).bidAmount);
			if(jobList.get(position).isRideCancel.equalsIgnoreCase("Y")){
					holder.waitText.setText("Ride cancelled");
		    }else if(jobList.get(position).bidAmount.equalsIgnoreCase("")
				||jobList.get(position).bidAmount.equalsIgnoreCase("0.00")){
				holder.waitText.setText("You can bid for this ride");
			}else{
				holder.waitText.setText("Waiting for confirmation");
				holder.tvPickupdate.setVisibility(View.VISIBLE);
			}
			
			holder.tvPickupdate.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String bidId = jobList.get(position).bidId;
					if(LiveryCabStaticMethods.isInternetAvailable(JobListActivity.this)){
						fetchJobList(bidId);
					}
				}
			});
			
			return convertView;
		}
		class ViewHolder 
		{
			TextView driverName,tvPickupdate,pickupAddress,destinationAddress,bidAmount,
			waitText,miles;
		}
	}
	private void fetchJobList(final String bidId){		
		
		try {
			pd = LiveryCabStaticMethods.returnProgressBar(JobListActivity.this);
			if(bidId.equalsIgnoreCase("")){
				parserObject = new JobLIstParserClass(UrlStatics
						.getJOblist(LiveryCabStatics.userId,
						URLEncoder.encode(LiveryCabStatics.currentLatitude,"utf-8"),
						URLEncoder.encode(LiveryCabStatics.currentLongitude,"utf-8"), limit),pd);
			}else{
				parserObject = new JobLIstParserClass(UrlStatics
						.delJOblist(LiveryCabStatics.userId,
						URLEncoder.encode(LiveryCabStatics.currentLatitude,"utf-8"),
						URLEncoder.encode(LiveryCabStatics.currentLongitude,"utf-8"), limit,bidId),pd);
			}
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		parserObject.start();
		
		pd.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				// Setting adapter to the list
				if(LiveryCabStatics.currentLatitude.equalsIgnoreCase("0.0") || 
						LiveryCabStatics.currentLongitude.equalsIgnoreCase("0.0")){
					LiveryCabStaticMethods.showAlert(JobListActivity.this, 
						"Location Error!!","Current location is not available",R.drawable.attention,true,false,"Ok",null,null,null);
				}else if(LiveryCabStatics.jobList.isEmpty()){
					LiveryCabStaticMethods.showAlert(JobListActivity.this, 
						"No Records!!","No job found for you",R.drawable.attention,true,false,"Ok",null,null,null);
				}else if(LiveryCabStatics.jobList.get(0).count.equalsIgnoreCase("0")){
					LiveryCabStaticMethods.showAlert(JobListActivity.this, 
						"No Records!!","No job found for you",R.drawable.attention,true,false,"Ok",null,null,null);
				}else{
					jobListAdapter.notifyDataSetChanged();
					jobList.setSelection(index);
					if(!bidId.equalsIgnoreCase("")){
						showToast("Your bid deleted successfully",0);
					}
				}
			}
		});		 
	}

	public void showToast(String string, int i) {
		Toast.makeText(JobListActivity.this, string, i).show();
	}
	
}
