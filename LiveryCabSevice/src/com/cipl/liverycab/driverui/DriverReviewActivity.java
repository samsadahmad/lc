package com.cipl.liverycab.driverui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.cipl.liverycab.parserclasses.DriverReviewListParser;
import com.cipl.liverycab.passengerui.PassengerMenuActivity;
import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class DriverReviewActivity extends Activity {

	private EfficientAdapter jobListAdapter;
	private ListView reviewList;
	private DriverReviewListParser parserObject;
	private String driverId,driverName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.driverreview);
		
		Bundle extras = getIntent().getExtras();
		driverId = extras != null? extras.getString("DriverId"): "";
		driverName = extras != null? extras.getString("DriverName"): "";
		
		reviewList = (ListView)findViewById(R.id.listviewDriverReviews);
		parserObject = new DriverReviewListParser(UrlStatics
				.getReviewList(driverId),DriverReviewActivity.this);
		parserObject.start();
		
		LiveryCabStaticMethods.returnProgressBar(DriverReviewActivity.this)
		.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface arg0) {
				// TODO Auto-generated method stub
				if(LiveryCabStatics.reviewList.isEmpty()){
					LiveryCabStaticMethods.showAlert(DriverReviewActivity.this, 
						"No review!!","There is no current review for "+driverName,R.drawable.attention,true,false,"Ok",null,null,null);
				}else if(LiveryCabStatics.reviewList.get(0).status.equalsIgnoreCase("0")){
					LiveryCabStaticMethods.showAlert(DriverReviewActivity.this, 
						"No review!!","There is no current review for "+driverName,R.drawable.attention,true,false,"Ok",null,null,null);
				}else{
					reviewList.setAdapter(new EfficientAdapter(DriverReviewActivity.this));
				}
			}
		});
		
		
		((Button)findViewById(R.id.buttonMenuDriverReviews))
    		.setOnClickListener(new OnClickListener() {
    		@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(DriverReviewActivity.this,PassengerMenuActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			}
    	});
		
		reviewList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				AlertDialog.Builder reviewDialog = new Builder(DriverReviewActivity.this) ;
				reviewDialog.setTitle(LiveryCabStatics.reviewList.get(position).fName+" "+LiveryCabStatics.reviewList.get(position).lName);
				reviewDialog.setMessage(LiveryCabStatics.reviewList.get(position).reviewMsg);
				reviewDialog.setIcon(null);
				reviewDialog.setNeutralButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.cancel();
					}
				});
				reviewDialog.show();
			}
		});
	}
	
	class EfficientAdapter extends BaseAdapter 
	{
		private LayoutInflater inflater;

		public EfficientAdapter(Context context) 
		{
			inflater = LayoutInflater.from(context);
		}

		public int getCount() 
		{
			return LiveryCabStatics.reviewList.size();
		}

		public Object getItem(int position) 
		{
			return null;
		}

		public long getItemId(int position) 
		{
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			// A ViewHolder is used to keep references to children views
			ViewHolder holder;
			if (convertView == null) 
			{
				convertView = inflater.inflate(R.layout.driverreviewcustomcell,null);
				holder = new ViewHolder();
				holder.passengerName = (TextView) convertView
						.findViewById(R.id.textViewDriverNameDriverReview);
				holder.msgReview = (TextView) convertView
						.findViewById(R.id.textViewReviewDriverReview);
				convertView.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) convertView.getTag();
			}
			holder.passengerName.setText(LiveryCabStatics.reviewList.get(position).fName+" "+LiveryCabStatics.reviewList.get(position).lName);
			holder.msgReview.setText(LiveryCabStatics.reviewList.get(position).reviewMsg);
			return convertView;
		}
	}
	class ViewHolder 
	{
		TextView passengerName;
		TextView msgReview;
	}
}
