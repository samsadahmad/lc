package com.cipl.liverycab.driverui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.cipl.liverycab.parserclasses.JobLIstParserClass;
import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class JobListMapViewActivity extends MapActivity {

	private JobLIstParserClass parserObject;
	private int limit = 0;
	private MapController mapController;
	private MapView mapView;
	private SitesOverlay mapOverlay;
	private Drawable marker;
	private LinearLayout zoomLayout;
	private ProgressDialog pd;
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.joblistmapview);

		//getting map view and some basic controls on it 
		marker = getResources().getDrawable(R.drawable.bluebgforpin);

		mapView = (MapView) findViewById(R.id.mapviewJobListMapView);
		mapController = mapView.getController();

		zoomLayout = (LinearLayout) findViewById(R.id.linearLayoutzoomJobListMapView);
		View zoomView = mapView.getZoomControls();

		zoomLayout.addView(zoomView, new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		mapView.displayZoomControls(true);
		mapController = mapView.getController();
		mapController.setZoom(14);

		//mapView.invalidate();
		if(LiveryCabStaticMethods.isInternetAvailable(JobListMapViewActivity.this)){
			DataFetchFromServer();			
		}	



		((Button)findViewById(R.id.buttonListViewJobListMapView))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(LiveryCabStaticMethods.isInternetAvailable(JobListMapViewActivity.this)){

					startActivity(new Intent(JobListMapViewActivity.this,JobListActivity.class));
				}				
			}
		});

		((Button)findViewById(R.id.buttonMenuJobListMapView))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
				Intent intent = new Intent(JobListMapViewActivity.this,DriverMenuActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});

		((Button)findViewById(R.id.buttonRefreshJobListMapView))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				DataFetchFromServer();
			}
		});

	}

	private void DataFetchFromServer() {
		// TODO Auto-generated method stub
		pd = LiveryCabStaticMethods.returnProgressBar(JobListMapViewActivity.this);
		try {
			parserObject = new JobLIstParserClass(UrlStatics
					.getJOblist(LiveryCabStatics.userId,URLEncoder.encode(LiveryCabStatics.currentLatitude,"utf-8"),
							URLEncoder.encode(LiveryCabStatics.currentLongitude,"utf-8"), limit), pd);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		parserObject.start();
		
		
		pd.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				// Setting adapter to the list
				if(LiveryCabStatics.currentLatitude.equalsIgnoreCase("0.0") || 
						LiveryCabStatics.currentLongitude.equalsIgnoreCase("0.0")){
					LiveryCabStaticMethods.showAlert(JobListMapViewActivity.this, 
							"Location Error!!","Current location is not available",R.drawable.attention,true,false,"Ok",null,null,null);
				}else if(LiveryCabStatics.jobList.isEmpty()){
					LiveryCabStaticMethods.showAlert(JobListMapViewActivity.this, 
							"No Records!!","No job found for you",R.drawable.attention,true,false,"Ok",null,null,null);
				}else if(LiveryCabStatics.jobList.get(0).count.equalsIgnoreCase("0")){
					LiveryCabStaticMethods.showAlert(JobListMapViewActivity.this, 
							"No Records!!","No job found for you",R.drawable.attention,true,false,"Ok",null,null,null);
				}else{
					mapOverlay = new SitesOverlay(marker);
					List<Overlay> listOfOverlays = mapView.getOverlays();
					listOfOverlays.clear();
					listOfOverlays.add(mapOverlay);
				}
			}
		});
	}

	private GeoPoint getPoint(double lat, double lon) {
		return (new GeoPoint((int) (lat * 1000000.0), (int) (lon * 1000000.0)));
	}

	private class SitesOverlay extends ItemizedOverlay<OverlayItem> {
		private List<OverlayItem> items = new ArrayList<OverlayItem>();
		private Drawable marker = null;
		private Bitmap mBubbleIcon;
		private Paint mInnerPaint, mBorderPaint, mTextPaint;

		public SitesOverlay(Drawable marker) {
			super(marker);
			this.marker = marker;	
			mBubbleIcon = BitmapFactory.decodeResource(getResources(), R.drawable.manformap);
			for(int i=0; i< LiveryCabStatics.jobList.size();i++)
			{
				if(LiveryCabStatics.jobList.get(i).isRideCancel.equalsIgnoreCase("N")){
					items.add(new OverlayItem(getPoint(
							Double.parseDouble(LiveryCabStatics.jobList.get(i).pickUpLatitude),
							Double.parseDouble(LiveryCabStatics.jobList.get(i).pickupLongitude)),
							LiveryCabStatics.jobList.get(i).firstName +" "
									+LiveryCabStatics.jobList.get(i).lastName,
									LiveryCabStatics.jobList.get(i).pickupAdd));
					mapController.animateTo(getPoint(
							Double.parseDouble(LiveryCabStatics.jobList.get(i).pickUpLatitude),
							Double.parseDouble(LiveryCabStatics.jobList.get(i).pickupLongitude)));
				}

			}			
			populate();
		}


		@Override
		protected OverlayItem createItem(int i) {
			return (items.get(i));
		}

		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			super.draw(canvas, mapView, shadow);
			//Set Marker only
			//boundCenterBottom(marker);
			//Set marker with canvas
			drawMapLocations(canvas, mapView, shadow);
		}

		private void drawMapLocations(Canvas canvas, MapView	mapView, boolean shadow) {	    	

			Point screenCoords = new Point();
			try {
				int itemSize = items.size();
				int listSize = LiveryCabStatics.jobList.size();
				if(listSize >= itemSize){
					for(int i=0; i < itemSize; i++){
						mapView.getProjection().toPixels(items.get(i).getPoint(), screenCoords);
						//canvas.drawBitmap(mBubbleIcon, screenCoords.x, screenCoords.y - mBubbleIcon.getHeight(),null);
						canvas.drawBitmap(mBubbleIcon, screenCoords.x - mBubbleIcon.getWidth()/2, screenCoords.y - mBubbleIcon.getHeight(),null);

						//Setup the info window
						int INFO_WINDOW_WIDTH = 100;
						int INFO_WINDOW_HEIGHT = 50;
						RectF infoWindowRect = new RectF(0,0,INFO_WINDOW_WIDTH,INFO_WINDOW_HEIGHT);				
						int infoWindowOffsetX = screenCoords.x-INFO_WINDOW_WIDTH/2;
						int infoWindowOffsetY = screenCoords.y;
						infoWindowRect.offset(infoWindowOffsetX,infoWindowOffsetY);

						//Drawing the inner info window
						canvas.drawRoundRect(infoWindowRect, 5, 5, getmInnerPaint());

						//Drawing the border for info window
						canvas.drawRoundRect(infoWindowRect, 5, 5, getmBorderPaint());

						//  Draw the MapLocation's name
						int TEXT_OFFSET_X = 10;
						int TEXT_OFFSET_Y = 15;

						Paint strokePaint = new Paint();
						strokePaint.setARGB(255, 255, 255, 255);	              
						strokePaint.setStyle(Style.STROKE);  
						
						//int beganIndexforMiles = (LiveryCabStatics.jobList.get(i).distanceInMiles).indexOf(".");
						String passengerName = ""+LiveryCabStatics.jobList.get(i).firstName+" "+LiveryCabStatics.jobList.get(i).lastName;

						canvas.drawText(passengerName,infoWindowOffsetX+TEXT_OFFSET_X,infoWindowOffsetY+TEXT_OFFSET_Y,getmTextPaint());
						canvas.drawText("ETA: "+LiveryCabStatics.jobList.get(i).etaTime,infoWindowOffsetX+TEXT_OFFSET_X,infoWindowOffsetY+TEXT_OFFSET_Y+20,getmTextPaint());
					}
				}
			} catch (Exception e) {
				
			}				    
		}


		@Override
		protected boolean onTap(int index) {
			OverlayItem itemClicked = items.get(index);
			GeoPoint p = itemClicked.getPoint();
			Point screenPts = new Point();
			mapView.getProjection().toPixels(p, screenPts);
			Rect rect = new Rect(screenPts.x, screenPts.y + 60, screenPts.x
					+ mapView.getWidth(), screenPts.y + 60);
			QuickActionJoblist qa = new QuickActionJoblist(JobListMapViewActivity.this,mapView,rect);
			qa.setTitle(LiveryCabStatics.jobList.get(index).firstName+" "+LiveryCabStatics.jobList.get(index).lastName);
			qa.setText(LiveryCabStatics.jobList.get(index).pickupAdd);
			int beganIndexforMiles = (LiveryCabStatics.jobList.get(index).distanceInMiles).indexOf(".");
			qa.setTextMiles((LiveryCabStatics.jobList.get(index).distanceInMiles).substring(0,beganIndexforMiles+2)+" miles");
			qa.setIcon(R.drawable.blue_arrow);
			Intent jobDetailIntent = new Intent(JobListMapViewActivity.this,JobDetailActivity.class);
			jobDetailIntent.putExtra("Index",index);
			qa.setIntent(jobDetailIntent);
			qa.show();
			return true;
		}
		@Override
		public int size() {
			return (items.size());
		}

		public Paint getmInnerPaint() {
			if ( mInnerPaint == null) {
				mInnerPaint = new Paint();
				mInnerPaint.setARGB(225, 50, 50, 50); //inner color
				mInnerPaint.setAntiAlias(true);
			}
			return mInnerPaint;
		}

		public Paint getmBorderPaint() {
			if ( mBorderPaint == null) {
				mBorderPaint = new Paint();
				mBorderPaint.setARGB(255, 255, 255, 255);
				mBorderPaint.setAntiAlias(true);
				mBorderPaint.setStyle(Style.STROKE);
				mBorderPaint.setStrokeWidth(2);
			}
			return mBorderPaint;
		}

		public Paint getmTextPaint() {
			if ( mTextPaint == null) {
				mTextPaint = new Paint();
				mTextPaint.setARGB(255, 255, 255, 255);
				mTextPaint.setAntiAlias(true);
			}
			return mTextPaint;
		}
		@SuppressWarnings("unused")
		public void addOverlay(OverlayItem overlay) {
			// add item to our overlay
			items.add(overlay);
			populate();
		}
	}
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
}
