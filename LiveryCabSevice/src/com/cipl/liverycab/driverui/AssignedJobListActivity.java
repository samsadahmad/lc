package com.cipl.liverycab.driverui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cipl.liverycab.parserclasses.AssignedJobListParser;
import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class AssignedJobListActivity extends Activity {

	private ListView assignedJobList;
	private AssignedJobListParser parserObject;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.assignedjoblist);
		
		Bundle extras = getIntent().getExtras();
		final String pickupid = extras!=null ? extras.getString("pickupid"):"";
		
		assignedJobList = (ListView)findViewById(R.id.listviewAssignedJobList);
		
		
		if(LiveryCabStaticMethods.isInternetAvailable(this)){
			parserObject = new AssignedJobListParser(UrlStatics
					.getAssignedJOblist(LiveryCabStatics.userId));
			parserObject.start();
			
			LiveryCabStaticMethods.returnProgressBar(AssignedJobListActivity.this)
			.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				// Setting adapter to the list
				if(LiveryCabStatics.assignedJobList.isEmpty()){
					LiveryCabStaticMethods.showAlert(AssignedJobListActivity.this,"Error Occured!",
							"Please wait until a passenger books a ride",R.drawable.attention,true,false,"Ok",null,null,null);
				}else if(LiveryCabStatics.assignedJobList.get(0).message.equalsIgnoreCase("Record_not_found")){
					LiveryCabStaticMethods.showAlert(AssignedJobListActivity.this,"Error Occured!",
							"Please wait until a passenger books a ride",R.drawable.attention,true,false,"Ok",null,null,null);
				}else if(LiveryCabStatics.isNotificationReceive){
					assignedJobList.setAdapter(new EfficientAdapter(AssignedJobListActivity.this));
					int listSize=LiveryCabStatics.assignedJobList.size();
					int count=0;
					for(;count<listSize;count++){
						if(LiveryCabStatics.assignedJobList.get(count).pickupId.equals(pickupid)){
							break;
						}
					}
					if(count<listSize){
						Intent intent = new Intent(AssignedJobListActivity.this,AssignedJobDetailActivity.class);
						intent.putExtra("Index", count);
						startActivity(intent);
					}
				}else{
					assignedJobList.setAdapter(new EfficientAdapter(AssignedJobListActivity.this));
				}
			}
		  });
		}		
		
		assignedJobList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				Intent assignedJobIntent = new Intent(AssignedJobListActivity.this,AssignedJobDetailActivity.class);
				assignedJobIntent.putExtra("Index",position);
				startActivity(assignedJobIntent);
			}
		});
		
		((Button)findViewById(R.id.buttonMenuAssignedJobList))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(AssignedJobListActivity.this,DriverMenuActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			}
		});
	}
	class EfficientAdapter extends BaseAdapter 
	{
		private LayoutInflater inflater;

		public EfficientAdapter(Context context) 
		{
			inflater = LayoutInflater.from(context);
		}

		public int getCount() 
		{
			return LiveryCabStatics.assignedJobList.size();
		}

		public Object getItem(int position) 
		{
			return null;
		}

		public long getItemId(int position) 
		{
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			// A ViewHolder is used to keep references to children views
			ViewHolder holder;
			if (convertView == null) 
			{
				convertView = inflater.inflate(R.layout.joblistcustomcell,null);
				holder = new ViewHolder();
				// getting the views from the layout
				holder.driverName = (TextView) convertView
						.findViewById(R.id.textviewDriverNameJobListCustom);
				holder.pickupAddress = (TextView) convertView
						.findViewById(R.id.textviewPickupAddressJobListCustom);
				holder.destinationAddress = (TextView) convertView
						.findViewById(R.id.textviewDestinationAddressJobListCustom);
				holder.miles = (TextView) convertView
						.findViewById(R.id.textviewMilesJobListCustomCell);
				holder.bidAmount = (TextView) convertView
						.findViewById(R.id.textviewBidAmtJobListCustomCell);
				holder.waitText = (RelativeLayout) convertView
						.findViewById(R.id.relativelayoutWaitingTextBgJobListCustomCell);
				convertView.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) convertView.getTag();
			}
			int beganIndexforMiles = (LiveryCabStatics.assignedJobList.get(position).distanceInMiles).indexOf(".");
			holder.driverName.setText(LiveryCabStatics.assignedJobList.get(position).firstName+" "+LiveryCabStatics.assignedJobList.get(position).lastName);
			holder.pickupAddress.setText(LiveryCabStatics.assignedJobList.get(position).pickupAdd);
			holder.destinationAddress.setText(LiveryCabStatics.assignedJobList.get(position).destinationAdd);
			holder.miles.setText((LiveryCabStatics.assignedJobList.get(position).distanceInMiles).substring(0,beganIndexforMiles+2)+" miles");
			holder.bidAmount.setText("$"+LiveryCabStatics.assignedJobList.get(position).bidAmount);
			holder.waitText.setVisibility(RelativeLayout.GONE);
			return convertView;
		}
	}
	class ViewHolder 
	{
		TextView driverName;
		TextView pickupAddress;
		TextView destinationAddress;
		TextView bidAmount;
		RelativeLayout waitText;
		TextView miles;
	}
}
