package com.cipl.liverycab.driverui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.cipl.liverycab.passengerui.BookRideActivity;
import com.cipl.liverycab.passengerui.LiveryCabActivity;
import com.cipl.liverycab.passengerui.LoginActivity;
import com.cipl.liverycab.passengerui.MapViewActivity;
import com.cipl.liverycab.passengerui.PassengerMenuActivity;
import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class DriverMenuActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drivermenu);
		
		  ((Button)findViewById(R.id.buttonJobListDriverMenu))
		 	.setOnClickListener(new OnClickListener(){
		 		@Override
		 		public void onClick(View v) {
		 		LiveryCabStatics.passengerMenuOptionId = "JobList";
				startActivity(new Intent(DriverMenuActivity.this,JobListMapViewActivity.class));
			}
		});
		  
		  ((Button)findViewById(R.id.buttonAssignedJobDriverMenu))
		 	.setOnClickListener(new OnClickListener(){
		 		@Override
		 		public void onClick(View v) {
		 			LiveryCabStatics.passengerMenuOptionId = "AssignedJobList";
					if(LiveryCabStatics.isLoginDriver){
						startActivity(new Intent(DriverMenuActivity.this,AssignedJobListActivity.class));
					}else{
						LiveryCabStaticMethods.showAlert(DriverMenuActivity.this,
								"Login ","Please Log in to continue",R.drawable.attention, true, false, 
								"Ok", new DialogInterface.OnClickListener() 
						{
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								// TODO Auto-generated method stub
								startActivity(new Intent(DriverMenuActivity.this,LoginActivity.class));
							}
						},null,null);
					}
			}
		});
		  ((Button)findViewById(R.id.buttonProfileDriverMenu))
		 	.setOnClickListener(new OnClickListener(){
		 		@Override
		 		public void onClick(View v) {
		 		LiveryCabStatics.passengerMenuOptionId = "Profile";
				if(LiveryCabStatics.isLoginDriver){
					startActivity(new Intent(DriverMenuActivity.this,DriverProfileActivity.class));
				}else{
					LiveryCabStaticMethods.showAlert(DriverMenuActivity.this,
						"Login ","Please Log in to continue",R.drawable.attention, true, false, 
						"Ok", new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							// TODO Auto-generated method stub
							startActivity(new Intent(DriverMenuActivity.this,LoginActivity.class));
						}
					},null,null);
				}
			}
		});
		  
		  ((Button)findViewById(R.id.buttonLogoutDriverMenu))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				LiveryCabStatics.isLoginDriver = false;
				LiveryCabStatics.driverId = null;
				LiveryCabStatics.userId = null;
				
				startActivity(new Intent(DriverMenuActivity.this,LiveryCabActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				deletePreferences("USER_TYPE");
				deletePreferences("PASS_ID");
				deletePreferences("DRIVER_ID");
				deletePreferences("TRUE_PASSENGER");
			}
		});
		  
		// This is method for Contact Developer
			((Button) findViewById(R.id.buttonContactDriverMenu))
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							
							 Intent emailIntent = new Intent(
										android.content.Intent.ACTION_SEND);
								emailIntent.setType("plain/text");
								String msg = "";
								emailIntent.putExtra(
										android.content.Intent.EXTRA_SUBJECT,
										"Contact Liverycab Support");
								emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
										new String[]{"help@liverycab.com"});
								emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
										msg);
								startActivity(Intent.createChooser(emailIntent,
										"Send mail..."));
						}
					});
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
		Intent intent = new Intent(DriverMenuActivity.this,LiveryCabActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
	
	/** Method used to get Shared Preferences */
	public SharedPreferences getPreferences() 
	{
	    return getSharedPreferences("LOGIN_DETAILS", MODE_PRIVATE);
	}
	/** Method used to delete Preferences */
	public boolean deletePreferences(String key)
	{
	    SharedPreferences.Editor editor=getPreferences().edit();
	    editor.remove(key).commit();
	    return false;
	}
}