package com.cipl.liverycab.driverui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cipl.liverycab.dataclasses.DriverProfileParser;
import com.cipl.liverycab.passengerui.ChangePasswordActivity;
import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class DriverProfileActivity extends Activity implements OnClickListener {

	private ImageView driverimgImageView,carImageimgView;
	private TextView driverNameTxtView,emailTxtView,dlPlateNumberTxtView,
			numberOfReviewsTxtView,phoneNumTxtView,mtvHandicappedValue,mtvDriverLikeValue;
	private String settingMiles;
	public static DriverProfileParser objectParser;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profiledriver);
		
		inItView();
		
		
	}
	private void inItView() {
		// TODO Auto-generated method stub
		driverimgImageView = (ImageView)findViewById(R.id.imageviewDriverImageProfileDriver);
		carImageimgView = (ImageView)findViewById(R.id.imageviewCarImageProfileDriver);
		driverNameTxtView = (TextView)findViewById(R.id.textviewDriverNameProfileDriver);
		emailTxtView = (TextView)findViewById(R.id.textviewEmailAddressProfileDriver);
		dlPlateNumberTxtView = (TextView)findViewById(R.id.textviewLicencePlateNumberProfileDriver);
		numberOfReviewsTxtView = (TextView)findViewById(R.id.textviewDriverReviewProfileDriver);
		phoneNumTxtView = (TextView)findViewById(R.id.textviewPhoneNumberProfileDriver);
		mtvDriverLikeValue = (TextView)findViewById(R.id.tvDriverLikeValue);
		mtvHandicappedValue = (TextView)findViewById(R.id.tvHandicappedValue);
		
		Button mbuttonEditProfileProfileDriver = (Button)findViewById(R.id.buttonEditProfileProfileDriver);
		mbuttonEditProfileProfileDriver.setOnClickListener(this);
		
		Button mbuttonMenuProfileDriver = (Button)findViewById(R.id.buttonMenuProfileDriver);
		mbuttonMenuProfileDriver.setOnClickListener(this);
		
		Button mbuttonchangePasswordProfileDriver = (Button)findViewById(R.id.buttonchangePasswordProfileDriver);
		mbuttonchangePasswordProfileDriver.setOnClickListener(this);
		
		Button mbuttonSettingProfileDriver = (Button)findViewById(R.id.buttonSettingProfileDriver);
		mbuttonSettingProfileDriver.setOnClickListener(this);
		
		if(LiveryCabStaticMethods.isInternetAvailable(this)){
			objectParser = new DriverProfileParser(UrlStatics.getProfile(
					LiveryCabStatics.driverId,LiveryCabStatics.userType),
					DriverProfileActivity.this);
			objectParser.start();
			
			LiveryCabStaticMethods.returnProgressBar(DriverProfileActivity.this)
			.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				// TODO Auto-generated method stub
				driverNameTxtView.setText(objectParser.dataObject.firstName+" "+objectParser.dataObject.lastName);
				mtvHandicappedValue.setText(objectParser.dataObject.handicapped.equalsIgnoreCase("Y") ? "Yes" : "No");
				emailTxtView.setText(objectParser.dataObject.email);
				emailTxtView.setMovementMethod(new ScrollingMovementMethod());
				dlPlateNumberTxtView.setText(objectParser.dataObject.dlNumber);
				dlPlateNumberTxtView.setMovementMethod(new ScrollingMovementMethod());
				numberOfReviewsTxtView.setText("("+objectParser.dataObject.numberOfReviews+")");
				phoneNumTxtView.setText(objectParser.dataObject.phone);
				phoneNumTxtView.setMovementMethod(new ScrollingMovementMethod());
				mtvDriverLikeValue.setText("("+""+objectParser.dataObject.ratingValue+")");
				settingMiles = objectParser.dataObject.settingInfo;
				if(objectParser.dataObject.driverImage!=null)
					driverimgImageView.setImageDrawable(objectParser.dataObject.driverImage);
				if(objectParser.dataObject.carImage!=null)
					carImageimgView.setImageDrawable(objectParser.dataObject.carImage);
			}
		  });
	    }
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.buttonSettingProfileDriver:
			settingProfile();
			break;
		
		case R.id.buttonchangePasswordProfileDriver:
			changePassword();
			break;
			
		case R.id.buttonMenuProfileDriver:
			profileDrriver();
			break;
		
		case R.id.buttonEditProfileProfileDriver:
			editProfileDrriver();
			break;
			
		default:
			break;
		}
	}
	private void editProfileDrriver() {
		// TODO Auto-generated method stub
		startActivity(new Intent(DriverProfileActivity.this,
				DriverEditProfileActivity.class));
		finish();
	}
	private void profileDrriver() {
		// TODO Auto-generated method stub
		startActivity(new Intent(DriverProfileActivity.this,DriverMenuActivity.class)
		.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
	}
	private void changePassword() {
		// TODO Auto-generated method stub
		startActivity(new Intent(DriverProfileActivity.this,ChangePasswordActivity.class));
	}
	private void settingProfile() {
		// TODO Auto-generated method stub
		Intent settingIntent = new Intent(DriverProfileActivity.this,SettingsActivity.class);
		settingIntent.putExtra("Miles",settingMiles);
		startActivity(settingIntent);
	}
}