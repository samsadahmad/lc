package com.cipl.liverycab.driverui;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class SettingsActivity extends Activity {

	private ForgotPasswordParserClass parserObject;
	private ProgressDialog pd;
	private String message = "";
	private Button saveButton;
	private Spinner milesSpinner;
	private String selectedMIles;
	private String settingMiles;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		
		saveButton = (Button)findViewById(R.id.buttonSaveSettings);
		milesSpinner = (Spinner)findViewById(R.id.spinnerMilesSetting);
		
		Bundle extras = getIntent().getExtras();
		settingMiles = extras!=null ? extras.getString("Miles"):"";
		
		ArrayAdapter<CharSequence> distanceArray = ArrayAdapter
				.createFromResource(SettingsActivity.this,R.array.distanceArray,
						android.R.layout.simple_spinner_item);
		distanceArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		milesSpinner.setAdapter(distanceArray);
		int index = distanceArray.getPosition(settingMiles);
		milesSpinner.setSelection(index);
		
		milesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				selectedMIles = milesSpinner.getItemAtPosition(arg2).toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				selectedMIles= "";
			}
		});
		
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pd = LiveryCabStaticMethods.returnProgressBar(SettingsActivity.this);
				try {
					parserObject =  new ForgotPasswordParserClass(UrlStatics.getSettingsUrl(LiveryCabStatics.userId,
									URLEncoder.encode(selectedMIles,"utf-8")),pd);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				parserObject.start();
				pd.setOnDismissListener(new OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						if(parserObject.actualDismiss){
							if(parserObject.loginDataObj!=null)
								message = parserObject.loginDataObj.message;
								if(message.equalsIgnoreCase("save_succefully_settings")){
									LiveryCabStaticMethods.showAlert(SettingsActivity.this,
									"Success","Settings saved successfully",R.drawable.success, true, false, 
									"Ok",  new DialogInterface.OnClickListener(){
										@Override
										public void onClick(DialogInterface arg0, int arg1) {
											finish();
									}
								},null,null);
								}else if(message.equalsIgnoreCase("Session_expire")){
									LiveryCabStaticMethods.showAlert(SettingsActivity.this,
											"Error Occured!","Session has been expired",R.drawable.attention, true, false, 
											"Ok", null,null,null);
								}else{
									LiveryCabStaticMethods.showAlert(SettingsActivity.this,
											"Error Occured!","Try again!",R.drawable.attention, true, false, 
											"Ok", null,null,null);
								}
						}
					}
				});
			}
		});
	}
}
