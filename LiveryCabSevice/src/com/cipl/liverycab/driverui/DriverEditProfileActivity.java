package com.cipl.liverycab.driverui;

import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class DriverEditProfileActivity extends Activity {

	private ImageView driverimgImageView,carImageimgView;
	private EditText firstName,lastName,emailTxtView,dlPlateNumberTxtView,
			phoneNumTxtView;
	private int SELECT_PICTURE_DRIVER = 0;
	private int START_CAMERA_FORDRIVER = 1;

	private int SELECT_PICTURE_CAR = 2;
	private int START_CAMERA_FORCAR = 3;
	
	private String[] pictureItemsMenu = {"Choose Existing Photo","Take New Photo" };
	private ProgressDialog pd;
	
	String selectedPathDriverImage = "NONE";
	String selectedPathCarImage = "NONE";
	JSONObject jObject = null;
	JSONObject jobj ;
	private String EditPassengerResult = "";
	private Uri mImageCaptureUri_driver,mImageCaptureUri_Pass;
	private File fileDriver,filePass;
	private CheckBox mimabtnCheckHandicam;
	private String mhandicapped = "N";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drivereditprofile);
		
		driverimgImageView = (ImageView)findViewById(R.id.imageviewDriverPicDriverEditProfile);
		carImageimgView = (ImageView)findViewById(R.id.imageviewCarPicDriverEditProfile);
		
		firstName = (EditText)findViewById(R.id.edittextFirstNameDriverEditProfile);
		lastName = (EditText)findViewById(R.id.edittextLastNameDriverEditProfile);
		emailTxtView = (EditText)findViewById(R.id.edittextEmailDriverEditProfile);
		dlPlateNumberTxtView = (EditText)findViewById(R.id.edittextLicencePlateNumberDriverEditProfile);
		phoneNumTxtView = (EditText)findViewById(R.id.edittextCellPhoneNumberDriverEditProfile);
		
		firstName.setText(DriverProfileActivity.objectParser.dataObject.firstName);
		lastName.setText(DriverProfileActivity.objectParser.dataObject.lastName);
		emailTxtView.setText(DriverProfileActivity.objectParser.dataObject.email);
		dlPlateNumberTxtView.setText(DriverProfileActivity.objectParser.dataObject.dlNumber);
		phoneNumTxtView.setText(DriverProfileActivity.objectParser.dataObject.phone);
		
		mimabtnCheckHandicam = (CheckBox)findViewById(R.id.imabtnCheckHandicam);
		if(DriverProfileActivity.objectParser.dataObject.handicapped.equalsIgnoreCase("Y")){
			mimabtnCheckHandicam.setChecked(true);
		}
		
		if(DriverProfileActivity.objectParser.dataObject.driverImage!=null)
			driverimgImageView.setImageDrawable(DriverProfileActivity.objectParser.dataObject.driverImage);
		if(DriverProfileActivity.objectParser.dataObject.carImage!=null)
			carImageimgView.setImageDrawable(DriverProfileActivity.objectParser.dataObject.carImage);
		
		((TextView)findViewById(R.id.textviewEditDriverPicDriverEditProfile))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						DriverEditProfileActivity.this);
				builder.setTitle("Select Picture");
				builder.setItems(pictureItemsMenu,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int item) {
								if (item == 0) {
									Intent intent = new Intent();
									intent.setType("image/*");
									intent.setAction(Intent.ACTION_GET_CONTENT);
									startActivityForResult(Intent
											.createChooser(intent,
													"Select Picture"),
											SELECT_PICTURE_DRIVER);
								} else if (item == 1) {
									Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					                fileDriver = new File(Environment.getExternalStorageDirectory(),
					                                    "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
					                mImageCaptureUri_driver = Uri.fromFile(fileDriver);

					                try {
					                    intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri_driver);
					                    intent.putExtra("return-data", true);

					                    startActivityForResult(intent, START_CAMERA_FORDRIVER);
					                } catch (Exception e) {
					                    e.printStackTrace();
					                }
								}
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
		
		((TextView)findViewById(R.id.textviewEditCarPicDriverEditProfile))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						DriverEditProfileActivity.this);
				builder.setTitle("Select Picture");
				builder.setItems(pictureItemsMenu,new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						if (item == 0) {
							Intent intent = new Intent();
							intent.setType("image/*");
							intent.setAction(Intent.ACTION_GET_CONTENT);
							startActivityForResult(Intent.createChooser(intent,"Select Picture"),
										SELECT_PICTURE_CAR);
						} else if (item == 1) {
							Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			                filePass = new File(Environment.getExternalStorageDirectory(),
			                                    "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
			                mImageCaptureUri_Pass = Uri.fromFile(filePass);

			                try {
			                    intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri_Pass);
			                    intent.putExtra("return-data", true);

			                    startActivityForResult(intent, START_CAMERA_FORCAR);
			                } catch (Exception e) {
			                    e.printStackTrace();
			                }
						}
					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
		
		((Button)findViewById(R.id.buttonSaveDriverEditProfile))
			.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final String mEmail = emailTxtView.getText().toString();
				final String mFName = firstName.getText().toString();
				final String mLName = lastName.getText().toString();
				final String mPhone = phoneNumTxtView.getText().toString();
				final String mLicence = dlPlateNumberTxtView.getText().toString();
				if(mimabtnCheckHandicam.isChecked()){
					mhandicapped = "Y";
				}
				

				if (mEmail.equalsIgnoreCase("")) {
					emailTxtView.setError("Please enter email address");
				} else if (mFName.equalsIgnoreCase("")) {
					firstName.setError("Please enter first name");
				} else if (mLName.equalsIgnoreCase("")) {
					lastName.setError("Please enter last name");
				} else if (mPhone.equalsIgnoreCase("")) {
					phoneNumTxtView
							.setError("Please enter cell phone number");
				} else if (mLicence.equalsIgnoreCase("")) {
					dlPlateNumberTxtView
							.setError("Please enter cell phone number");
				} else if (mEmail.equalsIgnoreCase("")
						&& mFName.equalsIgnoreCase("")
						&& mLName.equalsIgnoreCase("")
						&& mPhone.equalsIgnoreCase("")) {
					LiveryCabStaticMethods.showAlert(
							DriverEditProfileActivity.this, "Error Occured!",
							"All fields are mendatory",
							R.drawable.attention, true, false, "Ok",
							null, null, null);
				} else if (!LiveryCabStaticMethods.isEmail(mEmail)) {
					LiveryCabStaticMethods.showAlert(
							DriverEditProfileActivity.this, "Error Occured!",
							"Please enter a valid email address",
							R.drawable.attention, true, false, "Ok",
							null, null, null);
				} else {
					
					
					pd = LiveryCabStaticMethods.returnProgressBar(DriverEditProfileActivity.this);
					Thread thread = new Thread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub							
							EditPassengerResult = uploadDriverData(mFName, mLName,
									mEmail, mLicence, mPhone,mhandicapped);							
						}
					});
					thread.start();
					pd.setOnDismissListener(new OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							try {
								if(EditPassengerResult.length()== 0){
									return;
								}
								jobj = new JSONObject(EditPassengerResult);
								if (jobj.getString("Status") != null&& jobj.getString("Status").equalsIgnoreCase("1")) {
									if (jobj.getString("Message").equalsIgnoreCase("profile_updated_sucessfully")) {
										LiveryCabStaticMethods.showAlert(DriverEditProfileActivity.this,"Success!!",
												"Profile updated successfully",R.drawable.success, true,false,"Ok", 
												new DialogInterface.OnClickListener() {
													@Override
													public void onClick(DialogInterface dialog, int which) {
														startActivity(new Intent(DriverEditProfileActivity.this,DriverProfileActivity.class));
														finish();
													}
												}, null, null);
									}
								} else if (jobj.getString("Status") != null&& jobj.getString("Status").equalsIgnoreCase("0")) {
									if (jobj.getString("Message").equalsIgnoreCase("session_expire")) {
										LiveryCabStaticMethods.showAlert(DriverEditProfileActivity.this,"Error Occured!",
												"You are not logged in. Please log in again",R.drawable.attention, true,false, "Ok", null, null, null);
									}else {
										LiveryCabStaticMethods.showAlert(DriverEditProfileActivity.this,"Error Occured!", "Invalid User",
												R.drawable.attention, true,false, "Ok", null, null, null);
									}
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});
				}
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == RESULT_OK) {
			//Uri selectedImageUri = data.getData();
			Uri selectedImageUri = null;
			if (data != null) {
				if (requestCode == SELECT_PICTURE_DRIVER) {
					selectedImageUri = data.getData();
					selectedPathDriverImage = getPath(selectedImageUri);
					driverimgImageView.setImageURI(Uri.parse(selectedPathDriverImage));
				}				
				if (requestCode == SELECT_PICTURE_CAR) {
					selectedImageUri = data.getData();
					selectedPathCarImage = getPath(selectedImageUri);
					carImageimgView.setImageURI(Uri.parse(selectedPathCarImage));
				}
			}else {
				if (requestCode == START_CAMERA_FORDRIVER) {
					selectedPathDriverImage = fileDriver.getAbsolutePath();
					driverimgImageView.setImageURI(Uri.parse(selectedPathDriverImage));
				}
				if (requestCode == START_CAMERA_FORCAR) {
					selectedPathCarImage = filePass.getAbsolutePath();
					carImageimgView.setImageURI(Uri.parse(selectedPathCarImage));
				}
			}			
			Toast.makeText(DriverEditProfileActivity.this, "Images Selected",
					Toast.LENGTH_LONG).show();
		}
	}

	private String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	private String uploadDriverData(String fname, String lname,
			String email,String license, String phone,String handicapped) {
		File driverImage=null;
		File carImage =null;
		String response_str = "";
		String urlString = UrlStatics.demoServerLink
				+ "LIVERYCAB_DRIVER_EDITPROFILE&uid=" +
				"&fname=&lname=&licence=&phone=&driverimage=&carimage=&handicapped=";
		if(!selectedPathDriverImage.equals("NONE"))
			driverImage = new File(selectedPathDriverImage);
		if(!selectedPathCarImage.equals("NONE"))
			carImage = new File(selectedPathCarImage);
		try {
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(urlString);
			FileBody bin1=null,bin2=null;
			MultipartEntity reqEntity = new MultipartEntity();
			if(driverImage!=null){
				bin1 = new FileBody(driverImage);
				reqEntity.addPart("driverimage", bin1);
			}
			if(carImage!=null){
					 bin2 = new FileBody(carImage);
					 reqEntity.addPart("carimage", bin2);
			}
			reqEntity.addPart("fname", new StringBody(fname));
			reqEntity.addPart("lname", new StringBody(lname));
			reqEntity.addPart("licence", new StringBody(license));
			reqEntity.addPart("phone", new StringBody(phone));
			reqEntity.addPart("handicapped", new StringBody(handicapped));
			reqEntity.addPart("uid", new StringBody(LiveryCabStatics.userId));
			post.setEntity(reqEntity);
			HttpResponse response = client.execute(post);
			HttpEntity resEntity = response.getEntity();
			response_str = EntityUtils.toString(resEntity);
			if (resEntity != null) {
				Log.i("RESPONSE", response_str);
				runOnUiThread(new Runnable() {
					public void run() {
						try {
							//jObject = new JSONObject(response_str);
							if(pd.isShowing())
								pd.dismiss();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		} catch (Exception ex) {
			Log.e("Debug", "error: " + ex.getMessage(), ex);
		}
		return response_str;
	}
}