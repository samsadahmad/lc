package com.cipl.liverycab.driverui;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.cipl.liverycab.parserclasses.ForgotPasswordParserClass;
import com.cipl.liverycab.parserclasses.GetJsonObjectClass;
import com.cipl.liverycab.passengerui.R;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;
import com.cipl.liverycab.statics.UrlStatics;

public class AssignedJobDetailActivity extends Activity {

	private int index = 0; 									//variable to get the index of clicked list
	private String pickupId;
	private Button btnComplete,btnSendNotification;         //buttons on the screen
	private ProgressDialog pd;
	private ForgotPasswordParserClass parserObject;			//Parser class object
	private String message = "";							//variable to hold response message
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.assignedjobdetail);
		Bundle extras = getIntent().getExtras();
		index = (Integer)(extras!=null ? extras.getInt("Index"):"");
		/**
		 * Getting text views from xml file
		 */
		final TextView name = (TextView)findViewById(R.id.textviewDriverNameAssignedJobDetail);
		final TextView pickUp = (TextView)findViewById(R.id.textviewPickupAddressAssignedJobDetail);
		final TextView desti = (TextView)findViewById(R.id.textviewDestinationAddressAssignedJobDetail);
		final TextView miles = (TextView)findViewById(R.id.textviewMilesAssignedJobDetail);
		final TextView dateTime = (TextView)findViewById(R.id.textviewPickupDateTimeAssignedJobDetail);
		final TextView bidAmt = (TextView)findViewById(R.id.textviewBidAmountAssignedJobDetail);
		final TextView etaTime = (TextView)findViewById(R.id.textviewGivenETATimeAssignedJobDetail);
		btnComplete  = (Button)findViewById(R.id.buttonCompleteAssignedJobDetail);
		btnSendNotification  = (Button)findViewById(R.id.buttonSendNotificationAssignedJobDetail);
		
		int beganIndexforMiles = (LiveryCabStatics.assignedJobList.get(index).distanceInMiles).indexOf(".");
		name.setText(LiveryCabStatics.assignedJobList.get(index).firstName+" "+LiveryCabStatics.assignedJobList.get(index).lastName);
		pickUp.setText(LiveryCabStatics.assignedJobList.get(index).pickupAdd);
		desti.setText(LiveryCabStatics.assignedJobList.get(index).destinationAdd);
		miles.setText((LiveryCabStatics.assignedJobList.get(index).distanceInMiles).substring(0,beganIndexforMiles+2)+" mi");
		dateTime.setText(LiveryCabStatics.assignedJobList.get(index).dateTime);
		bidAmt.setText("$"+LiveryCabStatics.assignedJobList.get(index).bidAmount);
		etaTime.setText(LiveryCabStatics.assignedJobList.get(index).etaTime+" minutes");
		
		if(LiveryCabStatics.assignedJobList.get(index).notification.equalsIgnoreCase("1"))
			btnSendNotification.setEnabled(false);

		/**
		 * Button click event: for notifying about completion of ride
		 */
		btnComplete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				if(LiveryCabStaticMethods.isInternetAvailable(AssignedJobDetailActivity.this)){
					String URL = UrlStatics.
							getUrlToCompleteRideButton(LiveryCabStatics.
							assignedJobList.get(index).pickupId,LiveryCabStatics.userId);
					new AssignJobDetailsAsyTask(URL).execute();
				}
			}
		});
		
		/**
		 * Button click event: sending notification to passenger
		 */
		btnSendNotification.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(LiveryCabStaticMethods.isInternetAvailable(AssignedJobDetailActivity.this)){
					pd = LiveryCabStaticMethods.returnProgressBar(AssignedJobDetailActivity.this);
					parserObject = new ForgotPasswordParserClass(UrlStatics.
							getApiforSendingNotification(LiveryCabStatics.assignedJobList.get(index).pickupId, LiveryCabStatics.assignedJobList.get(index).passengerId),pd);
					parserObject.start();
					
					pd.setOnDismissListener(new OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface arg0) {
							// TODO Auto-generated method stub
							if(parserObject.actualDismiss){
								if(parserObject.loginDataObj!=null)
									message = parserObject.loginDataObj.message;
								if(message.equalsIgnoreCase("ride_booked")){
									LiveryCabStaticMethods.showAlert(AssignedJobDetailActivity.this,
											"Success!!","Notification has been sent successfully to passenger.",R.drawable.success, true, false, 
											"Ok", null,null,null);
								}else if(message.equalsIgnoreCase("session_expire")){
									LiveryCabStaticMethods.showAlert(AssignedJobDetailActivity.this,
											"Error Occured!","Please Log in to Continue",R.drawable.attention, true, false, 
											"Ok", null,null,null);
								}
							}
						}
					});
					
				}
			}
		});
	}
	
	class AssignJobDetailsAsyTask  extends AsyncTask<Void, Void, String>{
		private String url;
		private String result = "";
		public AssignJobDetailsAsyTask(String url) {
			// TODO Auto-generated constructor stub
			this.url = url;
		}
		
		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			JSONObject mJSONObject = GetJsonObjectClass.getJSONObjectfromURL(url);
			if(mJSONObject != null){
				try {
					result = mJSONObject.getString("message").toString();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					result="";
				}
			}
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			pd.dismiss();
			if(result.equalsIgnoreCase("ride_completed")){
				LiveryCabStaticMethods.showAlert(AssignedJobDetailActivity.this,
						"Success!!","This ride is completed",R.drawable.success, true, false, 
						"Ok", new DialogInterface.OnClickListener() 
						{
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								startActivity(new Intent(AssignedJobDetailActivity.this,AssignedJobListActivity.class));
								finish();
						}
					},null,null);
			}else if(result.equalsIgnoreCase("session_expire")){
				LiveryCabStaticMethods.showAlert(AssignedJobDetailActivity.this,
						"Error Occured!","You are not log in. Please log in again",R.drawable.attention, true, false, 
						"Ok", null,null,null);
			}else{
				LiveryCabStaticMethods.showAlert(AssignedJobDetailActivity.this,
						"Error Occured!","Try again!",R.drawable.attention, true, false, 
						"Ok", null,null,null);
			}
			
			super.onPostExecute(result);
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = LiveryCabStaticMethods.returnProgressBar(AssignedJobDetailActivity.this);
			super.onPreExecute();
		}
	}
}