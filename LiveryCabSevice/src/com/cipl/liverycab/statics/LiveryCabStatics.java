package com.cipl.liverycab.statics;

import java.util.ArrayList;

import android.content.Context;

import com.cipl.liverycab.dataclasses.AssignedJobListDataClass;
import com.cipl.liverycab.dataclasses.BidListDataClass;
import com.cipl.liverycab.dataclasses.BookedRideDataClass;
import com.cipl.liverycab.dataclasses.CreditCardDataClass;
import com.cipl.liverycab.dataclasses.HistoryDataClass;
import com.cipl.liverycab.dataclasses.JobListDataClass;
import com.cipl.liverycab.dataclasses.NotificationData;
import com.cipl.liverycab.dataclasses.PopularAddressCategoryDataClass;
import com.cipl.liverycab.dataclasses.PopularAddressDataClass;
import com.cipl.liverycab.dataclasses.ReceentAddressDataClass;
import com.cipl.liverycab.dataclasses.ReviewDataClass;

public class LiveryCabStatics {
	
	public static boolean isDriver = false;
	public static boolean isPassenger = false;
	public static boolean isRideNow = false;
	public static boolean isRideLater = false;
	public static boolean isLoginPassenger = false;
	public static boolean isLoginDriver = false;
	public static boolean destiMapView = false;
	public static boolean recentDesti = false;
	public static boolean popularDesti = false;
	public static boolean isNotificationReceive = false;
	public static String ridestatus = "";
	public static String passengerId = "";
	public static String driverId = "";
	public static String userId = "";
	public static String registrationID = null;
	public static String DeviceID = null;
	public static String deviceType = "Android";
	public static String userType = "Driver";
	public static String passengerMenuOptionId = "BookRide";
	public static String driverMenuOptionId = "JobList";
	public static String currentAddress = "";
	public static String pickupAddress = "";
	public static String destinationAddress = "";
	public static String sourceLatitude = "";
	public static String sourceLongitude = "";
	public static String destinationLatitude ="";
	public static String destinationLongitude = "";
	public static String date_Selected = "";
	public static String currentLatitude = "";
	public static String currentLongitude = "";
	public static CreditCardDataClass ccdata = new CreditCardDataClass();
	public static ArrayList<ReceentAddressDataClass> myList= new ArrayList<ReceentAddressDataClass>();
	public static ArrayList<BookedRideDataClass> passengerBookedRideList = new ArrayList<BookedRideDataClass>();
	public static ArrayList<JobListDataClass> jobList= new ArrayList<JobListDataClass>();
	public static ArrayList<PopularAddressDataClass> popularAddList= new ArrayList<PopularAddressDataClass>();
	public static ArrayList<PopularAddressCategoryDataClass> popularAddCatList= new ArrayList<PopularAddressCategoryDataClass>();
	public static ArrayList<AssignedJobListDataClass> assignedJobList= new ArrayList<AssignedJobListDataClass>();
	public static ArrayList<BidListDataClass> bidList= new ArrayList<BidListDataClass>();
	public static ArrayList<HistoryDataClass> historyList= new ArrayList<HistoryDataClass>();
	public static ArrayList<ReviewDataClass> reviewList= new ArrayList<ReviewDataClass>();
	public static ArrayList<NotificationData> notificationList= new ArrayList<NotificationData>();
	public static Context context;
	public static boolean isAddPickup = false;
	
	
}
