package com.cipl.liverycab.statics;

/**
 *This is the class which is only containing methods to return URL  
 */
public class UrlStatics {

	public final static String baseurlStageDb = "http://samsad.stagedb.com/liverycab/api/?keyword=";
	public final static String demoServerLink = "http://livercab.demos.classicinformatics.com/liverycab/api/?keyword=";

	/** 
	 * methods to get URL for passenger interface
	 * */
	public static String getLoginUrl(String username, String password,
			String deviceId, String deviceTokan, String deviceType,
			String userType) {
		return demoServerLink + "LIVERYCAB_DRIVERPASSENGER_LOGIN&email="
				+ username + "&password=" + password + "&deviceId=" + deviceId
				+ "&deviceToken=" + deviceTokan + "&deviceType=" + deviceType
				+ "&userType=" + userType;
	}

	public static String getForgotPasswordUrl(String email, String userType) {
		return demoServerLink
				+ "LIVERYCAB_DRIVERPASSENGER_FORGOTPASSWORD&email=" + email
				+ "&userType=" + userType;
	}

	public static String getPassengerSignupUrl(String fname, String lname,
			String phone, String email, String passwrod,
			String deviceTokan, String deviceid, String deviceType) {
		return demoServerLink + "LIVERYCAB_PASSENGER_REGISTRATION&fname="+fname+"&lname="+lname
				+"&phone="+phone+"&email="+email+"&password="+passwrod
				+"&deviceId="+deviceid+"&deviceToken="+deviceTokan+"&deviceType="+deviceType;
	}

	public static String getProfile(String userId, String userType) {
		return demoServerLink + "LIVERYCAB_DRIVERPASSENGER_PROFILE&uid="
				+ userId + "&userType=" + userType;
	}

	public static String getChangePasswordUrl(String userId, String oldPass,
			String newPass, String userType) {
		return demoServerLink + "LIVERYCAB_DRIVERPASSENGER_CHANGEPASSWORD&uid="
				+ userId + "&oldpass=" + oldPass + "&npass=" + newPass
				+ "&userType=" + userType;
	}

	public static String getPassengerEditUrl(String userId, String fname,
			String lname, String phone) {
		return demoServerLink + "LIVERYCAB_PASSENGER_EDITPROFILE&uid="+userId
				+"&fname="+fname+"&lname="+lname+"&phone="+phone ;
	}

	public static String getUrlforSavingSourceAndDesti(String userId,
			String pickUp, String destination, String latS, String longS,
			String latD, String longD, String datetime, String ridestatus) {
		return demoServerLink + "LIVERYCAB_PASSENGER_ADD_PICKUP&passId="
				+ userId + "&pickupSource=" + pickUp + "&pickupDestination="
				+ destination + "&latS=" + latS + "&lngS=" + longS + "&latD="
				+ latD + "&lngD=" + longD + "&datetime=" + datetime+"&ridestatus="+ridestatus;
	}

	public static String getRecentAddressUrl(String userID,String sourceordesti, String lat,
			String longi, int limit) {
		return demoServerLink + "LIVERYCAB_RECENT_PICKUP_ADDRESS&passid="+userID+"&addresstype="
				+ sourceordesti + "&currentaddress=" + lat + "," + longi
				+ "&limit=" + limit;
	}

	public static String getPassengerBidListUrl(String uid) {
		return demoServerLink + "LIVERYCAB_PASSENGER_BIDLIST&uid=" + uid;
	}

	public static String getDriverDetailUrl(String bidId) {
		return demoServerLink + "LIVERYCAB_DRIVER_PROFILE_PICKUP&bid=" + bidId;
	}

	public static String getBidConfirmUrl(String bidID, String passId, String did, String fname,
					String lname,String ctype,String cNum, String expYear, 
					String expMonth,String cvv, String amount) {
		return demoServerLink + "LIVERYCAB_RIDE_CONFIRM&bid="+bidID+"&passid="+passId+"&did="+did+"&fname="
								+fname+"&lname="+lname+"&ctype="+ctype+"&cnum="
								+cNum+"&expyear="+expYear+"&expmonth="+expMonth
								+"&cvv="+cvv+"&amount="+amount;
	}
	
	public static String getFourSquareApi(String currentAdd,String CATID){
		return demoServerLink+"LIVERYCAB_POPULAR_ADDRESS&currentaddress="+currentAdd+"&catid="+CATID;
	}
	
	public static String getFourSquareApiCat(){
		return demoServerLink+"LIVERYCAB_FOURSQUARE_VENUE_CATEGORY";
	}
	
	public static String getHistoryListApi(String userID){
		return demoServerLink+"LIVERYCAB_PASSENGER_HISTORY&passId="+userID;
	}
	
	public static String getHistoryDetailApi(String driverId,String bidId,String passId){
		return demoServerLink+"LIVERYCAB_PASSENGER_HISTORY_DETAILS&did="+driverId+"&bid="+bidId+"&passid="+passId;
	}
	
	public static String getApiforReviewSubmission(String passId,String driverID,
					String rateval,String reviewMsg, String rateId){
		return demoServerLink+"LIVERYCAB_DRIVER_RATINGREVIEW&passid="+passId+
				"&did="+driverID+"&ratevalue="+rateval+"&ratemsg="+reviewMsg+"&rateid="+rateId;
	}
	
	public static String getApiforSendingNotification(String pickupId,String passengerId){
		return demoServerLink+"LIVERYCAB_PASSENGER_NOTIFICATION&passid="+passengerId
				+"&pickupid="+pickupId;
	}
	
	public static String getNotificationList(String passId){
		return demoServerLink +"LIVERYCAB_PASSENGER_NOTIFICATION_GET&passid="+passId;
	}
	
	public static String getReviewList(String driverId){
		return demoServerLink + "LIVERYCAB_PASSENGER_REVIEW_LIST&did="+driverId;
	}
	
	/**
	 * methods to get URL for driver interface
	 **/
	public static String getDriverSignupUrl(String fname, String lname,
			String licencenum, String phone, String email, String passwrod,
			String driverImage, String carImage, String deviceTokan,
			String deviceid, String deviceType) {
		return demoServerLink + "LIVERYCAB_DRIVER_REGISTRATION&fname=" + fname
				+ "&lname=" + lname + "&licence=" + licencenum + "&phone="
				+ phone + "&email=" + email + "&password=" + passwrod + ""
				+ "&driverimage=" + driverImage + "&carimage=" + carImage
				+ "&deviceId=" + deviceid + "&deviceToken=" + deviceTokan
				+ "&deviceType=" + deviceType;
	}

	public static String getSettingsUrl(String userId, String miles) {
		return demoServerLink + "LIVERYCAB_DRIVER_SETTING&did=" + userId
				+ "&miles=" + miles;
	}

	public static String getJOblist(String userID, String currentLat,
			String currentLong, int limit) {
		return demoServerLink + "LIVERYCAB_DRIVER_JOBLIST&uid=" + userID
				+ "&currentlat=" + currentLat + "&currentlng=" + currentLong
				+ "&limit=" + limit;
	}

	public static String getAssignedJOblist(String userID) {
		return demoServerLink + "LIVERYCAB_DRIVER_ASSIGNJOB&uid=" + userID;
	}

	public static String getUrlForEnteringBidAmount(String userID,
			String pickUpId, String bidAmt, String etaTime) {
		return demoServerLink + "LIVERYCAB_DRIVER_BIDDING&uid=" + userID
				+ "&pickupid=" + pickUpId + "&bidamount=" + bidAmt + "&eta="
				+ etaTime;
	}
	public static String getUrlToCompleteRideButton(String pickID,String driverId){
		return demoServerLink + "LIVERYCAB_RIDE_COMPLETE&pickupid="+pickID+"&dId="+driverId;
	}
	/**Passenger job list*/
	public static String getUrlToPassengerJobList(String uid){
		return demoServerLink + "LIVERYCAB_PASSENGER_JOBLIST&uid="+uid;
	}
	/**Cancel Passenger job */
	public static String getUrlToDeletePassJobList(String uid,String pid){
		return demoServerLink + "LIVERYCAB_DEL_PASS_JOB&uid="+uid+"&pid="+pid;
	}
	/**Delete Bid of driver */	
	
	public static String delJOblist(String userID, String currentLat,
			String currentLong, int limit,String bidId) {
		return demoServerLink + "LIVERYCAB_DRIVER_BID_DELETE&uid=" + userID
				+ "&bidId="+bidId+"&currentlat=" + currentLat + "&currentlng=" + currentLong
				+ "&limit=" + limit;
	}
	
	/**Delete Pickup and Bid*/
	public static String deletePickupAndBid(String pickupId){
		return demoServerLink+"LIVERYCAB_DELETE_PICKUP&pickupid="+pickupId;
	}
}