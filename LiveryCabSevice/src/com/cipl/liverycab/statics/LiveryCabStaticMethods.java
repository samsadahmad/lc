package com.cipl.liverycab.statics;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.cipl.liverycab.passengerui.DestinationMapViewActivity;
import com.cipl.liverycab.passengerui.R;

public class LiveryCabStaticMethods {
	static Bitmap bmp;
	private static File cacheDir;
	static Bitmap bitmap = null;
	static HttpURLConnection conn;
	static final int TIME_DIALOG = 0;
	static final int DATE_DIALOG = 1;
	public static ProgressDialog mProgressDialog;
	private static int pos;

	public static void showMessageDialog(Context context, String message) {
		AlertDialog.Builder dialog = new Builder(context);
		dialog.setMessage(message);
		dialog.setCancelable(false);
		dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		dialog.show();
	}

	public static ProgressDialog returnProgressBar(Context context) {
		mProgressDialog = ProgressDialog.show(context, "",
				"Loading. Please wait...", true, false);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		return mProgressDialog;
	}
	
	public static boolean isInternetAvailable(final Context context) {
		boolean returnTemp = true;

		ConnectivityManager conManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo i = conManager.getActiveNetworkInfo();
		if ((i == null) || (!i.isConnected()) || (!i.isAvailable())) {
			AlertDialog.Builder dialog = new Builder(context);
			dialog.setTitle("Attention.");
			// dialog.setIcon(R.drawable.attention);
			dialog.setMessage("There is no network connection right now. Please try again later.");
			dialog.setCancelable(false);
			dialog.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

						}
					});
			dialog.show();
			return false;
		}
		return true;
	}
	public static boolean isInternetAvailibility(final Context context) {
		boolean returnTemp = true;
		ConnectivityManager conManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo i = conManager.getActiveNetworkInfo();
		if ((i == null) || (!i.isConnected()) || (!i.isAvailable())) {
			return false;
		}
		return true;
	}
	

	public static boolean showAlert(Context context, String title,
			String message, int drawableID, boolean okButton,
			boolean cancelButton, String okButtonTitle,
			DialogInterface.OnClickListener OkButtonOnClickListener,
			String cancleButtonTitle,
			DialogInterface.OnClickListener cancleButtonOnClickListener) {
			LayoutInflater factory = LayoutInflater.from(context);
			AlertDialog.Builder messageAlert = new Builder(context);
			messageAlert.setCancelable(false);
			messageAlert.setTitle(title);
			messageAlert.setIcon(drawableID);
			messageAlert.setMessage(message);

		// unsuccessfullyLogin.setView(textEntryView);
		if (okButton) {
			messageAlert.setPositiveButton(okButtonTitle,
					OkButtonOnClickListener);
		}
		if (cancelButton) {
			messageAlert.setNeutralButton(cancleButtonTitle,
					cancleButtonOnClickListener);
		}
		messageAlert.show();
		return false;
	}

	public static boolean isCardNumberValid(String number) {
		int s1 = 0, s2 = 0;
		String reverse = new StringBuffer(number).reverse().toString();
		for (int i = 0; i < reverse.length(); i++) {
			int digit = Integer.parseInt(reverse.charAt(i) + "");
			if (i % 2 == 0) {// this is for odd digits, they are 1-indexed in
								// the algorithm
				s1 += digit;
			} else {// add 2 * digit for 0-4, add 2 * digit - 9 for 5-9
				s2 += 2 * digit;
				if (digit >= 5) {
					s2 -= 9;
				}
			}
		}
		return (s1 + s2) % 10 == 0;
	}

	public static boolean isEmail(String email) {
		boolean mResult = false;
		Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher matcher = pattern.matcher(email);
		mResult = matcher.matches();
		return mResult;
	}

	public static Dialog createDialog(int id, Context mcontext) {
		Calendar c = Calendar.getInstance();
		int cyear = c.get(Calendar.YEAR);
		int cmonth = c.get(Calendar.MONTH);
		int cday = c.get(Calendar.DAY_OF_MONTH);
		int chour = c.get(Calendar.HOUR_OF_DAY);
		int cmin = c.get(Calendar.MINUTE);
		int cSec = c.get(Calendar.SECOND);
		if (id == 0) {
			return new TimePickerDialog(mcontext, new TimePickerDialog.OnTimeSetListener() {
						@Override
						public void onTimeSet(TimePicker view, int hourOfDay,
								int minute) {
							// TODO Auto-generated method stub

						}
					}, chour, cmin, false);
		}

		else {
			return new DatePickerDialog(mcontext,new DatePickerDialog.OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker view, int year,
								int monthOfYear, int dayOfMonth) {
							// TODO Auto-generated method stub
							((Activity) DestinationMapViewActivity.mContext)
									.showDialog(0);
						}
					}, cyear, cmonth, cday);
		}
	}
	
	public static String getDateTime()
	{
		String[] monthArray = {"Jan","Feb","Mar","Apr","May","June","Jul","Aug","Sep","Oct","Nov","Dec"};
		Calendar cal = Calendar.getInstance();
		int mnth = cal.get(Calendar.MONTH);
		String sDate = "" ;
		if(LiveryCabStatics.isRideNow)
		{
			sDate = cal.get(Calendar.DAY_OF_MONTH) + " " 
				+ monthArray[mnth]
				+ " " + cal.get(Calendar.YEAR) 
				+ " " + cal.get(Calendar.HOUR_OF_DAY) 
				+ ":" + cal.get(Calendar.MINUTE)
				+ ":" + cal.get(Calendar.SECOND);
		}
		else
			sDate = LiveryCabStatics.date_Selected;
		return sDate;
	}
	
	public static JSONArray getJSONfromURL(String url) {
	    InputStream is = null;
	    String result = "";
	    JSONArray jArray = null;
	    try{
	        HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost(url);
	        HttpResponse response = httpclient.execute(httppost);
	        HttpEntity entity = response.getEntity();
	        is = entity.getContent();
	 
	    }catch(Exception e){
	        Log.e("log_tag", "Error in http connection "+e.toString());
	    }
	    try{
	        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	        StringBuilder sb = new StringBuilder();
	        String line = null;
	        while ((line = reader.readLine()) != null) {
	        	if(line.trim().equals("\n"))
	        		continue;
	        	sb.append(line + "\n");
	        }
	        is.close();
	        result= sb.toString();
	        if(!(result.contains("["))){
	        	result = "["+ result +"]";
	        }
	       
	    }catch(Exception e){
	    	 Log.e("log_tag", "Error converting result "+e.toString());
	    }
	    try{
	    	jArray = new JSONArray(result);
	    }catch(JSONException e){
	        Log.e("log_tag", "Error parsing data "+e.toString());
	    }
	     return jArray;
	}
	
	
	private static InputStream fetch(String urlString)
			throws MalformedURLException, IOException {
		
		conn = (HttpURLConnection)(new URL(urlString)).openConnection();
        conn.setDoInput(true);
        conn.connect();
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet request = new HttpGet(urlString);
		HttpResponse response = httpClient.execute(request);
		return  conn.getInputStream();
	}
	Resources res;
	// ***********New Function For Fatching
	public static Drawable LoadImage(String URL, BitmapFactory.Options options, Context mContext) {
		Drawable drawable = null; Bitmap bm = null;
		InputStream obj =null;
		try {
			if ((obj=fetch(URL)) != null) {
				try {
					 
                     bm = BitmapFactory.decodeStream(obj,null,options);
                    obj.close();
                    conn.disconnect();
				} catch(OutOfMemoryError e){}
				catch(IndexOutOfBoundsException e){}
					 drawable = new BitmapDrawable(bm);
					return drawable;
			} else {
				bitmap = BitmapFactory.decodeResource(
						mContext.getResources(),
						R.drawable.smallprofileimage);
			 drawable = new BitmapDrawable(bitmap);
				return drawable;
			}

		} catch (Exception e) {
			Log.e("GUIMethodClasas", "fetchDrawable failed", e);
			return null;
		
		}
	}
	
	public static double calculateDistance(String pickLat, String pickLon, String destLat, String destLon){
		double distance; 
		double slat = Double.parseDouble(pickLat);
		double slong = Double.parseDouble(pickLon);
		double dlat = Double.parseDouble(destLat);
		double dLong = Double.parseDouble(destLon);
		
		Location pickupLocation = new Location("Pickup Location");  
		pickupLocation.setLatitude(slat);  
		pickupLocation.setLongitude(slong);  
		  
		Location destinationLocation = new Location("Destination Location");  
		destinationLocation.setLatitude(dlat);  
		destinationLocation.setLongitude(dLong);  
		  
		distance = pickupLocation.distanceTo(destinationLocation);  
		return distance;
	}
	
}
