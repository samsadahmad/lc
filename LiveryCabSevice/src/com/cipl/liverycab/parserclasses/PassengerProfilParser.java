package com.cipl.liverycab.parserclasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.util.Log;

import com.cipl.liverycab.dataclasses.LoginDataClass;
import com.cipl.liverycab.dataclasses.PassengerDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;

public class PassengerProfilParser extends Thread {
	private String urlPassengerProfile;
//	public ProgressDialog progressDialog;
	public PassengerDataClass profileDataObj;
	public boolean actualDismiss;
	public PassengerProfilParser(String url,ProgressDialog pd){
		super();
		this.urlPassengerProfile = url;
//		this.progressDialog = pd;
		Log.d("url",url);
	}	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		JSONObject jObject;
		profileDataObj =  new PassengerDataClass();
		jObject = getJSONfromURL(urlPassengerProfile);
		if(jObject == null){
			return;
		}
		try {
			profileDataObj.message =jObject.getString("Message");
			profileDataObj.firstName =jObject.getString("fname");
			profileDataObj.lastName =jObject.getString("lname");
			profileDataObj.phone =jObject.getString("phone");
			profileDataObj.email =jObject.getString("email");
			profileDataObj.status = jObject.getString("Status");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(LiveryCabStaticMethods.mProgressDialog.isShowing()){
        	actualDismiss =true;
        	LiveryCabStaticMethods.mProgressDialog.dismiss();
		}
	}
	public  JSONObject  getJSONfromURL(String url) 
	{
		InputStream mIs = null;
	    String result = "";
	    JSONObject jObjectLogin = null;
	    try {
	    	HttpClient httpclient = new DefaultHttpClient();
	    	HttpPost httppost = new HttpPost(url);
	    	HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			mIs = entity.getContent();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try{
	        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(mIs,"iso-8859-1"),8);
	        StringBuilder stringBuilder = new StringBuilder();
	        String line = null;
	        while ((line = bufferReader.readLine()) != null) {
	        	if(line.trim().equals("\n"))
	        		continue;
	        	stringBuilder.append(line + "\n");
	        }
	        mIs.close();
	        result=stringBuilder.toString();
	    }catch(Exception e){
	    	 Log.e("log_tag", "Error converting result "+e.toString());
	    }
	    try {
			jObjectLogin = new JSONObject(result);
		} catch (JSONException e){
			e.printStackTrace(); 
		}
		return jObjectLogin;
	}
}
