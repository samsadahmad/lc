package com.cipl.liverycab.parserclasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cipl.liverycab.dataclasses.HistoryDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class HIstoryListParserClass extends Thread{
	private String urlString;
	public HistoryDataClass historyListDataObj;
	public boolean actualDismiss;
	
	public HIstoryListParserClass(String url) {
		super();
		this.urlString = url;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		JSONArray json =LiveryCabStaticMethods.getJSONfromURL(urlString);
		if(json!=null){
			LiveryCabStatics.historyList.clear();
    		for (int i = 0; i < json.length(); i++){
    			try {
					JSONObject rowData = json.getJSONObject(i);
					historyListDataObj = new HistoryDataClass();
					if(rowData.getString("Status") != null && !(rowData.getString("Status").equalsIgnoreCase("")))
						historyListDataObj.status = rowData.getString("Status");
    				if(rowData.getString("Message") != null && !(rowData.getString("Message").equalsIgnoreCase("")))
    					historyListDataObj.message= rowData.getString("Message");
    				if(rowData.getString("bidid") != null && !(rowData.getString("bidid").equalsIgnoreCase("")))
    					historyListDataObj.bidid= rowData.getString("bidid");
    				if(rowData.getString("driverid") != null && !(rowData.getString("driverid").equalsIgnoreCase("")))
    					historyListDataObj.driverId= rowData.getString("driverid");
    				if(rowData.getString("pickupSource") != null && !(rowData.getString("pickupSource").equalsIgnoreCase("")))
    					historyListDataObj.source= rowData.getString("pickupSource");
    				if(rowData.getString("pickupDestination") != null && !(rowData.getString("pickupDestination").equalsIgnoreCase("")))
    					historyListDataObj.destination = rowData.getString("pickupDestination");
    				if(rowData.getString("datetime") != null && !(rowData.getString("datetime").equalsIgnoreCase("")))
    					historyListDataObj.datetime = rowData.getString("datetime");
    				String BIDDATAAMOUNT = "";
    				if(rowData.getString("amount") != null && !(rowData.getString("amount").equalsIgnoreCase(""))){
    					if(rowData.getString("amount").contains(".")){
        					BIDDATAAMOUNT  = rowData.getString("amount");
        				}else{
        					BIDDATAAMOUNT = rowData.getString("amount")+".00";
        				}
    					historyListDataObj.amount = BIDDATAAMOUNT;
    				} 
    					
    				LiveryCabStatics.historyList.add(historyListDataObj);
    				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		if(LiveryCabStaticMethods.mProgressDialog.isShowing()){
	        	actualDismiss =true;
	        	LiveryCabStaticMethods.mProgressDialog.dismiss();
			}
    	}
	}
}
