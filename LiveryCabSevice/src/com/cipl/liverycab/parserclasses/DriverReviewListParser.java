package com.cipl.liverycab.parserclasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.cipl.liverycab.dataclasses.ReviewDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class DriverReviewListParser extends Thread {

	public boolean actualDismiss;
	public Context context;
	private String urlAddData;
	public ReviewDataClass dataObject;
	
	public DriverReviewListParser(String url,Context mContext)
	{
		super();
		this.urlAddData = url;
		this.context = mContext;
	}
	
	@Override
	public void run() {
		JSONArray json = LiveryCabStaticMethods.getJSONfromURL(urlAddData);
		if(json!=null){
			LiveryCabStatics.reviewList.clear();
			for (int i = 0; i < json.length(); i++){
    			try{
    				JSONObject rowData = json.getJSONObject(i);
    				dataObject = new ReviewDataClass();
    				if(rowData.getString("Status") != null && !(rowData.getString("Status").equalsIgnoreCase("")))
    					dataObject.status = rowData.getString("Status");
    				if(rowData.getString("Message") != null && !(rowData.getString("Message").equalsIgnoreCase("")))
    					dataObject.message= rowData.getString("Message");
    				if(rowData.getString("reviewMessage") != null && !(rowData.getString("reviewMessage").equalsIgnoreCase("")))
    					dataObject.reviewMsg= rowData.getString("reviewMessage");
    				if(rowData.getString("fname") != null && !(rowData.getString("fname").equalsIgnoreCase("")))
    					dataObject.fName= rowData.getString("fname");
    				if(rowData.getString("lname") != null && !(rowData.getString("lname").equalsIgnoreCase("")))
    					dataObject.lName= rowData.getString("lname");
    				LiveryCabStatics.reviewList.add(dataObject);
    			}
    			catch(JSONException e) {
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	    		}
	 		    catch(NumberFormatException e){
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	 		    }
			}
		}
		if(LiveryCabStaticMethods.mProgressDialog.isShowing()){
	  		actualDismiss =true;
	  		LiveryCabStaticMethods.mProgressDialog.dismiss();
	  	}
	}
}
