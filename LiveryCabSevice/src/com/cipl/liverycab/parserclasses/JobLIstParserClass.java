package com.cipl.liverycab.parserclasses;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.util.Log;

import com.cipl.liverycab.dataclasses.JobListDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class JobLIstParserClass extends Thread{

	private String urlAddData;
	public JobListDataClass jobListDataObj;
	public boolean actualDismiss;
	private String pickupAdd;
	private String pickupDesti;
	private ProgressDialog pd;
	
	
	public JobLIstParserClass(String url,ProgressDialog pd){
		super();
		this.urlAddData = url;
		this.pd = pd;
		Log.d("url",url);
	}	
	
	

	@Override
	public void run() {
		// TODO Auto-generated method stub
		JSONArray json =LiveryCabStaticMethods.getJSONfromURL(urlAddData);
		if(json!=null){
			LiveryCabStatics.jobList.clear();
    		for (int i = 0; i < json.length(); i++){
    			try{
    				JSONObject rowData = json.getJSONObject(i);
    				jobListDataObj = new JobListDataClass();
    				jobListDataObj.count = rowData.getString("count");
    				jobListDataObj.status = rowData.getString("Status");
    				if(rowData.getString("pickupId") != null && !(rowData.getString("pickupId").equalsIgnoreCase("")))
    					jobListDataObj.pickUpId= rowData.getString("pickupId");
    				if(rowData.getString("fnameD") != null && !(rowData.getString("fnameD").equalsIgnoreCase("")))
    					jobListDataObj.firstName= rowData.getString("fnameD");
    				if(rowData.getString("lname") != null && !(rowData.getString("lname").equalsIgnoreCase("")))
    					jobListDataObj.lastName= rowData.getString("lname");
    				
					if(rowData.getString("pickupAddress") != null && !(rowData.getString("pickupAddress").equalsIgnoreCase("")))
    					pickupAdd = rowData.getString("pickupAddress");
    					jobListDataObj.pickupAdd = pickupAdd.replace(",null", "");
    				if(rowData.getString("pickupDestination") != null && !(rowData.getString("pickupDestination").equalsIgnoreCase("")))
    					pickupDesti = rowData.getString("pickupDestination");
    					jobListDataObj.destinationAdd = pickupDesti.replace(",null", "");
    				if(rowData.getString("distance") != null && !(rowData.getString("distance").equalsIgnoreCase("")))
    					jobListDataObj.distanceInMiles = rowData.getString("distance");
    				if(rowData.getString("latitude") != null && !(rowData.getString("latitude").equalsIgnoreCase("")))
    					jobListDataObj.pickUpLatitude = rowData.getString("latitude");
    				if(rowData.getString("longitude") != null && !(rowData.getString("longitude").equalsIgnoreCase("")))
    					jobListDataObj.pickupLongitude = rowData.getString("longitude");
    				
    				//Bid Amount
    				String BIDDATAAMOUNT = "";
    				if(rowData.getString("bidAmount") != null && !(rowData.getString("bidAmount").equalsIgnoreCase(""))){
    					if(rowData.getString("bidAmount").contains(".")){
    						BIDDATAAMOUNT = rowData.getString("bidAmount");
    					}else{
    						BIDDATAAMOUNT = rowData.getString("bidAmount")+".00";
    					}
    					jobListDataObj.bidAmount = BIDDATAAMOUNT;
    				}	
    					
    				if(rowData.getString("dateTime") != null && !(rowData.getString("dateTime").equalsIgnoreCase("")))
    					jobListDataObj.dateTime = rowData.getString("dateTime");
    				if(rowData.getString("ETA") != null && !(rowData.getString("ETA").equalsIgnoreCase("")))
    					jobListDataObj.etaTime = rowData.getString("ETA");
    				if(rowData.getString("bidId") != null && !(rowData.getString("bidId").equalsIgnoreCase("")))
    					jobListDataObj.bidId = rowData.getString("bidId");
    				if(rowData.getString("isRideCancel") != null)
    					jobListDataObj.isRideCancel = rowData.getString("isRideCancel");
    				if(rowData.getString("phoneP") != null)
    					jobListDataObj.phoneP = rowData.getString("phoneP").trim();
    				
    				LiveryCabStatics.jobList.add(jobListDataObj);
			    }
    			catch(JSONException e) {
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	    		}
	 		    catch(NumberFormatException e){
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	 		    }
    		}
		}
	  	if(pd.isShowing()){
	  		actualDismiss =true;
	  		pd.dismiss();
	  	}
	}
}