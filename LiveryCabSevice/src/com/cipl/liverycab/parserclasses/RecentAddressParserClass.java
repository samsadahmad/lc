package com.cipl.liverycab.parserclasses;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.cipl.liverycab.dataclasses.ReceentAddressDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class RecentAddressParserClass extends Thread  {
	
	
	private String urlAddData;
	public ReceentAddressDataClass addressDataObj;
	public boolean actualDismiss;
	
	public RecentAddressParserClass(String url){
		super();
		this.urlAddData = url;
		Log.d("url",url);
	}	
	
	@Override
	public void run() {
		JSONArray json = LiveryCabStaticMethods.getJSONfromURL(urlAddData);
    	if(json!=null){
    		LiveryCabStatics.myList.clear();
    		for (int i = 0; i < json.length(); i++) {
    			try{
    				JSONObject rowData = json.getJSONObject(i);
    				addressDataObj = new ReceentAddressDataClass();
				    addressDataObj.count = rowData.getString("Count");
				    addressDataObj.status = rowData.getString("Status");
				    addressDataObj.address = rowData.getString("pickupAddress");
				    addressDataObj.distanceInMiles = rowData.getString("distance");
				    addressDataObj.addLatitude = rowData.getString("latitude");
				    addressDataObj.addLongitude = rowData.getString("longitude");
				    LiveryCabStatics.myList.add(addressDataObj);
			    }
    			catch(JSONException e) {
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	    		}
	 		    catch(NumberFormatException e){
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	 		    }
    		}
		}
	  	if(LiveryCabStaticMethods.mProgressDialog.isShowing())
	  	{
	  		actualDismiss =true;
	  		LiveryCabStaticMethods.mProgressDialog.dismiss();
	  	}
	}
}
