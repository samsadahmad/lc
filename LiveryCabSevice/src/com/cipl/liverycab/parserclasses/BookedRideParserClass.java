package com.cipl.liverycab.parserclasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cipl.liverycab.dataclasses.BookedRideDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class BookedRideParserClass {
   /**Variable define here*/
	private JSONArray mJSONArray;
	private JSONObject mJSONObject;
	private BookedRideDataClass mBookedRideDataClass;
	public String isBookedPickupDel = "0";
	
	public void fetchBookedRide(String urlBookedData){
		try {
			mJSONArray = LiveryCabStaticMethods.getJSONfromURL(urlBookedData);
			if(mJSONArray == null){
				return; 
			}
			LiveryCabStatics.passengerBookedRideList.clear();
			for(int i = 0; i < mJSONArray.length(); i++){
				mJSONObject = mJSONArray.getJSONObject(i);		
				
				mBookedRideDataClass = new BookedRideDataClass();
				String pickupAdd = mJSONObject.getString("pickupAddress").toString();;
				mBookedRideDataClass.pickupAddress = pickupAdd.replace(",null", "");
				
				String pickupDesti = mJSONObject.getString("pickupDestination").toString();;
				mBookedRideDataClass.pickupDestination = pickupDesti.replace(",null", "");
				
				mBookedRideDataClass.dateTime = mJSONObject.getString("dateTime").toString();
				mBookedRideDataClass.pickupId = mJSONObject.getString("pickupId").toString();
				mBookedRideDataClass.distance = mJSONObject.getString("distance").toString();
				/*mBookedRideDataClass.rideCompleted = mJSONObject.getString("rideCompleted").toString();*/
				mBookedRideDataClass.bidCount = mJSONObject.getString("bidCount").toString();
				/*mBookedRideDataClass.cancelMsg = mJSONObject.getString("cancelMsg").toString();*/
				mBookedRideDataClass.isRideCancel = mJSONObject.getString("isRideCancel").toString();
				mBookedRideDataClass.bidId = mJSONObject.getString("bidId").toString();
				String BIDDATAAMOUNT = "";
				if(mJSONObject.getString("bidAmount").contains(".")){
					BIDDATAAMOUNT  = mJSONObject.getString("bidAmount");
				}else{
					BIDDATAAMOUNT = mJSONObject.getString("bidAmount")+".00";
				}				
				mBookedRideDataClass.bidAmount = BIDDATAAMOUNT;
				mBookedRideDataClass.bidDriverId = mJSONObject.getString("bidDriverId").toString();
				mBookedRideDataClass.bidEta = mJSONObject.getString("bidEta").toString();
				/**Add data into List*/
				LiveryCabStatics.passengerBookedRideList.add(mBookedRideDataClass);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void deleteBookedRide(String urlBookedDelData){
		try {
			mJSONArray = LiveryCabStaticMethods.getJSONfromURL(urlBookedDelData);
			if(mJSONArray == null){
				return; 
			}
			mJSONObject = mJSONArray.getJSONObject(0);
			isBookedPickupDel = mJSONObject.getString("Status").toString();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
