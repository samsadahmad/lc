package com.cipl.liverycab.parserclasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.cipl.liverycab.dataclasses.DriverDetailDataClass;
import com.cipl.liverycab.dataclasses.HistoryDetailDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;

public class HistoryDetailParser extends Thread {

	private String url;
	public boolean actualDismiss;
	private ProgressDialog progressDialoglogin;
	public HistoryDetailDataClass dataObject; 
	public Context context;
	
	public HistoryDetailParser(String url,ProgressDialog pd, Context mContext){
		this.url = url;
		this.progressDialoglogin = pd;
		this.context = mContext;
	}
	
	@Override
	public void run() {
		JSONObject jObject;
		dataObject = new HistoryDetailDataClass();
		jObject = getJSONfromURL(url);
		try {
			if(jObject.getString("Status")!=null)
				dataObject.status = jObject.getString("Status");
			if(jObject.getString("Message") != null && !(jObject.getString("Message").equalsIgnoreCase("")))
				dataObject.message= jObject.getString("Message");
			if(jObject.getString("fname") != null && !(jObject.getString("fname").equalsIgnoreCase("")))
				dataObject.firstName= jObject.getString("fname");
			if(jObject.getString("lname") != null && !(jObject.getString("lname").equalsIgnoreCase("")))
				dataObject.lastName= jObject.getString("lname");
			if(jObject.getString("Licence") != null && !(jObject.getString("Licence").equalsIgnoreCase("")))
				dataObject.licence= jObject.getString("Licence");
			if(jObject.getString("driverImage") != null && !(jObject.getString("driverImage").equalsIgnoreCase("")))
				dataObject.driverImage = LiveryCabStaticMethods.LoadImage
					(jObject.getString("driverImage"),new BitmapFactory.Options(),context);
			
			if(jObject.getString("pickupAddres") != null && !(jObject.getString("pickupAddres").equalsIgnoreCase("")))
				dataObject.pickupAddres = jObject.getString("pickupAddres");
			
			if(jObject.getString("PickupDestination") != null && !(jObject.getString("PickupDestination").equalsIgnoreCase("")))
				dataObject.destination = jObject.getString("PickupDestination");
			//Amount
			String BIDDATAAMOUNT = "";
			if(jObject.getString("bidAmount") != null && !(jObject.getString("bidAmount").equalsIgnoreCase(""))){
				if(jObject.getString("bidAmount").contains(".")){
					BIDDATAAMOUNT  = jObject.getString("bidAmount");
				}else{
					BIDDATAAMOUNT = jObject.getString("bidAmount")+".00";
				}
				dataObject.amount = BIDDATAAMOUNT;
			}	
			if(jObject.getString("DateTime") != null && !(jObject.getString("DateTime").equalsIgnoreCase("")))
				dataObject.datetime = jObject.getString("DateTime");
			if(jObject.getString("rating") != null && !(jObject.getString("rating").equalsIgnoreCase("")))
				dataObject.rating = jObject.getString("rating");
			if(jObject.getString("rateid") != null && !(jObject.getString("rateid").equalsIgnoreCase("")))
				dataObject.ratid = jObject.getString("rateid");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(LiveryCabStaticMethods.mProgressDialog.isShowing()){
        	actualDismiss =true;
        	progressDialoglogin.dismiss();
		}
	}
	public  JSONObject  getJSONfromURL(String url) 
	{
		InputStream mIs = null;
	    String result = "";
	    JSONObject jObjectLogin = null;
	    try {
	    	HttpClient httpclient = new DefaultHttpClient();
	    	HttpPost httppost = new HttpPost(url);
	    	HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			mIs = entity.getContent();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try{
	        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(mIs,"iso-8859-1"),8);
	        StringBuilder stringBuilder = new StringBuilder();
	        String line = null;
	        while ((line = bufferReader.readLine()) != null) {
	        	if(line.trim().equals("\n"))
	        		continue;
	        	stringBuilder.append(line + "\n");
	        }
	        mIs.close();
	        result=stringBuilder.toString();
	    }catch(Exception e){	    	
	        Log.e("log_tag", "Error converting result "+e.toString());
	    }
	    try {
			jObjectLogin = new JSONObject(result);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jObjectLogin;
	}

}
