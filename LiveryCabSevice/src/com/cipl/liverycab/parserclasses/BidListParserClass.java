package com.cipl.liverycab.parserclasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.cipl.liverycab.dataclasses.BidListDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class BidListParserClass extends Thread{
	
	private String urlAddData;
	public BidListDataClass bidListDataObj;
	public boolean actualDismiss;
	public Context context;
	private ProgressDialog mProgressDialog;
	public BidListParserClass(String url,Context mContext,ProgressDialog mProgressDialog)
	{
		super();
		this.urlAddData = url;
		this.context = mContext;
		this.mProgressDialog = mProgressDialog;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		JSONArray json = LiveryCabStaticMethods.getJSONfromURL(urlAddData);
		LiveryCabStatics.bidList.clear();
		
		if(json!=null){
    		for (int i = 0; i < json.length(); i++){
    			try{
    				JSONObject rowData = json.getJSONObject(i);
    				bidListDataObj = new BidListDataClass();
    				bidListDataObj.status = rowData.getString("Status");
    				if(rowData.getString("Message") != null && !(rowData.getString("Message").equalsIgnoreCase("")))
    					bidListDataObj.message= rowData.getString("Message");
    				if(rowData.getString("driverId") != null && !(rowData.getString("driverId").equalsIgnoreCase("")))
    					bidListDataObj.driverId= rowData.getString("driverId");
    				if(rowData.getString("fname") != null && !(rowData.getString("fname").equalsIgnoreCase("")))
    					bidListDataObj.firstName= rowData.getString("fname");
    				if(rowData.getString("lname") != null && !(rowData.getString("lname").equalsIgnoreCase("")))
    					bidListDataObj.lastName= rowData.getString("lname");
    				if(rowData.getString("driverImage") != null && !(rowData.getString("driverImage").equalsIgnoreCase("")))
    					bidListDataObj.driverImage = rowData.getString("driverImage");    							
    				if(rowData.getString("Distance") != null && !(rowData.getString("Distance").equalsIgnoreCase("")))
    					bidListDataObj.distance = rowData.getString("Distance");
    				if(rowData.getString("ETA") != null && !(rowData.getString("ETA").equalsIgnoreCase("")))
    					bidListDataObj.etaTime = rowData.getString("ETA");
    				String BIDDATAAMOUNT = "";
    				if(rowData.getString("bidAmount") != null && !(rowData.getString("bidAmount").equalsIgnoreCase(""))){
    					if(rowData.getString("bidAmount").contains(".")){
    						BIDDATAAMOUNT = rowData.getString("bidAmount");
    					}else{
    						BIDDATAAMOUNT = rowData.getString("bidAmount")+".00";
    					}
    					bidListDataObj.bidAmt = BIDDATAAMOUNT;
    				}     					
    				if(rowData.getString("lat") != null && !(rowData.getString("lat").equalsIgnoreCase("")))
    					bidListDataObj.latitude = rowData.getString("lat");
    				if(rowData.getString("lng") != null && !(rowData.getString("lng").equalsIgnoreCase("")))
    					bidListDataObj.longitude = rowData.getString("lng");
    				if(rowData.getString("bidId") != null && !(rowData.getString("bidId").equalsIgnoreCase("")))
    					bidListDataObj.bidId = rowData.getString("bidId");
    				if(rowData.getString("phoneNum") != null && !(rowData.getString("phoneNum").equalsIgnoreCase("")))
    					bidListDataObj.phoneNum = rowData.getString("phoneNum");
    				
    				LiveryCabStatics.bidList.add(bidListDataObj);
			    }
    			catch(JSONException e) {
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	    		}
	 		    catch(NumberFormatException e){
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	 		    }
    		}
		}
	  	if(mProgressDialog.isShowing()){
	  		actualDismiss =true;
	  		mProgressDialog.dismiss();
	  	}
	}
}
