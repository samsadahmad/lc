package com.cipl.liverycab.parserclasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import com.cipl.liverycab.dataclasses.LoginDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

import android.app.ProgressDialog;
import android.util.Log;

public class LoginParserClass  extends Thread   {
	
	private String urlLogin;
	public LoginDataClass loginDataObj;
	public boolean actualDismiss;
	public LoginParserClass(String url,ProgressDialog pd){
		super();
		this.urlLogin = url;
		Log.d("url",url);
	}	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		JSONObject jObject;
		loginDataObj =  new LoginDataClass();
		//JSON object get collected here
		jObject = getJSONfromURL(urlLogin);
		if(jObject==null){
			return;
		}
		try {
			//getting values from JSON object
			loginDataObj.message =jObject.getString("Message");
			loginDataObj.firstName =jObject.getString("fname");
			loginDataObj.lastName =jObject.getString("lname");
			if(LiveryCabStatics.isDriver)
			{
				if(jObject.getString("driverId") != null && !(jObject.getString("driverId").equalsIgnoreCase("")))
					loginDataObj.userID = jObject.getString("driverId");
			}
			if(LiveryCabStatics.isPassenger){
				if(jObject.getString("passengerId") != null && !(jObject.getString("passengerId").equalsIgnoreCase("")))
					loginDataObj.userID = jObject.getString("passengerId");
			}
			if(jObject.getString("Status")!= null)
				loginDataObj.status = jObject.getString("Status");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			
		}
		if(LiveryCabStaticMethods.mProgressDialog.isShowing()){
        	actualDismiss =true;
        	LiveryCabStaticMethods.mProgressDialog.dismiss();
		}
	}
	
	/**
	 * this method is used to get the JSON object from the API
	 */
	public  JSONObject  getJSONfromURL(String url) 
	{
		InputStream mIs = null;
	    String result = "";
	    JSONObject jObjectLogin = null;
	    try {
	    	HttpClient httpclient = new DefaultHttpClient();
	    	HttpPost httppost = new HttpPost(url);
	    	HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			mIs = entity.getContent();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try{
	        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(mIs,"iso-8859-1"),8);
	        StringBuilder stringBuilder = new StringBuilder();
	        String line = null;
	        while ((line = bufferReader.readLine()) != null) {
	        	if(line.trim().equals("\n"))
	        		continue;
	        	stringBuilder.append(line + "\n");
	        }
	        mIs.close();
	        result=stringBuilder.toString();
	    }catch(Exception e){
	        Log.e("log_tag", "Error converting result "+e.toString());
	    }
	    try {
			jObjectLogin = new JSONObject(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jObjectLogin;
	}
}
