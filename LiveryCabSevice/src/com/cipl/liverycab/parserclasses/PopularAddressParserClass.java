package com.cipl.liverycab.parserclasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;
import com.cipl.liverycab.dataclasses.PopularAddressDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class PopularAddressParserClass{
	
	private String urlAddData;
	public PopularAddressDataClass addressDataObj;
	public boolean actualDismiss;
	
	
	public void fetchPopularAddress(String urlAddData) {
		// TODO Auto-generated method stub		
		JSONArray json = LiveryCabStaticMethods.getJSONfromURL(urlAddData);
		System.out.println("URL=="+urlAddData);
		if(json!=null)
    	{
			LiveryCabStatics.popularAddList.clear();
			for (int i = 0; i < json.length(); i++) 
    		{
    			try
				{
    				JSONObject rowData = json.getJSONObject(i);
    				addressDataObj = new PopularAddressDataClass();
				    addressDataObj.message = rowData.getString("Message");
				    addressDataObj.status = rowData.getString("Status");
				    addressDataObj.address = rowData.getString("address");
				    addressDataObj.distance = rowData.getString("distance");
				    addressDataObj.addressType = rowData.getString("addressType");
				    addressDataObj.latitude = rowData.getString("latitude");
				    addressDataObj.longitude = rowData.getString("longitude");
				    
				    LiveryCabStatics.popularAddList.add(addressDataObj);
			    }
    			catch(JSONException e) {
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	    		}
	 		    catch(NumberFormatException e){
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	 		    }
    		}
		}
	}
}
