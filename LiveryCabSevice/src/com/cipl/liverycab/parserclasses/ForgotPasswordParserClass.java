package com.cipl.liverycab.parserclasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.util.Log;

import com.cipl.liverycab.dataclasses.LoginDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;

public class ForgotPasswordParserClass extends Thread{
	private String urlForgotPass;
	private ProgressDialog progressDialog;
	public LoginDataClass loginDataObj;
	public boolean actualDismiss;
	public ForgotPasswordParserClass(String url,ProgressDialog pd){
		super();
		this.urlForgotPass = url;
		this.progressDialog = pd;
		Log.d("url",url);
	}	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		JSONObject jObject;
		loginDataObj =  new LoginDataClass();
		jObject = getJSONfromURL(urlForgotPass);
		try {
			loginDataObj.message =jObject.getString("Message");
			loginDataObj.status = jObject.getString("Status");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(LiveryCabStaticMethods.mProgressDialog.isShowing()){
        	actualDismiss =true;
        	LiveryCabStaticMethods.mProgressDialog.dismiss();
		}
	}
	public  JSONObject  getJSONfromURL(String url) 
	{
		InputStream mIs = null;
	    String result = "";
	    JSONObject jObjectLogin = null;
	    try {
	    	HttpClient httpclient = new DefaultHttpClient();
	    	HttpPost httppost = new HttpPost(url);
	    	HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			mIs = entity.getContent();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try{
	        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(mIs,"iso-8859-1"),8);
	        StringBuilder stringBuilder = new StringBuilder();
	        String line = null;
	        while ((line = bufferReader.readLine()) != null) {
	        	if(line.trim().equals("\n"))
	        		continue;
	        	stringBuilder.append(line + "\n");
	        	
	        }
	        mIs.close();
	        result=stringBuilder.toString();
	    }catch(Exception e){
	    	 if(progressDialog.isShowing()){
             	actualDismiss =true;
             	progressDialog.dismiss();
             }
	        Log.e("log_tag", "Error converting result "+e.toString());
	    }
	    try {
			jObjectLogin = new JSONObject(result);
		}catch (JSONException e) {
			// TODO Auto-generated catch block
			if(progressDialog.isShowing()){
             	actualDismiss =true;
             	progressDialog.dismiss();
             }
			e.printStackTrace();
		}
		return jObjectLogin;
	}
}