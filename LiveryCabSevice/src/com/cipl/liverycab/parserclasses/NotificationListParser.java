package com.cipl.liverycab.parserclasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.cipl.liverycab.dataclasses.NotificationData;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class NotificationListParser extends Thread{
	
	private String url;
	public NotificationData notifyData;
	public boolean actualDismiss;
	
	public NotificationListParser(String url){
		super();
		this.url = url;
		Log.d("url",url);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		JSONArray json =LiveryCabStaticMethods.getJSONfromURL(url);
		if(json!=null){
			LiveryCabStatics.notificationList.clear();
    		for (int i = 0; i < json.length(); i++){
    			try{
    				JSONObject rowData = json.getJSONObject(i);
    				notifyData = new NotificationData();
    				notifyData.status = rowData.getString("Status");
    				if(rowData.getString("Message") != null && !(rowData.getString("Message").equalsIgnoreCase("")))
    					notifyData.message= rowData.getString("Message");
    				if(rowData.getString("fname") != null && !(rowData.getString("fname").equalsIgnoreCase("")))
    					notifyData.fname= rowData.getString("fname");
    				if(rowData.getString("lname") != null && !(rowData.getString("lname").equalsIgnoreCase("")))
    					notifyData.lname= rowData.getString("lname");
    				if(rowData.getString("ETA") != null && !(rowData.getString("ETA").equalsIgnoreCase("")))
    					notifyData.eta= rowData.getString("ETA");
    				if(rowData.getString("timestamp") != null && !(rowData.getString("timestamp").equalsIgnoreCase("")))
    					notifyData.timestamp = rowData.getString("timestamp");
    				if(rowData.getString("phone") != null && !(rowData.getString("phone").equalsIgnoreCase("")))
    					notifyData.phone = rowData.getString("phone");
    				LiveryCabStatics.notificationList.add(notifyData);
    			}catch(JSONException e) {
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	    		}
	 		    catch(NumberFormatException e){
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	 		    }
    		}
		}
		if(LiveryCabStaticMethods.mProgressDialog.isShowing()){
	  		actualDismiss =true;
	  		LiveryCabStaticMethods.mProgressDialog.dismiss();
	  	}
	}
}
