package com.cipl.liverycab.parserclasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.cipl.liverycab.dataclasses.AssignedJobListDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class AssignedJobListParser extends Thread{

	private String urlAddData;
	public AssignedJobListDataClass jobListDataObj;
	public boolean actualDismiss;
	
	public AssignedJobListParser(String url){
		super();
		this.urlAddData = url;
		//Log.d("url",url);
	}	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		JSONArray json =LiveryCabStaticMethods.getJSONfromURL(urlAddData);
		if(json!=null){
			LiveryCabStatics.assignedJobList.clear();
    		for (int i = 0; i < json.length(); i++){
    			try{
    				JSONObject rowData = json.getJSONObject(i);
    				jobListDataObj = new AssignedJobListDataClass();
    				jobListDataObj.status = rowData.getString("Status");
    				if(rowData.getString("Message") != null && !(rowData.getString("Message").equalsIgnoreCase("")))
    					jobListDataObj.message= rowData.getString("Message");
    				if(rowData.getString("passengerId") != null && !(rowData.getString("passengerId").equalsIgnoreCase("")))
    					jobListDataObj.passengerId= rowData.getString("passengerId");
    				if(rowData.getString("fname") != null && !(rowData.getString("fname").equalsIgnoreCase("")))
    					jobListDataObj.firstName= rowData.getString("fname");
    				if(rowData.getString("lname") != null && !(rowData.getString("lname").equalsIgnoreCase("")))
    					jobListDataObj.lastName= rowData.getString("lname");
    				String pickupDesti = null;
    				String pickupAddress = null;
					if(rowData.getString("pickupAddress") != null && !(rowData.getString("pickupAddress").equalsIgnoreCase(""))){
						pickupAddress = rowData.getString("pickupAddress").toString();;
    					jobListDataObj.pickupAdd = pickupAddress.replace(",null", "");
    					//jobListDataObj.pickupAdd = rowData.getString("pickupAddress");
					}	
    					
    				if(rowData.getString("PickupDestination") != null && !(rowData.getString("PickupDestination").equalsIgnoreCase(""))){
    					pickupDesti = rowData.getString("PickupDestination").toString();;
    					jobListDataObj.destinationAdd = pickupDesti.replace(",null", "");
    					//jobListDataObj.destinationAdd = rowData.getString("PickupDestination");
    				}
    					
    				if(rowData.getString("Distance") != null && !(rowData.getString("Distance").equalsIgnoreCase("")))
    					jobListDataObj.distanceInMiles = rowData.getString("Distance");
    				//Bid Amount
    				String BIDDATAAMOUNT = "";
    				if(rowData.getString("amount") != null && !(rowData.getString("amount").equalsIgnoreCase(""))){
    					if(rowData.getString("amount").contains(".")){
    						BIDDATAAMOUNT = rowData.getString("amount");
    					}else{
    						BIDDATAAMOUNT = rowData.getString("amount")+".00";
    					}
    					jobListDataObj.bidAmount = BIDDATAAMOUNT;
    				}	
    				if(rowData.getString("datetime") != null && !(rowData.getString("datetime").equalsIgnoreCase("")))
    					jobListDataObj.dateTime = rowData.getString("datetime");
    				if(rowData.getString("ETA") != null && !(rowData.getString("ETA").equalsIgnoreCase("")))
    					jobListDataObj.etaTime = rowData.getString("ETA");
    				if(rowData.getString("pickupId") != null && !(rowData.getString("pickupId").equalsIgnoreCase("")))
    					jobListDataObj.pickupId = rowData.getString("pickupId");
    				if(rowData.getString("notification") != null && !(rowData.getString("notification").equalsIgnoreCase("")))
    					jobListDataObj.notification = rowData.getString("notification");
    				LiveryCabStatics.assignedJobList.add(jobListDataObj);
			    }
    			catch(JSONException e) {
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	    		}
	 		    catch(NumberFormatException e){
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	 		    }
    		}
		}
	  	if(LiveryCabStaticMethods.mProgressDialog.isShowing()){
	  		actualDismiss =true;
	  		LiveryCabStaticMethods.mProgressDialog.dismiss();
	  	}
	}
}