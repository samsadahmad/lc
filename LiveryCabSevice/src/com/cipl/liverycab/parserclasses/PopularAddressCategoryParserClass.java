package com.cipl.liverycab.parserclasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

import com.cipl.liverycab.dataclasses.PopularAddressCategoryDataClass;
import com.cipl.liverycab.dataclasses.PopularAddressDataClass;
import com.cipl.liverycab.statics.LiveryCabStaticMethods;
import com.cipl.liverycab.statics.LiveryCabStatics;

public class PopularAddressCategoryParserClass{
	
	private String urlAddData;
	public PopularAddressCategoryDataClass addressCategoryDataObj;
	public boolean actualDismiss;
	
	public PopularAddressCategoryParserClass(String url){
		super();
		this.urlAddData = url;
		LiveryCabStatics.popularAddCatList.clear();
		Log.d("url",url);
	}	
	
	
	public void fetchPopularAddressCategory() {
		// TODO Auto-generated method stub		
		JSONArray json = LiveryCabStaticMethods.getJSONfromURL(urlAddData);
		if(json!=null)
    	{
			LiveryCabStatics.popularAddList.clear();
			for (int i = 0; i < json.length(); i++) 
    		{
    			try
				{
    				JSONObject rowData = json.getJSONObject(i);
    				addressCategoryDataObj = new PopularAddressCategoryDataClass();
    				addressCategoryDataObj.catId = rowData.getString("catId");
    				addressCategoryDataObj.catName = rowData.getString("catName");	
    				addressCategoryDataObj.message = rowData.getString("Message");
    				addressCategoryDataObj.type = rowData.getString("type");
				    
				    LiveryCabStatics.popularAddCatList.add(addressCategoryDataObj);
			    }
    			catch(JSONException e) {
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	    		}
	 		    catch(NumberFormatException e){
	 		         Log.e("log_tag", "Error parsing data "+e.toString());
	 		    }
    		}
		}
	}
}
