package com.cipl.liverycab.parserclasses;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.util.Log;

import com.cipl.liverycab.dataclasses.DriverSignUpDataClass;

public class DataAndImageUpload extends Thread {
	private String url;
	private ProgressDialog pd;
	public boolean actualDismiss;
	public String responseMessage;
	public DriverSignUpDataClass driverData;
	
	public DataAndImageUpload(String url, ProgressDialog pd) {
		super();
		this.url = url;
		this.pd = pd;
		driverData =  new DriverSignUpDataClass();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		JSONObject json = getJSONfromURL(url);
		try {
			responseMessage =json.getString("Message");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(pd.isShowing()){
        	actualDismiss =true;
        	pd.dismiss();
        }
	}

	private JSONObject getJSONfromURL(String url2) {
		// TODO Auto-generated method stub
		JSONObject jObject = null;
		String filePath = "";
		String mimeType = "image/jpeg";
		HttpURLConnection connection = null;
		DataOutputStream outStream = null;
		DataInputStream inStream = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		try {
			FileInputStream fileInputStream = null;
			try {
				if (filePath != null)
					fileInputStream = new FileInputStream(new File(filePath));
			} catch (FileNotFoundException e) {
				Log.d("Bug", "Bug");

			}
			URL linkUrl = new URL(url);
			connection = (HttpURLConnection) linkUrl.openConnection();
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);
			outStream = new DataOutputStream(connection.getOutputStream());
			outStream.writeBytes(addParam("fname", driverData.firstName,
					twoHyphens, boundary, lineEnd));
			outStream.writeBytes(addParam("lname", driverData.lastName, twoHyphens,
					boundary, lineEnd));
			outStream.writeBytes(addParam("licence", driverData.dlNumber, twoHyphens,
					boundary, lineEnd));
			outStream.writeBytes(addParam("phone", driverData.phone,twoHyphens, boundary, lineEnd));
			outStream.writeBytes(addParam("email", driverData.email, twoHyphens,
					boundary, lineEnd));
			outStream.writeBytes(addParam("password", driverData.password, twoHyphens, boundary,lineEnd));
			outStream.writeBytes(addParam("deviceId",driverData.deviceId, twoHyphens, boundary,
							lineEnd));
			outStream.writeBytes(addParam("deviceToken",driverData.deviceToken, twoHyphens, boundary,
					lineEnd));
			outStream.writeBytes(addParam("deviceType",driverData.deviceType, twoHyphens, boundary,
					lineEnd));
			if (!driverData.password.equals("")) {
				outStream.writeBytes(addParam("password", driverData.password,
						twoHyphens, boundary, lineEnd));
			}
			outStream.writeBytes(twoHyphens + boundary + lineEnd);
			if (!filePath.equals("")) {
				bytesAvailable = 0;
				outStream
						.writeBytes("Content-Disposition: form-data; name=\"uploadfile\";filename=\""
								//+ StaticData.selectedBleepImage
								+ "\""
								+ lineEnd
								+ "Content-Type: "
								+ mimeType
								+ lineEnd
								+ "Content-Transfer-Encoding: binary"
								+ lineEnd);
				outStream.writeBytes(lineEnd);
				if (fileInputStream != null)
					bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];
				bytesRead = 0;
				if (fileInputStream != null)
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0) {
					outStream.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					if (fileInputStream != null)
						bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}
				outStream.writeBytes(lineEnd);
				outStream.writeBytes(twoHyphens + boundary + twoHyphens
						+ lineEnd);
				if (fileInputStream != null)
					fileInputStream.close();
			}

			outStream.flush();
			outStream.close();
		} catch (MalformedURLException e) {
			Log.e("DEBUG", "[MalformedURLException while sending a picture]");
		} catch (IOException e) {
			Log.e("DEBUG", "[IOException while sending a picture]");
		}
		String result = "";
		try {
			inStream = new DataInputStream(connection.getInputStream());

			String str = null;
			while ((str = inStream.readLine()) != null) {
				if (str.equals("null"))
					continue;
				result += str;
			}

			Log.d("URL Publish", url);
			inStream.close();
		} catch (IOException e) {
			Log.e("DEBUG", "" + e.toString());
		}

		// try parse the string to a JSON object
		try {
			jObject = new JSONObject(result);

		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jObject;
	}

	private String addParam(String key, String value, String twoHyphens,
			String boundary, String lineEnd) {
		return twoHyphens + boundary + lineEnd
				+ "Content-Disposition: form-data; name=\"" + key + "\""
				+ lineEnd + lineEnd + value + lineEnd;
	}
}